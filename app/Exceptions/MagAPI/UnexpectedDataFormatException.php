<?php

namespace App\Exceptions\MagAPI;

/**
 * Class UnexpectedDataFormatException.
 * Throw when received data format is different from the expected.
 * @package App\Exceptions\MagAPI
 */
class UnexpectedDataFormatException extends \Exception {

    public function __construct($message = "", $code = 0, \Exception $previous = null)
    {
        $message = $message ? $message : 'Unexpected API data format';
        parent::__construct($message, $code, $previous);
    }
}