<?php

namespace App\Exceptions\MagAPI;

/**
 * Class ValidationException.
 * Throw when Magento API return 422 response code.
 * @package App\Exceptions\MagAPI
 */
class ValidationException extends \Exception{

}