<?php


namespace App\Models\Cart;

use App\Models\Cart\Product\Contract as ProductContract;

class State {

    protected $isLoaded = false;

    protected $options = [];

    protected $products = [];

    const TMP_STORE_KEY = 'cart:state';

    protected function getOption($key, $default = null)
    {
        $this->checkState();
        return isset($this->options[$key]) ? $this->options[$key] : $default;
    }

    public function getCount()
    {
        return $this->getOption('count', 0);
    }

    public function isFHT()
    {
        return $this->getOption('type') == AbstractCart::FHT_TYPE;
    }

    public function isNormal()
    {
        return $this->getOption('type') == AbstractCart::NORMAL_TYPE;
    }

    public function checkProduct(ProductContract $productContract)
    {
        return array_key_exists($productContract->getId(), $this->getOption('products', []));
    }

    public function setCount($count)
    {
        $this->options['count'] = $count;
        return $this;
    }

    public function getProducts()
    {
        return $this->getOption('products', []);
    }

    public function setType($type)
    {
        $this->options['type'] = $type;
        return $this;
    }

    public function setProducts($products)
    {
        $this->options['products'] = $products;
        return $this;
    }

    public function setDiscount($discount)
    {
        $this->options['discount'] = $discount;
        return $this;
    }

    public function setOptions($options)
    {
        $this->options = $options;
        return $this;
    }

    public function update()
    {
        \Session::put(self::TMP_STORE_KEY, $this->options);
    }

    public function reset()
    {
        \Session::forget(self::TMP_STORE_KEY);
    }

    protected function checkState()
    {
        if(!$this->isLoaded)
        {
            $data = \Session::get(self::TMP_STORE_KEY);

            if($data === null)
            {
                $cart = app('CartResolver')->fetch();
                $cart->updateState();
            }
            else
            {
                $this->setOptions($data);
            }

            $this->isLoaded = true;
            unset($data);
        }
    }

}