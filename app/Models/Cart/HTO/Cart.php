<?php

namespace App\Models\Cart\HTO;

use App\Models\Cart\Normal\Cart as NormalCart;
use App\Models\Cart\Product\Contract as ProductContract;
use App\Models\Services\HomeEyeCheckUp\HomeEyeCheckUp;

class Cart extends NormalCart {

    public $type = self::HTO_TYPE;

    public $coupon;
    public $vouchers;

    public $price;

    public function add(ProductContract $product)
    {
        if($product instanceof HomeEyeCheckUp)
        {
            $this->cleanPush();
            $this->setValue('is_hto', 1)
                ->setOptions($product->getSelectedOptions());

            return $this->addPush($product);
        }
        else
            throw new \ErrorException('Product must be instance of HomeEyeCheckUp model');
    }

    public function remove(ProductContract $product)
    {

    }

    public function setItems($items)
    {
        $this->items = $items;
        return $this;
    }

    public function setPrice($price)
    {
        $this->price = $price;
        return $this;
    }

    public function updateState()
    {
        $this->state
            ->setCount($this->count)
            ->setType($this->type)
            ->update();
    }

    public function setCoupon($data)
    {
        if(isset($data['code']))
            $this->coupon = $data['code'];
    }

    public function setVouchers($data)
    {
        if(isset($data['code'][0]))
            $this->vouchers = $data['code'][0];
    }
}