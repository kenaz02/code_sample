<?php

namespace App\Models\Cart\HTO;


use App\Models\Cart\AbstractCart;
use App\Models\Cart\Normal\Builder as NormalCartBuilder;

class Builder extends NormalCartBuilder {

    public function buildCart($data)
    {
        $this->data = $data;

        $this->makeCart()
            ->makeTotal()
            ->makeTaxCollected()
            ->makeShippingCharges()
            ->makeSubTotal()
            ->makeTotal()
            ->makeItems()
            ->makeDiscount()
            ->makeAttributes();

        return $this;
    }

}