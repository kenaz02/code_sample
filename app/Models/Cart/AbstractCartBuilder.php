<?php

namespace App\Models\Cart;


use App\Models\Common\Factory;

abstract class AbstractCartBuilder {

    protected $cart;

    protected $factory;

    protected $data = [];

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    public abstract function buildCart($data);

    public function makeCart()
    {
        $className = get_called_class();
        $className = substr($className, 0, strrpos($className, '\\'));
        $className = str_replace('app', '\App', $className);
        $this->cart =  app($className.'\\Cart');
        return $this;
    }

    protected static function getNamespace()
    {
        return __NAMESPACE__;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function makeAttributes()
    {
        $this->cart->setAttributes($this->data);
        return $this;
    }

    protected function getData($attribute, $cartAttribute = '', $unset = true)
    {
        $cartAttribute = $cartAttribute ? $cartAttribute : $attribute;

        if(!$this->cart instanceof AbstractCart)
            throw new \ErrorException('Builder cart must be initialized.');

        if(isset($this->data[$attribute]) && property_exists($this->cart, $cartAttribute))
        {
            $value = $this->data[$attribute];
            if($unset)
                unset($this->data[$attribute]);

            return $value;
        }
        else
            throw new \ErrorException("Attribute $attribute must be exists in data array and in cart instance.");
    }

}