<?php

namespace App\Models\Cart\Product;


class Product extends \App\Models\Product\Product {

    public $product_id;
    public $quantity;
    public $available_quantity;
    public $options = [];
    public $marketPrice;
    public $lenskartPrice;
    public $firstBuyPrice;
    public $totalPrice;

    public $is_surprise_me = false;

    const TYPE_HTO = 'hto';

    public function setImages($image)
    {
        $this->images = $image;
    }

    public function hasOptions()
    {
        return !empty($this->options);
    }

    public function setProductUrl($productUrl)
    {
        $this->product_url = preg_replace(['(.*lenskart.com/)', '(\?.*)'], '', $productUrl);
    }

}