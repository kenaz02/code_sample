<?php

namespace App\Models\Cart\Product;

use App\Models\Common\Factory;
use Illuminate\Container\Container;

class Resolver {

    protected $factory;
    protected $container;

    protected $builders = [
        Product::TYPE_CONTACT_LENS          => 'ContactLenses',
        Product::TYPE_EYE_GLASSES           => 'Eyeglasses',
        Product::TYPE_POWERED_SUN_GLASSES   => 'Eyeglasses',
        Product::TYPE_SUN_GLASSES           => 'Sunglasses',
        Product::TYPE_HTO                   => 'HTO',
    ];

    public function __construct(Factory $factory, Container $container)
    {
        $this->factory = $factory;
        $this->container = $container;
    }

    public function resolveType(array &$data)
    {
        if(isset($data['type']))
        {
            return $data['type'] == Product::TYPE_SUN_GLASSES && !empty($data['options']) ? Product::TYPE_POWERED_SUN_GLASSES : $data['type'];
        }
        else
            return false;
    }

    public function checkType(array &$data)
    {
        if($type =  $this->resolveType($data))
            return $type;
        else
            throw new \ErrorException('Unknown product type.');
    }

    public function makeBuilder(array &$data, Product $productInstance = null)
    {
        $productInstance = $productInstance ? $productInstance : $this->makeProductInstance();
        $type = $this->checkType($data);

        $name = static::getNamespace().'Builders\\'.$this->builders[$type];
        return new $name($productInstance, $this->factory, $type, $data);
    }

    protected static function getNamespace()
    {
        return __NAMESPACE__.'\\';
    }

    protected function makeProductInstance()
    {
        return $this->container->make(static::getNamespace().'Product');
    }

    public function makeProducts($data)
    {
        $productInstance = $this->makeProductInstance();

        $products = [];
        foreach($data as $item)
        {
            $products[] = $this->makeBuilder($item, clone $productInstance)->build()->getProduct();
        }

        return $products;
    }

    public function makeFHTProducts($data)
    {
        $productInstance = $this->makeProductInstance();

        $products = [];
        foreach($data as $item)
        {
            $builder = new Builders\FHT(clone $productInstance, $this->factory, Product::TYPE_EYE_GLASSES, $item);
            $products[] = $builder->build()->getProduct();
        }

        return $products;
    }

    public function makeProduct(array &$data)
    {
        return $this->makeBuilder($data)->build()->getProduct();
    }
}