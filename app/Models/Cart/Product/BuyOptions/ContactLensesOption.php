<?php

namespace App\Models\Cart\Product\BuyOptions;


use App\Models\Common\BaseModel;

class ContactLensesOption extends BaseModel {

    protected $additional = [];
    protected $base = [];
    protected $extra = [];
    protected $username;

    protected $options = [];

    const SIDE_LEFT = 'Left';
    const SIDE_RIGHT = 'Right';
    const BOXES_OPTION = 'Boxes';
    const EXTRA_OPTION = 'extra';

    public $hasLeft = false;
    public $hasRight = false;

    public function setAttributes($data, $attributes = false, $attrExcept = false)
    {
        $usernameData = array_pop($data);
        if(isset($usernameData['username']))
            $this->username = $usernameData['username'];

        $optionInstance = $this->factory->makeInstance('Cart\Product\BuyOptions\ContactLensesOptionItem');


        $rxOption = false;
        foreach($data as $item)
        {
            if(isset($item['model_name']) && $item['model_name'] == self::EXTRA_OPTION)
            {
                $rxOption = true;
                break;
            }

            if(isset($item['value']))
            {
                $option = clone $optionInstance;
                $option->setAttributes($item);
                $this->options[$option->key][$option->side] = $option;
            }
        }

        $this->hasLeft = isset($this->options[self::BOXES_OPTION][self::SIDE_LEFT]);
        $this->hasRight = isset($this->options[self::BOXES_OPTION][self::SIDE_RIGHT]);

        if($rxOption)
        {
            $rxOption = clone $optionInstance;
            $rxOption->setAttributes([
                'value' => 'Image Uploaded'
            ]);

            if($this->hasLeft)
                $this->options['Rx'][self::SIDE_LEFT] = clone $rxOption;

            if($this->hasRight)
                $this->options['Rx'][self::SIDE_RIGHT] = clone $rxOption;
        }

        $this->options[self::BOXES_OPTION] = array_pull($this->options, self::BOXES_OPTION);
    }

    public function getOptions()
    {
        return $this->options;
    }

    public function hasLeftOptions()
    {
        return isset($this->options[self::BOXES_OPTION][self::SIDE_LEFT]);
    }

    public function hasRightOptions()
    {
        return isset($this->options[self::BOXES_OPTION][self::SIDE_RIGHT]);
    }

    public function getLeftBoxes()
    {
        return $this->options[self::BOXES_OPTION][self::SIDE_LEFT];
    }

    public function getRightBoxes()
    {
        return $this->options[self::BOXES_OPTION][self::SIDE_RIGHT];
    }

    public function getUserName()
    {
        return $this->username;
    }
}