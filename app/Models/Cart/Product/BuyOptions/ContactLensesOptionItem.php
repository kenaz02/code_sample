<?php

namespace App\Models\Cart\Product\BuyOptions;

use App\Models\Common\BaseModel;


class ContactLensesOptionItem extends BaseModel{

    public $id;
    public $model_name;
    public $value;

    public $side;
    public $key;

    public function setModelName($modelName)
    {
        $this->model_name = $modelName;
        $this->key = preg_replace('/(left\s|right\s)/i', '', $this->model_name);
    }

}