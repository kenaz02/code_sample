<?php

namespace App\Models\Cart\Product\BuyOptions;


use App\Models\Common\BaseModel;

class EyeglassesOption extends BaseModel{

    public $id;
    public $model_name;

    public $price;
    public $marketPrice;
    public $lenskartPrice;

    protected function getAttributeAliases()
    {
        return [
            'price' => 'package_price|price'
        ];
    }

    public function setPrice($prices)
    {
        list($this->marketPrice, $this->lenskartPrice) = $this->factory->makeCollection('Price', $prices);
    }
}