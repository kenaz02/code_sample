<?php

namespace App\Models\Cart\Product\Builders;

use App\Models\Cart\Product\Product;
use App\Models\Common\Factory;

abstract class AbstractBuilder {

    protected $data;

    protected $type;

    public function __construct(Product $product, Factory $factory, $type, array &$data)
    {
        $this->product = $product;
        $this->factory = $factory;
        $this->data = $data;
        $this->type = $type;
    }

    abstract public function build();

    public function getProduct()
    {
        return $this->product;
    }

    public function makeType()
    {
        $this->product->type = $this->type;
        unset($this->data['type']);
        return $this;
    }

    protected function getData($attribute, $unset = true)
    {
        if(isset($this->data[$attribute]))
            return $unset ? array_pull($this->data, $attribute) : $this->data[$attribute];
        else
            throw new \ErrorException("Attribute $attribute must be exists in data array.");
    }

    public function makeAttributes()
    {
        $this->product->setAttributes($this->data);
        return $this;
    }
}