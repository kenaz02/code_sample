<?php

namespace App\Models\Cart\Product\Builders;


class Sunglasses extends AbstractBuilder {

    public function makePrices()
    {
        list($this->product->marketPrice, $this->product->lenskartPrice) = $this->factory->makeCollection('Price', $this->getData('prices'));
        return $this;
    }

    public function makeTotalPrice()
    {
        $this->product->totalPrice = $this->product->quantity * $this->product->lenskartPrice->price;
        return $this;
    }

    public function makeQuantity()
    {
        $this->product->quantity = $this->getData('quantity');
        return $this;
    }

    public function build()
    {
        $this->makeType()
             ->makePrices()
             ->makeQuantity()
             ->makeTotalPrice()
             ->makeAttributes();

        return $this;
    }
}