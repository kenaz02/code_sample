<?php

namespace App\Models\Cart\Product\Builders;

class ContactLenses extends AbstractBuilder {

    public function makeOptions()
    {
        $this->product->options = $this->factory->makeModel('Cart\Product\BuyOptions\ContactLensesOption', $this->getData('options'));
        return $this;
    }

    public function makePrices()
    {
        list($this->product->marketPrice, $this->product->lenskartPrice) = $this->factory->makeCollection('Price', $this->getData('prices'));
        return $this;
    }

    public function makeQuantity()
    {
        $this->product->quantity = $this->getData('quantity');
        return $this;
    }

    public function makeTotalPrice()
    {
        $this->product->totalPrice = (int)$this->product->quantity * $this->product->lenskartPrice->price;
        return $this;
    }

    public function build()
    {
        $this->makeType()
             ->makeOptions()
             ->makePrices()
             ->makeQuantity()
             ->makeTotalPrice()
             ->makeAttributes();
        return $this;
    }
}