<?php

namespace App\Models\Cart\Product\Builders;

use App\Models\Cart\Product\Product;

class HTO extends AbstractBuilder {

    public function makeType()
    {
        $this->product->type = Product::TYPE_HTO;
    }

    public function makePrices()
    {
        list($this->product->marketPrice, $this->product->lenskartPrice) = $this->factory->makeCollection('Price', $this->getData('prices'));
        return $this;
    }

    public function makeTotalPrice()
    {
        $this->product->totalPrice = $this->product->lenskartPrice;
        return $this;
    }

    public function build()
    {

    }
}