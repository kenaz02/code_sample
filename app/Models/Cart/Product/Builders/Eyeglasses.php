<?php

namespace App\Models\Cart\Product\Builders;


class Eyeglasses extends AbstractBuilder {


    public function makeOptions()
    {
        $options = $this->getData('options');
        if($options)
            $this->product->options = $this->factory->makeModel('Cart\Product\BuyOptions\EyeglassesOption', $options[0]);

        return $this;
    }

    public function makePrices()
    {
        $prices = $this->factory->makeCollection('Price', $this->getData('prices'));
        list($this->product->marketPrice, $this->product->lenskartPrice) = $prices;
        $this->product->firstBuyPrice = isset($prices[3]) ? $prices[3] : null;

        return $this;
    }

    public function makeTotalPrice()
    {
        $this->product->totalPrice = $this->product->quantity * (!empty($this->product->options) ? $this->product->lenskartPrice->price + $this->product->options->lenskartPrice->price : $this->product->lenskartPrice->price);
        return $this;
    }

    public function makeQuantity()
    {
        $this->product->quantity = $this->getData('quantity');
        return $this;
    }

    public function build()
    {
        $this->makeType()
             ->makeOptions()
             ->makePrices()
             ->makeQuantity()
             ->makeTotalPrice()
             ->makeAttributes();

        return $this;
    }
}