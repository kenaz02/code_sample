<?php

namespace App\Models\Cart\Product\Builders;


class FHT extends AbstractBuilder {

    public function build()
    {
        array_forget($this->data, ['options', 'prices']);
        $this->makeType()
             ->makeAttributes();
        return $this;
    }
}