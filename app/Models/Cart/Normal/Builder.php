<?php

namespace App\Models\Cart\Normal;


use App\Models\Cart\AbstractCartBuilder;
use App\Models\Cart\Options\PricesTrait;
use App\Models\Cart\Options\ProductItemsTrait;

class Builder extends AbstractCartBuilder {

    use PricesTrait, ProductItemsTrait;

    public function makeItems()
    {
        $this->cart->items = app('App\Models\Cart\Product\Resolver')->makeProducts($this->getData('items'));
        return $this;
    }

    public function buildCart($data)
    {
        $this->data = $data;
        $this->makeCart()
            ->makeTotal()
            ->makeTaxCollected()
            ->makeShippingCharges()
            ->makeSubTotal()
            ->makeTotal()
            ->makeItems()
            ->makeDiscount()
            ->makeAttributes();

        return $this;
    }
}