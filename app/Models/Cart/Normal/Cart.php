<?php

namespace App\Models\Cart\Normal;

use App\Events\CartCombineConflict;
use App\Exceptions\Cart\CombineException;
use App\Models\Cart\AbstractCart;
use App\Models\Cart\Product\Contract as ProductContract;
use App\Exceptions\MagAPI\ValidationException;
use App\Models\Cart\Product\Product;

class Cart extends AbstractCart {

    public $type = self::NORMAL_TYPE;

    public $coupon;
    public $reward_points;
    public $vouchers;
    public $credits;
    public $discount;
    public $sub_total;
    public $total;
    public $shipping_charges;
    public $tax_collected;

    public function add(ProductContract $product)
    {
        return $this->addPush($product);
    }

    public function remove(ProductContract $product)
    {
        return $this->removePush($product);
    }

    public function changeQuantity($id, $quantity)
    {
        try {

            $data = $this->api->push('updatequantity', [
                'request' => [
                    'id'        => $id,
                    'quantity' => $quantity
               ]
            ]);
            $data = $data['result'];

            $this->afterPush($data);
            return false;
       }
       catch(ValidationException $e)
       {
           return $e->getMessage();
       }
    }

    public function updateState()
    {
        $this->state
            ->setCount($this->count)
            ->setType($this->type)
            ->update();
    }

    protected function pushCartCode($url, $request)
    {
        try {
            $data = $this->api->push($url, [
                'request' => $request
            ]);
            $data = $data['result'];

            $this->afterPush($data);
            return false;
        }
        catch (ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function addGiftVoucher($code)
    {
         return $this->pushCartCode('addgiftvoucher', ['code' => $code]);
    }

    public function removeGiftVoucher($code)
    {
        return $this->pushCartCode('removegiftvoucher', ['code' => $code]);
    }

    public function addCoupon($code)
    {
        return $this->pushCartCode('usecoupon', ['code' => $code]);
    }

    public function addStoreCredit($code, $ammount)
    {
        return $this->pushCartCode('usecoupon', [
            'code' => $code,
            'value' => $ammount
        ]);
    }

    public function getEyeglassesProducts()
    {
        $eyeglassesItems = [];
        foreach($this->items as $item)
        {
            if(in_array($item->type, [Product::TYPE_EYE_GLASSES, Product::TYPE_POWERED_SUN_GLASSES]))
                $eyeglassesItems[] = $item;
        }
        return $eyeglassesItems;
    }
}