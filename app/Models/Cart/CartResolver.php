<?php

namespace App\Models\Cart;

use App\Services\MagAPI;
use Illuminate\Container\Container;
use App\Exceptions\MagAPI\UnexpectedDataFormatException;

class CartResolver {

    protected $api;
    protected $cart;
    protected $data = [];

    protected $container;

    protected $state;

    protected $cartInstances = [
        AbstractCart::FHT_TYPE    => 'FHT\Cart',
        AbstractCart::NORMAL_TYPE => 'Normal\Cart',
        AbstractCart::HTO_TYPE => 'HTO\Cart'
    ];

    protected $builders = [
        AbstractCart::FHT_TYPE    => 'FHT\Builder',
        AbstractCart::NORMAL_TYPE => 'Normal\Builder',
        AbstractCart::HTO_TYPE => 'HTO\Builder'
    ];

    const TMP_CART_CONFLICT = 'cart:conflict';

    public function __construct(MagAPI $magAPI, Container $container)
    {
        $this->container = $container;
        $this->api = $magAPI;
    }

    public function make($type)
    {
        return $this->container->make($this->resolveClassName($type));
    }


    public function makeBuilder($type)
    {
        if(!isset($this->builders[$type]))
            throw new \ErrorException('Unknown cart builder type');

        return $this->container->make('\App\Models\Cart\\'.$this->builders[$type]);
    }


    public function fetch()
    {
        $data = $this->loadData();

        if(!$data['type'])
            throw new UnexpectedDataFormatException('Attribute type must be exists in data array');

        $builder = $this->makeBuilder($data['type']);

        if($builder instanceof AbstractCartBuilder)
        {
            $cart = $builder->buildCart($data)->getCart();
            return $cart;
        }
    }

    protected function loadData()
    {
        $data = \Session::get(AbstractCart::TMP_STORE_KEY);
        if($data === null)
        {
            $data = $this->api->fetch('cart');
            $data = $data['result'];
        }
        return $data;
    }


    protected function resolveClassName($type)
    {
        return __NAMESPACE__.'\\'.$this->cartInstances[$type];
    }

    public function makeNormal()
    {
        return $this->makeNormalBuilder()->makeCart()->getCart();
    }

    public function makeFHT()
    {
        return $this->makeFHTBuilder()->makeCart()->getCart();
    }

    public function makeHTO()
    {
        return $this->makeHTOBuilder()->makeCart()->getCart();
    }

    public function makeNormalBuilder()
    {
        return $this->makeBuilder(AbstractCart::NORMAL_TYPE);
    }

    public function makeFHTBuilder()
    {
        return $this->makeBuilder(AbstractCart::FHT_TYPE);
    }

    public function makeHTOBuilder()
    {
        return $this->makeBuilder(AbstractCart::HTO_TYPE);
    }

    public function makeBackground()
    {
        $data = \Session::get(AbstractCart::BACKGROUND_STORE_KEY);
        if($data === null)
        {
            $cart = $this->fetch();

            \Session::put(AbstractCart::BACKGROUND_STORE_KEY, [
                'count' => $cart->getCount(),
                'type'  => $cart->type
            ]);

            $this->cart = $cart;
        }
        else {
            $this->cart = $this->make($data['type']);
            $this->cart->setCount($data['count']);
        }

        return $this;
    }

    public function getCart()
    {
        return $this->cart;
    }

}
