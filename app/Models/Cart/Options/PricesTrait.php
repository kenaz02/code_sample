<?php

namespace App\Models\Cart\Options;


trait PricesTrait {

    public function makeTotal()
    {
        $this->makePrice('total');
        return $this;
    }

    public function makeDiscount()
    {
        $this->makePrice('discount');
        return $this;
    }

    public function makeTaxCollected()
    {
        $this->makePrice('tax_collected');
        return $this;
    }

    public function makeSubTotal()
    {
        $this->makePrice('sub_total');
        return $this;
    }

    public function makeShippingCharges()
    {
        $this->makePrice('shipping_charges');
        return $this;
    }

    public function makePrice($optionName)
    {
        if(array_key_exists($optionName, $this->data) && property_exists($this->cart, $optionName) && is_array($this->data[$optionName]))
        {
            $this->cart->$optionName = $this->factory->makeModel('Price', $this->data[$optionName][0]);
            unset($this->data[$optionName]);
        }

        return $this;
    }
}