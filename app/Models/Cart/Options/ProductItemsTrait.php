<?php

namespace App\Models\Cart\Options;


use App\Models\Cart\Product\FHTFactory;
use App\Models\Cart\Product\Product;

trait ProductItemsTrait {

    protected $factoryRoot = 'App\Models\Cart\Product';

    protected $factories = [
        Product::TYPE_CONTACT_LENS        => 'ContactLensesFactory',
        Product::TYPE_EYE_GLASSES         => 'EyeglassesFactory',
        Product::TYPE_POWERED_SUN_GLASSES => 'EyeglassesFactory',
        Product::TYPE_SUN_GLASSES         => 'SunglassesFactory',
        Product::TYPE_HTO                 => 'HtoFactory',

    ];

    public function makeFactory($type, array &$data)
    {
        $class = $this->factoryRoot.'\\'.$this->factories[$type];
        return new $class($this->factory, $type, $data);
    }

    protected function resolveProductItemType(array $data)
    {
        if(isset($data['type']) && array_key_exists($data['type'], $this->factories))
        {
            return $data['type'] == Product::TYPE_SUN_GLASSES && !empty($data['options']) ? Product::TYPE_POWERED_SUN_GLASSES : $data['type'];
        }
        else
            return false;
    }

    protected function makeProducts(array $data)
    {
        $that = $this;
        $products = $this->factory->makeCollection('Cart\Product\Product', $data, function(Product $model, $item) use ($that){
            $type = $this->resolveProductItemType($item);
            if($type)
            {
                $factory = $this->makeFactory($type, $item);
                $model->makeOptions($factory);
            }
        });

        return $products;
    }

    protected function makeFHTProducts(array $data)
    {
        $products = $this->factory->makeCollection('Cart\Product\Product', $data, function(Product $model, $item) use ($that){
            $type = $this->resolveProductItemType($item);
            if($type)
            {
                $factory = new FHTFactory($this->factory, $type, $item);
                $model->makeOptions($factory);
            }
        });

        return $products;
    }

}