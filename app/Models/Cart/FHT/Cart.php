<?php

namespace App\Models\Cart\FHT;

use App\Exceptions\MagAPI\ValidationException;
use App\Models\Cart\AbstractCart;
use App\Models\Cart\Product\Contract as ProductContract;

class Cart extends AbstractCart {

    const MAX_CART_ITEMS = 5;
    const SELECT_FOR_ME_STORAGE_KEY = 'cart:select_for_me';

    public $type = self::FHT_TYPE;
    public $productIds = [];


    public function add(ProductContract $product)
    {
        $this->setValue('is_tbyb', 1);
        return $this->addPush($product);
    }

    public function remove(ProductContract $product)
    {
        return $this->removePush($product);
    }

    public function clean()
    {
        return $this->cleanPush();
    }

    public function selectForMe($again = false, $product = null)
    {
        try
        {
            $requestData = $again && $product instanceof ProductContract ? ['isAgain' => 1, 'currentId' => $product->getId()] : [];
            $data = $this->api->push('surpriseme', [
                'request' => $requestData
            ]);
            $data = $data['result'];

            $this->afterPush($data);
            return false;
        }
        catch (ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function setItems($items)
    {
        $this->items = $this->factory->makeCollection('Cart\Product', $items);
    }

    public function checkProduct(ProductContract $productContract)
    {
        return in_array($productContract->getId(), $this->productIds);
    }

    public function getAvailableSlotsCount()
    {
        return self::MAX_CART_ITEMS - (int) $this->count;
    }

    public function updateState()
    {
        $this->state
            ->setCount($this->count)
            ->setType($this->type)
            ->setProducts($this->productIds)
            ->update();
    }

    public function updateCount($data)
    {
        if (array_key_exists('items', $data) && is_array($data['items']))
        {
            $this->count = count($data['items']);
            $this->productIds = [];
            foreach($data['items'] as $item)
            {
                if(isset($item['product_id']))
                    $this->productIds[$item['product_id']] = $item['id'];
            }
        }
    }
}