<?php

namespace app\Models\Cart\FHT;

use App\Models\Cart\AbstractCartBuilder;

class Builder extends AbstractCartBuilder {

    public function makeItems()
    {
        $this->cart->updateCount($this->data);
         $this->cart->items = app('App\Models\Cart\Product\Resolver')->makeFHTProducts($this->getData('items'));
        return $this;
    }

    public function buildCart($data)
    {
        $this->data = $data;
        $this->makeCart()
             ->makeItems()
             ->makeAttributes();
        return $this;
    }
}