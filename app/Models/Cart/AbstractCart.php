<?php

namespace App\Models\Cart;

use Closure;
use App\Models\Common\BaseModel;
use App\Exceptions\MagAPI\ValidationException;
use App\Services\MagAPI;
use App\Models\Common\Factory;
use App\Models\Cart\Product\Contract as ProductContract;

abstract class AbstractCart extends BaseModel
{

    public $type;
    public $items = [];

    protected $count = 0;

    protected $state;

    protected $options = [];

    const NORMAL_TYPE = 'normal';
    const FHT_TYPE = 'tbyb';
    const HTO_TYPE = 'hto';

    const TMP_STORE_KEY = 'cart:tmp';
    const BACKGROUND_STORE_KEY = 'cart:state';

    public function __construct(MagAPI $api, Factory $factory, State $state)
    {
        $this->api = $api;
        $this->factory = $factory;
        $this->state = $state;
    }

    public abstract function add(ProductContract $product);

    public abstract function remove(ProductContract $product);

    protected abstract function updateState();

    public function setOption($key, $value)
    {
        return $this->setValue("options[$key]", $value);
    }

    public function setOptions(array $data)
    {
        foreach($data as $key => $value)
        {
            $this->setOption($key, $value);
        }
    }

    public function setValue($key, $value)
    {
        $this->options[$key] = $value;
        return $this;
    }

    public function setValues(array $data)
    {
        foreach($data as $key => $value)
        {
            $this->setValue($key, $value);
        }
    }

    public function getValues()
    {
        return $this->options;
    }

    protected function addPush(ProductContract $product, Closure $successCallback = null)
    {
        try {
            $data = $this->api->push('addtocart', [
                'request' => [
                        'id' => $product->getId(),
                    ] + $this->options
            ]);
            $data = $data['result'];
            $this->options = [];

            if ($successCallback)
                $successCallback($data);

            $this->afterPush($data);
            return false;
        } catch (ValidationException $e) {
            return $e->getMessage();
        }
    }

    protected function removePush(ProductContract $product, Closure $successCallback = null)
    {
        try {
            $data = $this->api->push('removefromcart', [
                'request' => [
                    'id' => $product->getId(),
                ]
            ]);
            $data = $data['result'];

            if ($successCallback)
                $successCallback($data);

            $this->afterPush($data);
            return false;
        } catch (ValidationException $e) {
            return $e->getMessage();
        }
    }

    protected function cleanPush(Closure $successCallback = null)
    {
        try {
            $data = $this->api->push('cleancart', [

            ]);
            $data = $data['result'];

            if ($successCallback)
                $successCallback($data);

            $this->afterPush($data);
            return false;
        } catch (ValidationException $e) {
            return $e->getMessage();
        }
    }

    public function isNormal()
    {
        return $this->type == self::NORMAL_TYPE;
    }

    public function isFHT()
    {
        return $this->type == self::FHT_TYPE;
    }

    protected function afterPush($data)
    {
        $this->updateCount($data);
        $this->updateState();
        \Session::flash(self::TMP_STORE_KEY, $data);
    }

    public function updateCount($data)
    {
        if (array_key_exists('items', $data) && is_array($data['items']))
            $this->count = count($data['items']);
    }

    public function setCount($count)
    {
        $this->count = $count;
    }

    public function getCount()
    {
        return $this->count;
    }

    public function getState()
    {
        return $this->state;
    }

}