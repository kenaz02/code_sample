<?php
namespace App\Models;

use App\Models\Common\BaseModel;

class Price extends BaseModel{

    public $name;
    public $currency_code = 'INR';
    public $price;
    public $currency_sym = '&#8377;';

    protected $currencies = [
        'INR' => '&#8377;'
    ];

    public function setAttributes($data, $attributes = false, $attrExcept = false)
    {
        if($attributes !== false)
        {
            parent::setAttributes($data, $attributes, $attrExcept);
        }
        else
        {
            $this->name = $data['name'];
            //$this->currency_code = $data['currency_code'];
            //$this->currency_sym = $this->currencies[$this->currency_code];
            $this->price = $data['price'];
        }
    }

    public function setCurrency($code)
    {
        if(array_key_exists($code, $this->currencies))
            $this->currency_sym = $this->currencies[$code];
    }

    public function __toString()
    {
        return (string) $this->price;
    }
}