<?php

namespace App\Models\Repositories;

use App\Exceptions\MagAPI\ValidationException;
use App\Models\Common\BaseRepository;
use App\Models\Order;
use App\Services\MagAPI;
use App\Models\Common\Factory;
use App\Models\Order\Builder as OrderBuilder;

class OrderRepository extends BaseRepository{

    protected $model = 'Order\Order';

    protected $orderBuilder;

    public function __construct(MagAPI $api, Factory $factory, OrderBuilder $orderBuilder)
    {
        $this->orderBuilder = $orderBuilder;
        parent::__construct($api, $factory);
    }

    public function getList()
    {
        $data = $this->api->fetch('orders');

        $orders = [];
        $data = is_array($data['result']) ? $data['result'] : [];
        foreach($data as $item)
        {
            $builder = clone $this->orderBuilder;
            $orders[] = $builder->setData($item)->forList()->getOrder();
        }

        return $orders;
    }

    public function getItem($id)
    {
        try
        {
            $data = $this->api->fetch('orders/id/:id', [
                'url' => ['id' => $id ]
            ]);
            $data = $data['result'];

            return $this->orderBuilder->setData($data)->forItem()->getOrder();
        }
        catch(ValidationException $er)
        {
            return false;
        }
    }

    public function itemPrescription($id)
    {
        try
        {
            $data = $this->api->fetch('getorderitemoptions/order_id/:id', [
                'url' => [
                    'id' => $id
                ]
            ]);
            $data = $data['result'];
            $data['id'] = $id;

            return $this->orderBuilder->setData($data)->forPrescription()->getOrder();
        }
        catch(ValidationException $e)
        {
            return false;
        }
    }
}