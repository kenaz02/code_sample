<?php

namespace App\Models\Repositories;


use App\Models\Common\BaseRepository;
use Illuminate\Support\Facades\Redis;


class FiltersRepository extends BaseRepository {

    protected $selectedOptions = [];

    protected $productsNumber;


    public function catalogSelected($gender, $catalog)
    {
        return $this->setSelected(
            $this->catalog($gender, $catalog)
        );
    }

    private function setSelected($filters)
    {
        foreach($filters as $key => $item)
        {
            foreach($item['options'] as $optionKey => $optionItem)
            {
                $filters[$key]['options'][$optionKey]['selected'] = array_key_exists($item['id'], $this->selectedOptions) && in_array($optionItem['id'], $this->selectedOptions[$item['id']]);
            }
        }
        return $filters;
    }

    public function catalog($gender, $catalog)
    {
        $filters = $this->api->fetch('filters/gender/:gender/catalog/:catalog', ['url' => [
            'gender'   => $gender,
            'catalog'  => $catalog
        ]]);

        return $filters['result'];
    }

    public function offerSelected($id)
    {
        return $this->setSelected(
            $this->offer($id)
        );
    }

    public function offer($offerId, $selectedFilters = [])
    {
        $filters = $this->api->fetch('offerfiltersMSite/listing/:offerId', [
            'url' => [
                'offerId' => $offerId
            ],
            'request' => $selectedFilters
        ]);

        $this->productsNumber = $filters['result']['number_of_products'];
        return $filters['result']['filters'];
    }

    public function getProductNumber()
    {
        if($this->productsNumber == null)
            throw new \ErrorException('Load repository. Call offer method first');
        return $this->productsNumber;
    }

    public function searchSelected($query)
    {
        return $this->setSelected(
            $this->search($query)
        );
    }

    public function search($query)
    {
        $filters = $this->api->fetch('searchfilters', [
            'request' => [
                'query' => $query
            ]
        ]);
        return $filters['result'];
    }

    public function preSetOptions($options)
    {
        foreach($options as $key => $item)
        {
            $this->selectedOptions[$key] = explode(',', $item);
        }
    }

    public function offerActiveSelected($offerId, $selectedFilters)
    {
        $selectedOptions = [];
        foreach($this->offer($offerId, $selectedFilters) as $item)
        {
            foreach($item['options'] as $optionItem)
            {
                $key = 'check-'.$item['id'].'-'.$optionItem['id'];
                if($optionItem['no_of_products'] > 0)
                    $selectedOptions[$key] = $optionItem['no_of_products'];
            }
        }

        return $selectedOptions;
    }

}