<?php

namespace App\Models\Repositories;

use App\Models\Address;
use App\Models\Common\BaseRepository;
use App\Exceptions\MagAPI\UnexpectedDataFormatException;
use App\Exceptions\MagAPI\ValidationException;
use App\Models\Services\Location;

class AddressRepository extends BaseRepository{

    protected $model = 'Address';

    public function getList(){
        $data = $this->api->fetch('addresses');

        return $this->fill($data['result']);
    }

    public function item($id)
    {
        $data = $this->api->fetch('addresses/id/:id', [
            'url' => [
                'id' => $id
            ]
        ]);

        return $this->fillModel($data['result']);
    }

    public function getStatesList()
    {
        $data = $this->api->fetch('countrystate');

        if(!isset($data['result']['states']))
            throw new UnexpectedDataFormatException();

        return $this->factory->makeCollection('Services\Location', $data['result']['states'], function(Location $model, $item) {
            $model->code = $model->name = $item;
        });
    }

    public function getPincodeItem($id)
    {
        try
        {
            $data = $this->api->fetch('checkpincode/:pincode', [
                'url' => [
                    'pincode' => $id
                ]
            ]);
            $data = $data['result'];

            $instance = app('App\Models\Services\Pincode', ['id' => null]);
            return $this->factory->makeModel($instance, $data);
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }
}