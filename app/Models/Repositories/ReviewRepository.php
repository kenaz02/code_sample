<?php

namespace App\Models\Repositories;

use App\Models\Common\BaseRepository;

class ReviewRepository extends BaseRepository{

    protected $model = 'Product\Review';

    public function getList($id)
    {
        $data = $this->api->fetch('product/:id/reviews', [
            'url' => [
                'id' => $id
            ]
        ]);

        return $this->fill($data['result']);
    }
}