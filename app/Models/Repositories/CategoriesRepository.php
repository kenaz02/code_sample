<?php

namespace App\Models\Repositories;

use App\Exceptions\MagAPI\UnexpectedDataFormatException;
use App\Models\Common\BaseRepository;

/**
 * Class CategoriesRepository
 * Loads categories list of home menu and subcategory page items
 * @package App\Models\Repositories
 */
class CategoriesRepository extends BaseRepository {

    /**
     * Id of face category - need to show appropriate section in categories list view
     */
    const FACE_CATEGORY_ID = 5447;

    protected $model = 'Category';

    /**
     * Fetch home menu items
     * @return array
     * @throws UnexpectedDataFormatException
     */
    public function home()
    {
        $data = $this->api->fetch('category');

        if(!isset($data['result']['category']))
            throw new UnexpectedDataFormatException();

        return $this->fill($data['result']['category']);
    }

    /**
     * Fetch subcategory items
     * @param $catalog
     * @param $gender
     * @return array
     * @throws UnexpectedDataFormatException
     */
    public function subcategories($catalog, $gender)
    {
        $data = $this->api->fetch('subcategory/gender/:gender/catalog/:catalog', ['url' => [
            'gender'   => $gender,
            'catalog'  => $catalog
        ]]);

        if(!isset($data['result']['subcategory']))
            throw new UnexpectedDataFormatException();

        return $this->fill($data['result']['subcategory']);
    }

    /**
     * Fetch our service section items
     * @return mixed
     * @throws UnexpectedDataFormatException
     */
    public function ourServices()
    {
        $data = $this->api->fetch('ourServices');

        if(!isset($data['result']['services']))
            throw new UnexpectedDataFormatException();

        return $data['result']['services'];
    }
}