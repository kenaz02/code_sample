<?php

namespace App\Models\Repositories;

use App\Models\Common\BaseRepository;
use App\Models\Common\Factory;
use App\Models\Product;
use App\Models\Product\Builders\ItemBuilder;
use App\Models\ThirdPartyProductAdapter;
use App\Services\PaginationContract;
use App\Services\ThirdPartyAPI;
use App\Services\MagAPI;
use Illuminate\Support\Facades\Redis;

/**
 * Class ProductRepository
 * Load category, home page, search product list, single product item
 * @package App\Models\Repositories
 */
class ProductRepository extends BaseRepository implements PaginationContract {

    const ITEMS_PER_PAGE = 48;

    private $dataItemsCount;

    private $totalItems;
    private $categoryName;
    private $helpBlock;

    protected $thirdPartyAPI;

    private $withDescription = false;

    protected $model = 'Product';

    public function __construct(MagAPI $api, ThirdPartyAPI $thirdPartyAPI, Factory $factory)
    {
        $this->api = $api;
        $this->factory = $factory;
        $this->thirdPartyAPI = $thirdPartyAPI;
    }

    /**
     * Loads data for home/index page.
     * Offers data, product items - Best Sellers, New Arrivals, Recommendation, Recently Viewed
     * @return mixed|null
     */
    public function home()
    {
        $data = $this->api->fetch('home');
        $data = $data['result'];

        $thirdPartyData = $this->thirdPartyAPI->homeData();

        //TODO: Remove before production
        $redis = Redis::connection();
        foreach($data['offers'] as $key=>$item)
        {
            $url = str_replace('http://www.lenskart.com/', '', $item['category_url']);
            $data['offers'][$key]['category_url'] = $url;
            $redis->set($url, 'cat:'.$item['id']);
        }

        $data['best_sellers'] = $this->fill($data['best_sellers']);
        $data['new_arrivals'] = $this->fill($data['new_arrivals']);

        $data['recommendation'] = $this->fillThirdPartyModels($thirdPartyData['rhf']['recommendations']['recommendedItems']);
        $data['recently_viewed'] = $this->fillThirdPartyModels($thirdPartyData['rhf']['recentHistory']['viewedItems']);

        return $data;
    }

    public function fillWithCount($data)
    {
        $this->dataItemsCount = count($data);
        return $this->factory->makeBuilderCollection('Product\Builders\ListBuilder', $data);
    }

    public function fillThirdPartyModels($data)
    {
        return parent::fillModels(
            $data,
            function(Product $instance, $data)
            {
                $productAdapter = new ThirdPartyProductAdapter($instance);
                $productAdapter->setAttributes($data);
            }
        );
    }

    public function getList($catalog, $gender, $filters = [])
    {
        $data = $this->api->fetch('products/gender/:gender/catalog/:catalog', [
            'url' => [
                'gender'    => $gender,
                'catalog'  => $catalog
            ],
            'request' => $filters
        ]);
        return  $this->fillWithCount($data['result']);
    }

    public function offer($offerId, $filters = [])
    {
        $filters['id'] = $offerId;
        $data = $this->api->fetch('categoryinfo', [
           'request' => $filters
        ]);

        $data = $data['result'];

        $this->categoryName = $data['category_name'];
        $this->totalItems = $data['num_of_products'];

        if($data['is_block_present'])
            $this->helpBlock = $this->thirdPartyAPI->cms('freeFrameBlock');

        return  $this->fillWithCount($data['product_list']);
    }

    public function search($query, $filters = [])
    {
        $data = $this->api->fetch('search/query/:query', [
            'url' => [
                'query'    => $query
            ],
            'request' => $filters
        ]);
        return $this->fillWithCount($data['result']);
    }

    public function terms($query)
    {
        $data = $this->api->fetch('search/query/:query/terms', [
            'url' => [
                'query'    => $query
            ]
        ]);
        $data = $data['result'];

        $terms = [];
        foreach($data['terms'] as $key => $item)
        {
            $terms[$key] = [
                'id'    => $data['id'][$key],
                'label' => $data['terms'][$key],
            ];
        }
        unset($data);

        return $terms;
    }


    protected function fetchItemData($id)
    {
        $data = $this->api->fetch('product/:id', [
            'url' => [
                'id' => $id
            ]
        ]);

        return $data['result'];
    }

    public function item($id)
    {
        $data = $this->fetchItemData($id);
        return $this->factory->makeBuilderModel('Product\Builders\ItemBuilder', $data);
    }

    public function reviews($id)
    {
        $data = $this->fetchItemData($id);
        $model = $this->factory->makeBuilderModel('Product\Builders\ItemBuilder', $data, function(ItemBuilder $builder, $itemData){
            $builder->setData($itemData)->buildReviews();
        });
        return $model;
    }

    public function buyOptions($id)
    {
        $data = $this->fetchItemData($id);
        $model = $this->factory->makeBuilderModel('Product\Builders\ItemBuilder', $data, function(ItemBuilder $builder, $itemData){
            $builder->setData($itemData)->buildBuyOptions();
        });
        return $model;
    }

    public function withDescription()
    {
        $this->withDescription = true;
        return $this;
    }

    protected function fetchItemDescription(Product $product)
    {
        if($this->withDescription && $product->description === null)
        {
            $product->description = $this->thirdPartyAPI->cms('description', ['id' => $product->id]);
            \Cache::put('product:item', $product, 5);
        }
    }

    public function getCount()
    {
        return $this->dataItemsCount;
    }

    public function getTotalCount()
    {
        return $this->totalItems;
    }

    public function getCategoryName()
    {
        return $this->categoryName;
    }

    public function getHelpBlock()
    {
        return $this->helpBlock;
    }

    public function getHTO()
    {
        $data = $this->api->fetch('hto');
        return $this->factory->makeModel('Services\HomeEyeCheckUp\HomeEyeCheckUp', $data['result']);
    }
}