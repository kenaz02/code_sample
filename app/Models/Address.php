<?php

namespace App\Models;

use App\Models\Common\BaseModel;
use App\Exceptions\MagAPI\ValidationException;
use App\Models\Services\Pincode;

class Address extends BaseModel{

    public $id;
    public $firstname;
    public $lastname;
    public $street0;
    public $street1;
    public $telephone;
    public $alt_telephone;
    public $pincode;
    public $state;
    public $city;
    public $country;
    public $locality;
    public $default;

    public static function rules()
    {
        $phoneRules = ['digits_between:10,12', 'numeric', 'regex:/^(7|8|9).+$/u'];
        return [
            'firstname'     => 'required|max:25|regex:/^[\pL,\'\s]+$/u',
            'lastname'      => 'required|max:25|regex:/^[\pL,\'\s]+$/u',
            'street0'       => 'required|regex:/^[\pL0-9,\'\s]+$/u',
            'telephone'     => array_merge($phoneRules, ['required']),
            'alt_telephone' => $phoneRules,
            'pincode'       => 'required|numeric',
            'city'          => 'required',
            'locality'      => 'max:50|regex:/^[\pL-,\(\)\'\s]+$/u'
        ];
    }

    public function setAttributesFromPincode(Pincode $pincode)
    {
        $data = $pincode->getAttributes([
            'city',
            'state',
            'country',
            'id'
        ]);
        $data['pincode'] = $data['id'];
        unset($data['id']);
        $this->setAttributes($data);
    }

    public function save()
    {
        try
        {
            if($this->id)
            {
                $url = 'addresses/id/:id/edit';
                $pushData['url'] = [
                    'id' => $this->id
                ];
            }
            else
            {
                $url = 'addresses/create';
            }

            $data = $this->getAttributes(['country', 'state', 'pincode'], true);
            $data['country_id'] = $this->country;
            $data['region']     = $this->state;
            $data['postcode']   = $this->pincode;

            $pushData['request'] = $data;

            $this->api->push($url, $pushData);
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function delete()
    {
        $this->api->push('/addresses/id/:id/delete', [
            'url' => [
                'id' => $this->id
            ]
        ]);
    }
}