<?php


namespace App\Models\Order\Product;

use App\Models\Product;
use App\Models\Product\Builders\AbstractBuilder;

class PrescriptionBuilder extends AbstractBuilder {


    public function makeOptions()
    {
        $options = $this->factory->makeCollection('Order\Product\Option', $this->getData('buy_options'));
        foreach($options as $item)
        {
            if($item instanceof Option)
                $this->product->buy_options[$item->type][$item->key][$item->side] = $item;
        }
        return $this;
    }

    public function build()
    {
        $this->makeOptions()
             ->makeAttributes();
        return $this;
    }

}