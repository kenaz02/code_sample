<?php


namespace App\Models\Order\Product;


use App\Models\Common\BaseModel;

class Option extends BaseModel {

    public $id;
    public $label;
    public $type;
    public $values = [];
    public $side;
    public $key;

    const SIDE_LEFT = 'left';
    const SIDE_RIGHT = 'right';

    public function setValues($data)
    {
        $this->values = [];
        foreach($data as $item)
        {
            $this->values[$item['id']] = $item['model_name'];
        }
    }

    public function setLabel($label)
    {
        $this->label = $label;
        $this->key = preg_replace('/(left\s+|right\s+)/i', '', $label);
        $this->side = stripos($label, self::SIDE_LEFT) !== false ? self::SIDE_LEFT : self::SIDE_RIGHT;
    }

}