<?php

namespace App\Models\Order;

use App\Models\Common\BaseModel;

class Order extends BaseModel
{

    public $id;
    public $items;
    public $order_date;
    public $cost;
    public $status;
    public $address;

}