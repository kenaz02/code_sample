<?php

namespace App\Models\Order;


class Result {

    protected $errors = false;
    protected $orderId;
    protected $otpSent;
    protected $powerRequired;

    public function setOptions($options)
    {
        if(isset($options[0]['id']))
            $options = $options[0];

        if(isset($options['id']))
            $this->orderId = $options['id'];
        else
            throw new \ErrorException('Submitted order id must exists in options array');

        if(isset($options['power_required']))
            $this->powerRequired = $options['power_required'];
        else
            throw new \ErrorException('Submitted order power_required option must exists in options array');
    }

    public function setErrors($errors)
    {
        $this->errors = $errors;
    }

    public function getErrors()
    {
        return $this->errors;
    }

    public function getOrderId()
    {
        return $this->orderId;
    }

    public function isPowerRequired()
    {
        return (boolean) $this->powerRequired;
    }

}