<?php


namespace App\Models\Order;


use App\Models\Common\Factory;

class Builder {

    protected $order;

    protected $data;

    public function __construct(Order $order, Factory $factory)
    {
        $this->order = $order;
        $this->factory = $factory;
    }

    public function setData(&$data)
    {
        $this->data = $data;
        return $this;
    }

    public function makeAddress()
    {
        $this->order->address = $this->factory->makeModel('Address', $this->getData('address'));
        return $this;
    }

    public function makeCost()
    {
        $data = $this->getData('cost');
        $this->order->cost = $this->factory->makeModel('Price', $data[0]);
        return $this;
    }

    public function makeItems()
    {
        //$this->order->items = $this->factory->makeCollection('')

        $builderInstance = app('App\Models\Order\Product\PrescriptionBuilder');
        foreach($this->getData('items') as $item)
        {
            $builder = clone $builderInstance;
            $this->order->items[] = $builder->setData($item)->makeAttributes()->getProduct();
        }

        return $this;
    }

    public function makeAttributes()
    {
        $this->order->setAttributes($this->data);
        return $this;
    }

    public function forPrescription()
    {
        $this->order->id = $this->getData('id');

        $builderInstance = app('App\Models\Order\Product\PrescriptionBuilder');
        foreach($this->getData('items') as $item)
        {
            $builder = clone $builderInstance;
            $this->order->items[] = $builder->setData($item)->build()->getProduct();
        }

        return $this;
    }

    public function forItem()
    {
        return $this->makeItems()
                    ->makeAddress()
                    ->makeCost()
                    ->makeAttributes();
    }

    public function forList()
    {
        return $this->makeItems()
                    ->makeCost()
                    ->makeAttributes();
    }

    public function getOrder()
    {
        return $this->order;
    }

    public function __clone()
    {
        $this->order = clone $this->order;
    }

    protected function getData($attribute, $unset = true)
    {
        if(isset($this->data[$attribute]))
            return $unset ? array_pull($this->data, $attribute) : $this->data[$attribute];
        else
            throw new \ErrorException("Attribute $attribute must be exists in data array.");
    }
}
