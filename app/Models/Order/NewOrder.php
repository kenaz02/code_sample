<?php

namespace App\Models\Order;

use App\Exceptions\MagAPI\ValidationException;
use App\Models\Address;
use App\Services\MagAPI;

/**
 * Class NewOrder
 * Process new order submit.
 * Used for normal cart checkout.
 * @package App\Models\Order
 */
class NewOrder {

    /**
     * Address model instance
     * @var \App\Models\Address
     */
    protected $address;

    /**
     * Magento API instance
     * @var \App\Services\MagAPI
     */
    protected $api;

    /**
     * New order options array.
     * Use to collect all options using for order submit.
     * @var array
     */
    protected $options = [];

    /**
     * Declare special/const options.
     * Using in child classes.
     * @var array
     */
    protected $specialOptions = [];

    /**
     *
     * paytbyb = Try Before You Buy
        nb = Net Banking
    cc = Credit Card
    dc = Debit Card
    cod = Cash on Delivery
    bt = Bank Transfer
     */
    const P_TRY_BEFORE_YOU_BUY = 'paytbyb';
    const P_NET_BANKING        = 'nb';
    const P_CREDIT_CART        = 'cc';
    const P_DEBIT_CART         = 'dc';
    const P_CASH_ON_DELIVERY   = 'cod';
    const P_BANK_TRANSFER      = 'bt';

    const P_PAY_TM             = 'paytm_cc';
    const P_PAY_U              = 'shared';


    /**
     * Inject class dependencies.
     * @param MagAPI $magAPI
     */
    public function __construct(MagAPI $magAPI)
    {
        $this->api = $magAPI;
    }

    /**
     * Set address instance.
     * @param Address $address
     * @return $this
     */
    public function setAddress(Address $address)
    {
        $this->address = $address;
        return $this;
    }

    /**
     * Set email option.
     * @param $email
     * @return $this
     */
    public function setEmail($email)
    {
        $this->options['email'] = $email;
        return $this;
    }

    /**
     * Set paymentmethod option.
     * @param $paymentMethod
     * @return $this
     * @throws \ErrorException
     */
    public function setPaymentMethod($paymentMethod)
    {
        $existingMethods = [
            self::P_NET_BANKING,
            self::P_CREDIT_CART,
            self::P_DEBIT_CART,
            self::P_CASH_ON_DELIVERY,
            self::P_BANK_TRANSFER,
            self::P_PAY_TM,
            self::P_PAY_U
        ];

        if(in_array($paymentMethod, $existingMethods))
        {
            $this->options['paymentmethod'] = $paymentMethod;
            return $this;
        }
        else
            throw new \ErrorException('Payment method is not exist');
    }

    /**
     * Set landmark option.
     * @param $landmark
     * @return $this
     */
    public function setLandmark($landmark)
    {
        $this->options['landmark'] = $landmark;
        return $this;
    }

    /**
     * Check if payment exists
     * @param $payment
     * @return boolean
     */
    public function isPaymentExists($payment)
    {
        return ;
    }


    /**
     * Make order.
     * Merge all options and push it to API.
     * Reset cart state.
     * @return bool|string - if order submitted with errors error message will
     * be return else false (submitted without errors).
     */
    public function make()
    {
        if($this->address instanceof Address)
        {
            $result = new Result();
            try {
                $options = $this->address->getAttributes(['id', 'default'], true);
                $options = $options + $this->options + $this->specialOptions;

                $data = $this->api->push('neworder', [
                    'request' => $options
                ]);

                $result->setOptions($data['result']);
                app('App\Models\Cart\State')->reset();
            }
            catch (ValidationException $e)
            {
                $result->setErrors($e->getMessage());
            }
            return $result;
        }
    }

}