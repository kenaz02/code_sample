<?php

namespace App\Models\Order;

/**
 * Class NewOrderFHT
 * Process Free Home Trial new order submit.
 * Used for Free Home Trial cart checkout.
 * @package App\Models\Order
 */
class NewOrderFHT extends NewOrder {

    /**
     * Free Home Trial order special options.
     * @var array
     */
    protected $specialOptions = [
        'is_tbyb' => 1,
        'paymentmethod' => self::P_TRY_BEFORE_YOU_BUY
    ];

}