<?php

namespace App\Models;


class ThirdPartyProductAdapter {

    private $product;

    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function setAttributes($data)
    {
        $this->product->id          = $data['itemId'];
        if(isset($data['itemRating']))
            $this->product->brand_name  = $data['itemRating'];
        $this->product->setProductUrl($data['itemURL']);
        $this->product->setPrices($data['itemPrice']);
        $this->product->setImages($data['itemImage']);
    }
}