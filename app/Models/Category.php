<?php

namespace App\Models;

use App\Models\Common\BaseModel;
use App\Services\MagAPI;
use App\Models\Common\Factory;
use Illuminate\Support\Facades\Redis;

/**
 * Class Category
 * Site category model. Describe item of the home menu and subcategories page.
 * @package App\Models
 */
class Category extends BaseModel {

    public $id;
    public $name;
    public $url;
    public $children;
    public $icon_image;
    public $textStyle;

    //TODO: Remove before production
    private $redis;

    /**
     * Model constructor
     * @param MagAPI $api
     * @param Factory $factory
     */
    public function __construct(MagAPI $api, Factory $factory)
    {
        //TODO: Remove before production
       $this->redis = Redis::connection();
       parent::__construct($api, $factory);
    }

    //TODO: Remove before production
    public function setUrl($url)
    {
        $this->url = preg_replace(['(.*lenskart.com/)', '(mobile\/)', '(\?.*)'], '', $url);
        $this->redis->set($this->url, 'cat:'.$this->id);
    }

    /**
     * Set children category elements
     * @param $children
     * @throws \ErrorException
     */
    public function setChildren($children)
    {
        $this->children = $this->factory->makeCollection('Category', $children);
    }

    /**
     * Serialization method
     * Provide list of the properties which will be serialized and cached.
     * @return array
     */
    public function __sleep()
    {
        return ['id', 'name', 'children', 'url'];
    }

}