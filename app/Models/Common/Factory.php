<?php

namespace App\Models\Common;

use Closure;
use ErrorException;

/**
 * Class Factory
 * Create models and repositories objects, create collection (array) of the models.
 * Make as singleton instance.
 * @package App\Models\Common
 */
class Factory {

    /**
     * Models root namespace
     * @var string
     */
    protected $rootNamespace = 'App\Models';

    public function setInstance($name, $data = null)
    {

    }

    /**
     * Create object via framework service containers
     * @param $abstract - model class name - 'SomeFolder\SomeModel'
     * or array with parameters: class name and arguments - ['SomeFolder\SomeModel', $arg1, $arg2]
     * @return mixed
     */
    public function makeInstance($abstract)
    {
        $args = [];
        if(is_array($abstract))
        {
            $args = $abstract;
            $abstract = array_shift($args);
            $args = $this->resolveArgs($args);
        }
        return \App::make($this->rootNamespace.'\\'.$abstract, $args);
    }

    /**
     * Prepare objects passed via constructor arguments for loading via framework container.
     * @param $args - array with parameters
     * @return array
     */
    protected function resolveArgs($args)
    {
        $resolvedArgs = [];
        foreach($args as $key => $item)
        {
            if($instance = get_class($item))
                $resolvedArgs[strtolower(class_basename($instance))] = $item;
            else
                $resolvedArgs[$key] = $item;
        }
        unset($args);
        return $resolvedArgs;
    }

    /**
     * Make collection(array) of the models from passed data
     * @param $abstract - model name, array with parameters or model instance
     * @param $data - collection of the model data arrays
     * @param $callback - callback function for each make iteration.
     * If callback is null model setAttributes will be used.
     * @return array
     * @throws ErrorException
     */
    public function makeCollection($abstract, $data, Closure $callback = null)
    {
        if(!is_array($data))
            throw new ErrorException('Collection input data must be an array.');

        if(!is_object($abstract))
            $abstract = $this->makeInstance($abstract);

        if($abstract instanceof BaseModel) {
            $collection = [];
            foreach ($data as $item) {
                $model = clone $abstract;
                if ($callback !== null)
                    $callback($model, $item);
                else
                    $model->setAttributes($item);
                $collection[] = $model;
            }
            return $collection;
        }
        else
        {
            throw new ErrorException('Model must be an instance of the BaseModel class.');
        }
    }

    /**
     * Make model from passed data
     * @param $abstract - model name or array with parameters
     * @param array $data - array model data
     * @return mixed
     * @throws ErrorException
     */
    public function makeModel($abstract, $data = [])
    {
        if(!is_object($abstract))
            $abstract = $this->makeInstance($abstract);

        if($abstract instanceof BaseModel) {
            $abstract->setAttributes($data);
            return $abstract;
        }
        else
        {
            throw new ErrorException('Model must be an instance of the BaseModel class.');
        }
    }

    /**
     * Make collection(array) of the models from passed data via appropriate builder class
     * @param $abstract - builder class name or instance
     * @param $data - collection of the model data arrays
     * @param Closure|null $callback - callback function for each make iteration.
     * @return array
     * @throws ErrorException
     */
    public function makeBuilderCollection($abstract, $data, Closure $callback = null)
    {
        if(!is_array($data))
            throw new ErrorException('Collection input data must be an array.');

        if(!is_object($abstract))
            $abstract = $this->makeInstance($abstract);

        if($abstract instanceof BaseBuilder)
        {
            $collection = [];
            foreach($data as $item)
            {
                $builder = clone $abstract;
                if($callback !== null)
                    $callback($builder, $item);
                else
                    $builder->setData($item)->build();

                $collection[] = $builder->getModel();
            }
            return $collection;
        }
        else
        {
            throw new ErrorException('Builder must be an instance of the BaseBuilder class.');
        }
    }

    /**
     * Make model from passed data via appropriate builder class
     * @param $abstract - builder class name or instance
     * @param array $data - array model data
     * @param Closure|null $callback
     * @return mixed
     * @throws ErrorException
     */
    public function makeBuilderModel($abstract, $data = [], Closure $callback = null)
    {
        if(!is_object($abstract))
            $abstract = $this->makeInstance($abstract);

        if($abstract instanceof BaseBuilder)
        {
                if($callback !== null)
                    $callback($abstract, $data);
                else
                    $abstract->setData($data)->build();

            return $abstract->getModel();;
        }
        else
        {
            throw new ErrorException('Builder must be an instance of the BaseBuilder class.');
        }
    }



}