<?php

namespace App\Models\Common;

use App\Services\MagAPI;

/**
 * Class BaseModel
 * Base application model class, provide interface for processing API data items.
 * @package App\Models\Common
 */
class BaseModel {

    /**
     * Magento API instance
     * @var \App\Services\MagAPI
     */
    protected $api;

    /**
     * Models factory instance
     * @var \App\Models\Common\Factory
     */
    protected  $factory;

    private $_attributes = [];
    private $_setters = [];

    public function __construct(MagAPI $api, Factory $factory)
    {
        $this->api = $api;
        $this->factory = $factory;
    }

    protected function getAttributeAliases()
    {
        return [];
    }

    public function setAttributes($data, $attributes = false, $attrExcept = false)
    {
        $attributeAliases = $this->getAttributeAliases();
        foreach($this->getClassAttributes() as $classAttr)
        {
            $aliasAttr = false;
            $dataAttr = $classAttr;
            if($attributeAliases && array_key_exists($classAttr, $attributeAliases))
            {
                $aliases = explode('|', $attributeAliases[$classAttr]);
                foreach($aliases as $alias)
                {
                    if(array_key_exists($alias, $data))
                    {
                        $dataAttr  = $alias;
                        $aliasAttr = true;
                        break;
                    }
                }
            }

            if($aliasAttr || array_key_exists($dataAttr, $data))
            {
                if(is_array($attributes) && ($attrExcept == in_array($classAttr, $attributes)))
                    continue;

                if(array_key_exists($classAttr, $this->_setters))
                {
                    $setter = $this->_setters[$classAttr];
                    $this->$setter($data[$dataAttr]);
                }
                else
                {
                    $this->$classAttr = $data[$dataAttr];
                }
            }
        }
    }

    public function getAttributes($attributes = false, $attrExcept = false)
    {
        $data = [];
        foreach ($this->getClassAttributes() as $classAttr) {
            if(is_array($attributes) && ($attrExcept == in_array($classAttr, $attributes)))
                    continue;
            $data[$classAttr] = $this->$classAttr;
        }
        return $data;
    }

    private function getClassAttributes()
    {
        if(!$this->_attributes)
            $this->callReflection();
        return $this->_attributes;
    }

    private function getClassSetters()
    {
        if(!$this->_setters)
            $this->callReflection();
        return $this->_setters;
    }

    private function callReflection()
    {
        $reflection =  new \ReflectionObject($this);
        $attrs = $reflection->getProperties(\ReflectionProperty::IS_PUBLIC);
        foreach($attrs as $item)
        {
            $name = $item->getName();
            $this->_attributes[] = $name;
            $setterName = camel_case('set_'.$name);
            if($reflection->hasMethod($setterName))
                $this->_setters[$name] = $setterName;
        }
    }

    public static function setBatch($dataArr)
    {
        $instance = self::getClassInstance();
        $list = [];
        foreach($dataArr as $dataItem)
        {
            $model = clone $instance;
            $model->setAttributes($dataItem);
            $list[] = $model;
        }
        return $list;
    }

    public static function getClassInstance()
    {
        return \App::make(get_called_class());
    }
}