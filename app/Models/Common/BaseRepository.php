<?php

namespace App\Models\Common;

use App\Services\MagAPI;
use App\Models\Common\Factory;

/**
 * Class BaseRepository
 * Base models repositories class, provide all operation for fetching data from API.
 * @package App\Models\Common
 */
class BaseRepository {

    /**
     * Magento API instance
     * @var \App\Services\MagAPI
     */
    protected $api;
    protected $modelInstance;
    protected $modelInstanceName;

    protected $factory;

    protected $model;

    /**
     * Create new repository instance
     * @param MagAPI $api
     * @param Factory $factory
     */
    public function __construct(MagAPI $api, Factory $factory)
    {
        $this->factory = $factory;
        $this->api = $api;
    }

    public function fillModels($data, $closure)
    {
        if(!$data)
            return [];

        $convertedData = [];
        $instance = $this->getModelInstance();
        foreach($data as $item)
        {
            $model = clone $instance;
            $closure($model, $item);
            $convertedData[] = $model;
        }
        return $convertedData;
    }

    public function fill($data)
    {
        return $this->factory->makeCollection($this->model, $data);
    }

    public function getModelInstance()
    {
        if(is_object($this->model))
            return $this->model;

        return $this->factory->makeInstance($this->model);
    }

    public function fillModel($data)
    {
        return $this->factory->makeModel($this->model, $data);
    }
}