<?php

namespace App\Models\Common;


trait BaseDynamicTrait {

    abstract protected  function getDynamicInstance();

    public function __get($name)
    {
        $getter = 'get'.ucfirst($name);
        $instance = $this->getDynamicInstance();
        if(property_exists($instance, $getter))
            return $this->$instance;
    }

    public function __set($name, $values)
    {
        $setter = 'set'.ucfirst($name);
        if(method_exists($this->getInstance(), $setter))
            $this->$setter($values);
    }
}