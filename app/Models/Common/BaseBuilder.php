<?php

namespace App\Models\Common;

use App\Exceptions\MagAPI\UnexpectedDataFormatException;

/**
 * Class BaseBuilder
 * Base common builder class. Used for flexible building of the complex object from data array.
 * @package App\Models\Common\
 */
abstract class BaseBuilder {

    /**
     * Array of the input data
     * @var array
     */
    protected $data = [];

    /**
     * Factory instance
     * @var \App\Models\Common\Factory
     */
    protected $factory;

    public function __construct(Factory $factory)
    {
        $this->factory = $factory;
    }

    /**
     * Method for object building
     * @return mixed
     */
    abstract function build();

    /**
     * Get builded model
     * @return mixed
     */
    abstract function getModel();

    /**
     * Clone method used for collection assignment
     * @return mixed
     */
    abstract public function __clone();

    /**
     * Set data input array
     * @param $data
     * @return $this
     */
    public function setData(&$data)
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Get value from data array by key attribute
     * @param $attribute
     * @param bool|true $unset
     * @return mixed
     * @throws UnexpectedDataFormatException
     */
    protected function getData($attribute, $unset = true)
    {
        $this->checkAttribute($attribute);
            return $unset ? array_pull($this->data, $attribute) : $this->data[$attribute];
    }

    /**
     * Remove value from data array by key attribute
     * @param $attribute
     * @return $this
     * @throws UnexpectedDataFormatException
     */
    protected function forgetData($attribute)
    {
        $this->checkAttribute($attribute);
        array_forget($this->data, $attribute);
        return $this;
    }

    /**
     * Check if data attribute exist
     * @param $attribute
     * @throws UnexpectedDataFormatException
     */
    protected function checkAttribute($attribute)
    {
        if(!isset($this->data[$attribute]))
            throw new UnexpectedDataFormatException("Attribute $attribute must be exists in data array.");
    }

}