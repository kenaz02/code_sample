<?php

namespace App\Models\Common;
use App\Exceptions\MagAPI\ValidationException;

/**
 * Trait SubmitTrait
 * Provide validation and submit methods for model API submit/pushing
 * @package App\Models\Common
 */
trait SubmitTrait {

    /**
     * Validator instance
     * @var \Illuminate\Contracts\Validation\Factory
     */
    protected $validator;

    /**
     * Implement all submit business logic
     * @return mixed
     */
    abstract protected function onSubmit();

    /**
     * Validation rules
     * @return array
     */
    public static function rules()
    {
        return [];
    }

    /**
     * Validation messages
     * @return array
     */
    public static function messages()
    {
        return [];
    }

    /**
     * Submit model, call onSubmit method and catch validation exception
     * @return bool
     */
    public function submit()
    {
        try
        {
            $this->onSubmit();
            return true;
        }
        catch(ValidationException $e)
        {
            $this->getValidator()->messages()->add('submit', $e->getMessage());
            return false;
        }
    }

    /**
     * Validate model
     * @return mixed
     */
    public function validate()
    {
        return $this->getValidator()->fails();
    }

    /**
     * Make validator
     * @return \Illuminate\Contracts\Validation\Factory|mixed
     */
    public function getValidator()
    {
        if($this->validator == null)
            $this->validator = $this->getValidationFactory()->make($this->getAttributes(), self::rules(), self::messages());

        return $this->validator;
    }

    /**
     * Get instance of the validation factory
     * @return \Illuminate\Foundation\Application|mixed
     */
    protected function getValidationFactory()
    {
        return app('Illuminate\Contracts\Validation\Factory');
    }

}