<?php

namespace App\Models\Services;

use App\Models\Common\BaseModel;

class City extends BaseModel{

    public $id;
    public $city;

    public function setCity($city)
    {
        $this->city = $city;
    }
}