<?php

namespace App\Models\Services;

use App\Services\MagAPI;

class Otp {

    protected $api;

    public function __construct(MagAPI $api)
    {
        $this->api = $api;
    }

    public function check($optCode)
    {
        $data = $this->api->push('checkotp', [
           'request' => [
               'otp' => $optCode
           ]
        ]);
        $data = $data['result'];

        if(array_key_exists('is_code_valid', $data) && array_key_exists('message', $data))
            return [
                'status'  => (boolean) $data['is_code_valid'],
                'message' => $data['message']
            ];
        else
            throw new \ErrorException('Check OTP response is broken.');
    }

    public function send($mobile)
    {
        $mobile = preg_replace('/\s+/', '', $mobile);
        $data = $this->api->push('sendtbybotp', [
            'request' => [
                'mobile' => $mobile
            ]
        ]);
        $data = $data['result'];

        //TODO: Replace before production
        \Session::flash('key_opt', $data);

        if(isset($data['code']) && $data['code'] == 200)
            return true;
        else
            throw new \ErrorException('Send OTP response is broken.');
    }
}