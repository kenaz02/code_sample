<?php

namespace App\Models\Services;

use App\Models\Common\BaseModel;
use App\Models\Common\SubmitTrait;

/**
 * Class TrackOrder
 * Model. Implement track order logic and form submitting
 * @package App\Models\Services
 */
class TrackOrder extends BaseModel {

    use SubmitTrait;

    /**
     * @var | Model property
     */
    public $orderId;

    /**
     * @var | Model property
     */
    public $email;

    /**
     * Model validation rules
     * @return array
     */
    public static function rules()
    {
        return [
            'orderId'  => 'required',
            'email'    => 'required|email'
        ];
    }

    /**
     * Model submit business logic
     */
    protected function onSubmit()
    {
        $this->api->push('product/:id/reviews/create', [
            'request' => $this->getAttributes()
        ]);
    }
}