<?php

namespace App\Models\Services\HomeEyeCheckUp;

use App\Models\Common\BaseModel;
use App\Exceptions\MagAPI\ValidationException;
use App\Services\MagAPI;
use App\Models\Common\Factory;
use App\Models\Cart\Product\Contract as ProductContract;

/**
 * Class HomeEyeCheckUp
 * Home Eye Check Up service models. Process all Home Eye Check Up logic.
 * @package App\Models\Services\HomeEyeCheckUp
 */
class HomeEyeCheckUp extends BaseModel implements ProductContract{

    /**
     * Default id
     * @var int
     */
    public $id = 47552;

    public $date;
    public $time_slots;
    public $is_hto = 1;

    public $landmark;
    public $floor;
    public $lift_available;
    public $address_type;
    public $paymentmethod;
    public $email;

    public $images;

    public $prices;

    public $time_slot;

    public $city;
    public $cities = [];

    public $selectedCity;
    public $selectedDate;
    public $selectedTimeSlot;

    public $cityOptionId;
    public $timeSlotOptionId;
    public $dateOptionId;


    protected $selectedOptions = [];
    protected $optionsKeys = [];

    /**
     * Home Eye Check Up Cart model instance.
     * @var \App\Models\Services\HomeEyeCheckUp\Order
     */
    protected $order;

    /**
     * Constructor.
     * @param MagAPI $api
     * @param Factory $factory
     * @param Order $order
     */
    public function __construct(MagAPI $api, Factory $factory, Order $order)
    {
        $this->api = $api;
        $this->factory = $factory;
        $this->order = $order;
    }

    /**
     * Cart ProductContract contract interface implementation.
     * Provide interface to add the product to cart.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Attribute aliases.
     * @return array
     */
    protected function getAttributeAliases()
    {
        return [
            'images' => 'image_url|image_urls'
        ];
    }


    /**
     * Fetch home eye checkup model data from API.
     */
    public function load()
    {
        $data = $this->api->fetch('hto');
        $this->setAttributes($data['result']);
    }

    /**
     * Model prices setter.
     * @param $data
     * @throws \ErrorException
     */
    public function setPrices($data)
    {
        $this->prices = $this->factory->makeCollection('Price', $data);
    }

    /**
     * Model time slot setter.
     * @param $timeSlotData
     */
    public function setTimeSlot($timeSlotData)
    {
        $this->optionsKeys['timeSlot'] = $timeSlotData['id'];
        $this->time_slots = $timeSlotData['time_slots'];
    }

    /**
     * Model date setter.
     * @param $dateData
     */
    public function setDate($dateData)
    {
        $this->optionsKeys['date'] = $dateData['id'];
    }

    /**
     * Load available time slots by city_id and date.
     * @param $cityId
     * @param $date
     * @return array
     */
    public function loadTimeSlots($cityId, $date)
    {
        try
        {
            $data = $this->api->fetch('htoslot', [
                'request' => [
                    'city'   => $cityId,
                    'date'   => $date
                ]
            ]);

            $resultData = [];
            foreach($data['result']['slots'] as $key => $item)
            {
                 $resultData[$key]['id'] = $item['id'];
                 $resultData[$key]['time'] = $item['label'];
            }

            return $resultData;
        }
        catch (ValidationException $e)
        {
            return [ 'error' => $e->getMessage() ];
        }
    }


    public function loadTimeSlotsAvailableDays($pincode, $month = null)
    {
        try
        {
            if($month)
            {
                $firstDay = $month == date('m/Y') ? date('d') : '01';
                $dateFrom = date_format(date_create_from_format('d/m/Y', $firstDay.'/'.$month), 'Y-m-d');
            }
            else
            {
                $dateFrom = date('Y-m-d');
            }


            $dateTo = date('Y-m-t', strtotime($dateFrom));

            $data = $this->api->fetch('htoslot', [
                'request' => [
                    'pincode' => $pincode,
                    'from'    => $dateFrom,
                    'to'      => $dateTo
                ]
            ]);

            $data = $data['result'];

            $resultData = [];
            if($data)
                foreach($data as $item)
                {
                    $dateExplodedData = explode('-', $item['date']);

                    $key = (int) $dateExplodedData[2];
                    $resultData[$key]['is_available'] = false;
                    $resultData[$key]['date'] = $item['date'];

                    if(isset($item['slots']) && is_array($item['slots']))
                        foreach($item['slots'] as $slotItem)
                        {
                            if((int) $slotItem['num_slots'] > 0)
                            {
                                $resultData[$key]['is_available'] = true;
                                break;
                            }
                        }
                }

            return $resultData;
        }
        catch (ValidationException $e)
        {
            return [ 'error' => $e->getMessage() ];
        }
    }

    /**
     * Model cities setter. Create cities array.
     * @param $cityData
     * @throws \ErrorException
     */
    public function setCity($cityData)
    {
        if(array_key_exists('cities', $cityData) && array_key_exists('id', $cityData))
        {
            $this->optionsKeys['city'] = $cityData['id'];
            $this->cities = $this->factory->makeCollection('Services\City', $cityData['cities']);
        }
    }

    /**
     * Find city in cities array.
     * @param $id
     * @return bool
     */
    public function getCityById($id)
    {
        if($id)
        {
            foreach($this->cities as $item)
            {
                if($item->id == $id)
                    return $item;
            }
        }

        return false;
    }

    public function findCityByName($name)
    {
        $name = strtolower($name);

        foreach($this->cities as $item)
        {
            if(strtolower($item->city) == $name)
                return $item;
        }

        return false;
    }

    public function setCityOption($city)
    {
        $this->selectedOptions['city'] = $city;
        return $this;
    }

    public function setTimeSlotOption($timeSlot)
    {
        $this->selectedOptions['timeSlot'] = $timeSlot;
        return $this;
    }

    public function setDateOption($date)
    {
        $this->selectedOptions['date'] = $date;
        return $this;
    }

    public function getSelectedOptions()
    {
        return array_combine($this->optionsKeys, $this->selectedOptions);
    }

}