<?php

namespace App\Models\Services\HomeEyeCheckUp;

use App\Models\Order\NewOrder;

/**
 * Class Order
 * Home Eye Check Up service new order process.
 * Describes only additional setters and special options.
 * @package App\Models\Services\HomeEyeCheckUp
 */
class Order extends NewOrder {

    /**
     * Service special options. is_hto = 1 for Home Eye Check Up.
     * @var array
     */
    protected $specialOptions = [
        'is_hto' => 1
    ];

    /**
     * Landmark option setter.
     * @param $landmark
     * @return $this
     */
    public function setLandmark($landmark)
    {
        $this->options['landmark'] = $landmark;
        return $this;
    }

    /**
     * Floor option setter.
     * @param $floor
     * @return $this
     */
    public function setFloor($floor)
    {
        $this->options['floor'] = $floor;
        return $this;
    }

    /**
     * Lift Available option setter.
     * @param $liftAvailable
     * @return $this
     */
    public function setLiftAvailable($liftAvailable)
    {
        $this->options['lift_available'] = $liftAvailable;
        return $this;
    }

    /**
     * Address Type option setter.
     * @param $addressType
     * @return $this
     */
    public function setAddressType($addressType)
    {
        $this->options['addressType'] = $addressType;
        return $this;
    }

    /**
     * Cash on delivery payment method setter.
     * @return $this
     */
    public function setPaymentCod()
    {
        return $this->setPaymentMethod(self::P_CASH_ON_DELIVERY);
    }

}