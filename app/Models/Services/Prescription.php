<?php

namespace App\Models\Services;


use App\Exceptions\MagAPI\ValidationException;
use App\Models\Common\BaseModel;

class Prescription extends BaseModel{

    protected $options = [];

    public function setOption($productId, $optionId, $optionValue)
    {
        return $this->setValue("options[$productId][$optionId]", $optionValue);
    }

    public function setUsername($username)
    {
        return $this->setValue('username', $username);
    }

    public function setDob($dob)
    {
        return $this->setValue('dob', $dob);
    }

    public function setGender($gender)
    {
        return $this->setValue('gender', $gender);
    }

    public function setNotes($notes)
    {
        return $this->setValue('notes', $notes);
    }

    public function setValue($key, $value)
    {
        $this->options[$key] = $value;
        return $this;
    }

    public function submit()
    {
        try
        {
            $data = $this->api->push('submitprescription', [
                'request' =>  $this->options
            ]);
            $data = $data['result'];
            $this->options = [];
            return false;
        }
        catch (ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function uploadForEyeglasses($productId, $fileName, $mimeType)
    {
        $fileName = $this->uploadFile('uploadpresc?item_id=:id', $productId, $fileName, $mimeType);
        return $fileName;
    }

    public function uploadForContactLenses($productId, $fileName, $mimeType)
    {
        return $this->uploadFile('uploadprescContactlense?item_id=:id', $productId, $fileName, $mimeType);
    }

    protected function uploadFile($url, $productId, $fileName, $mimeType)
    {
        $data = $this->api->push($url, [
            'request' => [
                'myfile' => $this->api->makeFile($fileName, $mimeType)
            ],
            'url' => [
                'id' => $productId
            ]
        ]);
        return $data['result']['filename'];
    }
}