<?php

namespace App\Models\Services;


use App\Exceptions\MagAPI\ValidationException;
use App\Models\Address;
use App\Models\Common\BaseModel;
use App\Services\MagAPI;
use App\Models\Common\Factory;

class Pincode extends BaseModel {

    public $id;
    public $hto_available;
    public $tbyb_available;
    public $city;
    public $state;
    public $country;
    public $locality;

    protected $value;

    public function __construct($id, MagAPI $api, Factory $factory)
    {
        parent::__construct($api, $factory);
        $this->id = $id;
    }

    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    public function find()
    {
        try
        {
            $data = $this->api->fetch('checkpincode/:pincode', [
                'url' => [
                    'pincode' => $this->id
                ]
            ]);
            $data = $data['result'];
            $this->setAttributes($data);
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }
}