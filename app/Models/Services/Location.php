<?php

namespace App\Models\Services;

use App\Models\Common\BaseModel;

class Location extends BaseModel {

    public $code;
    public $name;

    protected function getAttributeAliases()
    {
        return [
            'name'  => 'country_name|city',
            'code'  => 'country_code'
        ];
    }
}