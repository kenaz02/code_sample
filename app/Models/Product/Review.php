<?php

namespace App\Models\Product;

use App\Models\Common\BaseModel;
use App\Exceptions\MagAPI\ValidationException;

/**
 * Class Review
 * Product reviews model
 * @package App\Models\Product
 */
class Review extends BaseModel{

    /**
     * Model property
     * @var
     */
    public $name;

    /**
     * Model property
     * @var
     */
    public $title;

    /**
     * Model property
     * @var
     */
    public $rating;

    /**
     * Model property
     * @var
     */
    public $description;

    /**
     * Model property
     * @var
     */
    public $date;

    /**
     * List of the properties for HTML tag escaping
     * @var array
     */
    private $escapedAttr = [
        'name',
        'title',
        'description'
    ];

    /**
     * Validation rules
     * @return array
     */
    public static function rules()
    {
        return [
            'name'        => 'required',
            'description' => 'required',
            'title'       => 'required',
            'rating'      => 'required|in:1,2,3,4,5'
        ];
    }

    /**
     * Validation messages
     * @return array
     */
    public static function messages()
    {
        return [
            'required'        => 'Fill this field',
            'rating.required' => 'Select rating'
        ];
    }

    /**
     * Convert/calculate rating in percentage format
     * @return float
     */
    public function getConvertedRating()
    {
        return $this->rating/5 * 100;
    }

    /**
     * Create new review item
     * @param $id
     * @return bool|string
     */
    public function create($id)
    {
        try
        {
            $this->escapeAttributes();
            $this->api->push('product/:id/reviews/create', [
                'url'     => [
                    'id' => $id
                ],
                'request' => $this->getAttributes()
            ]);
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    /**
     * Escape HTML tags from properties
     */
    private function escapeAttributes()
    {
        foreach($this->escapedAttr as $attr)
            $this->$attr = strip_tags($this->$attr);
    }

    /**
     * Generate new review date, use ISO 8601 2004-02-12T15:19:21+00:00 format
     */
    public function setNewItemDate()
    {
        $this->date = date('c');
    }
}