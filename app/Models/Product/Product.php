<?php

namespace App\Models\Product;

use App\Models\Common\BaseModel;
use Illuminate\Support\Facades\Redis;
use App\Models\Cart\Product\Contract as CartProductContract;

/**
 * Class Product
 * Base application product class
 * @package App\Models\Product
 */
class Product extends BaseModel implements CartProductContract {

    //Base properties
    public $id;
    public $type;
    public $model_name;
    public $brand_name;
    public $full_name;

    //Additional properties
    public $images;
    public $product_url;
    //TODO: Check property
    public $url;
    public $color;
    public $is_try_now_available;
    public $try_on_image;
    public $offer_image;
    public $headturn_male;
    public $headturn_female;
    public $tryOnline;
    public $specifications;
    public $quantity;

    //Technical specification properties
    public $technicalSpecs;
    public $generalSpecs;

    //Reviews properties
    public $avg_rating;
    public $total_no_of_ratings;
    public $number_of_reviews;
    public $reviews = [];

    //Description property
    public $description;

    //Related product list properties
    public $related_items = [];
    public $color_options = [];

    //Buy options properties
    public $buy_options;

    //FHT properties
    public $is_tbyb;
    public $is_in_fht = false;


    //Frame details properties
    public $frame_details;
    public $frame_size;
    public $frame_style;
    public $frame_color;
    public $frame_measure;
    public $frame_bridge_size;
    public $frame_eye_size;
    public $frame_temple_size;

    //Prices properties
    public $lenskartPrice;
    public $marketPrice;
    public $firstBuyPrice;


    const TYPE_EYE_GLASSES         = 'Eyeglasses';
    const TYPE_SUN_GLASSES         = 'Sunglasses';
    const TYPE_POWERED_SUN_GLASSES = 'Powered Sunglasses';
    const TYPE_CONTACT_LENS        = 'Contact Lens';

    protected function getAttributeAliases()
    {
        return [
            'images'        => 'image_url|image_urls|itemImage',
            'frame_size'    => 'size'
        ];
    }

    /**
     * Cart ProductContract contract interface implementation.
     * Provide interface to add the product to cart.
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    //TODO: Replace before production
    public function setProductUrl($productUrl)
    {
        $this->product_url = preg_replace(['(.*lenskart.com/)', '(\?.*)'], '', $productUrl);
        $redis = Redis::connection();
        $redis->set($this->product_url, 'prod:'.$this->id);
    }

    /**
     * Set product images as array
     * @param $images
     */
    public function setImages($images)
    {
        $this->images = is_array($images) ? $images : [$images];
    }

    //TODO: add first buy price
    public function getFirstBuyPrice()
    {
        return $this->marketPrice;
    }

}