<?php


namespace App\Models\Product\BuyOptions;


use App\Models\Common\Factory;
use App\Models\Product\Product;
use App\Models\Common\BaseModel;

class GlassesLenses extends BaseModel {

    public $extraId;

    protected $product;
    protected $eyeglassesOption;
    protected $factory;

    protected $options = [];
    protected $extraKey = 'Extra';

    protected $activeOption;

    public function __construct(EyeglassesOption $eyeglassesOption, Factory $factory, Product $product)
    {
        $this->eyeglassesOption = $eyeglassesOption;
        $eyeglassesOption->setProduct($product);

        $this->factory = $factory;
    }

    public function setAttributes($data, $attributes = false, $attrExcept = false)
    {
        foreach($data as $key => $item)
        {
            if($this->extraKey == $item['label'])
            {
                $this->extraId = $item['id'];
                unset($data[$key]);
                break;
            }
        }

        $this->options = $this->factory->makeCollection($this->eyeglassesOption, $data);
    }

    public function getActive($option)
    {
        foreach($this->options as $item)
        {
            if($item->label == $option)
                return $item;
        }
    }
}