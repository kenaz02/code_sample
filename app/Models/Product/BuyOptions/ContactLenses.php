<?php

namespace App\Models\Product\BuyOptions;


use App\Models\Common\BaseModel;

class ContactLenses extends BaseModel{

    public $extraId;

    public $left_boxes;
    public $right_boxes;
    public $left_power;
    public $right_power;

    protected $extraKey = 'extra';

    protected $propertiesIds = [
        'Left Boxes'        => 'left_boxes',
        'Right Boxes'       => 'right_boxes',
        'Left Power (SPH)'  => 'left_power',
        'Right Power (SPH)' => 'right_power',
    ];

    protected $contactLensesOption;

    public $leftOptions = [];
    public $rightOptions = [];

    const BOXES_OPTION = 'Boxes';
    const POWER_OPTION = 'Power (SPH)';

    const SIDE_LEFT = 'left';
    const SIDE_RIGHT = 'right';

    public function __construct(ContactLensesOption $contactLensesOption)
    {
        $this->contactLensesOption = $contactLensesOption;
    }

    public function setAttributes($data, $attributes = false, $attrExcept = false)
    {
        foreach($data as $key => $item)
        {
            if($this->extraKey == $item['label'])
            {
                $this->extraId = $item['id'];
                continue;
            }

            $option = clone $this->contactLensesOption;
            $option->setAttributes($item);

            if($option->side == self::SIDE_LEFT)
                $this->leftOptions[$option->label] = $option;
            elseif($option->side == self::SIDE_RIGHT)
                $this->rightOptions[$option->label] = $option;

//            if(array_key_exists($item['label'], $this->propertiesIds))
//            {
//                $property = $this->propertiesIds[$item['label']];
//                $this->$property = clone $this->contactLensesOption;
//                $this->$property->setAttributes($item);
//            }

        }
    }

    public function getLeftBoxes()
    {
        return array_key_exists(self::BOXES_OPTION, $this->leftOptions) ? $this->leftOptions[self::BOXES_OPTION] : [];
    }

    public function getRightBoxes()
    {
        return array_key_exists(self::BOXES_OPTION, $this->rightOptions) ? $this->rightOptions[self::BOXES_OPTION] : [];
    }
}
