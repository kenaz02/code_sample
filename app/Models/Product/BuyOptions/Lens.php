<?php
namespace App\Models\Product\BuyOptions;


use App\Models\Common\BaseModel;
use App\Models\Product\Product;
use App\Models\Common\Factory;

class Lens extends BaseModel{

    public $id;
    public $brand_image_url;
    public $warranty;
    public $model_name;
    public $prices;
    public $specifications;
    public $default_selection;
    public $totalPrice;

    public $marketPrice;
    public $lenskartPrice;


    protected $cart;
    protected $product;

    public function __construct(Factory $factory)
    {
        $this->cart = app('CartResolver')->fetch();

        $this->factory = $factory;
    }

    public function setProduct(Product $product)
    {
        $this->product = $product;
    }

    public function setPrices($prices)
    {
        list($this->marketPrice, $this->lenskartPrice) = $this->factory->makeCollection('Price', $prices);

        $this->totalPrice = $this->factory->makeModel('Price', [
            'name'          => 'Total Price',
            'currency_code' => '',
            'price' => (int)$this->product->lenskartPrice->price + (int)$this->lenskartPrice->price - (int)$this->cart->discount->price + (int)$this->cart->discount->price
        ]);
    }
}