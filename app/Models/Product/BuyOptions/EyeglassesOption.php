<?php


namespace App\Models\Product\BuyOptions;

use App\Models\Common\BaseModel;
use App\Models\Common\Factory;
use App\Models\Product\Product;

class EyeglassesOption extends BaseModel{

    public $id;
    public $label;
    public $extra_key;
    public $lenses;

    protected $lens;
    protected $has_default_selection;

    const BIOFOCAL      = 'Bifocal/Progressive';
    const ZERO_POWER    = 'Zero Power';
    const SINGLE_VISION = 'Single Vision';
    const FRAME_ONLY    = 'Frame Only';
    const WITHOUT_POWER = 'Without Power';
    const WITH_POWER    = 'With Power';

    public function __construct(Lens $lens, Factory $factory)
    {
        $this->lens = $lens;
        $this->factory = $factory;
    }

    public function setProduct(Product $product)
    {
        $this->lens->setProduct($product);
    }

    public function setLenses($lenses)
    {
        $defaultSelection = false;
        $this->lenses = $this->factory->makeCollection($this->lens, $lenses, function(Lens $lens, $data) use (&$defaultSelection) {
            $lens->setAttributes($data);
            if($lens->default_selection == true)
                $defaultSelection = true;
        });
        $this->has_default_selection = $defaultSelection;
    }
    public function hasDefaultPackage()
    {
        return $this->has_default_selection;
    }

}