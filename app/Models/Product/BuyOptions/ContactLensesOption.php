<?php

namespace App\Models\Product\BuyOptions;

use App\Models\Common\BaseModel;

class ContactLensesOption extends BaseModel{

    public $id;
    public $label;
    public $values;

    public $side;

    public function setValues($options)
    {
        $this->values = [];
        foreach($options as $item)
        {
            $this->values[$item['id']] = $item['model_name'];
        }
    }

    public function setLabel($label)
    {
        $this->label = $label;

        $this->makeSideOptions($label, ContactLenses::SIDE_LEFT,
            $this->makeSideOptions($label, ContactLenses::SIDE_RIGHT)
        );
    }

    protected function makeSideOptions($name, $side, $nextSide = true)
    {
        if($nextSide && stripos($name, $side) !== false)
        {
            $this->side = $side;
            $this->label = trim(str_ireplace($side, '', $name));
            return false;
        }

        return true;
    }

    public function isPowerOption()
    {
        return $this->label == ContactLenses::POWER_OPTION;
    }

    public function isBoxesOption()
    {
        return $this->label == ContactLenses::BOXES_OPTION;
    }
}