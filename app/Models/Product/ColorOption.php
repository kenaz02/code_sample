<?php

namespace App\Models\Product;

/**
 * Class ColorOption
 * Class for product color options
 * @package App\Models\Product
 */
class ColorOption extends Product {

    //Colors properties
    public $firstColor;
    public $secondColor;

    /**
     * Set color properties
     * @param $data
     */
    public function setColor($data)
    {
        $colorData = explode(',', $data);
        $this->firstColor = $colorData[0];
        $this->secondColor = count($colorData) == 1 ? $this->firstColor : $colorData[1];
    }
}