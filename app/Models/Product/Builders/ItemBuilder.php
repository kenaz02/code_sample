<?php

namespace App\Models\Product\Builders;

use App\Models\Product\Product;

/**
 * Class ItemBuilder
 * Product builder for single product item. Uses for product view, reviews and buy options pages.
 * @package App\Models\Product\Builders
 */
class ItemBuilder extends ListBuilder {

    /**
     * Set base product properties
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     */
    public function makeBase()
    {
        $this->product->setAttributes($this->data, ['id', 'type', 'model_name', 'brand_name', 'full_name']);

        $this->forgetData('type');
        if($this->product->type == Product::TYPE_SUN_GLASSES && count($this->getData('buy_options', false)) > 1)
            $this->product->type = Product::TYPE_POWERED_SUN_GLASSES;

        $this->product->setProductUrl($this->getData('url'));
        return $this;
    }

    /**
     * Make product specifications
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     */
    public function makeSpecifications()
    {
        $specs = $this->getData('specifications');
        $this->product->specifications = $specs;
        $this->product->technicalSpecs = $specs[0]['items'];
        $this->product->generalSpecs = $specs[1]['items'];

        return $this;
    }

    /**
     * Make frame details
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     */
    public function makeFrameDetails()
    {
        $framePropertiesAliases = [
            'Size'  => 'frame_size',
            'Style' => 'frame_style',
            'Color' => 'frame_color',
            'MEASURE' => 'frame_measure'
        ];

        $frameDetails = $this->getData('frame_details');

        $this->product->frame_details = $frameDetails;
        foreach($frameDetails as $item)
        {
            if(array_key_exists($item['name'], $framePropertiesAliases))
            {
                $property = $framePropertiesAliases[$item['name']];
                $this->product->$property = $item['value'];
            }
        }

        if($this->product->frame_measure !== null)
        {
            $sizes = explode('-', $this->product->frame_measure);
            $this->product->frame_eye_size    = $sizes[0];
            $this->product->frame_bridge_size = $sizes[1];
            $this->product->frame_temple_size = $sizes[2];
        }

        return $this;
    }

    /**
     * Set related products
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     * @throws \ErrorException
     */
    public function makeRelatedItems()
    {
        $this->product->related_items = $this->factory->makeBuilderCollection('Product\Builders\ListBuilder', $this->getData('related_items'));
        return $this;
    }

    /**
     * Set color options products
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     * @throws \ErrorException
     */
    public function makeColorOptions()
    {
        $prototype = $this->factory->makeInstance('Product\ColorOption');

        $builder = new ListBuilder($prototype, $this->factory, app('CartState'));
        $this->product->color_options = $this->factory->makeBuilderCollection($builder, $this->getData('color_options'));

        return $this;
    }

    /**
     * Set product reviews properties and load reviews list
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     */
    public function makeReviews()
    {

        $this->product->total_no_of_ratings = $this->getData('total_no_of_ratings');
        $this->product->avg_rating = $this->getData('avg_rating');

        $numberOfReviews = $this->getData('number_of_reviews');
        $this->product->number_of_reviews = !empty($numberOfReviews) ? $numberOfReviews : 0;

        if($this->product->number_of_reviews > 0)
            $this->product->reviews = app('App\Models\Repositories\ReviewRepository')->getList($this->product->id);

        return $this;
    }

    /**
     * Load and set product description
     * @return $this
     */
    public function makeDescription()
    {
        $this->product->description = app('App\Services\ThirdPartyAPI')->cms('description', ['id' => $this->product->id]);
        return $this;
    }

    /**
     * Make product buy options
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     */
    public function makeBuyOptions()
    {
        $buyOptions = $this->getData('buy_options');

        //TODO: Make via resolver;
        if($this->product->type == Product::TYPE_EYE_GLASSES ||  $this->product->type == Product::TYPE_POWERED_SUN_GLASSES)
        {
            $this->product->buy_options = $this->factory->makeModel(['Product\BuyOptions\GlassesLenses', $this->product], $buyOptions);
        }
        elseif($this->product->type == Product::TYPE_CONTACT_LENS)
        {
            $this->product->buy_options = $this->factory->makeModel('Product\BuyOptions\ContactLenses', $buyOptions);
        }

        return $this;
    }

    /**
     * Base build method. Build full product product (for view page).
     * @return $this
     */
    public function build()
    {
        $this->makeBase()
             ->makeRelatedItems()
             ->makeColorOptions()
             ->makeSpecifications()
             ->makeFrameDetails()
             ->makePrices()
             ->makeReviews()
             ->makeAttributes()
             ->makeDescription();

        return $this;
    }

    /**
     * Build product with reviews items (for reviews page).
     * @return $this
     */
    public function buildReviews()
    {
        $this->makeBase()
             ->makeReviews();

        return $this;
    }

    /**
     * Build product with buy options (for buy options page).
     * @return $this
     */
    public function buildBuyOptions()
    {
        $this->makeBase()
             ->makePrices()
             ->makeBuyOptions();

        return $this;
    }
}