<?php

namespace App\Models\Product\Builders;

use App\Models\Product\Product;
use App\Models\Common\Factory;
use App\Models\Cart\State;

/**
 * Class ListBuilder
 * Product builder for product list pages
 * @package App\Models\Product\Builders
 */
class ListBuilder extends AbstractBuilder {

    protected $cart;

    public function __construct(Product $product, Factory $factory, State $cartState)
    {
        $this->cart = $cartState;
        parent::__construct($product, $factory);
    }

    /**
     * Check if product in FHT cart
     * @return $this
     */
    public function isInFHTCart()
    {
        if($this->product->is_tbyb)
                $this->product->is_in_fht = $this->cart->checkProduct($this->product);

        return $this;
    }

    /**
     * Build product
     * @return $this
     */
    public function build()
    {
         $this->makePrices()
              ->makeAttributes()
              ->isInFHTCart();

         return $this;
    }
}