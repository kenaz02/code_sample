<?php

namespace App\Models\Product\Builders;

use App\Models\Common\BaseBuilder;
use App\Models\Common\Factory;
use App\Models\Product\Product;

/**
 * Class AbstractBuilder
 * Base product builder class
 * @package App\Models\Product\Builders
 */
abstract class AbstractBuilder extends BaseBuilder {

    /**
     * Product instance
     * @var \App\Models\Product\Product
     */
    protected $product;

    public function __construct(Product $product, Factory $factory)
    {
        $this->product = $product;
        parent::__construct($factory);
    }

    /**
     * Base builder abstract method implementation
     * @return Product
     */
    public function getModel()
    {
        return $this->getProduct();
    }

    /**
     * Product instance getter
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set simple product attributes
     * @return $this
     */
    public function makeAttributes()
    {
        $this->product->setAttributes($this->data);
        return $this;
    }

    /**
     * Base builder abstract method implementation
     */
    public function __clone()
    {
        $this->product = clone $this->product;
    }

    /**
     * Make product prices
     * @return $this
     * @throws \App\Exceptions\MagAPI\UnexpectedDataFormatException
     * @throws \ErrorException
     */
    public function makePrices()
    {
        list($this->product->marketPrice, $this->product->lenskartPrice) = $this->factory->makeCollection('Price', $this->getData('prices'));
        return $this;
    }
}