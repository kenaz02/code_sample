<?php
namespace App\Models\Forms;

use App\Models\Services\Pincode;
use App\Models\Address;
use App\Models\Services\HomeEyeCheckUp\Order as NewOrderHTO;
use App\Models\Services\HomeEyeCheckUp\HomeEyeCheckUp as HomeEyeCheckUpModel;

class HomeEyeCheckUp extends BaseCheckout {

    public $date;
    public $timeSlot;
    public $addressType;
    public $liftAvailable;
    public $landmark;
    public $floor;

    public $cityId;
    public $timeSlotLabel;

    protected $order;
    protected $cartResolver;
    protected $hto;

    public function __construct(Address $address, NewOrderHTO $order)
    {
        $this->address = $address;
        $this->order = $order;
    }

    const SESSION_STORAGE_KEY = 'service:home_eye_check_up';

    public static function phoneStageRules()
    {
        return self::makeRules([
            'telephone',
            'pincode',
            'stage'
        ]);
    }

    public static function phoneStageMessages()
    {
        return [];
    }

    public static function dateStageRules()
    {
        return self::makeRules([
            'stage',
            'date'     => 'required|date_format:"d/m/Y"',
            'timeSlot' => 'required'
        ]);
    }

    public static function addressStageRules()
    {
        return self::makeRules([
            'stage',
            'email',
            'firstname',
            'lastname',
            'street0',
            'addressType'   => 'required',
            'liftAvailable' => 'required',
            'landmark'      => 'max:100|regex:/^[\pL0-9,\'\s]+$/u'
        ]);
    }

    public static function addressStageMessages()
    {
        return [];
    }

    public function setHTO(HomeEyeCheckUpModel $model)
    {
        $this->hto = $model;
        return $this;
    }

    public function setTimeSlot($timeSlotData)
    {
        list($this->timeSlot, $this->timeSlotLabel) = explode('%%', $timeSlotData);
    }

    public function checkPincode($code)
    {
        return $this->checkPincodeBase($code, function(Pincode $pincode, $modelInstance){
            if(!$pincode->hto_available)
                return trans('validation.pincode_hto');

            $city = $modelInstance->getHTO()->findCityByName($pincode->city);

            if($city === false)
                return trans('validation.pincode_hto');

            $this->cityId = $city->id;
        });
    }

    public function submitOrder($codPayment = false)
    {
        if($codPayment)
            $this->order->setPaymentCod();

        $orderData = $this->order
            ->setAddress($this->address)
            ->setEmail($this->email)
            ->setAddressType($this->addressType)
            ->setLandmark($this->landmark)
            ->setLiftAvailable($this->liftAvailable)
            ->setFloor($this->floor)
            ->make();

        return $orderData;
    }

    public function addToCart()
    {
        $this->getHTO()->setCityOption($this->cityId)
                       ->setTimeSlotOption($this->timeSlot)
                       ->setDateOption($this->date);

        return app('CartResolver')->makeHTO()->add($this->getHTO());
    }

    public function getHto()
    {
        if($this->hto instanceof HomeEyeCheckUpModel)
            return $this->hto;
        else
            throw new \ErrorException('HTO property must be an instance of HomeEyeCheckUp model');
    }

    public function isWeekDay()
    {
        if(!$this->date)
            return false;

        $day = date_create_from_format('d/m/Y', $this->date)->format('N');
        return !($day == 0 || $day == 6);
    }
}