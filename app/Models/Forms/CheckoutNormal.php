<?php

namespace App\Models\Forms;

use App\Models\Address;
use App\Models\Order\NewOrder;
use App\Models\Order\Result;
use App\Models\User;
use App\Services\Payment;

class CheckoutNormal extends BaseCheckout {

    protected $order;

    protected $prescriptionItems;

    protected $newOrderResult;

    public $ammount;

    const SESSION_STORAGE_KEY = 'checkout:normal';

    public function __construct(Address $address, NewOrder $order)
    {
        $this->address = $address;
        $this->order = $order;
    }

    public static function signInStageGuestRules()
    {
        return self::makeRules([
            'email'
        ]);
    }

    public static function signInStageUserRules()
    {
        return User::loginRules();
    }

    public static function shippingGuestRules()
    {
        return self::makeRules([
            'telephone',
            'firstname',
            'lastname',
            'pincode',
            'street0',
            'city',
            'alt_telephone'
        ]);
    }

    public static function shippingAddressRules()
    {
        return self::makeRules([
            'stage',
            'checked_address' => 'required|numeric'
        ]);
    }

    public static function paymentCodRules()
    {
        return self::makeRules([
            'captcha' => 'required'
        ]);
    }

    public static function paymentStoreCreditRules()
    {
        return self::makeRules([
            'storeCreditAmount' => 'required',
            'storeCreditCode'   => 'required'
        ]);
    }

    public static function paymentGiftVoucher()
    {
        return self::makeRules([
            'giftVoucher' => 'required'
        ]);
    }

    public static function prescriptionRules()
    {
        return self::makeRules([
            'username' => 'required',
            'notes'    => '',
        ]);
    }
    public function setPaymentMethod($paymentMethod)
    {
        $this->order->setPaymentMethod($paymentMethod);
    }

    public function checkPincode($code)
    {
        return $this->checkPincodeBase($code);
    }

    public function setAddress(Address $address)
    {
        $this->address = $address;
    }

    public function submitOrder($paymentMethod)
    {
        $this->newOrderResult = $this->order
            ->setAddress($this->address)
            ->setEmail($this->email)
            ->setPaymentMethod($paymentMethod)
            ->make();

        return $this->newOrderResult;
    }

    public function getPrescriptionItems()
    {
        return $this->prescriptionItems;
    }

    public function getNewOrderResult()
    {
        return $this->newOrderResult;
    }
}