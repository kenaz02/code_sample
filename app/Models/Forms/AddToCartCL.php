<?php


namespace App\Models\Forms;

use App\Models\Common\BaseModel;


class AddToCartCL extends BaseModel {

    public $stage;

    public $username;
    public $dob;
    public $gender;
    public $notes;

    public $prescriptionFile;
    public $prescriptionLocalFile;

    const SESSION_STORAGE_KEY = 'product:buy_options:cl';


    /**
     * Validation rules
     * @return array
     */
    public static function stageRules()
    {
        return [
            'username'          => 'required|regex:/^[a-zA-Z\s]*$/',
            'dob'               => 'date_format:"d/m/Y"',
            'notes'             => 'max:250'
        ];
    }

    /**
     * Validation messages
     * @return array
     */
    public static function stageMessages()
    {
        return [
            'username.required' => 'Please Provide Patient Name',
            'dob.next_stage'    => 'Please Select Options Type',
            'dob.date_format'   => 'The Date of Birth is not valid'
        ];
    }


    public static function submitRules()
    {
        return [
            'side_opts'  => 'required'
        ] + self::stageRules();
    }

    public static function submitMessages()
    {
        return [
            'side_opts.required'  => 'Please provide boxes data'
        ] + self::stageMessages();
    }

    public static function prescriptionFileRules()
    {
        return [
            'prescription_file' => 'required|mimes:jpeg,bmp,png|max:10240'
        ];
    }

    public static function prescriptionFileMessages()
    {
        return [
            'prescription_file.required' => 'Please Provide Prescription Image',
            'prescription_file.mimes'    => 'The Prescription Image Must Be In Right Image Format',
            'prescription_file.max'      => 'The Prescription Image Must Be Less Than 10Mb'
        ];
    }
}
