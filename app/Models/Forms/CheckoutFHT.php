<?php

namespace App\Models\Forms;

use App\Models\Address;
use App\Models\Order;
use App\Models\Order\NewOrderFHT;
use App\Models\Services\Pincode;

/**
 * Class CheckoutFHT
 * Process Free Home Trial checkout
 * @package App\Models\Forms
 */
class CheckoutFHT extends BaseCheckout {

    /**
     * New Order instance
     * @var \App\Models\Order\NewOrderFHT
     */
    protected $order;

    /**
     * Alternate telephone option
     * @var
     */
    public $telephoneAlt;

    /**
     * Key for session form storage
     */
    const SESSION_STORAGE_KEY = 'checkout:fht';

    /**
     * Inject dependencies.
     * @param Address $address
     * @param NewOrderFHT $order
     */
    public function __construct(Address $address, NewOrderFHT $order)
    {
        $this->address = $address;
        $this->order = $order;
    }

    /**
     * Phone stage rules.
     * @return array
     */
    public static function phoneStageRules()
    {
        return self::makeRules([
            'telephone',
            'pincode',
            'stage'
        ]);
    }

    /**
     * Address stage rules.
     * @return array
     */
    public static function addressStageRules()
    {
        $rules = self::makeRules([
            'stage',
            'email',
            'telephone',
            'firstname',
            'lastname',
            'street0',
            'locality',
            'landmark'
        ]);
        $rules['telephoneAlt'] = $rules['telephone'];
        return $rules;
    }

    /**
     * Confirm stage rules.
     * @return array
     */
    public static function confirmStageRules()
    {
        return [
            'otp' => 'required|numeric'
        ];
    }

    /**
     * Submit new Free Home Trial order.
     * @return bool|string
     */
    public function submitOrder()
    {
        $errors = $this->order
            ->setAddress($this->address)
            ->setEmail($this->email)
            ->setLandmark($this->landmark)
            ->make();
        return $errors;
    }

    /**
     * Check pincode with custom Free Home Trial conditions.
     * @param $code
     * @return bool
     */
    public function checkPincode($code)
    {
        return $this->checkPincodeBase($code, function(Pincode $pincode, $modelInstance) {
            if(!$pincode->tbyb_available)
                return trans('validation.pincode_fht');
        });
    }

}