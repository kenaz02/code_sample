<?php

namespace App\Models\Forms;

use App\Models\Common\BaseModel;
use App\Models\Address;

/**
 * Class BaseCheckout
 * Process data storage, validation, new order submit for checkout features (complex forms).
 * Base class collect common methods and validation rules.
 * @package App\Models\Forms
 */
class BaseCheckout extends BaseModel {

    /**
     * Form/command stage
     * @var
     */
    public $stage;

    /**
     * Address model instance
     * @var \App\Models\Address
     */
    public $address;

    /**
     * Email option
     * @var
     */
    public $email;

    /**
     * List of localities provided for concrete pincode
     * @var
     */
    public $localityList;

    /**
     * Landmark option
     * @var
     */
    public $landmark;

    /**
     * Common/base validation rules
     * @var array
     */
    protected static $baseRules = [
        'email'     => 'required|email',
        'stage'     => 'required',
        'landmark'  => 'max:100|regex:/^[\pL0-9,\'\s]+$/u'
    ];

    /**
     * Inject class dependencies.
     * @param Address $address
     */
    public function __construct(Address $address)
    {
        $this->address = $address;
    }

    /**
     * Set magic method.
     * Set address model property via current form model interface.
     * @param $name
     * @param $arguments
     */
    public function __set($name, $arguments)
    {
        //TODO: Add exception
        if(property_exists($this->address, $name))
            $this->address->$name = $arguments;
    }

    /**
     * Get magic method.
     * Get address model property via current form model interface.
     * @param $name
     * @return mixed
     */
    public function __get($name)
    {
        //TODO: Add exception
        if(property_exists($this->address, $name))
            return $this->address->$name;
    }

    /**
     * Set form and address attributes.
     * @param $data
     * @param bool|false $attributes
     * @param bool|false $attrExcept
     */
    public function setAttributes($data,  $attributes = false, $attrExcept = false)
    {
        parent::setAttributes($data);
        $this->address->setAttributes($data);
    }

    /**
     * Get form and address attributes.
     * @param bool|false $attributes
     * @param bool|false $attrExcept
     * @return array
     */
    public function getAttributes($attributes = false, $attrExcept = false)
    {
        return array_merge(
            $this->getAttributes(),
            $this->address->getAttributes()
        );
    }

    /**
     * Make validation rules.
     * Merge address model, base and custom rules.
     * Address and base rules passed without params.
     * @param $rules
     * @return array
     */
    protected static function makeRules($rules)
    {
        $filteredRules = [];
        $baseRules = Address::rules() + self::$baseRules;
        foreach($rules as $key => $item)
        {
            if(is_numeric($key) && array_key_exists($item, $baseRules))
                $filteredRules[$item] = $baseRules[$item];
            else
                $filteredRules[$key] = $item;
        }
        return $filteredRules;
    }

    /**
     * Check pincode.
     * Use \App\Models\Services\Pincode service.
     * Call callback to process custom conditions.
     * Return message on error or false on success.
     * @param $code
     * @param \Closure|null $callback
     * @return bool
     */
    protected function checkPincodeBase($code, \Closure $callback = null)
    {
        $pincode = app('\App\Models\Services\Pincode', [$code]);
        $error = $pincode->find();

        if($error === false)
        {
            if($callback !== null && ($callbackResponse = $callback($pincode, $this)) !== null)
                return $callbackResponse;

            $this->address->setAttributesFromPincode($pincode);
            $this->localityList = $pincode->locality;
            return false;
        }
        else
        {
            return $error;
        }
    }

}