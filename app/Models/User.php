<?php

namespace App\Models;

use App\Services\MagAPI;
use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use App\Exceptions\MagAPI\ValidationException;
use App\Models\Common\BaseModel;
use Illuminate\Support\Facades\Redis;

use Illuminate\Auth\Authenticatable;

class User extends BaseModel implements UserContract{

    use Authenticatable;

    public $id;
    public $email;
    public $firstname;
    public $lastname;
    public $dob;
    public $gender;
    public $sessiontoken;
    public $referrer_link_url;
    public $reward_points;
    public $password;
    public $gift_voucher_balance;
    public $store_credit;
    public $default_address;

    protected $storageKey = 'user';

    public static function rules()
    {
        return [
            'firstname'     => 'required|max:25|regex:/^[\pL,\'\s]+$/u',
            'lastname'      => 'required|max:25|regex:/^[\pL,\'\s]+$/u',
            'dob'       => 'date_format:d/m/Y',
            'gender'    => 'required|in:male,female',
            'password'  => 'required|min:6',
            'email'     => 'required|email'
        ];
    }

    public static function loginRules()
    {
        return array_only(self::rules(), [
            'password',
            'email'
        ]);
    }

    public static function registerRules()
    {
        $rules = array_only(self::rules(), [
            'firstname',
            'lastname',
            'dob',
            'gender',
            'email',
            'password'
        ]);
        $rules['terms_agreement'] = 'required';
        $rules['password'] .= '|confirmed';
        return $rules;
    }

    public static function forgotPasswordRules()
    {
        return array_only(self::rules(), [
            'email'
        ]);
    }

    public function login($email, $password)
    {
        return $this->makeLogin([
            'email'      => $email,
            'password'   => $password
        ]);
    }

    public function loginSocial($token, $email)
    {
        return $this->makeLogin([
            'service'      => 'facebook',
            'auth_token'   => $token,
            'email'        => $email
        ]);
    }

    protected function makeLogin($request)
    {
        try
        {
            $responseData = $this->api->push('login', ['request' => $request]);
            $this->setAttributes($responseData['result']);
            $this->makeSession();
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    protected function makeSession()
    {
        $this->api->setToken($this->sessiontoken);
        $this->store();
    }

    public function register()
    {
        try
        {
            $responseData = $this->api->push('register', ['request' => [
                'firstname' => $this->firstname,
                'lastname'  => $this->lastname,
                'dob'       => $this->dob,
                'gender'    => $this->gender,
                'email'     => $this->email,
                'password'  => $this->password
            ]]);
            $this->setAttributes($responseData['result']);
            $this->makeSession();
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function forgotPassword()
    {
        try
        {
            $responseData = $this->api->push('forgot_password', ['request' => [
                'email' => $this->email
            ]]);
            $this->setAttributes($responseData['result']);
            return false;
        }
        catch(ValidationException $e)
        {
            return $e->getMessage();
        }
    }

    public function logout()
    {
        $this->api->push('logout', []);
        $this->forget();
    }

    public function getKey()
    {
        return $this->sessiontoken;
    }


    public function store()
    {
        $instance = clone $this;
        $instance->password = null;
        $this->getStorageConnection()->set($this->getStorageKey($this->sessiontoken), serialize($instance));
    }

    public function forget()
    {
        $this->getStorageConnection()->del($this->getStorageKey($this->sessiontoken));
    }

    public function retrieve($key)
    {
        return unserialize($this->getStorageConnection()->get($this->getStorageKey($key)));
    }

    protected function getStorageKey($sessionKey)
    {
        return $this->storageKey.':'.$sessionKey;
    }

    protected function getStorageConnection()
    {
        return Redis::connection();
    }
}