<?php namespace App\Handlers\Events;

use App\Events\CartCombineConflict;

use App\Models\Cart\CartResolver;
use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Support\Facades\Session;

class CartCombineConflictAction {

	/**
	 * Handle the event.
	 *
	 * @param  CartCombineConflict  $event
	 * @return void
	 */
	public function handle(CartCombineConflict $event)
	{
        $cart = $event->getCart();
        $values = $cart->getValues();
        $values['cart_type'] = $cart->type;
        $values['product_id'] =  $event->getProductId();
        $values['product_name'] = $event->getProductName();

        Session::flash(CartResolver::TMP_CART_CONFLICT, $values);

        if($event->getRedirect())
            throw new HttpResponseException(redirect()->route('cartConflict'));
	}

}
