<?php namespace App\Handlers\Events;

use App\Events\CartStageUpdated;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldBeQueued;

class CartStageConfirmation {

	/**
	 * Create the event handler.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Handle the event.
	 *
	 * @param  CartStageUpdated  $event
	 * @return void
	 */
	public function handle(CartStageUpdated $event)
	{
		dd($event);
	}

}
