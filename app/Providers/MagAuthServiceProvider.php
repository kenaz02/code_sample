<?php

namespace App\Providers;


use Illuminate\Contracts\Auth\Authenticatable as UserContract;
use Illuminate\Contracts\Auth\UserProvider;

/**
 * Class MagAuthServiceProvider
 * Auth provider for Magento API
 * @package App\Providers
 */
class MagAuthServiceProvider implements UserProvider {

    protected $model;

    public function __construct(UserContract $model)
    {
        $this->model = $model;
    }

    public function retrieveById($identifier)
    {
        return $this->model->retrieve($identifier);
    }

    public function retrieveByToken($identifier, $token)
    {

    }

    public function updateRememberToken(UserContract $user, $token)
    {

    }

    public function retrieveByCredentials(array $credentials)
    {
        return $this->model;
    }

    public function validateCredentials(UserContract $user, array $credentials)
    {
       return true;
    }

}

