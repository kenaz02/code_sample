<?php namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider {

    //protected $defer = true;

	/**
	 * Bootstrap any application services.
	 *
	 * @return void
	 */
	public function boot()
	{
        $that = $this;
        $this->app['auth']->extend('custom',function() use ($that)
        {
            return new MagAuthServiceProvider($that->app['App\Models\User']);
        });

        \View::composer('app', function($view) use ($that) {
            $view->with('cartState', $that->app['App\Models\Cart\State']);
            $view->with('client',    $that->app['App\Services\Client']);
        });
	}

	/**
	 * Register any application services.
	 *
	 * This service provider is a great spot to register your various container
	 * bindings with the application. As you can see, we are registering our
	 * "Registrar" implementation here. You can add your own bindings too!
	 *
	 * @return void
	 */
	public function register()
	{
		$this->app->bind(
			'Illuminate\Contracts\Auth\Registrar',
			'App\Services\Registrar'
		);

        $this->app->singleton('App\Models\Cart\State');
        $this->app->bind('CartState', 'App\Models\Cart\State');

        $this->app->singleton('App\Models\Cart\CartResolver');
        $this->app->bind('CartResolver', 'App\Models\Cart\CartResolver');


        $this->app->singleton('App\Models\Common\Factory');
        $this->app->singleton('App\Models\Cart\Product\Resolver');


        $this->app->singleton('App\Services\Client');
        $this->app->singleton('App\Services\ThirdPartyAPI');
        $this->app->singleton('App\Services\MagAPI');
        $this->app->singleton('App\Services\Payment');
	}

    public function providers()
    {
        return [
            'App\Models\Cart\CartResolver'
        ];
    }
}
