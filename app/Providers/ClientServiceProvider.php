<?php

namespace App\Providers;

use App\Services\Client;
use Illuminate\Support\ServiceProvider;

class ClientServiceProvider extends ServiceProvider {

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\Services\Client', function($app)
        {
            return new Client();
        });
    }


}