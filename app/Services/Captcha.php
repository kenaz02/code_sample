<?php

namespace App\Services;


/**
 * Class Captcha
 * Generate and handle captcha check
 * @package App\Services
 */
class Captcha {

    /**
     * Image property
     * @var
     */
    protected $image;

    /**
     * Answer propery
     * @var
     */
    protected $answer;

    /**
     * Magento API instance
     * @var \App\Services\MagAPI
     */
    protected $api;

    const STORAGE_KEY = 'captcha:answer';

    public function __construct(MagAPI $api)
    {
        $this->api = $api;
    }

    /**
     * Load captcha data (image and answer) and save in session storage
     * @return $this
     * @throws \ErrorException
     */
    public function make()
    {
        $data = $this->api->fetch('getcaptcha');
        $data = $data['result'];

        if(isset($data['url']) && isset($data['answer']))
        {
            $this->answer = $data['answer'];
            $this->image = $data['url'];
            \Session::put(self::STORAGE_KEY, $this->answer);
        }
        else
            throw new \ErrorException('Wrong data format for captcha service');


        return $this;
    }

    /**
     * Get captcha image
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Check captcha answer
     * @param $answer
     * @return bool
     */
    public function checkAnswer($answer)
    {
        $answer = preg_replace('/\s+/', '', $answer);
        return !is_null($answer) && \Session::pull(self::STORAGE_KEY) == $answer;
    }
}