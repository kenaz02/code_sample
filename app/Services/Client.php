<?php

namespace App\Services;

use Browser\Browser;
use Browser\Os;

/**
 * Class Client.
 * Detect client OS and browser.
 * Use gabrielbull/browser extension.
 * @package App\Services
 */
class Client {

    /**
     * Client browser property
     * @var browser
     */
    protected $browser;

    /**
     * Client OS property
     * @var os
     */
    protected $os;

    /**
     * Old devices clients list
     * @var array
     */
    protected $oldClients = [
        Browser::OPERA_MINI,
        Browser::NOKIA,
        Browser::NOKIA_S60,
        Browser::BLACKBERRY
    ];

    public function __construct()
    {
        $this->browser = new Browser();
        $this->os = new Os();
    }

    /**
     * Check Apple devices
     * @return bool
     */
    public function isIOs()
    {
        return $this->os->getName() == Os::IOS;
    }

    /**
     * Check Android devices
     * @return bool
     */
    public function isAndroid()
    {
        return $this->os->getName() == Os::ANDROID;
    }

    /**
     * Check old devices (like Nokia S40/60 and etc.)
     * @return bool
     */
    public function isOldBrowser()
    {
        return in_array($this->browser->getName(), $this->oldClients);
    }

}