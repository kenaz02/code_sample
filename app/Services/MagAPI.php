<?php

namespace App\Services;

use App\Exceptions\MagAPI\ValidationException;
use \Curl\Curl;
use \CURLFile;
use Illuminate\Support\Facades\Session;

/**
 * Class MagAPI
 * Handle and implement all Lenskart Magento API. Use CURL class to handle API connections.
 * All config stored in app/config/api.php.
 * @package App\Services
 */
class MagAPI {

    /**
     * API key. Config value
     * @var
     */
    private $key;

    /**
     * API root url. Config value
     * @var
     */
    private $url;

    /**
     * API device id. Config value
     * @var
     */
    private $deviceId;

    /**
     * API session token for each user. Stored in local user session
     * @var
     */
    private $sessionToken;

    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';

    /**
     * API session value key for local storage
     */
    const SESSION_STORAGE_KEY = 'api_session_id';

    /**
     * Constructor. Set config and initialize session
     */
    public function __construct()
    {
        $this->key = \Config::get('api.key');
        $this->url = \Config::get('api.url');
        $this->deviceId = \Config::get('api.deviceId');

        $this->initSession();
    }

    /**
     * Get data from API. Send GET method HTTP request
     *
     * @param $url - service URL address without API root part
     * @param array $data - request data. See request method description
     * @return mixed|null - response data
     */
    public function fetch($url, $data = [])
    {
        return $this->request(self::HTTP_GET, $url, $data);
    }

    /**
     * Send data to API. Send POST method HTTP request
     *
     * @param $url - service URL address without API root part.
     * @param $data - request data. See request method description.
     * @return mixed|null - response data.
     * @throws ValidationException
     */
    public function push($url, $data)
    {
        return $this->request(self::HTTP_POST, $url, $data);
    }


    /**
     * Base API method, implement request to API
     *
     * @param $httpMethod - HTTP request method
     * @param $url - service URL;
     * @param array $data - request data, format is:
     *      [
     *          'url' => [
     *              'ulr_param' => 'param_value'
     *           ],
     *          'request' => [
     *              'req_param' => 'param_value'
     *          ]
     *      ]
     *
     *      ulr - data passed to the service URL
     *      request - data passed with request
     * @param bool $parse - parse response data to $array variable
     * @return mixed|null - response data.
     * @throws ValidationException - handle validation error 422 response
     */
    protected function request($httpMethod, $url, $data = [], $parse = true)
    {
        $ch = new Curl();

        $httpMethodFunctionName = '';
        switch($httpMethod)
        {
            case self::HTTP_GET :
                $httpMethodFunctionName = 'get';
                break;
            case self::HTTP_POST :
                $httpMethodFunctionName = 'post';
                break;
        }

        $urlData = array_key_exists('url', $data) ? $data['url'] : [];
        $url = $this->prepareUrl($this->url, $url, $urlData);

        $ch->setHeader('key', $this->key);
        $ch->setHeader('udid', $this->deviceId);

        if($this->sessionToken !== null)
            $ch->setHeader('sessiontoken', $this->sessionToken);

        $requestData = array_key_exists('request', $data) ? $data['request'] : [];
        $ch->$httpMethodFunctionName($url, $requestData);

        if($ch->error)
        {
            if($ch->error_code == 422)
            {
                throw new ValidationException($ch->response->error);
            }
            else
            {

            }
        }
        else
        {
            $response = $ch->response;
            if($parse && $response instanceof \stdClass)
            {
                return json_decode(json_encode($response), true);
            }
            return $response;
        }
    }

    /**
     * Set values from $url data array to the url string
     *
     * @param $rootUrl - root URL (API or CMS)
     * @param $url - URL pattern
     * @param $data - URL data
     * @return string - parsed URL
     */
    protected function prepareUrl($rootUrl, $url, $data)
    {
        foreach($data as $key => $item)
        {
            $url = str_replace(':'.$key, $item, $url);
        }
        return $rootUrl.$url;
    }

    /**
     * Init user API session. Call /initsession service
     *
     * @throws ValidationException
     */
    private function initSession()
    {
        $this->sessionToken = Session::get(self::SESSION_STORAGE_KEY, null);
        if($this->sessionToken === null)
        {
            $data = $this->request(self::HTTP_GET, 'initsession');
            //TODO::Add execption
            $this->setToken($data['result']['sessiontoken']);
        }
    }

    /**
     * Set user API session
     *
     * @param $sessionToken
     */
    public function setToken($sessionToken)
    {
        $this->sessionToken = $sessionToken;
        Session::set(self::SESSION_STORAGE_KEY, $this->sessionToken);
    }

    /**
     * Prepare/pass loaded file for API uploading.
     * @param $path - file name from public/files directory
     * @param $mimeType - file mime type
     * @return string - prepared string contains file info, needed for Curl request
     */
    public function makeFile($path, $mimeType)
    {
        $fullPath = public_path('files/'.$path);
        return "@$fullPath;filename=$path;type=$mimeType;";
    }
}