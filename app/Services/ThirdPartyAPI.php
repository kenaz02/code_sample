<?php

namespace App\Services;

use App\Models\Product\Product;
use \Curl\Curl;

/**
 * Class ThirdPartyAPI
 * Implement all additional third party API services
 * @package App\Services
 */
class ThirdPartyAPI {

    const HTTP_GET = 'GET';
    const HTTP_POST = 'POST';

    const SESSION_COOKIE_KEY = 'third_api_session_cookies';
    /**
     * Fetch Recently Viewed and Recommendation product lists
     * @return mixed - response product list
     */
    public function homeData()
    {
        $ch = new Curl();

        if(\Session::has(self::SESSION_COOKIE_KEY))
        {
            $httpCookies = '';
            foreach(\Session::get(self::SESSION_COOKIE_KEY) as $cookieKey => $cookieItem)
            {
                $httpCookies .= $cookieKey.'='.$cookieItem.';';
            }
            $httpCookies = rtrim($httpCookies, ";");
            $ch->setOpt(CURLOPT_COOKIE, $httpCookies);
        }

        $ch->get(\Config::get('api.targetingmantra_home'), [
            'w'     => 'bs,na,rhf',
            'mid'   => 140215,
            'limit' => 16,
            'json'  => true
        ]);

        if($ch->error)
        {

        }
        else
        {
            return $this->parse($ch->response);
        }
    }

    public function indexProductItem(Product $product)
    {
        $ch = new Curl();
        $ch->setOpt(CURLOPT_HEADER , 1);


        if(\Session::has(self::SESSION_COOKIE_KEY))
        {
            foreach(\Session::get(self::SESSION_COOKIE_KEY) as $cookieKey => $cookieItem)
            {
                $ch->setCookie($cookieKey, $cookieItem);
            }
        }

        $httpMethodFunctionName = self::HTTP_GET;
        $result = $ch->$httpMethodFunctionName(\Config::get('api.targetingmantra_index'), [
            'mid' => 140215,
            'cid' => '',
            'pid' => $product->id,
            'prc' => $product->marketPrice->price,
            'stk' => 1,
            'eid' => 1
        ]);

        preg_match_all('/^Set-Cookie:\s*([^;]*)/mi', $result, $matches);

        if(isset($matches[1]))
        {
            $cookies = [];
            foreach($matches[1] as $key => $matchItem)
            {
                $cookieItem = explode('=', $matchItem);
                if($key == 1)
                    $cookies[$cookieItem[0]] = '"'.str_replace('"', '', urldecode($cookieItem[1])).'"';
                else
                    $cookies[$cookieItem[0]] = $cookieItem[1];
            }

            \Session::set(self::SESSION_COOKIE_KEY, $cookies);
        }
    }

    /**
     * Fetch CMS static content. Response content type is HTML.
     *
     * @param $url - static page URL part.
     * @param array $data
     * @return mixed|null - response data.
     */
    public function cms($url, $data = [])
    {
        $url = $this->prepareUrl(\Config::get('api.cms_url'), $url, []);
        return $this->call(self::HTTP_GET, $url, $data);
    }

    protected function prepareUrl($rootUrl, $url, $data)
    {
        foreach($data as $key => $item)
        {
            $url = str_replace(':'.$key, $item, $url);
        }
        return $rootUrl.$url;
    }

    public function call($httpMethod, $url, $data = [], $headers = [])
    {
        $ch = new Curl();

        $httpMethodFunctionName = '';
        switch($httpMethod)
        {
            case self::HTTP_GET :
                $httpMethodFunctionName = 'get';
                break;
            case self::HTTP_POST :
                $httpMethodFunctionName = 'post';
                break;
        }

        if($headers)
        {
            foreach($headers as $key => $item)
            {
                $ch->setHeader($key, $this->key);
            }
        }

        $ch->$httpMethodFunctionName($url, $data);

        if($ch->error)
        {

        }
        else
        {
            return $ch->response;
        }
    }

    public function parse($response)
    {
        return $response instanceof \stdClass ? json_decode(json_encode($response), true) : $response;
    }

}