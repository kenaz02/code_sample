<?php

namespace App\Services;

/**
 * Class Helper
 * Handle static help methods.
 * @package App\Services
 */
class Helper {

    /**
     * Format phone phone number from XXXXXXX to XXXX XXX
     * @param $phone
     * @return mixed
     */
    public static function formatPhone($phone)
    {
        return preg_replace('/^([0-9]{0,4})([0-9]+)/i', '$1 $2', $phone);
    }

    /**
     * Format date from one format to another
     * @param $date
     * @param $formatTo
     * @param string $formatFrom
     * @return bool|string
     */
    public static function formatDate($date, $formatTo, $formatFrom = 'd/m/Y')
    {
        return date_format(date_create_from_format($formatFrom, (string) $date), $formatTo);
    }

    /**
     * Format string from format '12345678' to '1234XXXX'
     * @param $order
     * @return string
     */
    public static function formatOrderNumber($order)
    {
        return substr($order, 0, 4).preg_replace ('/\S/', 'X', substr($order, 4));
    }

    /**
     * Fetch and cache header menu data
     * @return mixed
     */
    public static function menu()
    {
        $categoryData = \Cache::get('home:menu');
        if(!$categoryData)
        {
            $categoryData = app('App\Models\Repositories\CategoriesRepository')->home();
            \Cache::put('home:menu', $categoryData, 20);
        }

        return $categoryData;
    }

    /**
     * Generate back Url link
     * @param null $default
     * @return null|string
     */
    public static function backLink($default = null)
    {
        $previous = \URL::previous();

        if($previous == \URL::current())
            return $default ? $default : '/';

        return $previous;
    }
}