<?php

namespace App\Services;

use App\Models\Repositories\FiltersRepository;

/**
 * Class Filter
 * Store and generate product list filtering, sort, pagination options and links.
 *
 * @package App\Services
 */
class Filter {

    /**
     * Product list filter options
     * @var array
     */
    private $options = [];

    /**
     * Current product list route (offer, catalog, search) options
     * @var array
     */
    private $routeOptions = [];

    /**
     * Route on page with filters list
     * @var string
     */
    private $filterPageRoute = 'productListFilter';

    /**
     * Route for partials items loading (infinity scroll, etc.)
     * @var string
     */
    private $partialLoadingRoute = 'productListPartial';

    /**
     * Current product list route
     * @var
     */
    private $route;

    /**
     * Sorting option key
     * @var string
     */
    private $sortOption = 'sort';

    /**
     * Default sorting option value
     * @var string
     */
    private $defaultSortOption = 'popular';

    /**
     * Pagination next page url
     * @var
     */
    private $next;

    /**
     * Pagination previous page url
     * @var string
     */
    private $prev;

    /**
     * Current pagination position
     * @var
     */
    private $currentPageNum = 0;

    /**
     * Pagination option key
     * @var string
     */
    private $pageOption = 'page';

    /**
     * Default number of items per page
     * @var int
     */
    private $itemsPerPage = 20;

    /**
     * Filter Repository instance
     * @var FiltersRepository
     */
    private $filterRepository;

    /**
     * Temporary param. Set offerId for dynamic catalog routes.
     * @var
     */
    private $offerId;

    /**
     * Sort keys and labels list
     *
     * @var array
     */
    private $sortKeys = [
        'popular'       => 'Most Viewed',
        'best_sellers'  => 'Best Sellers',
        'saving'        => 'Biggest Saving',
        'created'       => 'Newest First',
        'low_price'     => 'Price -- Low to High',
        'high_price'    => 'Price -- High to Low'
    ];

    /**
     * Store selected sort key label
     * @var
     */
    private $selectedSortLabel;

    /**
     * Constructor. Set current page route value and Filters Repository instance
     * @param FiltersRepository $filtersRepository
     */
    public function __construct(FiltersRepository $filtersRepository)
    {
        $this->filterRepository = $filtersRepository;
        $this->setRoute();
    }

    /**
     * Set current page route
     *
     * @param null $route
     */
    public function setRoute($route = null)
    {
        $this->route = $route === null ? \Route::currentRouteName() : $route;
    }

    /**
     * Set filters request data and route params
     *
     * @param array $data
     * @param array $routeOptions
     * @param $offerId
     */
    public function fill(array $data, array $routeOptions = [], $offerId = null)
    {
        $this->routeOptions = $routeOptions;
        $this->options = array_values($routeOptions) + $data;
        $this->offerId = $offerId;
    }

    /**
     * Return route on page with filters list
     *
     * @return string
     */
    public function getFilterPageRoute()
    {
        return route($this->filterPageRoute, array_except($this->options, $this->sortOption));
    }

    /**
     * Set route on page with filters list
     *
     * @param $filterPageRoute
     */
    public function setFilterPageRoute($filterPageRoute)
    {
        $this->filterPageRoute = $filterPageRoute;
    }

    /**
     * Set route for partials loading service
     *
     * @param $partialLoadingRoute
     */
    public function setPartialLoadingRoute($partialLoadingRoute)
    {
        $this->partialLoadingRoute = $partialLoadingRoute;
    }

    /**
     * Return string with selected filters
     *
     * @return string
     */
    public function getFilterLabels()
    {
        $filters = [];
        switch($this->route)
        {
            case 'offer':
                $filters = $this->filterRepository->offer($this->routeOptions['offerId']);
                break;
            case 'page':
                $filters = $this->filterRepository->offer($this->offerId);
                break;
            case 'productList':
                $filters = $this->filterRepository->catalog($this->routeOptions['gender'], $this->routeOptions['catalog']);
                break;
            case 'search':
                $filters = $this->filterRepository->search($this->options['q']);
                break;
        }

        $labels = [];
        foreach($filters as $item)
        {
            if(array_key_exists($item['id'], $this->options))
                $labels[] = $item['name'];
        }

        return str_limit(implode(', ', $labels), 25);
    }

    /**
     * Return label for selected sort item
     *
     * @return mixed
     */
    public function sortLabel()
    {
        if($this->selectedSortLabel)
            return $this->selectedSortLabel;

        $key = array_key_exists($this->sortOption, $this->options) && array_key_exists($this->options[$this->sortOption], $this->sortKeys) ? $this->options[$this->sortOption] : $this->defaultSortOption;
        return  $this->selectedSortLabel = $this->sortKeys[$key];
    }

    /**
     * Prepare sort links list, include generated url and label with each sort option
     *
     * @return array
     */
    public function sortLinks()
    {
        $links = [];
        $options = $this->options;
        foreach($this->sortKeys as $key => $item)
        {
            $options[$this->sortOption] = $key;
            $links[$key]['url'] = route($this->route, $options);
            $links[$key]['label'] = $item;
        }
        return $links;
    }


    /**
     * Set pagination data from dataRepository. Use after fill method!!!
     *
     * @param PaginationContract $dataHandler
     */
    public function pagination(PaginationContract $dataHandler)
    {
        if(!$this->currentPageNum = array_pull($this->options, $this->pageOption))
            $this->currentPageNum = 0;

        if($dataHandler->getCount() ==  $this->itemsPerPage)
            $this->next = $this->currentPageNum + 1;

        if($this->currentPageNum > 0)
            $this->prev = $this->currentPageNum - 1;
    }

    /**
     * Return next page url
     *
     * @return string
     */
    public function next()
    {
        return $this->getPaginationUrl($this->next);
    }

    /**
     * Generate url for pagination links
     *
     * @param $page  - next|prev page
     * @return string
     */
    private function getPaginationUrl($page)
    {
        if($page !== null)
        {
            $options = $this->options;
            if($page != 0)
                $options[$this->pageOption] = $page;
            return route($this->route, $options);
        }
        else
            return '';
    }

    /**
     * Return prev page ulr
     *
     * @return string
     */
    public function prev()
    {
        return $this->getPaginationUrl($this->prev);
    }

    /**
     * Check is next page exists
     *
     * @return bool
     */
    public function hasNext()
    {
        return $this->next !== null;
    }

    /**
     * Check is prev page exists
     *
     * @return bool
     */
    public function hasPrev()
    {
        return $this->prev !== null;
    }

    public function getPartialLoadingRoute()
    {
        return route($this->partialLoadingRoute, [$this->offerId] + $this->options);
    }

    public function getCurrentPage()
    {
        return $this->currentPageNum;
    }

}