<?php

namespace App\Services;

/**
 * Interface PaginationContract
 *
 * Set interface for repositories with pagination feature
 *
 * @package App\Services
 */
interface PaginationContract {

    /**
     * Return total number of selected items
     * @return mixed
     */
    public function getCount();

    public function getTotalCount();
}