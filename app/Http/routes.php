<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Home page section
Route::get('/', ['as' => 'home', 'uses' => 'HomeController@getIndex']);

$catalogConfig = [
    'catalog' => '(eyeglasses|sunglasses|contact-lenses|power-sunglasses|zero-power|latest-collection)',
    'gender'   => '(men|women|kids)'
];


Route::get('/{catalog}/find-eyewear/{gender}/subcategories', ['as' => 'subcategories', 'uses' => 'HomeController@getSubcategories'])->where($catalogConfig);

//Product list section
Route::get('/filters/{url_name?}', ['as'=> 'catalogFilter', 'uses' => 'MappingController@getFilter'])->where('url_name', '(.*)');
Route::get('/reviews/{url_name?}', ['as'=> 'productReviews', 'uses' => 'Product\ItemController@getReviewForm'])->where('url_name', '(.*)');
Route::get('/buy/options/{url_name?}', ['as' => 'productBuyOptions', 'uses' => 'Product\ItemController@getBuyOptions'])->where('url_name', '(.*)');


//Auth section
Route::group(['prefix' => 'customer/account'], function()
{
    Route::get('login', ['as' => 'login', 'uses' => 'User\AuthController@getLogin']);
    Route::post('login', 'User\AuthController@postLogin');

    Route::get('login/social', ['as' => 'loginSocial', 'uses' => 'User\AuthController@getLoginSocial']);
    Route::get('login/social/callback', ['as' => 'loginSocialCallback', 'uses' => 'User\AuthController@getLoginSocialCallback']);

    Route::get('create', ['as' => 'register', 'uses' => 'User\AuthController@getRegister']);
    Route::post('create', 'User\AuthController@postRegister');

    Route::get('forgotpassword', ['as' => 'forgotpassword', 'uses' => 'User\AuthController@getForgotPassword']);
    Route::post('forgotpassword', 'User\AuthController@postForgotPassword');

    Route::get('logout', ['as' => 'logout', 'uses' => 'User\AuthController@getLogout']);

});

//User account section
Route::get( '/sales/order/history/',            ['as' => 'orders', 'uses' => 'User\OrderController@getList']);
Route::get( '/sales/order/view/order_id/{id}/', ['as' => 'orderItem', 'uses' => 'User\OrderController@getItem']);

Route::group(['prefix' => 'customer/address'], function()
{
    Route::get('/', ['as' => 'addresses', 'uses' => 'User\AddressController@getList']);

    Route::get('edit/id/{id}', ['as' => 'addressEdit', 'uses' => 'User\AddressController@getItem']);
    Route::get('delete/id/{id}', ['as' => 'addressDelete', 'uses' => 'User\AddressController@getDeleteItem']);

    Route::get('new', ['as' => 'addressNew', 'uses' => 'User\AddressController@getNew']);
    Route::post('edit/save', ['as' => 'addressSave', 'uses' => 'User\AddressController@postItem']);

});

//View product section
Route::get('/product/{id}', ['as' => 'product', 'uses' => 'Product\ItemController@getShow']);
Route::post('/product/{id}/reviews', ['as' => 'createReview', 'uses' => 'Product\ItemController@postReviews']);

//Cart section
Route::group(['prefix' => '/cart'], function() {
    Route::get('/', ['as' => 'cart', 'uses' => 'CartController@getCart']);
    Route::get('conflict', ['as' => 'cartConflict', 'uses' => 'CartController@getCartConflict']);
    Route::post('conflict', ['as' => 'cartConflict', 'uses' => 'CartController@postCartConflict']);

    Route::get('select-for-me', ['as' => 'selectForMe', 'uses' => 'CartController@getSelectForMe']);

    Route::post('remove', ['as' => 'removeFromCart', 'uses' => 'CartController@postRemoveFromCart']);
    Route::post('quantity', ['as' => 'quantityCart', 'uses' => 'CartController@postChangeQuantity']);

    Route::post('add/main', ['as' => 'addToCartMainStage', 'uses' => 'Product\OrderController@postAddToCartMainStage']);
    Route::post('add/buy-options', ['as' => 'addToCartBuyOptions', 'uses' => 'Product\OrderController@postAddToCartBuyOptionsStage']);
    Route::post('add/cl/validate', ['as' => 'addToCartCLValidate', 'uses' => 'Product\OrderController@postAddToCartCLValidate']);
    Route::post('add/fht', ['as' => 'addToCartFHT', 'uses' => 'Product\OrderController@postAddToCartFHT']);
    Route::post('add/fht/form', ['as' => 'addToCartFHTForm', 'uses' => 'Product\OrderController@postAddToCartFHTForm']);
    Route::post('clean', ['as' => 'cleanCart', 'uses' => 'CartController@postCleanCart']);
    Route::post('coupon/add', ['as' => 'cartCouponAdd', 'uses' => 'CartController@postAddCoupon']);
    Route::post('voucher/add', ['as' => 'cartVoucherAdd', 'uses' => 'CartController@postAddVoucher']);
});

//Checkout section
Route::group(['prefix' => '/checkout'], function() {

    Route::get('/onepage/', ['as' => 'checkout', 'uses' => 'CheckoutController@getStage']);
    Route::post('/onepage/', ['as' => 'checkout', 'uses' => 'CheckoutController@postStage']);
    Route::get('/captcha/update', ['as' => 'updateCaptcha', 'uses' => 'CheckoutController@getUpdateCaptcha']);
    Route::get('/pincode/check', ['as' => 'checkPincode', 'uses' => 'CheckoutController@getCheckPincode']);
    Route::post('/payment/submit/{type}', ['as' => 'paymentSubmit', 'uses' => 'CheckoutController@postSubmitPayment'])->where('type', '(citrus|payu|paytm)');

});

//Services
Route::get('/home-try-on-program', ['as' => 'homeEyeCheckUpOld', 'uses' => 'Product\ServicesController@getHomeEyeCheckUp']);
Route::get('/home-try-on-program.html', ['as' => 'homeEyeCheckUp', 'uses' => 'HomeEyeCheckUpController@getStage']);
Route::post('/home-try-on-program.html', ['as' => 'homeEyeCheckUp', 'uses' => 'HomeEyeCheckUpController@postStage']);
Route::get('/home-try-on-program/time-slot', ['as' => 'homeEyeCheckUpTimeSlot', 'uses' => 'HomeEyeCheckUpController@getTimeSlot']);
Route::get('/home-try-on-program/time-slot/dates', ['as' => 'homeEyeCheckUpTimeSlotDates', 'uses' => 'HomeEyeCheckUpController@getTimeSlotAvailable']);

Route::get('/sales/guest/form', ['as' => 'trackOrder', 'uses' => 'Product\ServicesController@getTrackOrder']);
Route::post('/sales/guest/form', ['as' => 'trackOrder', 'uses' => 'Product\ServicesController@postTrackOrder']);
Route::get('/otp/resend', ['as' => 'optResend', 'uses' => 'CheckoutController@getResendOtp']);

//Product catalog section
Route::get('/search/', ['as' => 'search', 'uses' => 'Product\ListController@getSearch']);
Route::get('/search/terms', ['as' => 'searchTerms', 'uses' => 'Product\ListController@getSearchTerms']);
Route::get('/search/filters', ['as' => 'searchFilter', 'uses' => 'Product\ListController@getSearchFilter']);

Route::get('/offer/{id}', ['as' => 'offer', 'uses' => 'Product\ListController@getOffer']);
Route::get('/offer/{id}/filters', ['as' => 'offerFilter', 'uses' => 'Product\ListController@getOfferFilter']);
Route::get('/offer/{id}/filters/active', ['as' => 'offerFilterActive', 'uses' => 'Product\ListController@getActiveOfferFilter']);


Route::get('/catalog/{id}/partials', ['as' => 'productListPartial', 'uses' => 'Product\ListController@getPartialsList']);



Route::get('/{catalog}/find-eyewear/{gender}', ['as' => 'productList', 'uses' => 'Product\ListController@getList'])->where($catalogConfig);

Route::get('/{catalog}/find-eyewear/{gender}/filters', ['as' => 'productListFilter', 'uses' => 'Product\ListController@getListFilter'])->where($catalogConfig);
/*
Static routes (route(key) and(-) redis storage value)
-------------------------------------------------
frame-size-guide    - frameSizeGuide
faq                 - FAQ
delivery-time.html  - deliveryTime
*/
Route::get('/{url_name?}', ['as'=> 'page', 'uses' => 'MappingController@getIndex'])->where('url_name', '(.*)');
