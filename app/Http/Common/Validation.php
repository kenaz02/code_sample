<?php

namespace App\Http\Common;

use Illuminate\Http\Exception\HttpResponseException;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Redirect;

trait Validation {

    protected $request;

    private $validationOptions;

    protected function validate($rules = [], $messages = [] , $data = [])
    {
        $validator = $this->makeValidator($rules, $messages, $data);

        if($validator->fails())
            $this->makeResponse($validator->messages());
    }

    protected function makeValidator($rules, $messages = [], $data = [])
    {
        $data = empty($data) ? $this->request->all() : $data;
        return \Validator::make($data, $rules, $messages);
    }

    protected function makeError($message, $key = 'submit')
    {
       $this->makeResponse([$key => $message]);
    }

    protected function makeResponse($errors)
    {
        if ($this->request->ajax() || $this->request->wantsJson())
            $response =  new JsonResponse($errors, 422);
        else
            $response = $this->makeErrorBack()->withInput($this->request->all())->withErrors($errors);

        throw new HttpResponseException($response);
    }

    protected function makeErrorBack()
    {
        $redirect = array_get($this->validationOptions, 'back_redirect');
        if($redirect instanceof Redirect)
            return $redirect;

        $anchor = array_get($this->validationOptions, 'anchor', '');

        return redirect()->away(\URL::previous() . $anchor);
    }

    protected function setErrorBack(Redirect $redirect)
    {
        $this->validationOptions['back_redirect'] = $redirect;
        return $this;
    }

    protected function setErrorBackAnchor($anchor)
    {
        $this->validationOptions['anchor'] = $anchor;
        return $this;
    }
}