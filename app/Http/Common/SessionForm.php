<?php

namespace App\Http\Common;


trait SessionForm {

    protected $form;

    abstract protected function getStorageKey();

    protected function saveForm()
    {
        \Session::set($this->getStorageKey(), $this->form);
    }

    protected function resetForm()
    {
        \Session::forget($this->getStorageKey());
    }

    protected function loadForm()
    {
        $this->form = \Session::get($this->getStorageKey(), $this->form);
        return $this->form;
    }
}