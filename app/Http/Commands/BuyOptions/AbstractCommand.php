<?php

namespace App\Http\Commands\BuyOptions;

use App\Http\Commands\CommandContract;
use App\Models\Product\Product;
use Illuminate\Http\Request;

abstract class AbstractCommand implements CommandContract{

    protected $product;

    protected $request;

    public function __construct(Product $product, Request $request)
    {
        $this->product = $product;
        $this->request = $request;
    }

    abstract public function execute();

}