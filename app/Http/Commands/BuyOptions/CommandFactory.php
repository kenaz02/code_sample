<?php

namespace App\Http\Commands\BuyOptions;

use App\Models\Product\Product;

/**
 * Class CommandFactory
 * Command pattern implementation. Make command add to cart process commands.
 * @package App\Http\Commands\BuyOptions
 */
class CommandFactory {

    /**
     * List of command (as key) and conditions for command executing (values).
     * @var array
     */
    protected static $conditions = [
        'ContactLensesCommand' => Product::TYPE_CONTACT_LENS,
        'PowerGlassesCommand'  => [Product::TYPE_EYE_GLASSES, Product::TYPE_POWERED_SUN_GLASSES]
    ];


    /**
     * Create and run command object according to specified conditions.
     * @param Product $product
     * @return \Illuminate\Http\RedirectResponse
     */
    public function make(Product $product)
    {
        //TODO::Refactor with checkout
        foreach(self::$conditions as $key=>$item)
        {
            if($product->type == $item || (is_array($item) && in_array($product->type, $item)))
                return \App::make(__NAMESPACE__.'\\'.$key, [$product]);
        }

        return $this->defaultResult();
    }

    /**
     * Default action when command not specified.
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function defaultResult()
    {
        return back();
    }

}