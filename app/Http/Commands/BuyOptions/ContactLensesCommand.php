<?php

namespace App\Http\Commands\BuyOptions;

use App\Models\Forms\AddToCartCL;
use App\Http\Commands\AddToCart\ContactLensesCommand as AddToCartCommand;

class ContactLensesCommand extends AbstractCommand {

    public function execute()
    {
        $stage = $this->request->get('stage');

        $data = [
            'product' => $this->product
        ];

        $view = 'default';
        if(in_array($stage, [AddToCartCommand::RX_SUBMIT, AddToCartCommand::PRESCRIPTION_SUBMIT]) && \Session::has(AddToCartCL::SESSION_STORAGE_KEY))
        {
            $view = $stage;
            $data['model'] =  \Session::get(AddToCartCL::SESSION_STORAGE_KEY);
        }

        return view('product_item.opts_contact_lenses.'.$view, $data);
    }

}