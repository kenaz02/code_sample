<?php

namespace App\Http\Commands\BuyOptions;


class PowerGlassesCommand extends AbstractCommand{

    public function execute()
    {
        $lensBuyOption = $this->request->only(['lens_buy_option']);
        $validator = \Validator::make($lensBuyOption, [
            'lens_buy_option' => 'required',
        ]);

        if($validator->fails())
            return \Redirect::to(route('page', [$this->product->product_url]) . '#add-to-cart')
                ->withInput($lensBuyOption)
                ->withErrors($validator->messages());

        $option = $this->product->buy_options->getActive($this->request->get('lens_buy_option'));

        return view('product_item.opts_eyeglasses', [
            'product' => $this->product,
            'option'  => $option
        ]);
    }
}