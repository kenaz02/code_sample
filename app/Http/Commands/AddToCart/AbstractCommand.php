<?php

namespace App\Http\Commands\AddToCart;

use App\Http\Commands\CommandContract;
use App\Models\Cart\Normal\Cart;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use App\Events\CartCombineConflict;

/**
 * Class AbstractCommand.
 * Abstract super class for command of the add to cart process.
 * @package App\Http\Commands\AddToCart
 */
abstract class AbstractCommand implements CommandContract{

    /**
     * Product model instance
     * @var \App\Models\Product\Product
     */
    protected $product;

    /**
     * Cart model instance
     * @var \App\Models\Cart
     */
    protected $cart;

    /**
     * HTTP request instance
     * @var \Illuminate\Http\Request
     */
    protected $request;


    protected $stage;

    const MAIN_STAGE = 1;
    const BUY_OPTIONS_STAGE = 2;

    /**
     * Constructor. Inject class dependencies
     * @param Product $product
     * @param Cart $cart
     * @param Request $request
     */
    public function __construct(Product $product, Cart $cart, Request $request)
    {
        $this->cart = $cart;
        $this->product = $product;
        $this->request = $request;
    }

    abstract protected function mainStage();

    abstract protected function buyOptionsStage();

    /**
     * Method to run/execute command, implemented by CommandContract
     * @throws \Exception
     */
    public function execute()
    {
        switch($this->stage)
        {
            case self::MAIN_STAGE :
                return $this->mainStage();
            break;
            case self::BUY_OPTIONS_STAGE :
                return $this->buyOptionsStage();
            break;
            default:
                throw new \Exception('Add to cart stage type must be selected. Use stage selected methods.');

        }
    }

    public function setMainStage()
    {
        return $this->setStage(self::MAIN_STAGE);
    }

    public function setBuyOptionsStage()
    {
        return $this->setStage(self::BUY_OPTIONS_STAGE);
    }

    private function setStage($stage)
    {
        $this->stage = $stage;
        return $this;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Add Product to Cart. Return redirect object to previous product page
     * @return \Illuminate\Http\RedirectResponse
     */
    public function addToCart()
    {
        if($this->cart->getState()->isFHT() && $this->cart->getState()->getCount() > 0)
            event(new CartCombineConflict($this->cart, $this->product->id, $this->product->full_name));

        $this->cart->add($this->product);
        \Session::flash('cart:adding:success', $this->product->full_name);
        return redirect()->route('cart');
    }

    /**
     * Return redirect object to product buy options page
     * @param array $params
     * @return \Illuminate\Http\RedirectResponse
     */
    public function goToBuyOptions($params = [])
    {
        return redirect()->route('productBuyOptions', [$this->product->product_url] + $params);
    }

    public function goToBack()
    {
        return back();
    }
    public function goToProductPage()
    {
        return redirect()->route('page', [$this->product->product_url]);
    }

    public function makeValidator($data, $rules = [], $messages = [])
    {
        return \Validator::make($data, $rules, $messages);
    }
}