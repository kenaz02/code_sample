<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Models\Forms\AddToCartCL;

class RxSubmitDecorator extends AbstractContactLensesDecorator{

    public function execute()
    {
        $request = $this->request;
        $validator = $this->command->makeValidator($request->all(), AddToCartCL::submitRules(), AddToCartCL::submitMessages());

        if($validator->fails())
            return redirect()->back()
                ->withInput($request->all())
                ->withErrors($validator->messages());

        $this->command->setCartOptions();

        return $this->command->addToCart();
    }
}