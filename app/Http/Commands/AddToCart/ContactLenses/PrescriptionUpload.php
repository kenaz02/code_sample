<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Models\Forms\AddToCartCL;

trait PrescriptionUpload {

    protected $prescription;

    protected function uploadImage()
    {


        if($this->request->hasFile('prescription_file'))
        {
            $file = $this->request->file('prescription_file');
            $fileName = md5(time()).'.'.$file->getClientOriginalExtension();
            $mimeType = $file->getMimeType();
            $file->move('files', $fileName);

            $this->addToCartCL->prescriptionFile = $this->prescription->uploadForContactLenses($this->command->getProduct()->id, $fileName, $mimeType);
            $this->addToCartCL->prescriptionLocalFile = '/files/'.$fileName;
        }
    }

    protected function validateImage($all = false)
    {
        $validator = $this->command->makeValidator(['prescription_file' => $this->request->file('prescription_file')], AddToCartCL::prescriptionFileRules(), AddToCartCL::prescriptionFileMessages());

        if($validator->fails())
        {
            $response = back()->withErrors($validator->messages());

            if($all)
                $response->withInput($this->request->all());

            return $response;
        }
        else
            return false;
    }
}