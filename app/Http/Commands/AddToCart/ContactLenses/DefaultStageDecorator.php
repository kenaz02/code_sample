<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Models\Forms\AddToCartCL;
use App\Models\Services\Prescription;
use Illuminate\Http\Request;
use App\Http\Commands\AddToCart\ContactLensesCommand;

class DefaultStageDecorator extends AbstractContactLensesDecorator {

    use PrescriptionUpload;

    public function __construct(ContactLensesCommand $contactLensesCommand, Request $request, Prescription $prescription, AddToCartCL $addToCartCL)
    {
        $this->command = $contactLensesCommand;
        $this->request = $request;
        $this->prescription = $prescription;
        $this->addToCartCL = $addToCartCL;
    }

    public function execute()
    {
        $values = $this->request->all();

        $validator = $this->command->makeValidator($values, self::rules(), self::messages());

        if($validator->fails())
            return back()
                ->withInput($values)
                ->withErrors($validator->messages());

        $values['stage'] = $values['next_stage'];
        $this->addToCartCL->setAttributes($values);

        if($values['stage'] == ContactLensesCommand::PRESCRIPTION_SUBMIT)
        {
            $errorResponse = $this->validateImage(true);
            if($errorResponse !== false)
                return $errorResponse;

            $this->uploadImage();
        }

        \Session::set(AddToCartCL::SESSION_STORAGE_KEY, $this->addToCartCL);


        return $this->command->goToBuyOptions(['stage' => $values['stage']]);
    }

    public static function rules()
    {
        return [
            'next_stage' => 'required|in:'.ContactLensesCommand::PRESCRIPTION_SUBMIT.','.ContactLensesCommand::RX_SUBMIT
        ] + AddToCartCL::stageRules();
    }

    public static function messages()
    {
        return [
            'next_stage.required' => 'Please Select Buy Options'
        ] + AddToCartCL::stageMessages();
    }
}