<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Models\Forms\AddToCartCL;
use App\Models\Services\Prescription;
use Illuminate\Http\Request;
use App\Http\Commands\AddToCart\ContactLensesCommand;

class Retake extends AbstractContactLensesDecorator{

    use PrescriptionUpload;

    public function __construct(ContactLensesCommand $contactLensesCommand, Request $request, Prescription $prescription)
    {
        $this->command = $contactLensesCommand;
        $this->request = $request;
        $this->prescription = $prescription;
    }

    public function execute()
    {
        if($this->setAddToCartCL() === false)
            return $this->command->goToBuyOptions();

        $errorResponse = $this->validateImage(true);
        if($errorResponse !== false)
            return $errorResponse;

        $this->uploadImage();

        \Session::set(AddToCartCL::SESSION_STORAGE_KEY, $this->addToCartCL);

        return $this->command->goToBuyOptions(['stage' => ContactLensesCommand::PRESCRIPTION_SUBMIT]);
    }

    public function setAddToCartCL()
    {
        if(!\Session::has(AddToCartCL::SESSION_STORAGE_KEY))
            return false;

        $this->addToCartCL = \Session::get(AddToCartCL::SESSION_STORAGE_KEY, null);
        return true;
    }
}