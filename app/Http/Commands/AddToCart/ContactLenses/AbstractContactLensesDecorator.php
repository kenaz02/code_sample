<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Http\Commands\CommandContract;
use App\Models\Forms\AddToCartCL;
use Illuminate\Http\Request;
use App\Http\Commands\AddToCart\ContactLensesCommand;

abstract class AbstractContactLensesDecorator implements CommandContract {

    protected $command;

    protected $request;

    protected $addToCartCL;

    public function __construct(ContactLensesCommand $contactLensesCommand, Request $request, AddToCartCL $addToCartCL)
    {
        $this->command = $contactLensesCommand;
        $this->request = $request;
        $this->addToCartCL = $addToCartCL;
    }

    abstract public function execute();
}