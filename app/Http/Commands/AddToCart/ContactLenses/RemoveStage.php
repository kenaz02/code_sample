<?php

namespace App\Http\Commands\AddToCart\ContactLenses;

use App\Models\Forms\AddToCartCL;

class RemoveStage extends AbstractContactLensesDecorator{

    public function execute()
    {
        \Session::forget(AddToCartCL::SESSION_STORAGE_KEY);
        return $this->command->goToBuyOptions();
    }
}