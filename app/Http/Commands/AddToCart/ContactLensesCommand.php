<?php

namespace App\Http\Commands\AddToCart;

use App\Models\Product\Product;
use App\Models\Cart\Normal\Cart;
use Illuminate\Http\Request;
use App\Http\Commands\BaseCommandFactory;

/**
 * Class ContactLensesCommand
 * Add to cart process for Contact Lenses product type.
 * @package App\Http\Commands\AddToCart
 */
class ContactLensesCommand extends AbstractCommand {

    const DEFAULT_SUBMIT      = 'default';
    const PRESCRIPTION_SUBMIT = 'prescription';
    const RX_SUBMIT           = 'rx';
    const STAGE_REMOVE        = 'stage_rm';
    const PRESCRIPTION_RETAKE = 'prescription_retake';

    protected static $buyOptionsStageCond = [
        'AddToCart\ContactLenses\DefaultStageDecorator'       => self::DEFAULT_SUBMIT,
        'AddToCart\ContactLenses\PrescriptionSubmitDecorator' => self::PRESCRIPTION_SUBMIT,
        'AddToCart\ContactLenses\RxSubmitDecorator'           => self::RX_SUBMIT,
        'AddToCart\ContactLenses\RemoveStage'                 => self::STAGE_REMOVE,
        'AddToCart\ContactLenses\Retake'                      => self::PRESCRIPTION_RETAKE
    ];

    protected $factory;

    protected $action;

    public function __construct(Product $product, Cart $cart, Request $request, BaseCommandFactory $factory)
    {
        $this->cart = $cart;
        $this->product = $product;
        $this->request = $request;
        $this->factory = $factory;
    }

    protected function mainStage() {
        return $this->goToBuyOptions();
    }

    protected function buyOptionsStage() {

        $action = $this->action ? $this->action : $this->request->get('stage');
        $command = $this->factory->make($this, self::$buyOptionsStageCond, function($conditionItem, $instance) use ($action)
        {
            return $action === $conditionItem;
        });

        return $command->execute();
    }

    public function setAction($param)
    {
        if(in_array($param, array_values(self::$buyOptionsStageCond)))
            $this->action = $param;
        else
            throw new \ErrorException('Contact lenses buy options action does not exist');

        return $this;
    }

    public function setCartOptions()
    {
        $this->cart
            ->setValue('username', $this->request->get('username'))
            ->setValue('dob',      $this->request->get('dob'))
            ->setValue('gender',   $this->request->get('gender'))
            ->setValue('notes',    $this->request->get('notes'));

        $sideLeft   = $this->request->has('side_opts.left');
        $sideRight  = $this->request->has('side_opts.right');

        //TODO: Replace opt_left and opt_right from view
        foreach($this->request->all() as $name => $value)
        {
            if($sideLeft && strpos($name, 'opt_left_') === 0)
                $optionKey = 'opt_left_';
            elseif($sideRight && strpos($name, 'opt_right_') === 0)
                $optionKey = 'opt_right_';
            elseif(strpos($name, 'opt_') === 0)
                $optionKey = 'opt_';
            else
                continue;

            $this->cart->setOption(str_replace($optionKey, '', $name), $value);
        }

        return $this->cart;
    }
}