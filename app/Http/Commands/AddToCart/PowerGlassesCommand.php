<?php

namespace App\Http\Commands\AddToCart;

use App\Models\Product\BuyOptions\EyeglassesOption;
use Psy\Exception\ErrorException;

/**
 * Class PowerGlassesCommand
 * Add to cart process for Eyeglasses and Powered sunglasses product types.
 * @package App\Http\Commands\AddToCart
 */
class PowerGlassesCommand extends AbstractCommand {

    /**
     * Array of the product option types without any packages.
     * @var array
     */
    private $typesWithoutOptions = [
        EyeglassesOption::FRAME_ONLY,
        EyeglassesOption::WITHOUT_POWER
    ];


    protected static $mainStageRules = [
        'lens_buy_option' => 'required'
    ];

    protected static $buyOptionsStageRules = [
        'id'            => 'required',
        'extra_id'      => 'required',
        'extra_value'   => 'required',
        'option_id'     => 'required',
        'option_value'  => 'required'
    ];

    /**
     * Execute command
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function mainStage() {

        $lensBuyOption = $this->request->only(['lens_buy_option']);
        $validator = $this->makeValidator($lensBuyOption, self::$mainStageRules);

        if($validator->fails())
            return \Redirect::to(\URL::previous() . '#' . 'add-to-cart')
                ->withInput($lensBuyOption)
                ->withErrors($validator->messages());

        return in_array($lensBuyOption['lens_buy_option'], $this->typesWithoutOptions) ? $this->addToCart() : $this->goToBuyOptions($lensBuyOption);
    }

    protected function buyOptionsStage() {

        $validator = $this->makeValidator($this->request->all(), self::$buyOptionsStageRules);

        if($validator->fails())
            throw new \ErrorException('Not all request data');

        $this->cart->setOption($this->request->get('extra_id'), $this->request->get('extra_value'))
                   ->setOption($this->request->get('option_id'), $this->request->get('option_value'));

        return $this->addToCart();
    }
}