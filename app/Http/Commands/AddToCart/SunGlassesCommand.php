<?php

namespace App\Http\Commands\AddToCart;

/**
 * Class SunglassesCommand
 * Add to cart process for Sunglasses product type.
 * @package App\Http\Commands\AddToCart
 */
class SunGlassesCommand extends AbstractCommand{

    /**
     * Execute command
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function mainStage() {
        return $this->addToCart();
    }

    protected function buyOptionsStage() {

    }
}