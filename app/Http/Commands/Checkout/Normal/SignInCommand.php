<?php


namespace App\Http\Commands\Checkout\Normal;


use App\Models\User;
use Illuminate\Http\Request;
use App\Models\Forms\CheckoutNormal;
use App\Models\Cart\CartResolver;

class SignInCommand extends AbstractCommand {

    protected  $user;

    public function __construct(CheckoutNormal $form, Request $request, CartResolver $cartResolver, User $user)
    {
        parent::__construct($form, $request, $cartResolver);
        $this->user = $user;
    }

    public function execute()
    {
        if($this->request->get('check_pass') == 'guest')
        {
            $this->validate(CheckoutNormal::signInStageGuestRules());
        }
        else
        {
            $this->validate(CheckoutNormal::signInStageUserRules());

            if($errors = $this->user->login($this->request->get('email'), $this->request->get('password')))
                $this->makeError($errors, 'email');

            \Auth::login($this->user);
        }

        $this->setEmail($this->request->get('email'));

        return $this->goToStage(self::STAGE_SHIPPING);
    }

    public function view()
    {
        if(\Auth::check())
        {
            $this->setEmail(\Auth::user()->email);
            return $this->goToStage(self::STAGE_SHIPPING);
        }

        $cart = $this->cartResolver->fetch();

        return view('checkout.normal.sign_in', [
            'form' => $this->form,
            'cart' => $cart
        ]);
    }

    protected function setEmail($email)
    {
        $this->form->email = $email;
        $this->saveForm();
    }
}