<?php

namespace App\Http\Commands\Checkout\Normal;


interface CommandContract extends \App\Http\Commands\CommandContract{

    /**
     * Available Home Eye Check Up feature stages (and commands).
     */
    const STAGE_SIGN_IN   = 'sign_in';
    const STAGE_SHIPPING  = 'shipping';
    const STAGE_PAYMENT   = 'payment';
    const STAGE_SUCCESS   = 'success';
    const BACK_STATUS     = 'edit';

    /**
     * View method. Describe stages views loading.
     * @return mixed
     */
    public function view();
}