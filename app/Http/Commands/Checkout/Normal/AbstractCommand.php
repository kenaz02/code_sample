<?php

namespace App\Http\Commands\Checkout\Normal;


use App\Http\Common\SessionForm;
use App\Http\Common\Validation;
use Illuminate\Http\Request;
use App\Models\Forms\CheckoutNormal;
use App\Models\Cart\CartResolver;

abstract class AbstractCommand implements CommandContract {

    use Validation, SessionForm;

    protected $cartResolver;

    public function __construct(CheckoutNormal $form, Request $request, CartResolver $cartResolver)
    {
        $this->request = $request;
        $this->form = $form;
        $this->cartResolver = $cartResolver;

        $this->boot();
    }

    abstract public function execute();

    abstract public function view();


    protected function boot()
    {

    }

    protected function getStorageKey()
    {
        return CheckoutNormal::SESSION_STORAGE_KEY;
    }

    protected function goToStage($stage, $data = [])
    {
        $data = ['stage' => $stage] + $data;
        return redirect()->route('checkout', $data);
    }
}