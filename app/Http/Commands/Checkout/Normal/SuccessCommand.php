<?php

namespace App\Http\Commands\Checkout\Normal;

use App\Http\Commands\Checkout\Normal\CommandContract;
use App\Models\Repositories\OrderRepository;
use App\Models\Services\Prescription;
use Illuminate\Http\Request;
use App\Models\Forms\CheckoutNormal;
use App\Models\Cart\CartResolver;

class SuccessCommand extends AbstractCommand{

    const PRESCRIPTION_IMAGE = 'presc_image';
    const PRESCRIPTION_MANUALLY = 'presc_manually';
    const SESSION_PRESCRIPTION_SUBMITTED = 'presc_submited';

    protected $orderRepository;

    protected $prescription;

    public function __construct(CheckoutNormal $form, Request $request, CartResolver $cartResolver, OrderRepository $orderRepository, Prescription $prescription)
    {
        $this->orderRepository = $orderRepository;
        $this->prescription = $prescription;

        parent::__construct($form, $request, $cartResolver);
    }

    public function boot()
    {
        $this->loadForm();
    }

    public function execute()
    {
        $this->validate(CheckoutNormal::prescriptionRules());

        $data = $this->request->all();

        $this->prescription
            ->setUsername(array_pull($data, 'username'))
            ->setNotes(array_pull($data, 'notes'))
            ->setDob(array_pull($data, 'dob'))
            ->setGender(array_pull($data, 'gender'));

        foreach($data as $productId => $item)
        {
            if(is_array($item))
            {
                $prescriptionType = array_pull($item, 'prescription_type');

                if($prescriptionType == self::PRESCRIPTION_IMAGE)
                {
                    $file = $this->request->file($productId);
                    $file = $file['file'];
                    $fileName = md5(time()).'.'.$file->getClientOriginalExtension();
                    $mimeType = $file->getMimeType();
                    $file->move('files', $fileName);
                    $this->prescription->uploadForEyeglasses($productId, $fileName, $mimeType);

                    \File::delete('files/'.$fileName);
                }
                elseif($prescriptionType == self::PRESCRIPTION_MANUALLY)
                {
                    foreach($item as $name => $value)
                    {
                        if(strpos($name, 'option_') !== false) {
                            $optionValues = explode('_', $name);
                            $this->prescription->setOption($productId, $optionValues[1], $value);
                        }
                    }
                }
            }
        }

        if($errors = $this->prescription->submit())
            $this->makeError($errors);

        \Session::flash(self::SESSION_PRESCRIPTION_SUBMITTED, 'true');
        return route('checkout', ['stage' => CommandContract::STAGE_SUCCESS]);

    }

    public function view()
    {

        $submitResult = $this->form->getNewOrderResult();

        $isPowerRequired = $submitResult->isPowerRequired() && \Session::has(self::SESSION_PRESCRIPTION_SUBMITTED) !== true;
        $order = $isPowerRequired ? $this->orderRepository->itemPrescription($submitResult->getOrderId()) : null;

        return view('checkout.normal.success', [
            'form'    => $this->form,
            'result'  => $submitResult,
            'order'   => $order,
            'isPowerRequired' => $isPowerRequired
        ]);
    }
}