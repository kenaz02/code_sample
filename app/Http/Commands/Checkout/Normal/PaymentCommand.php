<?php

namespace App\Http\Commands\Checkout\Normal;

use App\Models\Forms\CheckoutNormal;

use App\Models\Order\NewOrder;
use App\Services\Captcha;
use App\Services\Payment;
use Illuminate\Http\Request;
use App\Models\Cart\CartResolver;

class PaymentCommand extends AbstractCommand{

    const COD_TYPE = 'cod';
    const NET_BANKING = 'nb';

    const STORE_CREDIT_TYPE = 'storeCredit';
    const GIFT_VOUCHER_TYPE = 'giftVoucher';
    const THIRD_PARTY_TYPE_WALLET = 'thirdPartyWallet';
    const THIRD_PARTY_TYPE_CC = 'thirdPartyCC';

    const THIRD_PARTY_PAYU = 'payu';
    const THIRD_PARTY_PAYTM = 'paytm';
    const THIRD_PARTY_AIRTEL_MONEY = 'airmoney';


    protected $captcha;

    public function __construct(CheckoutNormal $form, Request $request, CartResolver $cartResolver, Captcha $captcha)
    {
        parent::__construct($form, $request, $cartResolver);
        $this->captcha = $captcha;
    }

    public function boot()
    {
        $this->loadForm();
    }

    public function execute()
    {
        $type = $this->request->get('payment_type');

        $method = 'type'.studly_case($type);
        if($type == null || !method_exists($this, $method))
            abort(404);
        else
            return $this->$method();
    }

    public function typeCod()
    {
        $this->validate(CheckoutNormal::paymentCodRules());

        if(!$this->captcha->checkAnswer($this->request->get('captcha')))
            $this->makeError(trans('validation.captcha'), 'captcha');

        return $this->submitOrder(NewOrder::P_CASH_ON_DELIVERY);
    }

    public function typeStoreCredit()
    {
        $this->validate(CheckoutNormal::paymentStoreCreditRules());

        $errors = app('CartResolver')->makeNormal()->addStoreCredit(
            $this->request->get('storeCreditCode'),
            $this->request->get('storeCreditAmount')
        );
        if($errors)
            $this->makeError($errors, 'storeCreditAmount');

        return $this->submitOrder(NewOrder::P_CASH_ON_DELIVERY);
    }

    public function typeGiftVoucher()
    {
        $this->validate(CheckoutNormal::paymentGiftVoucher());

        $errors = app('CartResolver')->makeNormal()->addGiftVoucher(
            $this->request->get('giftVoucher')
        );
        if($errors)
            $this->makeError($errors, 'giftVoucher');


        //$this->form->setPaymentMethod(NewOrder::P_CASH_ON_DELIVERY);
        return $this->submitOrder(NewOrder::P_CASH_ON_DELIVERY);
    }

    public function typeThirdPartyWallet()
    {
        return $this->thirdPartyAction();
    }

    public function typeThirdPartyCC()
    {
        return $this->thirdPartyAction();
    }

    protected function thirdPartyAction()
    {
        $type = $this->request->get('payment_third_service');

        switch ($type) {
            case self::THIRD_PARTY_AIRTEL_MONEY:
            case self::THIRD_PARTY_PAYU:
                $payment = app('App\Services\Payment\PayU');
                $isAirtelMoney = $type == self::THIRD_PARTY_AIRTEL_MONEY;
                $payment->makePay($this->form->address, $this->form->email, $this->form->ammount, $isAirtelMoney);
                break;
            case self::THIRD_PARTY_PAYTM:
                $payment = app('App\Services\Payment\PayTm');
                return $payment->makePay($this->form->address, $this->form->email, $this->form->ammount);
                break;
            default:
                throw new \ErrorException('Unknown Third Party API payment type');
        }
    }

    public function submitPayment($type)
    {
        if(!in_array($type, ['citrus', 'payu', 'paytm']))
            abort(404);

        $method = 'submitPayment'.studly_case($type);
        return $this->$method();
    }

    public function submitPaymentCitrus()
    {
        $payment = app('App\Services\Payment');

        $rules = array_fill_keys($payment->getConfirmationKeys(), 'required');
        $validator = $this->makeValidator($rules);

        $errors = $validator->fails() ? 'Not all data' : false;

        if($errors == false)
        {
            $confirmationOptions = $this->request->only($payment->getConfirmationKeys());
            $errors = $payment->setConfirmationOptions($confirmationOptions)->checkConfirmation();
        }


        if($errors !== false)
        {
            return $this->goToStage(self::STAGE_PAYMENT)->withErrors([
                'payment_submit' => $errors
            ]);
        }
        else
        {
            return $this->submitOrder(NewOrder::P_CREDIT_CART);
        }
    }

    public function submitPaymentPayu()
    {
        $error = (int) $this->request->get('error');

        if($error === 0)
            return $this->submitOrder(NewOrder::P_PAY_TM);
        else
            return $this->goToStage(self::STAGE_PAYMENT)->withErrors([
                'payment_submit' => $this->request->get('error_Message')
            ]);
    }

    public function submitPaymentPaytm()
    {

    }

    protected function submitOrder($paymentMethod)
    {
        if($errors = $this->form->submitOrder($paymentMethod)->getErrors())
            $this->makeError($errors);

        $this->saveForm();
        return $this->goToStage(self::STAGE_SUCCESS);
    }

    public function view()
    {
        $cart = $this->cartResolver->fetch();

        $captchaImage = $this->captcha->make()->getImage();

        $payment = app('App\Services\Payment');
        $bill = json_encode($payment->makeBill($this->form->address, $this->form->email, $cart->total));

        $this->form->ammount = $cart->total;
        $this->saveForm();

        return view('checkout.normal.payment', [
            'cart'    => $cart,
            'form'    => $this->form,
            'captchaImage' => $captchaImage,
            'bill'      => $bill,
            'payment'   => $payment
        ]);
    }

}