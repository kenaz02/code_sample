<?php

namespace App\Http\Commands\Checkout\Normal;

use App\Models\Forms\CheckoutNormal;
use App\Models\Repositories\AddressRepository;
use Illuminate\Http\Request;
use App\Models\Cart\CartResolver;

class ShippingCommand extends AbstractCommand {

    protected $addressRepository;

    public function __construct(CheckoutNormal $form, Request $request, CartResolver $cartResolver, AddressRepository $addressRepository)
    {
        parent::__construct($form, $request, $cartResolver);
        $this->addressRepository = $addressRepository;
    }

    public function boot()
    {
        $this->loadForm();
    }

    public function execute()
    {

        if(\Auth::check())
        {
            $this->validate(CheckoutNormal::shippingAddressRules());

            $address = $this->addressRepository->item($this->request->get('checked_address'));
            $this->form->setAddress($address);
        }
        else
        {
            $this->validate(CheckoutNormal::shippingGuestRules());
            $this->form->setAttributes($this->request->all());

            if($errors = $this->form->checkPincode($this->request->get('pincode')))
                $this->makeError($errors, 'pincode');
        }

        $this->saveForm();
        return $this->goToStage(self::STAGE_PAYMENT);
    }

    public function view()
    {
        $viewData = [];
        $view = 'shipping';

        if(\Auth::check())
        {
            $viewData['addressList'] = $this->addressRepository->getList();
            $viewData['user'] = \Auth::user();
            $view = 'address';
        }
        else
        {
            $viewData['states'] = $this->addressRepository->getStatesList();
        }

        $viewData['form'] = $this->form;
        $viewData['cart'] = $this->cartResolver->fetch();

        return view('checkout.normal.'.$view, $viewData);
    }
}