<?php

namespace App\Http\Commands\Checkout\FHT;

use App\Http\Common\SessionForm;
use App\Http\Common\Validation;
use App\Models\Forms\CheckoutFHT;
use Illuminate\Http\Request;

/**
 * Class AbstractCommand
 * Abstract command base class, includes all common methods.
 * Declare methods which must be included by stages command classes.
 * @package App\Http\Commands\Checkout\FHT
 */
abstract class AbstractCommand implements CommandContract {

    use Validation, SessionForm;

    /**
     * Inject form model of Free Home Trial checkout and request object.
     * Call boot method.
     * @param CheckoutFHT $form
     * @param Request $request
     */
    public function __construct(CheckoutFHT $form, Request $request)
    {
        $this->request = $request;
        $this->form = $form;

        $this->boot();
    }

    /**
     * Command method, executed on form/stage submit.
     * @return mixed
     */
    abstract public function execute();

    /**
     * Command method, executed on form/stage submit.
     * @return mixed
     */
    abstract public function view();


    /**
     * Boot method. Call in constructor.
     * Process all actions common for execute() and view() methods.
     */
    protected function boot()
    {

    }

    /**
     * Return key for form session store.
     * Declared by SessionForm trait.
     * @return string
     */
    protected function getStorageKey()
    {
        return CheckoutFHT::SESSION_STORAGE_KEY;
    }


    /**
     * Go to concrete command stage
     * @param $stage
     * @param array $data
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function goToStage($stage, $data = [])
    {
        $data = ['stage' => $stage] + $data;
        return redirect()->route('checkout', $data);
    }

}