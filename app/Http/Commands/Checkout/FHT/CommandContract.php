<?php

namespace App\Http\Commands\Checkout\FHT;

/**
 * Interface CommandContract.
 * @package App\Http\Commands\Checkout\FHT
 */
interface CommandContract extends \App\Http\Commands\CommandContract{

    /**
     * Available Home Eye Check Up feature stages (and commands).
     */
    const STAGE_PHONE   = 'phone';
    const STAGE_ADDRESS = 'address';
    const STAGE_CONFIRM = 'confirm';
    const BACK_STATUS   = 'edit';

    /**
     * View method. Describe stages views loading.
     * @return mixed
     */
    public function view();

}