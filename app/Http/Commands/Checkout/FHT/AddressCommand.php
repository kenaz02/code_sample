<?php

namespace App\Http\Commands\Checkout\FHT;

use App\Models\Forms\CheckoutFHT;
use App\Models\Services\Otp;
use Illuminate\Http\Request;

/**
 * Class AddressCommand
 * Process Free Home Trial address form stage.
 * @package App\Http\Commands\Checkout\FHT
 */
class AddressCommand extends AbstractCommand {

    /**
     * Otp model service instance
     * @var \App\Models\Services\Otp
     */
    protected  $otp;

    /**
     * Inject command dependencies.
     * @param CheckoutFHT $form
     * @param Request $request
     * @param Otp $otp
     */
    public function __construct(CheckoutFHT $form, Request $request, Otp $otp)
    {
        $this->otp = $otp;
        parent::__construct($form, $request);
    }

    /**
     * Boot method.
     * Load session form.
     */
    public function boot()
    {
        $this->loadForm();
    }

    /**
     * Command submit method.
     * Validate address form data. Update session form instance. Send otp check code.
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ErrorException
     */
    public function execute()
    {
        $this->validate(CheckoutFHT::addressStageRules());

        $this->form->setAttributes($this->request->all());
        $this->saveForm();

        $this->otp->send($this->form->telephone);

        return $this->goToStage(CommandContract::STAGE_CONFIRM);
    }

    /**
     * Command view method.
     * Render address form.
     * @return \Illuminate\View\View
     */
    public function view()
    {
        if(\Auth::check())
            $this->form->email = \Auth::user()->email;

        return view('checkout.fht.address', [
            'form' => $this->form
        ]);
    }

}