<?php

namespace App\Http\Commands\Checkout\FHT;

use App\Models\Forms\CheckoutFHT;
use Illuminate\Http\Request;
use App\Models\Services\Otp;

/**
 * Class ConfirmCommand
 * Process Free Home Trial confirm (final) form stage.
 * @package App\Http\Commands\Checkout\FHT
 */
class ConfirmCommand extends AbstractCommand {

    /**
     * Otp model service instance
     * @var \App\Models\Services\Otp
     */
    protected  $otp;

    /**
     * Inject command dependencies.
     * @param CheckoutFHT $form
     * @param Request $request
     * @param Otp $otp
     */
    public function __construct(CheckoutFHT $form, Request $request, Otp $otp)
    {
        $this->otp = $otp;
        parent::__construct($form, $request);
    }

    /**
     * Boot method.
     * Load session form.
     */
    public function boot()
    {
        $this->loadForm();
    }

    /**
     * Command submit method.
     * Validate and check otp code. Make Free Home Trial order. Return to success page.
     * @return string
     * @throws \ErrorException
     */
    public function execute()
    {
        $this->validate(CheckoutFHT::confirmStageRules());

        list($status, $message) = array_values($this->otp->check($this->request->get('otp')));

        if(!$status)
            $this->makeError($message, 'otp');

        if($errors = $this->form->submitOrder())
            $this->makeError($errors);

        //return redirect()->route('checkout/success');
        return 'The success page will be here soon';
    }

    /**
     * Command view method.
     * Render otp check form.
     * @return \Illuminate\View\View
     */
    public function view()
    {
        return view('checkout.fht.confirm', [
           'form' => $this->form
        ]);
    }

}