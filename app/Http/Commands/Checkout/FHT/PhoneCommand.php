<?php

namespace App\Http\Commands\Checkout\FHT;

use App\Models\Forms\CheckoutFHT;

/**
 * Class PhoneCommand
 * Process Free Home Trial phone form stage.
 * @package App\Http\Commands\Checkout\FHT
 */
class PhoneCommand extends AbstractCommand{

    /**
     * Command submit method.
     * Validate phone form data. Update session form instance.
     * Check pincode and update form by pincode value.
     * @return \Illuminate\Http\RedirectResponse
     * @throws \ErrorException
     */
    public function execute()
    {
        $this->validate(CheckoutFHT::phoneStageRules());

        $this->loadForm()->setAttributes($this->request->all());

        if($errors = $this->form->checkPincode($this->request->get('pincode')))
            $this->makeError($errors, 'pincode');

        $this->saveForm();
        return $this->goToStage(CommandContract::STAGE_ADDRESS);
    }

    /**
     * Command view method.
     * Render phone form.
     * @return \Illuminate\View\View
     */
    public function view()
    {
        if($this->request->get('back') == self::BACK_STATUS)
            $this->loadForm();
        else
            $this->resetForm();

        return view('checkout.fht.phone', [
            'form' => $this->form
        ]);
    }

}