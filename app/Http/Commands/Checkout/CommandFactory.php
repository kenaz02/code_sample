<?php

namespace App\Http\Commands\Checkout;

use App\Http\Commands\Checkout\FHT\CommandContract as FHTCommandContract;
use App\Http\Commands\Checkout\Normal\CommandContract as NormalCommandContract;
use App\Models\Cart\State;

/**
 * Class CommandFactory.
 * Create and return concrete Command class for concrete method.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
class CommandFactory {

    protected $cart;

    public function __construct(State $cartState)
    {
        $this->cart = $cartState;
    }

    /**
     * Describe condition for Command class loading.
     * @var array
     */
    protected static $fhtConditions = [
        FHTCommandContract::STAGE_PHONE   => 'FHT\PhoneCommand',
        FHTCommandContract::STAGE_CONFIRM => 'FHT\ConfirmCommand',
        FHTCommandContract::STAGE_ADDRESS => 'FHT\AddressCommand',
    ];

    protected static $normalConditions = [
        NormalCommandContract::STAGE_SIGN_IN  => 'Normal\SignInCommand',
        NormalCommandContract::STAGE_SHIPPING => 'Normal\ShippingCommand',
        NormalCommandContract::STAGE_PAYMENT  => 'Normal\PaymentCommand',
        NormalCommandContract::STAGE_SUCCESS  => 'Normal\SuccessCommand',
    ];

    /**
     * Load and return Command class.
     * @param $stage - stage key, must be present if conditions keys.
     * @return mixed
     */
    public function make($stage)
    {
        if($this->cart->isFHT())
        {
            $conditions = self::$fhtConditions;
            $defaultStage = FHTCommandContract::STAGE_PHONE;
        }
        else
        {
            $conditions = self::$normalConditions;
            $defaultStage = NormalCommandContract::STAGE_SIGN_IN;
        }

        $stage = array_key_exists($stage, $conditions) ? $stage : $defaultStage;
        return $this->makeInstance($conditions[$stage]);
    }


    public function makePayment()
    {
        return $this->makeInstance(self::$normalConditions[NormalCommandContract::STAGE_PAYMENT]);
    }

    protected function makeInstance($className)
    {
        return \App::make(__NAMESPACE__.'\\'.$className);
    }
}