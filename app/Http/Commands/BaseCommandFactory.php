<?php

namespace App\Http\Commands;


class BaseCommandFactory {

    public function make($args, array $conditions, \Closure $callback)
    {
        foreach($conditions as $key=>$item)
        {
            if($callback($item, $args))
                return \App::make(__NAMESPACE__.'\\'.$key, [$args]);
        }

        return $this->defaultResult();
    }

    public function defaultResult()
    {
        return back();
    }
}