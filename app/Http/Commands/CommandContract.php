<?php

namespace App\Http\Commands;

/**
 * Interface CommandContract
 * Command pattern implementation. Use to avoid and encapsulate complex logic in controllers.
 * Use Request and Response objects.
 * @package App\Http\Commands
 */
interface CommandContract{
    /**
     * Method to run/execute command
     * @return mixed
     */
    public function execute();
}