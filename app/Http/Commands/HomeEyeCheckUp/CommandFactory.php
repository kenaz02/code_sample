<?php

namespace App\Http\Commands\HomeEyeCheckUp;

/**
 * Class CommandFactory.
 * Create and return concrete Command class for concrete method.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
class CommandFactory {

    /**
     * Describe condition for Command class loading.
     * @var array
     */
    protected static $conditions = [
        CommandContract::STAGE_PHONE   => 'PhoneCommand',
        CommandContract::STAGE_DATE    => 'DateCommand',
        CommandContract::STAGE_ADDRESS => 'AddressCommand',
    ];

    /**
     * Load and return Command class.
     * @param $stage - stage key, must be present if conditions keys.
     * @return mixed
     */
    public function make($stage)
    {
        $stage = array_key_exists($stage, self::$conditions) ? $stage : CommandContract::STAGE_PHONE;
        return \App::make(__NAMESPACE__.'\\'.self::$conditions[$stage]);
    }

}