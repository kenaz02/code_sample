<?php

namespace App\Http\Commands\HomeEyeCheckUp;

use App\Models\Forms\HomeEyeCheckUp as HomeEyeCheckUpForm;

/**
 * Class AddressCommand
 * Process Address (Final) form stage.
 * @package App\Http\Commands\HomeEyeCheckUp\
 */
class AddressCommand extends AbstractCommand {

    /**
     * Boot method. Load form, fetch HomeEyeCheckUp service data and validate stage.
     */
    public function boot()
    {
        $this->checkupService = $this->productRepository->getHTO();
        $this->loadForm();
        $this->validateStage([self::STAGE_DATE, self::STAGE_ADDRESS]);
    }

    /**
     * Process form submit. Submit Home Eye Check Up order.
     * @return $this
     */
    public function execute()
    {
        $this->validate(HomeEyeCheckUpForm::addressStageRules());

        $this->form->setAttributes($this->request->all());

        if($this->request->get('payment_type') == 'visit')
        {
            if($errors = $this->form->submitOrder(true))
                $this->makeError($errors);

        }


        return 'true';
    }

    /**
     * Load stage view.
     * @return \Illuminate\View\View
     */
    public function view()
    {
        if(\Auth::check())
            $this->form->email = \Auth::user()->email;

        $cart = app('CartResolver')->fetch();

        return view('services.home_eye_checkup.address', [
            'checkupService' => $this->checkupService,
            'form'           => $this->form,
            'cart'           => $cart
        ]);
    }
}