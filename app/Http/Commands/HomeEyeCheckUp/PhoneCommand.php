<?php

namespace App\Http\Commands\HomeEyeCheckUp;

use App\Models\Forms\HomeEyeCheckUp as HomeEyeCheckUpForm;

/**
 * Class PhoneCommand
 * Process Phone form stage.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
class PhoneCommand extends AbstractCommand{

    public function boot()
    {
        $this->checkupService = $this->productRepository->getHTO();
    }


    /**
     * Process form submit. Redirect to next Date command.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function execute()
    {
        $this->validate(HomeEyeCheckUpForm::phoneStageRules());

        $this->loadForm()
             ->setHTO($this->checkupService)
             ->setAttributes($this->request->all());

        if($errors = $this->form->checkPincode($this->request->get('pincode')))
            $this->makeError($errors, 'pincode');

        $this->saveForm();
        return $this->goToStage(CommandContract::STAGE_DATE);
    }

    /**
     * Load stage view. Reset form in storage.
     * @return \Illuminate\View\View
     */
    public function view()
    {

        if($this->request->get('back') == self::BACK_STATUS)
            $this->loadForm();
        else
            $this->resetForm();

        return view('services.home_eye_checkup.phone', [
            'checkupService' => $this->checkupService,
            'form'           => $this->form
        ]);
    }
}