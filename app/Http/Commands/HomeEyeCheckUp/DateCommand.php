<?php

namespace App\Http\Commands\HomeEyeCheckUp;

use App\Models\Forms\HomeEyeCheckUp as HomeEyeCheckUpForm;

/**
 * Class DateCommand
 * Process Date form stage.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
class DateCommand extends AbstractCommand {

    /**
     * Boot method. Load form, fetch HomeEyeCheckUp service data and validate stage.
     */
    protected function boot()
    {
        $this->checkupService = $this->productRepository->getHTO();
        $this->loadForm();
        $this->validateStage([self::STAGE_PHONE, self::STAGE_DATE, self::STAGE_ADDRESS]);
    }

    /**
     * Process form submit. Redirect to next Address command.
     * @return \Illuminate\Http\RedirectResponse
     */
    public function execute()
    {
        $this->validate(HomeEyeCheckUpForm::dateStageRules());

        $this->form
            ->setHTO($this->checkupService)
            ->setAttributes($this->request->all());

        if($errors = $this->form->addToCart())
            $this->makeError($errors);

        $this->saveForm();

        return $this->goToStage(CommandContract::STAGE_ADDRESS);
    }

    /**
     * Load stage view.
     * @return \Illuminate\View\View
     */
    public function view()
    {
        $availableSlots = json_encode($this->checkupService->loadTimeSlotsAvailableDays($this->form->pincode));
        $availableTimeSlots = $this->checkupService->loadTimeSlots($this->form->cityId, $this->form->date ? $this->form->date : date('d/m/Y', strtotime('+1 day')));

        return view('services.home_eye_checkup.date', [
            'checkupService'     => $this->checkupService,
            'form'               => $this->form,
            'availableSlots'     => $availableSlots,
            'availableTimeSlots' => $availableTimeSlots
        ]);
    }
}