<?php

namespace App\Http\Commands\HomeEyeCheckUp;

/**
 * Interface CommandContract.
 * Home Eye Check Up feature command interface.
 * View method - Describe stages views loading.
 * Execute method - Describe stages forms submitting.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
interface CommandContract extends \App\Http\Commands\CommandContract{

    /**
     * Available Home Eye Check Up feature stages (and commands).
     */
    const STAGE_PHONE   = 'phone';
    const STAGE_ADDRESS = 'address';
    const STAGE_DATE    = 'date';
    const BACK_STATUS   = 'edit';

    /**
     * View method. Describe stages views loading.
     * @return mixed
     */
    public function view();
}