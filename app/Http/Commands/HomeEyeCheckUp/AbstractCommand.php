<?php

namespace App\Http\Commands\HomeEyeCheckUp;

use App\Http\Common\SessionForm;
use App\Http\Common\Validation;
use App\Models\Repositories\ProductRepository;
use App\Models\Services\HomeEyeCheckUp\HomeEyeCheckUp;
use App\Models\Forms\HomeEyeCheckUp as HomeEyeCheckUpForm;
use Illuminate\Http\Request;
use Illuminate\Http\Exception\HttpResponseException;

/**
 * Class AbstractCommand
 * Abstract command base class, includes all common methods.
 * @package App\Http\Commands\HomeEyeCheckUp
 */
abstract class AbstractCommand implements CommandContract{

    use Validation, SessionForm;

    /**
     * HomeEyeCheckUp service model instance
     * @var \App\Models\Services\HomeEyeCheckUp\HomeEyeCheckUp
     */
    protected $checkupService;

    protected $productRepository;

    /**
     * Constructor method.
     * @param ProductRepository $productRepository
     * @param HomeEyeCheckUpForm $form
     * @param Request $request
     */
    public function __construct(ProductRepository $productRepository, HomeEyeCheckUpForm $form, Request $request)
    {
        $this->request = $request;
        $this->productRepository = $productRepository;
        $this->form = $form;
        $this->boot();
    }

    /**
     * CommandContract execute method implementation.
     * @return mixed
     */
    abstract public function execute();

    /**
     * CommandContract execute method view.
     * @return mixed
     */
    abstract public function view();

    /**
     * Check available stage for concrete command.
     * @param $stage
     */
    protected function validateStage($stage)
    {
        if($this->form->stage === null ||!in_array($this->form->stage, $stage))
            throw new HttpResponseException(redirect()->route('homeEyeCheckUp'));
    }

    /**
     * Empty method. Called in constructor. Can be overwritten in child classes.
     */
    protected function boot()
    {

    }

    /**
     * Go to concrete command stage
     * @param $stage
     * @param array $data
     * @param boolean $anchor
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function goToStage($stage, $data = [], $anchor = true)
    {
        $data = ['stage' => $stage] + $data;
        $anchor = $anchor ? '#form' : '';
        return redirect(route('homeEyeCheckUp', $data).$anchor);
    }

    protected function getStorageKey()
    {
        return HomeEyeCheckUpForm::SESSION_STORAGE_KEY;
    }

    protected function makeErrorBack()
    {
        return redirect(\URL::previous() . '#form');
    }
}