<?php

namespace App\Http\Controllers;

use App\Http\Commands\Checkout\CommandFactory;
use App\Http\Common\Validation;
use App\Models\Repositories\AddressRepository;
use App\Models\Services\Otp;
use App\Models\Services\Pincode;
use App\Services\Captcha;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class CheckoutController extends Controller{

    use Validation;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }
    
    public function getStage(CommandFactory $commandFactory)
    {
        $stage = $this->request->get('stage');
        return $commandFactory->make($stage)->view();
    }

    public function postStage(CommandFactory $commandFactory)
    {
        $stage = $this->request->get('stage');
        return $commandFactory->make($stage)->execute();
    }

    public function getResendOtp(Otp $otp)
    {
        $this->validate([
           'tel' => 'required|numeric'
        ]);

        $otp->send($this->request->get('tel'));

        return back();
    }

    public function getCheckPincode(AddressRepository $addressRepository)
    {
        $this->validate([
            'id' => 'required'
        ]);

        $pincodeData = $addressRepository->getPincodeItem($this->request->get('id'));

        return $pincodeData instanceof Pincode ? $pincodeData->getAttributes() : new JsonResponse($pincodeData, 422);
    }

    public function getUpdateCaptcha(Captcha $captcha)
    {
        $image = $captcha->make()->getImage();
        return [
            'image' => $image
        ];
    }

    public function postSubmitPayment($type, CommandFactory $commandFactory)
    {
        return $commandFactory->makePayment()->submitPayment($type);
    }
}