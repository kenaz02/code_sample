<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Services\ThirdPartyAPI;
use Illuminate\Support\Facades\Redis;
use Symfony\Component\Finder\Iterator\DepthRangeFilterIterator;

/**
 * Class MappingController
 * Middleware controller, parse catalog and product page url
 * @package App\Http\Controllers\Product
 */
class MappingController extends Controller{

    /**
     * Main controller. Search redis array to find required page id by url pattern.
     * Call required controller method.
     * @param $page - required page url
     */
    public function getIndex($page)
    {
       $redis = Redis::connection();

        if($complexId = $redis->get($page))
        {
            list($type, $id) = explode(':', $complexId);
            $methods = [
                'prod' => 'App\Http\Controllers\Product\ItemController@getShow',
                'cat' => 'App\Http\Controllers\Product\ListController@getList',
                'stat' => 'App\Http\Controllers\Product\MappingController@getStaticPage'
            ];
            return \App::call($methods[$type], ['id' => $id ]);
        }
        else
        {
            return abort(404);
        }
    }

    /**
     * Load required category filters page by category url name.
     * @param $page - required page url
     * @return null
     */
    public function getFilter($page)
    {

        $redis = Redis::connection();
        if($complexId = $redis->get($page))
        {
            list($type, $id) = explode(':', $complexId);
            return \App::call('App\Http\Controllers\Product\ListController@getFilters', ['id' => $id ]);
        }
        else
        {
            return abort(404);
        }
    }

    public function getStaticPage($id, ThirdPartyAPI $thirdPartyAPI)
    {
        $data = $thirdPartyAPI->cms($id);
        return view('static.index')->withPage($data);
    }


}