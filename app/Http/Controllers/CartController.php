<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product\Product;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Session;

class CartController extends Controller{

    public function __construct()
    {
        //$this->middleware('guest');
    }

    public function getCart(Cart\CartResolver $cartResolver)
    {
        $cart = $cartResolver->fetch();

        $view = $cart->isFHT() ? 'fht' : 'normal';

        return view('cart.'.$view, [
            'cart' => $cart
        ]);
    }

    public function postChangeQuantity(Request $request, Cart\CartResolver $cartResolver)
    {
        $cart = $cartResolver->makeNormal();
        $validator = \Validator::make($request->all(), [
            'id'        => 'required|integer',
            'quantity'  => 'required|integer'
        ]);

        if(!$validator->fails()) {

            $submitErrors = $cart->changeQuantity(
                $request->get('id'),
                $request->get('quantity')
            );

            if ($submitErrors === false) {
                \Session::flash('cart:quantity:updated', $request->get('full_name'));
                return redirect()->back();
            }
            else
            {
                $validator->messages()->add('cart:quantity:submit', $submitErrors);
                $validator->messages()->add('cart:quantity:product', $request->get('full_name'));
            }
        }

        return redirect()->back()
            ->withInput($request->all())
            ->withErrors($validator->messages());
    }

    public function postRemoveFromCart(Request $request, Product $product, Cart\CartResolver $cartResolver)
    {
        $cart = $cartResolver->fetch();

        //TODO:: Make Validate
//        $this->validate([
//            'id' => 'required'
//        ]);

        $product->id = $request->get('id');
        $cart->remove($product);
        \Session::flash('cart:remove:submit', $request->get('full_name'));
        return $cart->getState()->getCount() ? back() : redirect()->route('cart');
    }

    public function postCleanCart(Cart\CartResolver $cartResolver)
    {
        $cart = $cartResolver->makeFHT();
        $cart->clean();
        return back();
    }

    public function postAddCoupon(Request $request, Cart\CartResolver $cartResolver)
    {
        if(!$request->has('code'))
            return new JsonResponse(['message' => trans('validation.required', ['attribute' => 'coupon code'])], 422);

        $cart = $cartResolver->makeNormal();
        $errors = $cart->addCoupon($request->get('code'));

        return $errors === false ? '' : new JsonResponse([ 'message' => $errors ], 422);
    }

    public function postAddVoucher(Request $request, Cart\CartResolver $cartResolver)
    {
        if(!$request->has('code'))
            return new JsonResponse(['message' => trans('validation.required', ['attribute' => 'voucher code'])], 422);

        $cart = $cartResolver->makeNormal();
        $errors = $cart->addGiftVoucher($request->get('code'));

        return $errors === false ? '' : new JsonResponse([ 'message' => $errors ], 422);
    }

    public function getCartConflict()
    {
        if(!\Session::has(Cart\CartResolver::TMP_CART_CONFLICT))
            abort('404');

        \Session::reflash();
        return view('cart.combine_error');
    }

    public function postCartConflict(Cart\CartResolver $cartResolver, Product $product)
    {
        if(!\Session::has(Cart\CartResolver::TMP_CART_CONFLICT))
            abort('404');

        $data = \Session::get(Cart\CartResolver::TMP_CART_CONFLICT);

        $type = array_pull($data, 'cart_type');
        $cart = $cartResolver->makeBuilder($type)->makeCart()->getCart();
        $cart->setValues($data);

        $product->id = array_pull($data, 'product_id');
        $cart->add($product);
        \Session::flash('cart:adding:success', $product->full_name);
        return redirect()->route('cart');
    }

    public function getSelectForMe(Cart\CartResolver $cartResolver, Request $request)
    {
        $product = null;
        if($isAgain = $request->has('id'))
        {
            $product = app('App\Models\Product');
            $product->id = $request->get('id');
        }

        $cartResolver->makeFHT()->selectForMe($isAgain, $product);
        return redirect()->route('cart');
    }



}