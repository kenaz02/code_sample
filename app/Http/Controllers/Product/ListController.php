<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\Repositories\FiltersRepository;
use App\Models\Repositories\ProductRepository;
use App\Services\Filter;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ListController extends Controller{

    public function __construct()
    {
        // $this->middleware('guest');
    }

    public function getList($id, Request $request, ProductRepository $productRepository, Filter $filter)
    {
        $filters = $request->all();
        $data = $productRepository->offer($id, $filters);

        $filter->fill($filters, \Route::current()->parameters(), $id);
        $filter->pagination($productRepository);
        $filter->setFilterPageRoute('catalogFilter');

        return view('product_list.index', [
            'data'          => $data,
            'filter'        => $filter,
            'repository'    => $productRepository
        ]);
    }

    public function getFilters($id, Request $request, FiltersRepository $filtersRepository)
    {
        $filtersRepository->preSetOptions($request->all());
        $filters = $filtersRepository->offerSelected($id);
        $productNumber = $filtersRepository->getProductNumber();

        $listPageUrl = route('page', \Route::current()->parameters());
        $activeServiceUrl = route('offerFilterActive', $id);

        return view('product_list.filters', [
            'filters'           => $filters,
            'listPageUrl'       => $listPageUrl,
            'activeServiceUrl'  => $activeServiceUrl,
            'productNumber'     => $productNumber
        ]);
    }

    public function getPartialsList($id, Request $request, ProductRepository $productRepository)
    {
        $filters = $request->all();
        $data = $productRepository->offer($id, $filters);

        return view('product_list.partials', [
            'data' => $data,
        ]);
    }

    public function getSearch(ProductRepository $productRepository, Request $request, Filter $filter)
    {
        $filters = $request->all();

        $validator = Validator::make($filters, ['q' => 'required']);
        if($validator->fails())
            return redirect()->back()
                ->withInput($request->only('q'))
                ->withErrors($validator->messages());

        $data = $productRepository->search(urlencode($filters['q']), $filters);

        $filter->fill($filters);
        $filter->pagination($productRepository);
        $filter->setFilterPageRoute('searchFilter');

        return view('product_list.search', [
            'data'      => $data,
            'query'     => $filters['q'],
            'filter'    => $filter,
            'repository' => $productRepository
        ]);
    }

    public function getSearchFilter(Request $request, FiltersRepository $filtersRepository)
    {
        //TODO: Check query
        $query = $request->input('q');

        $filtersRepository->preSetOptions($request->all());
        $filters = $filtersRepository->searchSelected(
            $query
        );

        $listPageUrl = route('search', ['q' => $query]);

        return view('product_list.filters', [
            'filters'     => $filters,
            'listPageUrl' => $listPageUrl
        ]);
    }

    public function getSearchTerms(ProductRepository $productRepository, Request $request)
    {
        $query = $request->input('q');

        if($query === null)
            return [];

        $data = $productRepository->terms(urlencode($query));

        foreach($data as $key => $item)
        {
            $data[$key]['url'] = route('product', $item['id']);
        }

        return $data;
    }

    public function getOffer($id,  Request $request, ProductRepository $productRepository, Filter $filter)
    {
        $filters = $request->all();

        $data = $productRepository->offer($id, $filters);

        $filter->fill($filters, [
            'offerId' => $id
        ]);

        $filter->pagination($productRepository);
        $filter->setFilterPageRoute('offerFilter');

        return view('product_list.index', [
            'data'          => $data,
            'filter'        => $filter,
            'repository'    => $productRepository
        ]);
    }


    public function getOfferFilter($id, Request $request, FiltersRepository $filtersRepository)
    {
        $filtersRepository->preSetOptions($request->all());
        $filters = $filtersRepository->offerSelected($id);

        $listPageUrl = route('offer', $id);

        return view('product.filters', [
            'filters'     => $filters,
            'listPageUrl' => $listPageUrl
        ]);
    }

    public function getActiveOfferFilter($id, Request $request, FiltersRepository $filtersRepository)
    {
        $responseData['filters'] = $filtersRepository->offerActiveSelected($id, $request->all());
        $responseData['productNumber'] = $filtersRepository->getProductNumber();
        return $responseData;
    }


}