<?php

namespace App\Http\Controllers\Product;

use App\Http\Commands\AddToCart\CommandFactory;
use App\Http\Commands\AddToCart\ContactLenses\DefaultStageDecorator;
use App\Http\Controllers\Controller;
use App\Models\Repositories\ProductRepository;
use Illuminate\Http\Request;
use App\Models\Cart;
use App\Models\Product\Product;
use Illuminate\Support\Facades\Validator;
use App\Events\CartCombineConflict;
use App\Http\Common\Validation as ValidatesRequests;

class OrderController extends Controller{

    use ValidatesRequests;

    public function __construct(Request $request)
    {
        //$this->middleware('guest');
        $this->request = $request;
    }

    public function postAddToCartMainStage(Product $product, CommandFactory $commandFactory)
    {
        $product->setAttributes($this->request->only([
            'id',
            'lens_buy_option',
            'type',
            'full_name',
            'product_url'
        ]));

        return $commandFactory->make($product)->setMainStage()->execute();
    }

    public function postAddToCartBuyOptionsStage(ProductRepository $productRepository, CommandFactory $commandFactory)
    {
        $this->checkRequestProductId($this->request);
        $product = $productRepository->item($this->request->get('id'));
        return $commandFactory->make($product)->setBuyOptionsStage()->execute();
    }

    public function postAddToCartCLValidate(Request $request)
    {
        $this->validate(DefaultStageDecorator::rules(), DefaultStageDecorator::messages());
    }

    public function postAddToCartFHT(Cart\CartResolver $cartResolver, Product $product)
    {
        $this->checkRequestProductId($this->request);

        $product->id = $this->request->get('id');
        if($product->id === null)
            abort('500', 'Not all request data.');

        $cart = $cartResolver->makeFHT();

        if($cart->getState()->checkProduct($product))
        {
            $products = $cart->getState()->getProducts();
            $product->id = $products[$product->id];
            $error = $cart->remove($product);
            $message = trans('messages.cart_fht_remove');

        }
        else
        {
            if($cart->getState()->isNormal() && $cart->getState()->getCount() > 0)
            {
                event(new CartCombineConflict($cart, $product->id, $product->full_name, false));
                $error = 'redirect';
            }
            else
            {
                $error = $cart->add($product);
                $message =  trans('messages.cart_fht_add');
            }

        }

        if($error === false)
            return ['count' => $cart->getCount(), 'message' => $message ];
        else
            return ['message' => $error, 'error' => true];
    }


    public function postAddToCartFHTForm(Cart\CartResolver $cartResolver, Product $product)
    {
        $this->checkRequestProductId($this->request);
        $product->id = $this->request->get('id');

        $cart = $cartResolver->makeFHT();
        if($cart->getState()->isNormal() && $cart->getState()->getCount() > 0)
            event(new CartCombineConflict($cart, $product->id, $product->full_name));

        $error = $cart->add($product);

        return redirect()->route('cart');
    }

    protected function checkRequestProductId()
    {
        if(!$this->request->has('id'))
            abort('500', 'Not all request data.');
    }


    private function getValidator($data, $rules)
    {
        return Validator::make($data, $rules);
    }
}