<?php

namespace App\Http\Controllers\Product;


use App\Http\Commands\BuyOptions\CommandFactory;
use App\Http\Common\Validation;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Repositories\ProductRepository;
use App\Models\Product\Review;
use App\Services\ThirdPartyAPI;
use Illuminate\Http\Request;

/**
 * Class ItemController
 * Single product item controller. Render product view, buy options and reviews pages.
 * @package App\Http\Controllers\Product
 */
class ItemController extends Controller {

    use Validation;

    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Show product page.
     * @param $id
     * @param ProductRepository $productRepository
     * @param ThirdPartyAPI $thirdPartyAPI
     * @return \Illuminate\View\View
     */
    public function getShow($id, ProductRepository $productRepository, ThirdPartyAPI $thirdPartyAPI)
    {

        $product = $productRepository->withDescription()->item($id);
        $thirdPartyAPI->indexProductItem($product);

        return view('product_item.index', [
            'product'    => $product,
        ]);
    }

    public function getBuyOptions($urlName, ProductRepository $productRepository, CommandFactory $commandFactory)
    {
        $product = $productRepository->buyOptions($this->getMappingId($urlName, self::PRODUCT_MAPPING));
        return $commandFactory->make($product)->execute();
    }


    /**
     * Show product reviews form.
     * @param $urlName
     * @param ProductRepository $productRepository
     * @return \Illuminate\View\View
     */
    public function getReviewForm($urlName, ProductRepository $productRepository)
    {
        $id = $this->getMappingId($urlName, self::PRODUCT_MAPPING);

        $product = $productRepository->reviews($id);

        return view('product_item.create_review', [
            'product'   => $product
        ]);
    }

    /**
     * New product review action.
     * @param $id
     * @param Request $request
     * @param Review $review
     * @return mixed
     */
    public function postReviews($id, Request $request, Review $review)
    {
        $this->setErrorBackAnchor('#create-review')->validate(Review::rules(), Review::messages());

        \Session::flash('create:review:submit', true);

        $review->setAttributes($request->all());
        $review->setNewItemDate();

        if($errors = $review->create($id))
            $this->makeError($errors);

        \Session::flash('create:review:success', true);

        return redirect()->away(\URL::previous() . '#create-review');
    }

    protected function getProductID($urlName)
    {
        return $this->getMappingId($urlName, self::PRODUCT_MAPPING);
    }
}