<?php

namespace App\Http\Controllers\Product;

use App\Http\Controllers\Controller;
use App\Models\HomeEyeCheckUp;
use App\Models\Services\TrackOrder;
use Illuminate\Http\Request;

class ServicesController extends Controller{

    public function getHomeEyeCheckUp(HomeEyeCheckUp $homeEyeCheckUp)
    {
        $homeEyeCheckUp->load();
        return view('checkup.index', [
            'model' => $homeEyeCheckUp
        ]);
    }

    public function getTrackOrder()
    {
        return view('services.track_order');
    }

    public function postTrackOrder(Request $request, TrackOrder $trackOrder)
    {
        $trackOrder->setAttributes($request->all());

        if($trackOrder->validate())
        {
            if($trackOrder->submit())
            {

            }
        }

        return redirect()->back()
            ->withInput($request->all())
            ->withErrors($trackOrder->getValidator()->messages());
    }
}