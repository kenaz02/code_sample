<?php

namespace App\Http\Controllers;

use App\Http\Commands\HomeEyeCheckUp\CommandFactory;
use App\Models\Services\HomeEyeCheckUp\HomeEyeCheckUp;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;

/**
 * Class HomeEyeCheckUpController
 * Controller for Home Eye Checkup feature. Use App\Http\Commands\HomeEyeCheckUp commands services.
 * @package App\Http\Controllers
 */
class HomeEyeCheckUpController extends Controller{

    /**
     * Render stage view. Call HomeEyeCheckUp Command Contract view method.
     * @param Request $request
     * @param CommandFactory $commandFactory
     * @return mixed
     */
    public function getStage(Request $request, CommandFactory $commandFactory)
    {
        $stage = $request->get('stage');
        return $commandFactory->make($stage)->view();
    }

    /**
     * Process stage form requests. Call HomeEyeCheckUp Command Contract execute method.
     * @param Request $request
     * @param CommandFactory $commandFactory
     * @return mixed
     */
    public function postStage(Request $request, CommandFactory $commandFactory)
    {
        $stage = $request->get('stage');
        return $commandFactory->make($stage)->execute();
    }

    /**
     * Load available time slots via AJAX.
     * @param Request $request
     * @param HomeEyeCheckUp $homeEyeCheckUp
     * @return array|JsonResponse
     */
    public function getTimeSlot(Request $request, HomeEyeCheckUp $homeEyeCheckUp)
    {
        $this->validate([
            'city_id' => 'required',
            'date'    => 'required|date_format:"d/m/Y"'
        ]);

        $data = $homeEyeCheckUp->loadTimeSlots($request->get('city_id'), $request->get('date'));

        return !array_key_exists('error', $data) ? $data : new JsonResponse($data, 422);
    }

    public function getTimeSlotAvailable(Request $request, HomeEyeCheckUp $homeEyeCheckUp)
    {
        $this->validate([
            'pincode' => 'required',
            'month'   => 'required|date_format:"m/Y"',
        ]);

        $data = $homeEyeCheckUp->loadTimeSlotsAvailableDays($request->get('pincode'), $request->get('month'));

        return !array_key_exists('error', $data) ? $data : new JsonResponse($data, 422);
    }
}