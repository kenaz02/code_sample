<?php

namespace App\Http\Controllers\User;

use App\Http\Common\Validation;
use App\Http\Controllers\Controller;
use App\Models\Address;
use App\Models\Repositories\AddressRepository;
use Illuminate\Http\Request;


class AddressController extends Controller {

    use Validation;

    public function __construct(Request $request)
    {
        $this->middleware('auth');
        $this->request = $request;
    }


    public function getItem($id, AddressRepository $addressRepository)
    {
        $address = $addressRepository->item($id);
        $states = $addressRepository->getStatesList();

        return view('account.address.item', [
            'address' => $address,
            'states'  => $states
        ]);
    }

    public function getNew(Address $address, AddressRepository $addressRepository)
    {
        $states = $addressRepository->getStatesList();

        return view('account.address.item', [
            'address' => $address,
            'states'  => $states
        ]);
    }

    public function postItem(Address $address)
    {
        $this->validate(Address::rules());

        $address->setAttributes($this->request->all());

        if($errors = $address->save())
            $this->makeError($errors);

        return redirect()->route('checkout', ['stage' => 'shipping']);
    }

    public function getDeleteItem($id, Address $address)
    {
        $address->id = $id;
        $address->delete();
        return redirect()->route('checkout', ['stage' => 'shipping']);
    }
}