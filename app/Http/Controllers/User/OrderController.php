<?php

namespace App\Http\Controllers\User;

use App\Exceptions\MagAPI\ValidationException;
use App\Http\Controllers\Controller;
use App\Models\Repositories\OrderRepository;

class OrderController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function getList(OrderRepository $orderRepository)
    {
        $orders = $orderRepository->getList();
        return view('account.orders.list', [
            'orders' => $orders
        ]);
    }

    public function getItem($id, OrderRepository $orderRepository)
    {
        $order = $orderRepository->getItem($id);

        return view('account.orders.item', [
            'order' => $order
        ]);
    }
}