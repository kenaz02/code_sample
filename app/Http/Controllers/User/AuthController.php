<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Support\Facades\Validator;
use App\Http\Common\Validation as ValidatesRequests;

class AuthController extends Controller{

    use ValidatesRequests;

    public function __construct(Request $request)
    {
        $this->request = $request;
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Show app sign in form page
     * @return \Illuminate\View\View
     */
    public function getLogin()
    {
        return view('auth.login');
    }

    /**
     * Handle app sign in form request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postLogin(User $user)
    {
        $this->validate(User::loginRules());

        $errors = $user->login($this->request->get('email'), $this->request->get('password'));
        if($errors !== false)
            $this->makeError($errors, 'email');

        \Auth::login($user);
        return redirect('/');
    }

    /**
     * Show app sign up form page
     * @return \Illuminate\View\View
     */
    public function getRegister()
    {
        return view('auth.register');
    }

    /**
     * Handle app sign up form request
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postRegister(User $user)
    {
        $this->validate(User::registerRules());

        $user->setAttributes($this->request->all());

        $errors = $user->register();
        if($errors !== false)
            $this->makeError($errors);

        \Auth::login($user);
        return redirect('/');
    }

    /**
     * Show restore pass form page
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getForgotPassword()
    {
        return view('auth.password');
    }

    /**
     * Handle app restore pass form request
     * @param User $user
     * @param Request $request
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function postForgotPassword(User $user, Request $request)
    {
        $validator = Validator::make($request->all(), User::forgotPasswordRules());

        if(!$validator->fails()) {
            $user->setAttributes($request->only([
                'email',
            ]));

            $submitErrors = $user->forgotPassword();

            if ($submitErrors === false)
                return redirect('/customer/account/index/');
            else
                $validator->messages()->add('submit', $submitErrors);
        }

        return redirect()->back()
            ->withInput($request->only([
                'email',
            ]))
            ->withErrors($validator->messages());
    }

    /**
     * Handler user logout action
     * @param User $user
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLogout(User $user)
    {
        $user->logout();
        \Auth::logout();
        return redirect('/');
    }

    /**
     * Redirect user to facebook sign in page
     * @return mixed
     */
    public function getLoginSocial()
    {
        return \Socialite::driver('facebook')->redirect();
    }

    /**
     * Handle facebook auth response and sign in user
     * @param User $user
     * @return $this|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function getLoginSocialCallback(User $user)
    {
        $socialUser = \Socialite::driver('facebook')->user();

        $errors = $user->loginSocial($socialUser->token, $socialUser->email);

        if($errors !== false)
            return redirect()->route('login')->withErrors([ 'submit' => $errors ]);

        \Auth::login($user);
        return redirect('/');
    }

}