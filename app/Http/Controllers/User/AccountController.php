<?php

namespace App\Http\Controllers\User;


use App\Exceptions\MagAPI\ValidationException;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Address;
use App\Models\Repositories\AddressRepository;
use App\Models\Repositories\OrderRepository;
use Illuminate\Support\Facades\Validator;

class AccountController extends Controller {

    public function __construct()
    {

    }

    public function getDashboard()
    {

    }

    public function getOrders(OrderRepository $orderRepository)
    {
        $data = $orderRepository->getList();
        return view('account.orders', [
            'data' => $data
        ]);
    }

    public function getAddresses(AddressRepository $addressRepository)
    {
        $data = $addressRepository->getList();
        return view('account.addresses', [
            'data' => $data
        ]);
    }

    public function getAddressEdit(AddressRepository $addressRepository, $id = null)
    {
        $model = $id ? $addressRepository->item($id) : $addressRepository->getModelInstance();
        return view('account.address_edit', [
            'model' => $model
        ]);
    }

    public function postAddressEdit(Address $address, Request $request, $id = null)
    {
        $validator = Validator::make($request->all(), Address::rules());

        if(!$validator->fails()) {
            $address->setAttributes($request->all());
            $address->id = $id;

            $submitErrors = $address->save();

            if ($submitErrors === false)
                return redirect(route('addresses'));
            else
                $validator->messages()->add('submit', $submitErrors);
        }

        return redirect()->back()
            ->withInput($request->all('email'))
            ->withErrors($validator->messages());
    }

}