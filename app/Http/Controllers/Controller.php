<?php namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Redis;

abstract class Controller extends BaseController {

	use DispatchesCommands;

    const PRODUCT_MAPPING   = 'prod';
    const CATEGORY_MAPPING  = 'cat';
    const STATIC_MAPPING    = 'stat';

    protected function parseMapping($pageURL)
    {
        $redis = Redis::connection();

        if($complexId = $redis->get($pageURL))
        {
            return explode(':', $complexId);
        }
        else
        {
            return abort(404);
        }
    }

    protected function getMappingId($pageURL, $mappingKey)
    {
        list($type, $id) = $this->parseMapping($pageURL);
        if($type !== $mappingKey)
            return abort(404);
        return $id;
    }

    protected function redirectToAnchor($anchor)
    {
        return \Redirect::to(\URL::previous() . '#' . $anchor);
    }
}
