<?php

namespace App\Http\Controllers;

use App\Models\Repositories\CategoriesRepository;
use App\Models\Repositories\ProductRepository;

class HomeController extends Controller {

	/**
	 * Index page action.
	 * @param ProductRepository $productRepository
	 * @return mixed
	 */
	public function getIndex(ProductRepository $productRepository)
	{
		$data = $productRepository->home();
		return view('home.index')
			->withData($data);
	}

	protected function prepareCatalogName($catalog)
	{
		return str_replace('-', '_', $catalog);
	}

    /**
     * Subcategories page action. Show all products subcategories by gender and product type.
     * @param $catalog
     * @param $gender
     * @param CategoriesRepository $categoriesRepository
     * @return \Illuminate\View\View
     */
	public function getSubcategories($catalog, $gender, CategoriesRepository $categoriesRepository)
	{
		$subcategories = $categoriesRepository->subcategories($this->prepareCatalogName($catalog), $gender);

		$ourServices = $categoriesRepository->ourServices();

		return view('home.subcategories', [
			'subcategories' => $subcategories,
			'ourServices'   => $ourServices
		]);
	}
}