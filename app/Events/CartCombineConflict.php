<?php namespace App\Events;

use App\Events\Event;

use App\Models\Cart\AbstractCart;
use Illuminate\Queue\SerializesModels;

class CartCombineConflict extends Event {

	use SerializesModels;

    protected $cart;
    protected $productId;
    protected $productName;
    protected $redirect;

    public function __construct(AbstractCart $cart, $productId, $productName, $redirect = true)
    {
        $this->cart = $cart;
        $this->productId = $productId;
        $this->productName = $productName;
        $this->redirect = $redirect;
    }

    public function getCart()
    {
        return $this->cart;
    }

    public function getProductId()
    {
        return $this->productId;
    }

    public function getProductName()
    {
        return $this->productName;
    }

    public function getRedirect()
    {
        return $this->redirect;
    }
}
