/* plugins */
var gulp         = require('gulp');
var elixir       = require('laravel-elixir');

var browserSync  = require('browser-sync');
var newer        = require('gulp-newer');
var imagemin     = require('gulp-imagemin');
var pngmin       = require('gulp-pngmin');
var pngquant     = require('imagemin-pngquant');
var colors       = require('colors');
var rigger       = require('gulp-rigger');

var autoprefixer = require('laravel-elixir/node_modules/gulp-autoprefixer');
var filter       = require('laravel-elixir/node_modules/gulp-filter');
var sass         = require('laravel-elixir/node_modules/gulp-sass');
var sourcemaps   = require('laravel-elixir/node_modules/gulp-sourcemaps');
var minifyCss    = require('laravel-elixir/node_modules/gulp-minify-css');
var inSequence   = require('laravel-elixir/node_modules/run-sequence');

var reload       = browserSync.reload;

/* source/build data */
var sourceDir = elixir.config.assetsDir, // "resources/assets/" by default
    publicDir = elixir.config.publicDir + '/', // "public" by default
    browsersList = [
        'last 4 versions',
        'iOS > 7',
        'Android 2.3',
        'last 100 OperaMini version',
        'OperaMobile >= 12',
        'ExplorerMobile >= 10',
        'BlackBerry >= 6'
    ];

var path = {
    src: {
        html: publicDir + 'markup/src/*.html',
        htmlWatch: publicDir + 'markup/src/**/*.html',
        style: sourceDir + 'sass/**/*.scss',
        js: sourceDir + 'js/**/*.js',
        jsSeparate: sourceDir + 'js/libs-separate/**/*.js',
        img: sourceDir + 'images/**/*.*',
        notImplImg: publicDir + 'markup/src/images/**/*',
        imgPng: sourceDir + 'images/**/*.png',
        fonts: sourceDir + 'fonts/**/*.*'
    },
    build: {
        html: publicDir + 'markup/dist/',
        js: publicDir + 'js/',
        css: publicDir + 'css/',
        img: publicDir + 'images/',
        notImplImg: publicDir + 'markup/dist/images/',
        fonts: publicDir + 'fonts/'
    }
};

/* tasks */
gulp.task('serve', ['sass'], function() {
    browserSync({
        server: {
            baseDir: publicDir,
            directory: true
        }
    });

    gulp.watch(path.src.style, ['sass']);
    gulp.watch(path.src.js, ['scripts']);
    gulp.watch(path.src.jsSeparate, ['moveJs']);
    gulp.watch(path.src.htmlWatch, ['html:build']);
    gulp.watch(path.src.fonts, ['fonts:build']);
    gulp.watch(path.src.notImplImg, ['moveNotImplImg']);
    gulp.watch(path.src.img, ['imagemin', 'pngmin']);
    gulp.watch([
            path.build.html + '*.html',
            path.build.js + '*.js'
        ]).on('change', reload);
});

gulp.task('sass', function () {
    gulp.src(path.src.style)
     .pipe(sourcemaps.init({loadMaps: true}))
     .pipe(sass().on('error', sass.logError))
     .pipe(sourcemaps.write())
     .pipe(autoprefixer({
         browsers: browsersList,
         cascade: false
     }))
     .pipe(gulp.dest(path.build.css)) // Write the CSS & Source maps
     .pipe(filter('**/*.css')) // Filtering stream to only css files
     .pipe(browserSync.reload({stream:true}));
});

gulp.task('sass:dist', function () {
    gulp.src(path.src.style)
         .pipe(sass({errLogToConsole: true}))
         .pipe(autoprefixer({
             browsers: browsersList,
             cascade: false
         }))
         .pipe(minifyCss())
         .pipe(gulp.dest(path.build.css));
});

gulp.task('html:build', function () {
    gulp.src(path.src.html)
        .pipe(rigger())
        .pipe(gulp.dest(path.build.html));
});

gulp.task('imagemin', function () {
    return gulp.src(path.src.img)
        .pipe(newer(path.build.img))
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(path.build.img));
});

gulp.task('pngmin', function() {
  return gulp.src(path.src.imgPng)
    .pipe(newer(path.build.img))
    .pipe(pngmin())
    .pipe(gulp.dest(path.build.img));
});

// just copy fonts from sorce dir to build dir
gulp.task('fonts:build', function() {
    gulp.src(path.src.fonts)
        .pipe(gulp.dest(path.build.fonts))
});

// task for not implemented images. This images used only in static markup.
// just copy not implemented images from source dir to build dir
gulp.task('moveNotImplImg', function() {
    gulp.src(path.src.notImplImg)
        .pipe(gulp.dest(path.build.notImplImg))
});

// copy separate js
gulp.task('moveJs', function() {
    gulp.src(path.src.jsSeparate)
        .pipe(gulp.dest(path.build.js))
});


/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Less
 | file for our application, as well as publishing vendor resources.
 |
 */
elixir.config.sourcemaps = false;

elixir(function(mix) {
    mix.scripts([
            'libs/*.js',
            '*.js',
            'plugins/**/*.js',
            'plugins-vendor/**/*.js'
        ], path.build.js + 'jquery.main.js')
        .version([
            'css/*.css',
            'js/*.js'
        ]);
});


/*
    general tasks
*/

/* frontend development task */
gulp.task('dev', ['serve']);

/* frontend final task (js files not minified) and default task at the same time */
gulp.task('default', function() {
    inSequence(
        ['sass:dist', 'scripts', 'moveJs', 'html:build', 'fonts:build', 'imagemin', 'pngmin', 'moveNotImplImg'], // These tasks can be done in parallel...
        'version' // ...then just do this
    );
});

/*
    FINAL TASK
    http://laravel.com/docs/5.0/elixir#gulp

    All tasks will assume a development environment, and will exclude minification. For production, use:
    "gulp --production"
*/
