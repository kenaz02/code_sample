<?php
return [
    'key'                   => env('MAG_API_KEY', ''),
    'url'                   => env('MAG_API_URL', ''),
    'deviceId'              => env('MAG_API_DEVICE_ID', ''),
    'cms_url'               => env('MAG_API_CMS_URL', ''),

    'targetingmantra_home'  => 'http://api.targetingmantra.com/TMWidgets',
    'targetingmantra_index' => 'http://api.targetingmantra.com/RecordEvent',
];