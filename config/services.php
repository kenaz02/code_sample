<?php

return [

	/*
	|--------------------------------------------------------------------------
	| Third Party Services
	|--------------------------------------------------------------------------
	|
	| This file is for storing the credentials for third party services such
	| as Stripe, Mailgun, Mandrill, and others. This file provides a sane
	| default location for this type of information, allowing packages
	| to have a conventional place to find your various credentials.
	|
	*/

	'mailgun' => [
		'domain' => '',
		'secret' => '',
	],

	'mandrill' => [
		'secret' => '',
	],

	'ses' => [
		'key' => '',
		'secret' => '',
		'region' => 'us-east-1',
	],

	'stripe' => [
		'model'  => 'User',
		'secret' => '',
	],

	'citrus' => [
		'key' 	 => env('CITRUS_ACCESS_KEY', ''),
		'secret' => env('CITRUS_SECRET_KEY', '')
	],

	'payu'	=> [
		'key'  => 'gtKFFx',
		'salt' => 'eCwWELxi'
	],

    'paytm' => [
        'key'     => '&_Hy85gfITDupRXS',
        'mid'     => 'Lenska27262006886308',
        'website' => 'Lenskart',
        // TEST or PROD
        'env'     => 'TEST'
    ],

	'facebook' => [
		'client_id' => env('FACEBOOK_CLIENT_ID', '489457081229964'),
		'client_secret' => env('FACEBOOK_CLIENT_SECRET', '19b3997d11ba111dc77bde875c6a9fb2'),
		'redirect' => 'http://'.$_SERVER['HTTP_HOST'].'/customer/account/login/social/callback',
	],

];
