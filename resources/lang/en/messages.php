<?php

return [
    'cart_fht_add'      => 'The product has been added to \'Free Home Trial\' cart.',
    'cart_fht_remove'   => 'The product has been removed from \'Free Home Trial\' cart.',
    'payment_msg_tx_id' => 'The transaction id is unknown'
];