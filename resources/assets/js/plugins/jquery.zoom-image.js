/* globals isBrowser */
/*
	zoom image plugin
*/
;(function ($) {
	'use strict';
	function ZoomImageGallery(options) {
		this.options = $.extend({
			zoomMask: '.zoom-unit__mask',
			zoomSrcAttr: 'data-image-big',
			btnClose: '.zoom-unit__close',
			btnZoomPrev: '.btn-prev',
			btnZoomNext: '.btn-next',
			showZoomClass: 'zoom-enabled',
			zoomHiddenClass: 'zoom-unit_state_hidden',
			zoomImageClass: 'zoom-unit__zoom',
			bulletsContainer: '.bullets',
			bulletActiveClass: 'active'
		}, options);

		this._init();
	}

	var html = $('html'),
		body = $('body'),
		win = $(window),
		initialMetaViewport = $('meta[name="viewport"]'),
		initialMetaContent = initialMetaViewport.attr('content'),
		zoomMetaContent = 'width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0';

	ZoomImageGallery.prototype = {
		_init: function () {
			this._initialData();
			this._attachEvents();
			this._makeCallback('onInit', this);
		},
		_initialData: function () {
			var self = this;

			this.container = $(this.options.container);
			this.srcElements = this.container.find('[' + this.options.zoomSrcAttr + ']');

			this.zoomContainer = $('#' + this.container.data('zoom-id'));
			this.zoomMask = this.zoomContainer.find(this.options.zoomMask);
			this.zoomImage = this.zoomMask.find('img');
			this.btnZoomPrev = this.zoomContainer.find(this.options.btnZoomPrev);
			this.btnZoomNext = this.zoomContainer.find(this.options.btnZoomNext);
			this.btnClose = this.zoomContainer.find(this.options.btnClose);
			this.bulletsContainer = this.zoomContainer.find(this.options.bulletsContainer);
			this.srcBig = [];
			this.curIndex = 0;

			this.srcElements.each(function () {
				self.srcBig.push(this.getAttribute(self.options.zoomSrcAttr));
			});
		},
		_attachEvents: function () {
			var self = this;

			this.srcElements.each(function (index) {
				$(this).on('click', function (e) {
					e.preventDefault();
					self.createZoom(index);
				});
			});

			this.btnClose.on('click', function (e) {
				e.preventDefault();
				self.closeZoom();
			});

			this.btnZoomPrev.on('click', function (e) {
				e.preventDefault();
				self.prevSlide();
			});

			this.btnZoomNext.on('click', function (e) {
				e.preventDefault();
				self.nextSlide();
			});

			if (!isBrowser.isOperaMini) {
				this.zoomMask.doubleTap(function () {
					$(this).toggleClass(self.options.zoomImageClass);
					self.setVerticalCenter();
				});
			} else {
				this.zoomMask.on('click', function () {
					$(this).toggleClass(self.options.zoomImageClass);
					self.setVerticalCenter();
				});
			}

			win.on('resize orientationchange', function () {
				self.setVerticalCenter();
			});

			// pinch event

			// var pinch = new Hammer.Pinch(),
			// 	pan = new Hammer.Pan();
			// this.swipeHandler = new Hammer.Manager(this.zoomMask[0]);
			// this.swipeHandler.add( new Hammer.Pan({
			// 	pointers: 2,
			// 	direction: Hammer.DIRECTION_ALL
			// 	}));

			// this.swipeHandler.add([pinch]);

			// this.swipeHandler.on('panstart', function(e) {
			// 	self.zoomMask.toggleClass(self.options.zoomImageClass);
			// 	alert(2);
			// });
		},
		createZoom: function (index) {
			html.css({
				height: '100%',
				overflow: 'hidden'
			});

			body.css({
				height: '101%',
				touchAction: 'none'
			});

			if (isBrowser.isOperaMini) {
				html.css({
					height: '',
					overflow: ''
				});

				body.css({
					height: '',
					touchAction: ''
				});
			}

			initialMetaViewport.attr('content', zoomMetaContent);
			body.addClass(this.options.showZoomClass);
			this.zoomContainer.removeClass(this.options.zoomHiddenClass);
			this.curIndex = index;

			if (this.zoomImage.length) {
				this.zoomImage.attr('src', this.srcBig[index]);
			} else {
				this.zoomImage = $('<img src="' + this.srcBig[index] + '">').appendTo(this.zoomMask);
			}

			this.setVerticalCenter();
			this.createBullets();
			this.setActiveBullet(index);
		},
		closeZoom: function () {
			html.css({
				height: '',
				overflow: ''
			});

			body.css({
				height: '',
				touchAction: 'auto'
			});

			initialMetaViewport.attr('content', initialMetaContent);
			body.removeClass(this.options.showZoomClass);
			this.zoomContainer.addClass(this.options.zoomHiddenClass);
		},
		setVerticalCenter: function () {
			var offsetTop = (this.zoomMask.height() - this.zoomImage.height()) / 2;

			if (offsetTop > 0) {
				this.zoomImage.css({
					marginTop: offsetTop
				});
			}
		},
		setSlide: function (slideIndex) {
			this.zoomImage.attr('src', this.srcBig[slideIndex]);
		},
		changeSlide: function (direction, edgeIndex) {
			if (this.curIndex !== edgeIndex) {
				this.curIndex = this.curIndex + direction;
			} else if (edgeIndex === 0) {
				this.curIndex = this.srcBig.length - 1;
			} else {
				this.curIndex = 0;
			}

			this.setSlide(this.curIndex);
			this.setActiveBullet(this.curIndex);
		},
		nextSlide: function () {
			this.changeSlide(1, this.srcBig.length - 1);
		},
		prevSlide: function () {
			this.changeSlide(-1, 0);
		},
		createBullets: function () {
			var self = this;

			if (!this.bullets) {
				this.bullets = $('<ul></ul>');

				for (var i = 0; i < this.srcElements.length; i++) {
					$('<li>' + i + '</li>').appendTo(this.bullets);
				}

				this.bulletItems = this.bullets.find('li');
				this.bullets.appendTo(this.bulletsContainer);

				this.bulletItems.each(function (index) {
					var curBullet = $(this);

					curBullet.on('click', function (e) {
						e.preventDefault();
						self.setSlide(index);
						self.setActiveBullet(index);
						self.curIndex = index;
					});
				});
			}
		},
		setActiveBullet: function (index) {
			this.bullets.find('.' + this.options.bulletActiveClass).removeClass(this.options.bulletActiveClass);
			this.bulletItems.eq(index).addClass(this.options.bulletActiveClass);
		},
		destroy: function() {

		},
		_makeCallback: function(name) {
			if (typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		}
	};

	$.fn.zoomImageGallery = function(options) {
		return this.each(function() {
			var params = $.extend({}, options, { container: this }),
				instance = new ZoomImageGallery(params);
			$.data(this, 'ZoomImageGallery', instance);
		});
	};
}(jQuery));
