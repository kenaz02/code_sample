/*
	open close with checkbox or radio-button
*/
;(function ($) {
	'use strict';
	function CheckOpenCloseSecond(options) {
		this.options = $.extend({
			opener: '[data-opener]',
			targetAttr: 'data-target',
			animSpeed: 400,
			delay: 0
		}, options);

		this._init();
	}

	CheckOpenCloseSecond.prototype = {
		_init: function () {
			this._initialData();
			this._attachEvents();
			this._makeCallback('onInit', this);

			this.toggleSlide();
		},
		_initialData: function () {
			this.container = $(this.options.container);
			this.id = this.container.attr('id');
			this.opener = $(this.options.opener + '[' + this.options.targetAttr + '="' + this.id + '"]');
		},
		_attachEvents: function () {
			var self = this;

			this.changeHandler = function () {
				self.toggleSlide();
			};

			this.opener.on('change', this.changeHandler);
		},
		showSlide: function () {
			var self = this;

			this.container.stop()
				.delay(this.options.delay)
				.slideDown(this.options.animSpeed, function () {
				self._makeCallback('onShowSlideEnd', this);
			});
		},
		hideSlide: function () {
			this.container.stop().slideUp(this.options.animSpeed);
		},
		toggleSlide: function () {
			if (this.opener.prop('checked')) {
				this.showSlide();
			} else {
				this.hideSlide();
			}
		},
		destroy: function() {

		},
		_makeCallback: function(name) {
			if (typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		}
	};

	$.fn.checkOpenClose = function(options) {
		return this.each(function() {
			var params = $.extend({}, options, { container: this }),
				instance = new CheckOpenCloseSecond(params);
			$.data(this, 'CheckOpenClose', instance);
		});
	};
}(jQuery));
