/* globals DocumentTouch */
// doubletap plugin
;(function($) {
	$.fn.doubleTap = function (doubleTapCallback) {
		return this.each(function () {
			var elm = this,
				lastTap = 0,
				isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
				isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent),
				ev = (isTouchDevice && 'touchstart') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerdown') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerDown');

			$(elm).on(ev, function () {
				var now = (new Date()).valueOf();
				var diff = (now - lastTap);
				lastTap = now ;
				if (diff < 250) {
					if ($.isFunction(doubleTapCallback)) {
						doubleTapCallback.call(elm);
					}
				}
			});
		});
	};
})(jQuery);
