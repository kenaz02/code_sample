// validate form
;(function($) {
	$.fn.validateForm = function(options) {
		var form = this,
		opt = $.extend({
			errorClass: 'error',
			successClass: 'success',
			parentElement: '.input-row',
			successFlag: true
		}, options);

		var regCredit = /^(([0-9]{4}\s){3})([0-9]{4})$/, // credit card 16 numbers in format 'XXXX XXXX XXXX XXXX'
			regCvv = /^([0-9]{3})$/, // cvv code 3 numbers in format 'XXX'
			regExpiry = /^(0{1}[1-9]{1})|(1{1}[0-2]{1})\/[0-9]{2}$/, // expiry date format MM/YY
			inputs = form.find('input, textarea, select');

		// check field
		function checkField(i, obj) {
			var currentObject = $(obj),
				currentParent = currentObject.closest(opt.parentElement),
				actualDate = new Date(),
				actualYear = parseFloat((actualDate.getFullYear() + '').slice(2)),
				expiryFlag = false,
				expiryYear,
				expiryYearFlag;

			// not empty fields
			if (currentObject.hasClass('required')) {
				setState(currentParent, currentObject, !currentObject.val().length /* || currentObject.val() === currentObject.prop('defaultValue') */) ;
			}
			// correct credit number fields
			if (currentObject.hasClass('required-credit')) {
				setState(currentParent, currentObject, !regCredit.test(currentObject.val()));
			}
			// correct cvv number fields
			if (currentObject.hasClass('required-cvv')) {
				setState(currentParent, currentObject, !regCvv.test(currentObject.val()));
			}
			// correct expiry date fields
			if (currentObject.hasClass('required-expiry')) {
				expiryFlag = regExpiry.test(currentObject.val());

				if (expiryFlag) {
					expiryYear = parseFloat(currentObject.val().slice(3));
					expiryYearFlag = expiryYear >= actualYear;
					setState(currentParent, currentObject, !expiryYearFlag);
				} else {
					setState(currentParent, currentObject, !expiryFlag);
				}
			}
		}

		// set state
		function setState(hold, field, error) {
			hold.removeClass(opt.errorClass).removeClass(opt.successClass);
			if (error) {
				hold.addClass(opt.errorClass);
				field.one('focus', function() {
					hold.removeClass(opt.errorClass).removeClass(opt.successClass);
				});
				opt.successFlag = false;
			} else {
				hold.addClass(opt.successClass);
			}
		}

		inputs.each(checkField);

		if (opt.successFlag) {
			return form;
		} else {
			return false;
		}
	}
})(jQuery);
