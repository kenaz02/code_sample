/*
	sticky panels plugin
*/
;(function ($) {
	'use strict';
	function StickyPanel(options) {
		this.options = $.extend({
			stickyType: 'down', // 'down', 'up', 'always' or 'initial'
			stickyItem: '.sticky-item',
			stickyClass: 'state-sticky',
			contentWrapper: '#wrapper',
			offStickyBlock: '' // for example #footer. This need for sticky panel which positioning to bottom
		}, options);

		this._init();
	}

	var win = $(window),
		isWinPhone = /Windows Phone/.test(navigator.userAgent);

	var getWindowHeight = function() {
			return window.innerHeight || document.documentElement.clientHeight;
		};

	StickyPanel.prototype = {
		_init: function () {
			this._initialData();
			this._attachEvents();
			this._makeCallback('onInit', this);
		},
		_initialData: function () {
			this.container = $(this.options.container);
			this.stickyItem = this.container.find(this.options.stickyItem);
			this.offStickyBlock = $(this.options.offStickyBlock);
			this.contentWrapper = $('#wrapper');
			this.scrollTop = win.scrollTop();

			this.stickyHandler();
			this.setContainerHeight();
		},
		_attachEvents: function () {
			var self = this;

			this.scrollHandler = function () {
				self.stickyHandler();
			};

			this.resizeHanlder = function () {
				self.setContainerHeight();
			};

			win.on('scroll', this.scrollHandler);
			win.on('resize orientationchange', this.resizeHanlder);
		},
		setSticky: function () {
			this.stickyItem.addClass(this.options.stickyClass);
		},
		removeSticky: function () {
			this.stickyItem.removeClass(this.options.stickyClass);
		},
		stickyHandler: function () {
			var winTop = win.scrollTop(),
				containerTop = this.container.offset().top,
				delta = 20;

			if (isWinPhone && Math.abs(this.scrollTop - winTop) <= delta) {
				return;
			}

			if (this.options.stickyType === 'up') {
				if (winTop < this.scrollTop && winTop > (containerTop + 20) && winTop + getWindowHeight() < this.contentWrapper.height()) {
					this.setSticky();
				} else {
					this.removeSticky();
				}

				this.scrollTop = win.scrollTop();
			}

			if (this.options.stickyType === 'down') {
				if (winTop > this.scrollTop && winTop + getWindowHeight() < (containerTop + this.stickyItem.height() - 20) && winTop > 0) {
					this.setSticky();
				} else {
					this.removeSticky();
				}

				this.scrollTop = win.scrollTop();
			}

			if (this.options.stickyType === 'always' && this.offStickyBlock.length) {
				this.offStickyHeight = this.offStickyBlock.offset().top;

				if (winTop > containerTop && winTop + getWindowHeight() - this.container.height() < this.offStickyHeight) {
					this.setSticky();
				} else {
					this.removeSticky();
				}
			} else if (this.options.stickyType === 'always') {
				if (winTop > containerTop) {
					this.setSticky();
				} else {
					this.removeSticky();
				}
			}

			if (this.options.stickyType === 'initial' && this.offStickyBlock.length) {
				this.offStickyHeight = this.offStickyBlock.offset().top;

				if (winTop + getWindowHeight() - this.container.height() > this.offStickyHeight) {
					this.removeSticky();
				} else {
					this.setSticky();
				}
			} else if (this.options.stickyType === 'initial') {
				this.setSticky();
			}
		},
		setContainerHeight: function () {
			this.container.css({
				minHeight: this.stickyItem.height()
			});
		},
		destroy: function() {
			win.off('scroll', this.scrollHandler);
			win.off('resize orientationchange', this.resizeHanlder);

			this.removeSticky();
			this.container.css({
				minHeight: ''
			});

			this.container.removeData('StickyPanel');
		},
		_makeCallback: function(name) {
			if (typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		}
	};

	$.fn.stickyPanel = function(options) {
		return this.each(function() {
			var params = $.extend({}, options, { container: this }),
				instance = new StickyPanel(params);
			$.data(this, 'StickyPanel', instance);
		});
	};
}(jQuery));
