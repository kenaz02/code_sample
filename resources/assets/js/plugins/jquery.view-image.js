/* globals Hammer, DocumentTouch, imagesLoaded */
/*
	180 view image plugin
*/
;(function ($) {
	'use strict';
	function ViewImage(options) {
		this.options = $.extend({
			mask: '.image-mask',
			slideset: 'image-slideset',
			btnPrev: '.btn-prev',
			btnNext: '.btn-next',
			activeSectionClass: 'active',
			imagesQty: 7
		}, options);

		this._init();
	}

	var win = $(window),
		isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
		isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent),
		touchStartEv = (isTouchDevice && 'touchstart') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerdown') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerDown'),
		touchEndEv = (isTouchDevice && 'touchend') || (isWinPhoneDevice && navigator.pointerEnabled && 'pointerup') || (isWinPhoneDevice && navigator.msPointerEnabled && 'MSPointerUp'),
		orientationEv = (('ondeviceorientation' in window) && 'deviceorientation') || (('onMozOrientation' in window) && 'MozOrientation');

	ViewImage.prototype = {
		_init: function () {
			this._initialData();
			this._attachEvents();
			this._makeCallback('onInit', this);
		},
		_initialData: function () {
			this.container = $(this.options.container);
			this.mask = this.container.find(this.options.mask);
			this.slideset = this.container.find(this.options.slideset);
			this.btnPrev = this.container.find(this.options.btnPrev);
			this.btnNext = this.container.find(this.options.btnNext);
			this.images = this.mask.find('img');
			// this.imagesQty = this.images.length;
			this.imagesQty = this.options.imagesQty;
			this.middleImageIndex = Math.floor(this.imagesQty / 2);
			this.imagesLoaded = false;

			if (this.container.hasClass(this.options.activeSectionClass)) {
				this.loadImages();
			}
		},
		_attachEvents: function () {
			var self = this,
				tillFlag = true;

			this.resizeHandler = function () {
				self.calcImagesWidth();
				self.changePosition(self.curImageIndex);
			};

			// touch handler
			this.PanHanlder = new Hammer(this.mask.get(0));
			this.PanHanlder.get('pan').set({
				threshold: 40
			});
			this.PanHanlder.on('tap panright panleft', function(e) {
				var left = self.mask.offset().left,
					curLeft = e.pointers[0].pageX - left;

				if (curLeft <= 0) {
					self.curImageIndex = 0;
				} else if (curLeft > self.imageWidth) {
					self.curImageIndex = self.imagesQty - 1;
				} else {
					self.curImageIndex = Math.ceil(curLeft * self.imagesQty / self.imageWidth) - 1;
				}

				self.changePosition(self.curImageIndex);
			});

			this.mask.on(touchStartEv, function () {
				tillFlag = false;
			});
			this.mask.on(touchEndEv, function () {
				tillFlag = true;
			});

			// device orientation handler
			this.orientationHandler = function (e) {
				if (tillFlag) {
					var firstLeft = -10,
						lastLeft = -20,
						firstRight = 10,
						lastRight = 20,
						curAngleShift;

					if (e.gamma < lastLeft) {
						self.curImageIndex = 0;
					} else if (e.gamma > lastRight) {
						self.curImageIndex = self.imagesQty - 1;
					} else if (e.gamma > firstLeft && e.gamma < firstRight) {
						self.curImageIndex = self.middleImageIndex;
					} else if (e.gamma < 0) {
						curAngleShift = lastLeft - e.gamma;
						self.curImageIndex = Math.ceil(curAngleShift * (self.middleImageIndex - 1) / (lastLeft - firstLeft));
					} else {
						curAngleShift = e.gamma - firstRight;
						self.curImageIndex = self.middleImageIndex + Math.ceil(curAngleShift * (self.middleImageIndex - 1) / (lastRight - firstRight));
					}

					self.changePosition(self.curImageIndex);
				}
			};

			if (touchStartEv) {
				this.btnPrev.add(this.btnNext).remove();
			} else {
				this.btnPrev.on('click', function (e) {
					e.preventDefault();
					self.prevPosition();
				});

				this.btnNext.on('click', function (e) {
					e.preventDefault();
					self.nextPosition();
				});
			}

			window.addEventListener(orientationEv, this.orientationHandler, false);

			win.on('resize orientationchange', this.resizeHandler);
		},
		loadImages: function () {
			var self = this;

			this.images.each(function () {
				var curImage = $(this);

				curImage.attr('src', curImage.data('src'));
			});

			imagesLoaded(self.container, function () {
				self.container.css({
					backgroundImage: 'none'
				});
			});

			this.calcImagesWidth();

			// get visible center image
			this.changePosition(this.middleImageIndex);

			this.imagesLoaded = true;
		},
		calcImagesWidth: function () {
			this.imageWidth = this.mask.width();
			var slidesetWidth = this.imageWidth * this.imagesQty;

			if (this.images.length > 1) {
				this.images.width(this.imageWidth);
			} else {
				this.images.width(this.imageWidth * this.imagesQty);
			}

			this.slideset.width(slidesetWidth + 10);
		},
		changePosition: function (posIndex) {
			this.slideset.css({
				left: -posIndex * this.imageWidth
			});
		},
		changeSinglePosition: function (direction, limitValue) {
			if (this.curImageIndex !== limitValue) {
				var curLeftPos = parseFloat(this.slideset.css('left'));

				this.curImageIndex += direction;
				this.slideset.css({
					left: curLeftPos - this.imageWidth * direction
				});
			}
		},
		nextPosition: function () {
			this.changeSinglePosition(1, this.imagesQty - 1);
		},
		prevPosition: function () {
			this.changeSinglePosition(-1, 0);
		},
		resize: function () {
			this.calcImagesWidth();
		},
		destroy: function() {

		},
		_makeCallback: function(name) {
			if (typeof this.options[name] === 'function') {
				var args = Array.prototype.slice.call(arguments);
				args.shift();
				this.options[name].apply(this, args);
			}
		}
	};

	$.fn.viewImage = function(options) {
		return this.each(function() {
			var params = $.extend({}, options, { container: this }),
				instance = new ViewImage(params);
			$.data(this, 'ViewImage', instance);
		});
	};
}(jQuery));
