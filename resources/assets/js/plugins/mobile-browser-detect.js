/*
 * Browser platform detection
 */

PlatformDetect = (function() {
	var detectModules = {};

	return {
		options: {
			cssPath: window.pathInfo ? pathInfo.base + pathInfo.css : 'css/'
		},
		addModule: function(obj) {
			detectModules[obj.type] = obj;
		},
		addRule: function(rule) {
			if (this.matchRule(rule)) {
				this.applyRule(rule);
				return true;
			}
		},
		matchRule: function(rule) {
			return detectModules[rule.type].matchRule(rule);
		},
		applyRule: function(rule) {
			var head = document.getElementsByTagName('head')[0], fragment, cssText;
			if (rule.css) {
				cssText = '<link rel="stylesheet" href="' + this.options.cssPath + rule.css + '" />';
				if (head) {
					fragment = document.createElement('div');
					fragment.innerHTML = cssText;
					head.appendChild(fragment.childNodes[0]);
				} else {
					document.write(cssText);
				}
			}

			if (rule.meta) {
				if (head) {
					fragment = document.createElement('div');
					fragment.innerHTML = rule.meta;
					head.appendChild(fragment.childNodes[0]);
				} else {
					document.write(rule.meta);
				}
			}
		},
		matchVersions: function(host, target) {
			target = target.toString();
			host = host.toString();

			var majorVersionMatch = parseInt(target, 10) === parseInt(host, 10);
			var minorVersionMatch = (host.length > target.length ? host.indexOf(target) : target.indexOf(host)) === 0;

			return majorVersionMatch && minorVersionMatch;
		}
	};
}());

// Android detection
PlatformDetect.addModule({
	type: 'android',
	parseUserAgent: function() {
		var match = /(Android) ([0-9.]*).*/.exec(navigator.userAgent);
		if (match) {
			return {
				deviceType: navigator.userAgent.indexOf('Mobile') > 0 ? 'mobile' : 'tablet',
				version: match[2]
			};
		}
	},
	matchRule: function(rule) {
		this.matchData = this.matchData || this.parseUserAgent();
		if (this.matchData) {
			var matchVersion = rule.version ? PlatformDetect.matchVersions(this.matchData.version, rule.version) : true;
			var matchDevice = rule.deviceType ? rule.deviceType === this.matchData.deviceType : true;
			return matchVersion && matchDevice;
		}
	}
});

// Blackberry detection
PlatformDetect.addModule({
	type: 'blackberry',
	parseUserAgent: function() {
		var match = /(BlackBerry).*Version\/([0-9.]*).*/.exec(navigator.userAgent);
		if (match) {
			return {
				version: match[2]
			};
		}
	},
	matchRule: function(rule) {
		this.matchData = this.matchData || this.parseUserAgent();
		if (this.matchData) {
			return rule.version ? PlatformDetect.matchVersions(this.matchData.version, rule.version) : true;
		}
	}
});

// Custom user agent detection
PlatformDetect.addModule({
	type: 'custom',
	uaMatch: function(str) {
		if (!this.ua) {
			this.ua = navigator.userAgent.toLowerCase();
		}
		return this.ua.indexOf(str.toLowerCase()) != -1;
	},
	matchRule: function(rule) {
		if (typeof rule.uaMatch === 'string' && rule.uaMatch.length) {
			var matchParts = rule.uaMatch.split(';');
			for (var i = 0; i < matchParts.length; i++) {
				if (!this.uaMatch(matchParts[i])) {
					return false;
				}
			}
			return true;
		}
	}
});

// Detect rules
PlatformDetect.addRule({ type: 'custom', uaMatch: 'Opera mini', css: 'opera-mini.css' });
PlatformDetect.addRule({ type: 'custom', uaMatch: 'Series40', css: 'nokia-s40.css' });
PlatformDetect.addRule({ type: 'android', version: 2, css: 'android-2.css' });
PlatformDetect.addRule({ type: 'blackberry', version: 6, css: 'blackberry-6.css' });
PlatformDetect.addRule({ type: 'blackberry', version: 7, css: 'blackberry-7.css' });
