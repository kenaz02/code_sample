/* globals HTML_updateFrame, DocumentTouch, imagesLoaded */

// page init
jQuery(function() {
	initTriggerLabel();

	initOpenClose();
	initAccordion();

	initCycleCarousel();
	initSlideCarousel();

	initTabs();

	initZoomImage();
	initViewImage();

	initChoseFilters();

	initStickyPanels();
	initCalcNavHeight();

	initCheckedClasses();

	initRemoveMessage();
	initRemoveAlerts();
	initRemoveError();

	initPackageAccordion();
	initShowPackages();
	initShowMoreItems();

	initShowPassword();

	initGoToReview();

	initRemoveProductLoader();

	initLoader();
	initStartBtnLoader();
	initPageLoader();

	initPageMessage();

	initCounter();

	initInputDate();
	initInputExpiryDate();
	initInputPhone();
	initInputCard();

	initCheckOpenClose();
	initCheckTypefile();
	initClearTypefile();
	initBtnUpload();

	initRemoveBlock();

	initInputsEffect();

	addFilledInputClass();

	selectOpenClose();

	initTrialPopup();

	initTryOnline();

	// $('#tab-2 [type="submit"').on('click', function (e) {
	// 	if (!$('#tab-2').validateForm()) {
	// 		e.preventDefault();
	// 	}
	// });
});

jQuery(window).load(function () {
	initSwitchViews();
	initRemoveTips();
});

// browser detect
var isBrowser = (function () {
	var uA = navigator.userAgent.toLowerCase();

	function checkUA (value) {
		return uA.indexOf(value) === -1 ? false : true;
	}

	return {
		isOperaMini: checkUA('opera mini'),
		isNokiaSeries40: checkUA('series40'),
		isAndroid2: checkUA('android 2'),
		isBlackberry6: checkUA('blackberry') && checkUA('version/6'),
		isBlackberry7: checkUA('blackberry') && checkUA('version/7'),
		isWinphone75: checkUA('windows phone os 7.5')
	};
}());

// trigger label-checkbox relationship in old browsers
function initTriggerLabel () {
	jQuery('label[for]').on('click', function () {});
}

// counter
function initCounter () {
	;(function ($) {
		var counter = $('.counter-item'),
			btnIncrease = '.counter-item__increase',
			btnDescrease = '.counter-item__decrease',
			input = '.counter-item__field',
			loadingClass = 'counter-loading',
			regInteger = /^[0-9]+$/;

		function createOverlay () {
			var overlay = $('<span class="page-overlay"></span>').appendTo($('body'));

			overlay.height($('#wrapper').height());
		}

		counter.each(function () {
			var curCounter = $(this),
				increase = curCounter.find(btnIncrease),
				descrease = curCounter.find(btnDescrease),
				field = curCounter.find(input);

			increase.one('click', function () {
				field.val(parseFloat(field.val()) + 1);
				// field.prop('disabled', true);
				descrease.off('click');
				curCounter.addClass(loadingClass);
				createOverlay();
			});

			descrease.one('click', function () {
				var val = parseFloat(field.val());

				if (val > 1) {
					field.val(val - 1);
				}

				// field.prop('disabled', true);
				increase.off('click');
				curCounter.addClass(loadingClass);
				createOverlay();
			});

			// validate input if user enter manually
			field.on('input', function () {
				var curField = $(this),
					curAttrVal = curField.attr('value'),
					curVal = curField.val();

				if (!regInteger.test(curVal) && curVal !== '') {
					curField.val(curAttrVal);
				} else {
					curField.attr('value', curVal);
				}
			});

			field.on('blur', function () {
				var curField = $(this);

				if (curField.val() === '') {
					curField.val(1);
					curField.attr('value', 1);
				}
			});
		});
	}(jQuery));
}

// try online
function initTryOnline () {
	jQuery('.product-view').each(function() {
		var hold = jQuery(this);
		var link = hold.find('.try-link');
		var cont = hold.find('#HTML_VTOCONTAINER');
		var config = cont.data();
		var clickHandler = function(e) {
			e.preventDefault();
			HTML_updateFrame(config.align, config.id, config.target, config.price);
		};

		// check if parent active
		if (link.closest('.tabs-list__item_state_active').length) {
			HTML_updateFrame(config.align, config.id, config.target, config.price);
		}

		link.on('click', clickHandler);
	});
}

// sticky panels
function initStickyPanels () {
	if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40 || isBrowser.isAndroid2 || isBrowser.isBlackberry6 || isBrowser.isBlackberry7 || isBrowser.isWinphone75)) {
		jQuery('.sticky-down').stickyPanel({
			stickyType: 'down',
			stickyItem: '.sticky-item'
		});

		jQuery('.sticky-up').stickyPanel({
			stickyType: 'up',
			stickyItem: '.sticky-item'
		});

		jQuery('.sticky-always-top').stickyPanel({
			stickyType: 'always',
			stickyItem: '.sticky-item'
		});

		jQuery('.product-buttons__item_sticky').stickyPanel({
			stickyType: 'always',
			stickyItem: '.sticky-item',
			offStickyBlock: '.product-button_pos_bottom'
		});

		jQuery('.btn-panel_sticky').stickyPanel({
			stickyType: 'initial',
			stickyItem: '.sticky-item',
			offStickyBlock: '.btn-panel'
		});
	}
}

// calc nav height
function initCalcNavHeight () {
	;(function ($) {
		if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40 || isBrowser.isAndroid2 || isBrowser.isBlackberry6 || isBrowser.isBlackberry7 || isBrowser.isWinphone75)) {
			var nav = $('.main-nav, .landing-nav'),
				win = $(window);

			nav.each(function () {
				var curNav = $(this),
					navOffsetTop = nav.parent().outerHeight();
				// calc window height
				function getWindowHeight () {
					return window.innerHeight || document.documentElement.clientHeight;
				}

				function calcNavHeight () {
					curNav.css({
						maxHeight: getWindowHeight() - navOffsetTop - 5
					});
				}

				win.on('load resize orientationchange', function () {
					calcNavHeight();
				});
			});
		}
	}(jQuery));
}

// add/remove class when some radio-button is checked from the list
function initCheckedClasses() {
	;(function ($) {
		var generalContainer = $('.vote-unit'),
			checkedClass = 'checked',
			parentCheckedClass = 'checked-parent',
			generalCheckedClass = 'checked-general',
			pairs = [];

		$('.vote-unit label[for]').each(function(index, label) {
			var input = $('#' + label.htmlFor);
			label = $(label);

			// click handler
			if (input.length) {
				pairs.push({ input:input,label:label });
				input.bind('click change', function() {
					if (input.is(':radio')) {
						$.each(pairs, function(index, pair) {
							refreshState(pair.input, pair.label);
						});
					} else {
						refreshState(input, label);
					}
				});
				refreshState(input, label);
			}
		});

		// refresh classes
		function refreshState(input, label) {
			if (input.is(':checked')) {
				input.parent().addClass(parentCheckedClass);
				label.addClass(checkedClass);

				if (!generalContainer.hasClass(generalCheckedClass)) {
					generalContainer.addClass(generalCheckedClass);
				}
			} else {
				input.parent().removeClass(parentCheckedClass);
				label.removeClass(checkedClass);
			}
		}
	}(jQuery));
}

// open-close init
function initOpenClose() {
	jQuery('.slide-unit').openClose({
		activeClass: 'expanded',
		opener: '.slide-unit__opener',
		slider: '.slide-unit__slide',
		animSpeed: 400,
		effect: 'slide'
	});

	jQuery('.info-unit').openClose({
		hideOnClickOutside: true,
		activeClass: 'opened',
		opener: '.info-unit__js-opener, .info-unit__js-close',
		slider: '.info-unit__popup',
		animSpeed: 400,
		effect: 'none'
	});

	jQuery('.package-item').openClose({
		parentBlock: '.package-list',
		activeClass: 'package-item_state_expanded',
		opener: '.package-item__js-opener',
		slider: '.package-item__slide',
		animSpeed: 400,
		effect: 'slide',
		onInit: function () {
			if (this.holder.hasClass(this.options.activeClass)) {
				this.holder.find('input[name="option_value"]').prop('checked', true);
			}
		},
		animStart: function () {
			if (arguments[0] === true) {
				this.holder.find('input[name="option_value"]').prop('checked', true);
			}
		}
	});

	jQuery('.main-nav-unit').openClose({
		activeClass: 'main-nav-expanded',
		opener: '.nav-opener',
		slider: '.main-nav',
		animSpeed: 200,
		effect: 'slide',
		onInit: function () {
			var landingNav = jQuery('.landing-nav-unit');

			this.opener.on('click', function () {
				if (landingNav.hasClass('landing-nav-expanded')) {
					landingNav.data('OpenClose').hideSlide();
				}
			});

			this.slider.css({
				marginLeft: 0
			});
		},
		animStart: function () {
			if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40)) {
				this.sliderHolder = this.slider.find('.main-nav__holder');

				this.sliderHolder.css({
					opacity: 0
				});
			}
		},
		animEnd: function () {
			if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40)) {
				this.sliderHolder.animate({
					opacity: 1
				}, this.options.animSpeed);
			}
		}
	});

	jQuery('.landing-nav-unit').openClose({
		activeClass: 'landing-nav-expanded',
		opener: '.landing-opener',
		slider: '.landing-nav',
		animSpeed: 200,
		effect: 'slide',
		onInit: function () {
			var mainNav = jQuery('.main-nav-unit');

			this.opener.on('click', function () {
				if (mainNav.hasClass('main-nav-expanded')) {
					mainNav.data('OpenClose').hideSlide();
				}
			});

			this.slider.css({
				marginRight: 0
			});
		},
		animStart: function () {
			if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40)) {
				this.sliderHolder = this.slider.find('.landing-nav__list');

				this.sliderHolder.css({
					opacity: 0
				});
			}
		},
		animEnd: function () {
			if (!(isBrowser.isOperaMini || isBrowser.isNokiaSeries40)) {
				this.sliderHolder.animate({
					opacity: 1
				}, this.options.animSpeed);
			}
		}
	});

	jQuery('.info-icon-unit').openClose({
		activeClass: 'expanded',
		opener: '.icon-info, .info-icon-unit__close',
		slider: '.info-icon-unit__popup',
		animSpeed: 400,
		effect: 'none'
	});

	jQuery('.details-unit').openClose({
		// hideOnClickOutside: true,
		activeClass: 'details-unit_expanded',
		opener: '.details-unit__opener',
		slider: '.details-unit__slide',
		animSpeed: 400,
		effect: 'slide',
		onInit: function () {
			if (this.opener.get(0).tagName.toLowerCase() === 'label') {
				this.options.preventDefault = false;
			}
		}
	});

	jQuery('.view-cart').openClose({
		activeClass: 'view-cart_state_expanded',
		opener: '.view-cart__link, .view-cart__close',
		slider: '.view-cart__slide',
		animSpeed: 400,
		effect: 'slide'
	});

	jQuery('.trial-info').openClose({
		hideOnClickOutside: true,
		activeClass: 'trial-info_expanded',
		opener: '.trial-add-list__help-link, .trial-popup__close',
		slider: '.trial-popup',
		animSpeed: 400,
		effect: 'none'
	});

	jQuery('.trial-trash').openClose({
		hideOnClickOutside: true,
		activeClass: 'trial-trash__expanded',
		opener: '.btn-trash, .trial-list__delete, .trial-note-buttons__btn_cancel',
		slider: '.trial-popup',
		animSpeed: 400,
		effect: 'none'
	});
}

// accordion menu init
function initAccordion() {
	jQuery('.category-unit__list').slideAccordion({
		opener: '.category-unit__link_js_opener',
		slider: '.category-unit__content',
		activeClass: 'expanded',
		animSpeed: 400
	});

	jQuery('.details-section').slideAccordion({
		opener: '.details-item__link_js_opener',
		slider: '.details-item__content',
		activeClass: 'expanded',
		animSpeed: 400
	});
}

// cycle scroll gallery init
function initCycleCarousel() {
	var autoRotation = true;

	if (isBrowser.isOperaMini || isBrowser.isNokiaSeries40) {
		autoRotation = false;
	}

	jQuery('.image-gallery').scrollAbsoluteGallery({
		mask: '.image-gallery__mask',
		slider: '.image-gallery__slideset',
		slides: '.image-gallery__slide',
		generatePagination: '.bullet-nav',
		stretchSlideToMask: true,
		maskAutoSize: true,
		autoRotation: autoRotation,
		switchTime: 5000,
		animSpeed: 800,
		onInit: function () {
			var win = jQuery(window),
				self = this;

			win.on('load', function () {
				if (self.mask.find('img').length) {
					self.mask.css({
						minHeight: 1
					});
				}
			});
		}
	});

	jQuery('.product-images').scrollAbsoluteGallery({
		mask: '.product-images__mask',
		slider: '.product-images__slideset',
		slides: '.product-images__slide',
		generatePagination: '.bullet-nav',
		btnPrev: '.btn-prev',
		btnNext: '.btn-next',
		stretchSlideToMask: true,
		maskAutoSize: true,
		animSpeed: 800
	});
}

// scroll gallery init
function initSlideCarousel() {
	jQuery('.color-choice').scrollGallery({
		mask: '.color-choice__mask',
		slider: '.color-choice__slideset',
		slides: '.color-choice__slide',
		btnPrev: '.btn-prev',
		btnNext: '.btn-next',
		autoRotation: false,
		switchTime: 3000,
		animSpeed: 500,
		step: 3
	});
}

// content tabs init
function initTabs() {
	jQuery('.filter-tabset').contentTabs({
		addToParent: true,
		tabLinks: 'a',
		activeClass: 'filter-tabset__item_state_active'
	});

	jQuery('.tabs-list').contentTabs({
		addToParent: true,
		tabLinks: 'a',
		activeClass: 'tabs-list__item_state_active'
	});
}

// init zoom image
function initZoomImage () {
	jQuery('.product-images').zoomImageGallery({
		bulletsContainer: '.bullet-nav',
		onInit: function () {
			var self = this,
				btnOpenZoom = this.container.find('.product-images__open-zoom');

			btnOpenZoom.on('click', function (e) {
				e.preventDefault();
				var curIndex = self.container.data('ScrollAbsoluteGallery').currentIndex;
				self.createZoom(curIndex);
			});
		}
	});
}

// init 180 view
function initViewImage () {
	jQuery('.view-image').viewImage({
		imageContainer: '.view-image__image',
		mask: '.view-image__mask',
		slideset: '.view-image__slideset',
		btnPrev: '.view-image__prev',
		btnNext: '.view-image__next',
		activeSectionClass: 'view-image_state_active'
	});
}

// remove unnecessary tips for 180 view
function initRemoveTips () {
	;(function ($) {
		var tipHolder = $('.view-tip'),
			tipTilt = '.view-tip__tilt',
			tipSwipe = '.view-tip__swipe',
			tipClickBtn = '.view-tip__click-btn',
			isTouchDevice = ('ontouchstart' in window) || window.DocumentTouch && document instanceof DocumentTouch,
			isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent),
			isTouchEv = isTouchDevice || (isWinPhoneDevice && navigator.pointerEnabled) || (isWinPhoneDevice && navigator.msPointerEnabled),
			isOrientationEv = ('ondeviceorientation' in window) || ('onMozOrientation' in window);

		tipHolder.each(function () {
			var curTip = $(this),
				curTilt = curTip.find(tipTilt),
				curSwipe = curTip.find(tipSwipe),
				curClickBtn = curTip.find(tipClickBtn);

			if (isTouchEv) {
				curClickBtn.remove();
			} else {
				curSwipe.remove();
			}

			if (!isOrientationEv) {
				curTilt.remove();
			}
		});
	}(jQuery));
}

// init switch views
function initSwitchViews () {
	;(function ($) {
		var win = $(window),
			viewsSection = $('.view-image-section'),
			viewTabsName = '.view-image',
			tabsetName = '.image-tabset',
			viewMaskName = '.view-image__mask',
			activeTabClass = 'view-image_state_active';

		viewsSection.each(function () {
			var curSection = $(this),
				tabsBlocks = curSection.find(viewTabsName),
				viewMask = curSection.find(viewMaskName),
				tabset = curSection.find(tabsetName);

			// set min-height on tabs section
			function setMinHeight () {
				tabsBlocks.css({
					minHeight: viewMask.height()
				});
			}

			setMinHeight();

			tabset.find('a').each(function () {
				var curLink = $(this);

				curLink.on('click', function (e) {
					e.preventDefault();

					var curTabBlock = viewsSection.find(curLink.attr('href'));

					if (!curTabBlock.hasClass(activeTabClass)) {
						viewsSection.find('.' + activeTabClass).removeClass(activeTabClass);
						curTabBlock.addClass(activeTabClass);

						if (!curTabBlock.data('ViewImage').imagesLoaded) {
							curTabBlock.data('ViewImage').loadImages();
						}
					}
				});
			});

			win.on('resize orientationchange', function () {
				setMinHeight();
			});
		});
	}(jQuery));
}

// add filter
function initChoseFilters () {
	;(function ($) {
		var filtersBlock = $('.filters-block'),
			filterContainers = filtersBlock.find('.filter-container'),
			chosenHolder = filtersBlock.find('.chosen-filters'),
			clearButton = filtersBlock.find('.btn-filter_type_reset'),
			checkItemsName = '.filter-value',
			labelTextName = '.filter-text',
			filterItemClass = 'filter-tag',
			filterItemHolderName = 'filter-tag__holder',
			filterItemStructure = '<span class="' + filterItemClass + '"></span>',
			closeButton = '<span class="close">Close</span>',
			itemsQtyHolderName = '.filter-items-qty',
			itemsQtyEnabledName = 'filter-items-qty_state_enabled',
			chosenItems = $();

		filterContainers.each(function () {
			var currContainer = $(this),
				checkItems = currContainer.find(checkItemsName),
				tabItem = $('[href="#' + currContainer.attr('id') + '"]'),
				itemsQtyHolder = tabItem.find(itemsQtyHolderName),
				curChosenItems = $(),
				itemsQty = 0;

			checkItems.each(function () {
				var currItem = $(this),
					filterItem = '',
					labelText;

				function chosenHanlder() {
					if (currItem.prop('checked')) {
						chosenItems = chosenItems.add(currItem);
						curChosenItems = curChosenItems.add(currItem);

						if (filterItem.length) {
							filterItem.appendTo(chosenHolder);
						} else {
							labelText = $('[for="' + currItem.attr('id') + '"]')
								.find(labelTextName)
								.clone();

							filterItem = $(filterItemStructure)
								.append(labelText)
								.append(closeButton)
								.wrapInner('<span class="' + filterItemHolderName + '"></span>')
								.appendTo(chosenHolder);

							filterItem.data('checkbox', currItem);
						}
					} else {
						if (filterItem.length) {
							chosenItems = chosenItems.not(currItem);
							curChosenItems = curChosenItems.not(currItem);
							filterItem.detach();
						}
					}

					itemsQty = curChosenItems.length;

					if (itemsQty) {
						itemsQtyHolder.text(itemsQty).addClass(itemsQtyEnabledName);
					} else {
						itemsQtyHolder.text(itemsQty).removeClass(itemsQtyEnabledName);
					}
				}

				chosenHanlder();
				currItem.on('change', chosenHanlder);
			});
		});

		chosenHolder.on('click', '.' + filterItemClass, function (e) {
			e.preventDefault();
			var curItem = $(this),
				closelyCheckbox = curItem.data('checkbox');

			closelyCheckbox.prop('checked', false).change();
		});

		clearButton.on('click', function (e) {
			e.preventDefault();

			if (chosenItems.length) {
				chosenItems.prop('checked', false).change();
			}
		});
	}(jQuery));
}

// remove message
function initRemoveMessage () {
	;(function ($) {
		var message = $('.thank-message, .global-error'),
			btnCloseName = '.thank-message__js-close, .global-error__close';

		message.each(function () {
			var curMessage = $(this),
				btnClose = curMessage.find(btnCloseName);

			btnClose.on('click', function (e) {
				e.preventDefault();
				curMessage.remove();
			});
		});
	}(jQuery));
}

// remove alert messages
function initRemoveAlerts () {
	;(function ($) {
		var removeItems = $('[data-remove-seconds]'),
			animSpeed = 1000;

		removeItems.each(function () {
			var curItem = $(this),
				delay = curItem.data('remove-seconds') * 1000;

			function removeItem () {
				curItem.animate({
					opacity: 0,
					marginTop: -(curItem.outerHeight() + parseFloat(curItem.css('margin-bottom')))
				}, animSpeed, function () {
					curItem.remove();
				});
			}

			setTimeout(removeItem, delay);
		});
	}(jQuery));
}

// remove error
function initRemoveError () {
	;(function ($) {
		var requiredFields = $('.required'),
			errorClass = 'error',
			errorItemName = '.' + errorClass;

		requiredFields.each(function () {
			var curField = $(this);

			function removeError () {
				var errorItem = curField.closest(errorItemName);

				if (errorItem.length) {
					errorItem.removeClass(errorClass);
				}
			}

			curField.on('focus', removeError);
		});
	}(jQuery));
}

// init packages accordion
function initPackageAccordion () {
	;(function ($) {
		var accordion = $('.package-list'),
			accordionItem = '.package-item',
			activeItem = '.package-item_state_expanded',
			openerName = '.package-item__js-opener';

		accordion.each(function () {
			var curAccordion = $(this);

			curAccordion.on('click', openerName, function () {
				var curOpener = $(this),
					curAccordionItem = curOpener.closest(accordionItem),
					curActive = curAccordion.find(activeItem).not(curAccordionItem);

				if (curActive.length) {
					curActive.data('OpenClose').hideSlide();
				}
			});
		});
	}(jQuery));
}

// show all packages or show default package
function initShowPackages () {
	;(function ($) {
		var btnShowDefault = $('.show-default-package'),
			btnViewAll = $('.view-all-packages'),
			packageList = $('.package-list'),
			packageItemOpener = '.package-item__js-opener',
			defaultPackage = packageList.find('.package-item_type_default'),
			expandedClass = '.package-item_state_expanded';

		function checkButtons () {
			if (packageList.has(expandedClass).length) {
				btnShowDefault.hide();
				btnViewAll.show();
			} else {
				btnShowDefault.show();
				btnViewAll.hide();
			}
		}

		checkButtons();

		btnShowDefault.on('click', function (e) {
			e.preventDefault();
			defaultPackage.data('OpenClose').showSlide();
			checkButtons();
		});

		btnViewAll.on('click', function (e) {
			e.preventDefault();
			packageList.find(expandedClass).data('OpenClose').hideSlide();
			checkButtons();
		});

		packageList.on('click', packageItemOpener, function () {
			checkButtons();
		});


	}(jQuery));
}

// show more items
function initShowMoreItems () {
	;(function ($) {
		var holder = $('.features-unit'),
			holderList = '.features-list',
			holderItems = '.features-list__item',
			lessClass = 'features-list_state_less',
			showBtn = '.btn-show-more',
			btnLessClass = 'btn-show-more_state_less';

		holder.each(function () {
			var curHolder = $(this),
				curHolderList = curHolder.find(holderList),
				curItems = curHolderList.find(holderItems),
				curBtn = curHolder.find(showBtn),
				showQty = curHolderList.data('less-qty');

			if (curHolderList.hasClass(lessClass)) {
				for (var i = showQty; i < curItems.length; i++) {
					curItems.eq(i).hide();
				}
			}

			function showItems () {
				for (var i = showQty; i < curItems.length; i++) {
					curItems.eq(i).slideDown();
				}

				curHolderList.removeClass(lessClass);
			}

			function hideItems () {
				for (var i = showQty; i < curItems.length; i++) {
					curItems.eq(i).slideUp();
				}

				curHolderList.addClass(lessClass);
			}

			curBtn.on('click', function (e) {
				e.preventDefault();
				if (curHolderList.hasClass(lessClass)) {
					showItems();
				} else {
					hideItems();
				}

				curBtn.toggleClass(btnLessClass);
			});
		});
	}(jQuery));
}

// show password
function initShowPassword () {
	;(function ($) {
		var checkElements = $('[data-show-pass]');

		checkElements.each(function () {
			var curElem = $(this),
				targetElem = $('#' + curElem.data('show-pass')),
				targetConfirmElem = $('#' + targetElem.data('confirm-pass')),
				target = targetElem.add(targetConfirmElem);

			curElem.on('change', function () {
				if (curElem.is(':checked')) {
					target.attr('type', 'text');
				} else {
					target.attr('type', 'password');
				}
			});
		});
	}(jQuery));
}

// go to review section
function initGoToReview () {
	;(function ($) {
		var link = $('.go-to-review'),
			page = $('body, html'),
			targetOpenedName = 'expanded',
			addShift = 5,
			animSpeed = 1000,
			delay = 700,
			isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent);

		link.each(function () {
			var curLink = $(this),
				targetBlock = $(curLink.attr('href')),
				offset;

			curLink.on('click', function (e) {
				e.preventDefault();

				if (targetBlock.length) {
					if (!targetBlock.hasClass(targetOpenedName)) {
						targetBlock.data('AccordionSlide').toggleSlide();
					}

					setTimeout(function() {
						offset = targetBlock.offset().top - addShift;

						if (isWinPhoneDevice) {
							page.scrollTop(offset);
						} else {
							page.stop().animate({
								scrollTop: offset
							}, animSpeed);
						}
					}, delay);
				}
			});
		});
	}(jQuery));
}

// remove products loader
function initRemoveProductLoader () {
	;(function ($) {
		var items = $('.product-item__image');

		items.each(function () {
			var curItem = $(this);

			imagesLoaded(curItem, function () {
				curItem.css({
					backgroundImage: 'none'
				});
			});
		});
	}(jQuery));
}

// init loader
function initLoader () {
	var loader = $('.loader');

	loader.each(function () {
		var curLoader = $(this),
			sceneQty = 12,
			counter = sceneQty,
			delta = -24,
			step = delta,
			animSpeed = 80,
			counterFlag;

		var loaderPlugin = {
			startLoader: function () {
				if (!curLoader.is(':visible')) {
					curLoader.show();

					this.interval = setInterval(function () {
						curLoader.css({
							backgroundPosition: '0 ' + step + 'px'
						});

						step += delta;
						counter -= 1;
						counterFlag = counter - 1;

						if (!(counterFlag)) {
							step = 0;
							counter = sceneQty;
						}
					}, animSpeed);
				}
			},
			destroyLoader: function () {
				var self = this;
				curLoader.hide();
				clearInterval(self.interval);
			}
		};

		curLoader.data('LoaderPlugin', loaderPlugin);
	});
}

// start btn loader
function initStartBtnLoader () {
	;(function ($) {
		var btn = $('.btn-show-loader'),
			loaderName = '.loader',
			loadingClass = 'loading';

		btn.each(function () {
			var curBtn = $(this),
				loader = curBtn.next(loaderName);

			curBtn.on('click', function () {
				setTimeout(function() {
					if (!curBtn.data('stopLoader')) {
						curBtn.addClass(loadingClass);
						loader.data('LoaderPlugin').startLoader();
					}
				}, 10);
			});
		});
	}(jQuery));
}

// date input
function initInputDate () {
	;(function ($) {
		var input = $('.date-mask'),
			placeholder = 'dd/mm/yyyy',
			birthClass = 'date-birth',
			regDate = /^((0{1}[1-9]{1})|([1-2]{1}[0-9]{1})|(3{1}[0-1]{1}))\/((0{1}[0-9])|(1{1}[0-2]{1}))\/[0-9]{4}$/, // date in format DD/MM/YYYY also check valid value for Day and Month
			actualDate = new Date(),
			actualYear = parseFloat(actualDate.getFullYear()),
			actualMonth = actualDate.getMonth() + 1,
			actualDay = actualDate.getDate(),
			minBirthYear = actualYear - 150,
			maxBirthYear = actualYear,
			errorInputClass = 'input-error',
			errorMessageText = '<span class="error-message error-message_date"><span class="error-message__holder">Unreal date of birth</span></span>';

		input.each(function () {
			var curInput = $(this),
				initialVal = curInput.val(),
				errorMessage = $(errorMessageText);

			curInput.mask('99/99/9999', {
				placeholder: placeholder,
				completed: function () {
					var curValue = this.val(),
						dateFlag = regDate.test(curValue),
						yearFlag,
						monthFlag,
						dayFlag,
						curYear,
						curMonth,
						curDay;

					if (this.hasClass(birthClass)) {
						if (dateFlag) {
							curYear = parseFloat(curValue.slice(-4));
							curMonth = parseFloat(curValue.slice(3, 5));
							curDay = parseFloat(curValue.slice(0, 3));

							// check correct year
							yearFlag = (curYear > minBirthYear) && (curYear <= maxBirthYear);

							// check correct month
							monthFlag = (curYear === actualYear) && (curMonth > actualMonth);

							// check correct day
							dayFlag = (curYear === actualYear) && (curMonth === actualMonth) && curDay > actualDay;

							setState(this, !yearFlag, errorMessage);

							if (yearFlag) {
								setState(this, monthFlag, errorMessage);
							}

							if (yearFlag && !monthFlag) {
								setState(this, dayFlag, errorMessage);
							}

							return;
						}
					}

					setState(this, !dateFlag, errorMessage);
				}
			});

			curInput.val(initialVal);

			curInput.on('blur', function () {
				if (curInput.val() === '') {
					curInput.val(initialVal);
				}
			});
		});


		// additional functions

		function setState(field, error, message) {
			function clearField () {
				field
					.val('')
					.removeClass(errorInputClass);

				message.detach();
			}

			if (error) {
				field.addClass(errorInputClass);

				message
					.insertAfter(field)
					.css({
						display: 'block'
					});

				field.on('blur.clear', clearField);
			} else {
				field
					.off('blur.clear')
					.removeClass(errorInputClass);
				message.detach();
			}
		}
	}(jQuery));
}

// date input
function initInputExpiryDate () {
	;(function ($) {
		var input = $('.expiry-date-mask');

		input.each(function () {
			var curInput = $(this);

			curInput.mask('99/99', {
				placeholder: ''
			});
		});
	}(jQuery));
}

// phone input
function initInputPhone () {
	;(function ($) {
		var input = $('.input-row input[type="tel"]');

		input.each(function () {
			var curInput = $(this);

			curInput.mask('9999 99999999', {
				placeholder: '',
				completed:function() {},
				autoclear: false
			});
		});

		// make phone number without spaces
		var phoneInputs = $('.reformat-phone');

		phoneInputs.each(function () {
			var curInput = $(this),
				connectInput = $(curInput.data('target')),
				form = curInput.closest('form');

			function setFormatValue () {
				curInput.val(connectInput.val().split(' ').join(''));
			}

			connectInput.on('input keydown keypress paste', setFormatValue);

			form.on('submit', setFormatValue);
		});
	}(jQuery));
}

// phone input
function initInputCard () {
	;(function ($) {
		var input = $('.card-mask');

		input.each(function () {
			var curInput = $(this);

			curInput.mask('9999 9999 9999 9999', {
				placeholder: '',
				autoclear: false
			});
		});

		// make phone number without spaces
		var reformatInput = $('.reformat-card');

		reformatInput.each(function () {
			var curInput = $(this),
				connectInput = $(curInput.data('target')),
				form = curInput.closest('form');

			function setFormatValue () {
				curInput.val(connectInput.val().split(' ').join(''));
			}

			connectInput.on('input keydown keypress paste', setFormatValue);

			form.on('submit', setFormatValue);
		});
	}(jQuery));
}

// checkbox open-close
function initCheckOpenClose () {
	;(function ($) {
		var isWinPhoneDevice = /Windows Phone/.test(navigator.userAgent),
			page = $('body, html');

		$('.js-check-collapse').checkOpenClose({
			animSpeed: 400,
			onInit: function () {
				var self = this,
					openerName = this.opener.attr('name'),
					allRadios = $('input[type="radio"][name="' + openerName + '"]'),
					fields = self.container.find('select, .input-text').filter(function () {
						return !$(this).parentsUntil(self.container).filter('.js-check-collapse').length;
					});

				// trigger change event on radios
				allRadios.each(function () {
					var curRadio = $(this);

					curRadio.on('change', function () {
						if (curRadio.prop('checked')) {
							allRadios.not(curRadio).change();
						}
					});
				});

				// disable/enable fields if checkbox is unchecked/checked
				function disableFields () {
					if (fields.length) {
						var opener = self.opener;

						if (opener.prop('checked')) {
							fields
								.prop('disabled', false)
								.attr('disabled', false);
						} else {
							fields
								.each(function (index, el) {
									if (el.selectedIndex) {
										el.selectedIndex = 0;
									}

									if (el.tagName.toLowerCase() === 'input') {
										el.value = '';
									}
								})
								.blur()
								.prop('disabled', true)
								.attr('disabled', true);
						}
					}
				}

				disableFields();

				this.opener.on('change', function () {
					disableFields();
				});

				// scrolling flag for check-tabs
				var checkTabs = this.container.closest('.check-tabs');

				if (checkTabs.length && this.container.hasClass('check-tabs-content')) {
					this.options.delay = this.options.animSpeed;

					if (!checkTabs.data('pageScrollFlag') && checkTabs.data('pageScrollFlag') !== false) {
						checkTabs.data('pageScrollFlag', false);
					}

					this.parentBlock = checkTabs;
				}
			},
			onShowSlideEnd: function () {
				// scroll to curent tab after slide appear
				if (this.parentBlock) {
					if (this.parentBlock.data('pageScrollFlag')) {
						var topOffset = this.opener.offset().top - 10;

						if (isWinPhoneDevice) {
							page.scrollTop(topOffset);
						} else {
							page.stop().animate({
								scrollTop: topOffset
							}, 500);
						}
					} else {
						this.parentBlock.data('pageScrollFlag', true);
					}
				}
			}
		});
	}(jQuery));
}

// check radio button when input type file change
function initCheckTypefile () {
	;(function ($) {
		var file = $('input[type="file"][data-for]');

		file.each(function () {
			var curFile = $(this),
				curCheck = $('#' + curFile.data('for')),
				siblingsCheck = $('[name="' + curCheck.attr('name') + '"]').not(curCheck);

			curFile.on('change', function () {
				if (curFile.val() !== '') {
					curCheck.prop('checked', true);
					siblingsCheck.change();
				} else {
					curCheck.prop('checked', false);
				}
			});
		});
	}(jQuery));
}

// clear input type file value
function initClearTypefile () {
	;(function ($) {
		var section = $('.enter-section'),
			label = section.find('.enter-unit_manually'),
			file = section.find('.enter-unit_upload').find('input[type="file"]');

		label.on('click', function () {
			file.val('');
		});
	}(jQuery));
}

// upload file and submit form
function initBtnUpload () {
	;(function ($) {
		var btn = $('.upload-btn_retake');

		btn.each(function () {
			var curBtn = $(this),
				form = curBtn.find('form'),
				file = curBtn.find('input[type="file"]'),
				loader = curBtn.find('.loader');

			file.on('change', function () {
				if (file.val() !== '') {
					loader.data('LoaderPlugin').startLoader();
					form.submit();
				}
			});
		});
	}(jQuery));
}

// remove section
function initRemoveBlock () {
	;(function ($) {
		var removeBtn = $('.remove-section');

		removeBtn.on('click', function (e) {
			e.preventDefault();
			$(this).parent().remove();
		});
	}(jQuery));
}

// inputs effect
function initInputsEffect () {
	;(function() {
		var inputs = $('input.input-text, textarea.input-textarea'),
			selects = $('select.effect-select'),
			fillClass = 'input--filled',
			parentName = '.input-row';

		inputs.each(function () {
			var inputEl = $(this);

			if (inputEl.val().trim() !== '') {
				inputEl.closest(parentName).addClass(fillClass);
			}

			// events:
			inputEl.on('focus', onElementFocus);
			inputEl.on('blur', onInputBlur);
		});

		selects.each(function (index, el) {
			var selEl = $(this);

			if (el.selectedIndex !== 0) {
				selEl.closest(parentName).addClass(fillClass);
			}

			// events:
			selEl.on('focus', onElementFocus);
			selEl.on('blur', onSelectBlur);
		});

		function onElementFocus() {
			$(this).closest(parentName).addClass(fillClass);
		}

		function onInputBlur() {
			var inputEl = $(this);

			if (inputEl.val().trim() === '') {
				inputEl.closest(parentName).removeClass(fillClass);
				inputEl.val(''); // need for input type="number" when not number entered
			}
		}

		function onSelectBlur() {
			var selEl = this;

			if (selEl.selectedIndex === 0) {
				$(selEl).closest(parentName).removeClass(fillClass);
			}
		}
	})();
}

// add/remove class when input filled
function addFilledInputClass () {
	;(function ($) {
		var items = $('.input-row_with-btn'),
			inputName = '.input-text',
			fillingClass = 'input-filling',
			appliedClass = 'input-applied';

		items.each(function () {
			var curItem = $(this),
				curInput = curItem.find(inputName);

			// if (curInput.val() !== '') {
			// 	curItem.addClass(appliedClass);
			// }

			curInput.on('input', function () {
				if (curInput.val() !== '' && !curItem.hasClass(fillingClass)) {
					curItem.addClass(fillingClass);
				} else if (curInput.val() === '' && curItem.hasClass(fillingClass)) {
					curItem.removeClass(fillingClass);
				}

				if (curItem.hasClass(appliedClass)) {
					curItem.removeClass(appliedClass);
				}
			});
		});
	}(jQuery));
}

// page loader
function initPageLoader () {
	;(function ($) {
		var loaderBox = $('.page-loader'),
			loader = loaderBox.find('.loader'),
			mainBlock = $('#wrapper'),
			body = $('body'),
			win = $(window),
			loadingClass = 'page-loading',
			animSpeed = 700;

		function getWindowHeight () {
			return window.innerHeight || document.documentElement.clientHeight;
		}

		var pageLoaderPlugin = {
			show: function () {
				body.addClass(loadingClass);

				loaderBox.height(mainBlock.height());
				loaderBox.fadeIn(animSpeed);

				loader.css({
					top: win.scrollTop() + getWindowHeight() / 2 - loader.height() / 2
				});

				loader.data('LoaderPlugin').startLoader();
			},
			hide: function () {
				body.removeClass(loadingClass);

				loader.data('LoaderPlugin').destroyLoader();

				loaderBox.fadeOut(animSpeed);
			}
		};

		loaderBox.data('pageLoaderPlugin', pageLoaderPlugin);
	}(jQuery));
}

// trial message
function initPageMessage () {
	;(function ($) {
		var messageUnit = $('.message-unit'),
			message = messageUnit.find('.message-unit__message'),
			mainBlock = $('#wrapper'),
			animSpeed = 700,
			delay = messageUnit.data('remove-time') * 1000 || 3000 + animSpeed,
			win = $(window);

		var messagePlugin = {
			show: function () {
				var self = this;

				messageUnit.height(mainBlock.height());
				message.css({
					top: win.scrollTop()
				});
				messageUnit.fadeIn(animSpeed);

				setTimeout(function() {
					self.hide();
				}, delay);

				messageUnit.on('click', function () {
					if ($(event.target).closest(message).length) {
						return;
					}

					self.hide();
				});
			},
			hide: function () {
				messageUnit.fadeOut(animSpeed);
			}
		};

		messageUnit.data('MessagePlugin', messagePlugin);
	}(jQuery));
}

function selectOpenClose () {
	;(function ($) {
		var selects = $('select[data-select-slide]'),
			openerClass = 'js-select-opener';

		selects.each(function () {
			var select = this,
				$select = $(this),
				options = $select.find('option'),
				slideAttr = $select.data('select-slide'),
				slide = $('[data-slide=' + slideAttr + ']'),
				openerNumber = 0;

			options.each(function (index, el) {
				if ($(el).hasClass(openerClass)) {
					openerNumber = index;
				}
			});

			$select.on('change', function () {
				if (select.selectedIndex === openerNumber) {
					slide.slideDown();
				} else if (slide.is(':visible')) {
					slide.slideUp();
				}
			});
		});
	}(jQuery));
}

function initTrialPopup () {
	;(function ($) {
		var holder = $('.trial-buttons'),
			openerClass = 'btn-trial_popup';

		holder.each(function () {
			var curHolder = $(this),
				btnOpen = curHolder.find('.btn-trial'),
				btnClose = curHolder.find('.trial-note-buttons__btn_no'),
				popup = curHolder.find('.trial-buttons__popup');

			btnOpen.on('click', function (e) {
				if (btnOpen.hasClass(openerClass)) {
					e.preventDefault();
					btnOpen.data('stopLoader', true);
					popup.slideDown();
				}
			});

			btnClose.on('click', function (e) {
				e.preventDefault();
				popup.slideUp();
			});
		});
	}(jQuery));
}
