// dkrt
var path="http://d37l6i9uhpr090.cloudfront.net/media/vtohtml5_final/";
// lenskart
var spath = "http://www.lenskart.com/media/vtohtml5_final/";

var flashvars = {
	width:"218",
	plwidth:"175",
	height:"300",
	pwidth:"290",
	pheight:"325",
	swfpath:path+"jsCore/vtolite.swf",
	expressinstallpath:path+"expressInstall.swf",
	chromeColor:"#ffffff",
	fontcolor:"#000000",
	panelcolors:"#f6f6f3, #f6f6f3",
	panelalphas:"1, 1",
	panelborder:"1",
	panelbordercolor:"#c6c5c4",
	panelheight:"50",
	buttons:"browse*"+path+"assets/browse.png,webcam*"+path+"assets/webcam.png",
	buttonstooltip:"browse*Click to browse,webcam*Take a photo,modelgallery*Choose Model",
	takepicicon:path+"assets/digital_camera.png",
	takepicicontooltip:"Click to Shoot",
	closebutton:path+"assets/cancel.png",
	closevto:path+"assets/close.png",
	okbutton:path+"assets/okbutton.png",
	cursorerror:"http://d37l6i9uhpr090.cloudfront.net/media/wysiwyg/blanco/no.png",

	psavebutton:path+"assets/psave.jpg",
	savebutton:path+"assets/save.png",
	savebuttontooltip:"Click to save",
	printbutton:path+"assets/print.png",
	printbuttontooltip:"Click to print",
	pfbbutton:path+"assets/pfb.jpg",
	fbbutton:path+"assets/fb.png",
	fbbuttontooltip:"Click to Share",
	ptweetbutton:path+"assets/ptweet.jpg",
	tweetbutton:path+"assets/tweet.png",
	tweetbuttontooltip:"Click to tweet",
	tooltiparrow:path+"assets/arrow.png",
	showcomparetext:"Compare",
	showcomparetooltip:"Show compare",

	zoom:"Zoom",
	rotate:"Rotate",
	okbuttontooltip:"Done",
	cancelbuttontooltip:"Cancel",
	closebuttontooltip:"Close",
	adjustmenterror:"Please set the pupil location!",
	noCamMsg:"NO cam found! please add one and it will automatically removed, or click at close button in panel",
	modelsimages:path+"assets/female1.jpg,"+path+"assets/female2.jpg,"+path+"assets/female3.jpg,"+path+"assets/male1.jpg,"+path+"assets/male2.jpg,"+path+"assets/male3.jpg",
	modeldata:"74-94:129-94, 84-48:134-47, 84-105:130-105, 88-85:129-85, 84-115:142-115, 78-94:132-94",
	pmodelsimages:path+"assets/female1.jpg,"+path+"assets/female2.jpg,"+path+"assets/female3.jpg,"+path+"assets/male1.jpg,"+path+"assets/male2.jpg,"+path+"assets/male3.jpg",
	pmodeldata:"104-100:173-100, 115-42:180-39, 112-116:174-116, 117-87:170-87, 114-128:190-126, 105-102:176-102",
	defaultmodel:"4",
	gacode:"", // paste your Google analytics code here like ours is "UA-4397897-6" 
	loader:path+"assets/loader.gif",
	leye:path+"assets/l.png",
	reye:path+"assets/r.png",
	lt:path+"assets/lt.png",
	rt:path+"assets/rt.png",
	rArrow:path+"assets/rightArrow.jpg",
	lArrow:path+"assets/leftArrow.jpg",
	tArrow:path+"assets/topArrow.png",
	dArrow:path+"assets/downArrow.png",
	reset:path+"assets/reset.png",
	max:path+"assets/max.png",
	min:path+"assets/min.png",
	rright:path+"assets/rRight.png",
	rleft:path+"assets/rLeft.png",
	uploadtooltip:"USE YOUR PHOTO",
	uploadtext:"USE YOUR PHOTO",
	uploadsrc:path+"assets/upload.jpg",
	compare:"3",
	comparebtn:path+"assets/compare.png",
	compareclose:path+"assets/compareclose.png",
	comparetext:"ADD TO COMPARE",
	comparetooltip:"ADD TO COMPARE",
	tooltipArrow:path+"assets/arrow2.gif",
	browsetext:"Upload Your Photo",
	browsetooltip:"Click to browse",
	webcamtext:"Take Your Photo (Webcam)",
	webcamtooltip:"Take a photo",
	vtopath:path,
	vtospath:spath,

	sharelabel:"Share - ",
	psharelabel:"SHARE - ",
	pmodeltext:"SELECT A MODEL",
	rlogo:path+"assets/logo_lens.png",
	llogo:path+"assets/sslogo.png",
	crossimg:path+"assets/cross.png",
};
