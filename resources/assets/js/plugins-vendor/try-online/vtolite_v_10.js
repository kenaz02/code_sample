var vtolite = function(id,img,pid,price){
	/* for VTO on product page */
	is_p = false;
	 /* for VTO on product page */
	var o = {
		init: function() {
			/*get id obj*/
			this.obj = jsDom.getNode(id);
			this.obj.style.height = (parseInt(flashvars.height)+parseInt(flashvars.panelheight))+'px';
			this.obj.style.width = parseInt(flashvars.width)+'px';
			this.id = 'vto'+this.obj.id;
			if(this.obj.innerHTML == ""){
				if(jsDom.getNode("vtoliteLogo")){
					jsDom.css("vtoliteLogo",{
						width:flashvars.width+"px",
						height:"20px"
					});
				}
				var imgsnapbtm;
				imgsnapbtm = 3*parseInt(flashvars.panelheight);
				
				var htmlContainer	='';
					htmlContainer	+='<div id="previewLoader" style="height:'+flashvars.height+'px; display:none"><img src="'+flashvars.loader+'" align="center" style="position:absolute;left:'+((flashvars.width-32)/2)+'px;top:'+((flashvars.height-32)/2)+'px; "></div>';
					htmlContainer	+='<div id="imgSnap" style="position:absolute;right:0px;bottom:'+(imgsnapbtm)+'px;display:none;z-index:8;"></div>';
					htmlContainer	+='<div id="preview" style="width:'+(flashvars.width-2)+'px;height:'+(flashvars.height-flashvars.panelheight)+'px;border:1px solid #ccc;overflow:hidden;position:absolute;"></div>';
				jsDom.html(this.obj,htmlContainer);
				/*check htnl5 or flash*/
				var check = this.detectCompatibility();
				switch(check){
					case 'html5':
						/*call html5*/
						this.setup_html5(this.obj,img,pid,price,is_p);
					break;
					case 'flash':
						/*call flash*/
						this.setup_flash(this.obj,img,pid,price,is_p);
					break;
					default:
						/*error*/
						this.setup_error(this.obj);
					break;
				}
			}else{
				this.show(img);
			}
		},
		/*detection*/
		detectCompatibility:function(){
			var detect = null;
			if(supportsLocalStorage && window.File && window.FileReader && window.FileList && window.Blob){
				detect = 'html5';
			}
			return detect;
		},
		setup_html5:function(obj,img,pid,price,is_p){ 
			vtocore.init(obj,img,pid,price,is_p);
		},
		setup_flash:function(obj,img,pid,price,is_p){
			jsDom.css('vto_button_' + pid,{right:" -17px",top:"1px"});
			var i = jsDom.getNode("productimgvto_"+pid);
			jsDom.css(i,{marginLeft:"0px"});
			jsDom.css(i.parentNode,{width:"100%"});
			var strUrl = img+','+price +',http://'+window.location.hostname+'/catalog/product/view/id/'+pid;
			VTO_APPLYFRAME('left',pid,strUrl,price);
		},
		setup_error:function(obj){
			jsDom.html(obj.querySelector('#preview'), '<div class="preview-error">Your browser does not support some of the features provided by www.lenskart.com.<br><br>You may add flash by clicking <a href="http://www.adobe.com/go/getflashplayer">here</a> or you may use Google Chrome</div>');
		},
		destroy:function(id){
			
		},
		show: function(img){
			/*check htnl5 or flash*/
			var check = this.detectCompatibility();
			switch(check){
				case 'html5':
					/*call html5*/
					vtocore.applyFrame(img);
				break;
				case 'flash':
					/*call flash*/
					frameImg = img;
					vtoFlashId = this.id;
					//if(vtoFlash)
					//	document.getElementById(this.id).updateFrame(img);
				break;
				default:
					/*error*/
				break;
			}
		}
	};
	o.init();
	return o;
};

function HTML_updateFrame(align, $id, $img, $price) {
	html_vto_id = $id;

	document.getElementById('HTML_VTOCONTAINER').style.height = "350px";
	document.getElementById('HTML_VTOCONTAINER').style.width = "255px";

	var p = jsDom.getNode('HTML_VTOCONTAINER');
	var id = p.querySelector('#vtolite' + $id);
	if (!id) {
		id = jsDom.createNode('div', {
			id: 'vtolite' + $id
		}, null, 'parent', p);
		jQuery('#vtolite' + $id).addClass('vtolite');
	}
	vtoContainer['vtolite' + p.id] = true;
	vtolite(id, $img, $id, $price);
	jQuery("#tArrow").css("z-index", "9");
	jQuery("#dArrow").css("z-index", "9");
	jQuery("#imgFrame").css("z-index", "9");
	HTML_VTO_ID = $id;

	document.getElementById('compare').style.display = 'none';
}