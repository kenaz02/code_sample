function jsAjax() {
    if (window.XMLHttpRequest) {
        xmlhttp = new XMLHttpRequest
    } else {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP")
    }
    return xmlhttp
}

function saveVTO(e, t) {
    var n = jsAjax();
    var r = flashvars.vtospath + "vtosaving.php";
    var i = "vto_image=" + e;
    n.open("POST", r, true);
    n.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
    n.send(i);
    xmlDoc = n.responseXML;
    n.onreadystatechange = function () {
	console.log(n.responseText.toString());
        if (n.readyState == 4 && n.status == 200) {
            t(n.responseText.toString())
        }
    }
}
/*
u url
cb call back function
*/ 
function get64(u,ret,cb) {
	if(ret){
		cb(u);
	}else{
	    var n1 = jsAjax();
	    var r1 = flashvars.vtospath+"getImage.php";
	    var i1 = "img="+encodeURIComponent(u);
	    n1.open("POST", r1, true);
	    n1.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
	    n1.send(i1);
	    xmlDoc = n1.responseXML;
	    n1.onreadystatechange = function () {
		if (n1.readyState == 4 && n1.status == 200) {
		console.log(n1.responseText.toString());
		    cb(n1.responseText.toString())
		}
	    }
	}
}

function dragCallback() {
    alert("dragCallback")
}

function addEventSimple(e, t, n) {
    if (e.addEventListener) e.addEventListener(t, n, false);
    else if (e.attachEvent) e.attachEvent("on" + t, n)
}

function removeEventSimple(e, t, n) {
    if (e.removeEventListener) e.removeEventListener(t, n, false);
    else if (e.detachEvent) e.detachEvent("on" + t, n)
}

function checktouch() {
    return "ontouchstart" in document.documentElement
}

function supportsLocalStorage() {
    try {
        return "localStorage" in window && window["localStorage"] !== null
    } catch (e) {
        return false
    }
}

function getStrData(e) {
    return localStorage.getItem(e)
}

function setStrData(e, t) {
    return localStorage.setItem(e, t)
}

function TBPosition(e, t) {
    this.X = e;
    this.Y = t;
    this.Add = function (e) {
        var t = new TBPosition(this.X, this.Y);
        if (e != null) {
            if (!isNaN(e.X)) t.X += e.X;
            if (!isNaN(e.Y)) t.Y += e.Y
        }
        return t
    };
    this.Subtract = function (e) {
        var t = new TBPosition(this.X, this.Y);
        if (e != null) {
            if (!isNaN(e.X)) t.X -= e.X;
            if (!isNaN(e.Y)) t.Y -= e.Y
        }
        return t
    };
    this.Min = function (e) {
        var t = new TBPosition(this.X, this.Y);
        if (e == null) return t;
        if (!isNaN(e.X) && this.X > e.X) t.X = e.X;
        if (!isNaN(e.Y) && this.Y > e.Y) t.Y = e.Y;
        return t
    };
    this.Max = function (e) {
        var t = new TBPosition(this.X, this.Y);
        if (e == null) return t;
        if (!isNaN(e.X) && this.X < e.X) t.X = e.X;
        if (!isNaN(e.Y) && this.Y < e.Y) t.Y = e.Y;
        return t
    };
    this.Bound = function (e, t) {
        var n = this.Max(e);
        return n.Min(t)
    };
    this.Check = function () {
        var e = new TBPosition(this.X, this.Y);
        if (isNaN(e.X)) e.X = 0;
        if (isNaN(e.Y)) e.Y = 0;
        return e
    };
    this.Apply = function (e) {
        if (typeof e == "string") e = document.getElementById(e);
        if (e == null) return;
        if (!isNaN(this.X)) e.style.left = this.X + "px";
        if (!isNaN(this.Y)) e.style.top = this.Y + "px"
    }
}

function hookEvent(e, t, n) {
    if (typeof e == "string") e = document.getElementById(e);
    if (e == null) return;
    if (e.addEventListener) {
        e.addEventListener(t, n, false)
    } else if (e.attachEvent) e.attachEvent("on" + t, n)
}

function unhookEvent(e, t, n) {
    if (typeof e == "string") e = document.getElementById(e);
    if (e == null) return;
    if (e.removeEventListener) e.removeEventListener(t, n, false);
    else if (e.detachEvent) e.detachEvent("on" + t, n)
}

function cancelEvent(e) {
    e = e ? e : window.event;
    if (e.stopPropagation) e.stopPropagation();
    if (e.preventDefault) e.preventDefault();
    e.cancelBubble = true;
    e.cancel = true;
    e.returnValue = false;
    return false
}

function getEventTarget(e) {
    e = e ? e : window.event;
    return e.target ? e.target : e.srcElement
}

function absoluteCursorPostion(e) {
    e = e ? e : window.event;
    if (isNaN(window.scrollX)) return new TBPosition(e.clientX + document.documentElement.scrollLeft + document.body.scrollLeft, e.clientY + document.documentElement.scrollTop + document.body.scrollTop);
    else return new TBPosition(e.clientX + window.scrollX, e.clientY + window.scrollY)
}

function dragObject(e, t, n, r, i, s, o, u) {
    function a(t) {
        if (d || !v || m) return;
        d = true;
        if (i != null) i(t, e);
        h = absoluteCursorPostion(t);
        p = new TBPosition(parseInt(e.style.left), parseInt(e.style.top));
        p = p.Check();
        hookEvent(document, "mousemove", f);
        hookEvent(document, "mouseup", l);
        return cancelEvent(t)
    }

    function f(t) {
        if (!d || m) return;
        var i = absoluteCursorPostion(t);
        i = i.Add(p).Subtract(h);
        i = i.Bound(n, r);
        i.Apply(e);
        if (s != null) s(i, e);
        return cancelEvent(t)
    }

    function l(e) {
        c();
        return cancelEvent(e)
    }

    function c() {
        if (!d || m) return;
        unhookEvent(document, "mousemove", f);
        unhookEvent(document, "mouseup", l);
        h = null;
        p = null;
        if (o != null) o(e);
        d = false
    }
    if (typeof e == "string") e = document.getElementById(e);
    if (e == null) return;
    var h = null;
    var p = null;
    var d = false;
    var v = false;
    var m = false;
    this.Dispose = function () {
        if (m) return;
        this.StopListening(true);
        e = null;
        t = null;
        n = null;
        r = null;
        i = null;
        s = null;
        o = null;
        m = true
    };
    this.StartListening = function () {
        if (v || m) return;
        v = true;
        hookEvent(t, "mousedown", a)
    };
    this.StopListening = function (e) {
        if (!v || m) return;
        unhookEvent(t, "mousedown", a);
        v = false;
        if (e && d) c()
    };
    this.IsDragging = function () {
        return d
    };
    this.IsListening = function () {
        return v
    };
    this.IsDisposed = function () {
        return m
    };
    if (typeof t == "string") t = document.getElementById(t);
    if (t == null) t = e;
    if (!u) this.StartListening()
}

function getMousePos(e) {
    e = e ? e : window.event;
    var t;
    if (isNaN(e.layerX)) return new TBPosition(e.offsetX, e.offsetY);
    else return new TBPosition(e.layerX, e.layerY)
}

function Trackbar(e, t, n, r) {
    function i(e, t) {
        var n = getMousePos(e);
        var r = getEventTarget(e);
        if (r == m) n.X += parseInt(m.style.left);
        else if (r == v) n.X += f - 1;
        n.X -= 4;
        n = n.Bound(g, y);
        n.Apply(m);
        s(n)
    }

    function s(e, t) {
        e.X += 4;
        c = Math.round(1e3 * (e.X / f * (a - u) + u)) / 1e3;
        if (l != null) l(c)
    }

    function o() {
        if (c < u) c = u;
        if (c > a) c = a;
        if (a != u) m.style.left = (c - u) / (a - u) * f - 4 + "px";
        else m.style.left = "0px"
    }
    var u = e;
    var a = t;
    var f = n;
    var l = r;
    var c = .5 * (a - u) + u;
    var h = document.createElement("DIV");
    h.style.position = "relative";
    h.style.height = "17px";
    h.style.width = f + "px";
    h.style.fontSize = "1px";
    var p = document.createElement("DIV");
    p.style.backgroundImage = "url(" + flashvars.vtopath + "image/leftCap_1x4.jpg)";
    p.style.position = "absolute";
    p.style.height = "4px";
    p.style.width = "1px";
    p.style.left = "0px";
    p.style.top = "7px";
    var d = document.createElement("DIV");
    d.style.backgroundImage = "url(" + flashvars.vtopath + "image/repeater_1x4.jpg)";
    d.style.position = "absolute";
    d.style.height = "4px";
    d.style.width = f - 3 + "px";
    d.style.left = "1px";
    d.style.top = "7px";
    var v = document.createElement("DIV");
    v.style.backgroundImage = "url(" + flashvars.vtopath + "image/rightCap_2x4.jpg)";
    v.style.position = "absolute";
    v.style.height = "4px";
    v.style.width = "2px";
    v.style.left = f - 2 + "px";
    v.style.top = "7px";
    var m = document.createElement("DIV");
    m.style.backgroundImage = "url(" + flashvars.vtopath + "image/pointer_9x17.gif)";
    m.style.position = "absolute";
    m.style.height = "17px";
    m.style.width = "9px";
    m.style.top = "0px";
    h.appendChild(p);
    h.appendChild(d);
    h.appendChild(v);
    h.appendChild(m);
    var g = new TBPosition(-4, 0);
    var y = new TBPosition(f - 4, 0);
    var b = new dragObject(m, h, g, y, i, s, null, true);
    o();
    this.GetMaxValue = function () {
        return a
    };
    this.GetMinValue = function () {
        return u
    };
    this.GetCurrentValue = function () {
        return c
    };
    this.GetWidth = function () {
        return f
    };
    this.SetMaxValue = function (e) {
        e = parseFloat(e);
        if (isNaN(e)) e = 1;
        a = e;
        o()
    };
    this.SetMinValue = function (e) {
        e = parseFloat(e);
        if (isNaN(e)) e = 0;
        u = e;
        o()
    };
    this.SetCurrentValue = function (e) {
        e = parseFloat(e);
        if (isNaN(e)) e = 0;
        c = e;
        o()
    };
    this.SetWidth = function (e) {
        e = parseInt(e);
        if (isNaN(e)) e = 100;
        if (e < 15) e = 15;
        f = e;
        d.style.width = f - 3 + "px";
        v.style.left = f - 2 + "px";
        y.X = f - 4;
        h.style.width = f + "px";
        o()
    };
    this.GetContainer = function () {
        return h
    };
    this.SetCallback = function (e) {
        l = e
    };
    this.StartListening = function () {
        b.StartListening()
    };
    this.StopListening = function () {
        b.StopListening()
    }
}

function fadeIn(e, t) {
    jsDom.show(e)
}

function fadeOut(e, t) {
    jsDom.hide(e)
}

function ietruebody() {
    return document.compatMode && document.compatMode != "BackCompat" ? document.documentElement : document.body
}

function ddrivetip(e, t, n) {
    if (ns6 || ie) {
        if (typeof t != "undefined") tipobj.style.width = t + "px";
        if (typeof n != "undefined" && n != "") tipobj.style.backgroundColor = n;
        tipobj.innerHTML = e;
        enabletip = true;
        return false
    }
}

function positiontip(e) {
    if (enabletip) {
        var t = false;
        var n = ns6 ? e.pageX : event.clientX + ietruebody().scrollLeft;
        var r = ns6 ? e.pageY : event.clientY + ietruebody().scrollTop;
        var i = ie && !window.opera ? ietruebody().clientWidth : window.innerWidth - 20;
        var s = ie && !window.opera ? ietruebody().clientHeight : window.innerHeight - 20;
        var o = ie && !window.opera ? i - event.clientX - offsetfromcursorX : i - e.clientX - offsetfromcursorX;
        var u = ie && !window.opera ? s - event.clientY - offsetfromcursorY : s - e.clientY - offsetfromcursorY;
        var a = offsetfromcursorX < 0 ? offsetfromcursorX * -1 : -1e3;
        if (o < tipobj.offsetWidth) {
            tipobj.style.left = n - tipobj.offsetWidth + "px";
            t = true
        } else if (n < a) tipobj.style.left = "5px";
        else {
            tipobj.style.left = n + offsetfromcursorX - offsetdivfrompointerX + "px";
            pointerobj.style.left = n + offsetfromcursorX + "px"
        } if (u < tipobj.offsetHeight) {
            tipobj.style.top = r - tipobj.offsetHeight - offsetfromcursorY + "px";
            t = true
        } else {
            tipobj.style.top = r + offsetfromcursorY + offsetdivfrompointerY + "px";
            pointerobj.style.top = r + offsetfromcursorY + "px"
        }
        tipobj.style.visibility = "visible";
        if (!t) pointerobj.style.visibility = "visible";
        else pointerobj.style.visibility = "hidden"
    }
}

function hideddrivetip() {
    if (ns6 || ie) {
        enabletip = false;
        tipobj.style.visibility = "hidden";
        pointerobj.style.visibility = "hidden";
        tipobj.style.left = "-1000px";
        tipobj.style.backgroundColor = "";
        tipobj.style.width = ""
    }
}(function (t) {
    t.vtocore = {
        init: function (e, t, n, r, i) {
            cvto.init(e);
            this.frameApplyable = true;
            this.oldVal = 0;
            $pid = n;
            $pr = r == undefined || r == null ? 0 : jsDom.makeInt(r);
            this.obj = e;
            this.cursorX = 0;
            this.is_product = i;
            $modelFrame = new Array;
            setStrData("defaultFrame", t);
            this.modelImages = this.is_product ? flashvars.modelsimages.toString().split(",") : flashvars.pmodelsimages.toString().split(",");

            // add user image to gallery
            if (getStrData("userImg")) {
                this.modelImages.push(getStrData("userImg"))
                this.hasUserImage = true;
            }

            this.loadHtmlVTO(e);
            if (getStrData("modelNo") != null) {
                vtocore.checkUser()
            } else {
                setStrData("modelNo", flashvars.defaultmodel);
                vtocore.loadModel()
            } if (checktouch()) {
                jsDom.remove("webcam")
            }
            // vtocore.getNode("imgSnap").addEventListener("click", vtocore.loadUserImg);

            vtocore.applyFrame(t);
            if (this.is_product) {
                jsDom.css("btnContainer", {
                    top: flashvars.height - flashvars.panelheight + 10 + "px"
                });
                jsDom.remove("modelContainer");
                jsDom.remove("upload");
                var s = jsDom.getNode("vtoLeftContainer");
                jsDom.createNode("div", {
                    id: "vto_left_upload"
                }, null, "parent", s);
                jsDom.css("vto_left_upload", {
                    width: flashvars.plwidth + "px"
                });
                jsDom.createNode("img", {
                    id: "left_upload",
                    src: flashvars.uploadsrc
                }, flashvars.browsetext, "parent", "vto_left_upload").addEventListener("click", vtocore.showTooltip, true);
                jsDom.css("left_upload", {
                    padding: "4px",
                    fontSize: "10px",
                    margin: "0",
                    width: "150px",
                    fontWeight: "bold"
                });
                var o = jsDom.createNode("div", {
                    id: "leftmodelContainer"
                }, null, "parent", s);
                var u = jsDom.createNode("div", {
                    width: flashvars.width + "px"
                }, flashvars.pmodeltext, "parent", o);
                jsDom.css(u, {
                    height: "20px",
                    lineHeight: "20px",
                    color: "#000"
                });
                jsDom.createNode("img", {
                    id: "tArrow",
                    src: flashvars.tArrow
                }, null, "parent", o).addEventListener("click", vtocore.moveModelV);
                var a = jsDom.createNode("div", {
                    id: "leftmodelContainer2"
                }, null, "parent", o);
                jsDom.createNode("img", {
                    id: "dArrow",
                    src: flashvars.dArrow
                }, null, "parent", o).addEventListener("click", vtocore.moveModelV);
                jsDom.css(o, {
                    width: flashvars.plwidth + "px",
                    height: "230px",
                    position: "absolute"
                });
                jsDom.css("tArrow", {
                    zIndex: "100",
                    position: "absolute",
                    height: "30px",
                    width: "50px",
                    left: (flashvars.plwidth - 50) / 2 + "px"
                });
                jsDom.css("dArrow", {
                    zIndex: "100",
                    position: "absolute",
                    height: "30px",
                    width: "50px",
                    left: (flashvars.plwidth - 50) / 2 + "px",
                    bottom: 0
                });
                jsDom.css("leftmodelContainer2", {
                    width: flashvars.plwidth + "px",
                    marginTop: "30px",
                    cssFloat: "left",
                    overflow: "hidden",
                    height: "150px",
                    position: "absolute"
                });
                vtocore.createthumbnail(a, {
                    width: "70",
                    height: "70",
                    models: this.modelImages
                })
            }
        },
        is_product: function () {
            return this.is_product
        },
        getNode: function (e, t) {
            if (t) return t.querySelector("#" + e);
            else return this.obj.querySelector("#" + e)
        },
        displayNode: function (e, t) {
            jsDom.displayNode(e, t)
        },
        displayNodeById: function (e, t, n) {
            var r = this.getNode(e, n);
            jsDom.displayNode(r, t)
        },
        loadHtmlVTO: function (e) {
            if (jsDom.getNode("vtoliteLogo")) {
                jsDom.createNode("a", {
                    id: "ssLogo",
                    href: "//thesoftsol.com",
                    target: "_blank"
                }, null, "parent", "vtoliteLogo");
                jsDom.createNode("img", {
                    id: "logoLeft",
                    src: flashvars.llogo
                }, null, "parent", "ssLogo");
                jsDom.createNode("img", {
                    id: "logoRight",
                    src: flashvars.rlogo
                }, null, "parent", "vtoliteLogo");
                jsDom.css("logoLeft", {
                    "float": "left",
                    height: "20px"
                });
                jsDom.css("logoRight", {
                    "float": "right",
                    height: "20px"
                })
            }
            _eyes = this.modelImages[flashvars.defaultmodel];
            var n = flashvars.buttons.toString().split(",");
            var r = (flashvars.width / n.length - 26) / 2;
            var i = (flashvars.panelheight - 26) / 2;
            var s = flashvars.panelcolors.split(",");
            var o = vtocore.getNode("preview");
            jsDom.css(o, {
                height: "-=" + flashvars.panelheight
            });
            jsDom.html(o, '<div><img id="prevImg" align="middle" style="position:absolute;"/></div>');
            var u = e;
            if (this.is_product) {
                u = jsDom.getNode("vtoLeftContainer")
            }
            var a = jsDom.createNode("div", {
                id: "upload_tooltip"
            }, null, "parent", u);
            if (vtocore.is_product) {
                jsDom.createNode("div", {
                    id: "pupload_tt_bg"
                }, null, "parent", "vtoLeftContainer").addEventListener("click", vtocore.hideTooltip);
                jsDom.css("pupload_tt_bg", {
                    position: "absolute",
                    display: "none",
                    zIndex: 999,
                    width: "100%",
                    height: "100%"
                })
            }
            jsDom.createNode("div", {
                id: "upload_tt_bg"
            }, null, "parent", e).addEventListener("click", vtocore.hideTooltip);
            jsDom.createNode("a", {
                id: "browse"
            }, flashvars.browsetext, "parent", a).addEventListener("click", vtocore.showTooltip, true);
            jsDom.createNode("a", {
                id: "webcam"
            }, flashvars.webcamtext, "parent", a).addEventListener("click", vtocore.onCam);
            jsDom.createNode("input", {
                type: "file",
                id: "files",
                name: "files[]"
            }, null, "parent", a).addEventListener("change", vtocore.onBrowse);
            jsDom.createNode("img", {
                id: "ttarrow",
                src: flashvars.tooltiparrow
            }, null, "parent", a).addEventListener("change", vtocore.onBrowse);
            jsDom.createNode("div", {
                id: "btnContainer"
            }, null, "parent", e);
            var f = jsDom.createNode("a", {
                id: "upload"
            }, flashvars.uploadtext, "parent", "btnContainer");
            f.addEventListener("click", vtocore.showTooltip, true);
            var l = jsDom.createNode("div", {
                id: "compareDiv"
            }, null, "parent", "btnContainer");
            vtocore.showCompareList();
            jsDom.createNode("div", {
                id: "modelContainer"
            }, null, "parent", e);
            jsDom.createNode("div", {
                id: "camControl"
            }, null, "parent", e);
            jsDom.createNode("img", {
                id: "takepic",
                src: flashvars.takepicicon
            }, null, "parent", "camControl");
            jsDom.createNode("img", {
                id: "cancelpic",
                src: flashvars.closebutton
            }, null, "parent", "camControl");
            var c = (flashvars.width / 2 - 38) / 2;
            jsDom.css(a, {
                display: "none",
                position: "absolute",
                width: "150px",
                padding: "10px",
                textAlign: "center",
                background: "#fff",
                zIndex: 1e3,
                marginBottom: "10px",
                border: "1px solid #ccc",
                boxShadow: "0px 0px 5px #ccc"
            });
            jsDom.css("upload_tt_bg", {
                position: "absolute",
                display: "none",
                zIndex: 999,
                width: "100%",
                height: "100%"
            });
            jsDom.css("ttarrow", {
                position: "absolute",
                bottom: "-15px",
                left: "70px"
            });
            jsDom.css("browse", {
                background: "#fff",
                color: "#7b7b7b",
                border: "1px solid #ccc",
                margin: "5px 0",
                padding: "5px",
                width: "130px",
                cursor: "pointer",
		display:"block"
            });
            jsDom.css("webcam", {
                background: "#fff",
                color: "#7b7b7b",
                border: "1px solid #ccc",
                marginBottom: "5px",
                padding: "5px",
                width: "130px",
                cursor: "pointer",
		display:"block"
            });
            var h = (flashvars.width - 100) / 2;
            jsDom.css("upload", {
                background: "#329C92",
                color: "#fff",
                border: "1px solid #329C92",
                padding: "3px",
                fontSize: "10px",
                margin: "2px " + h + "px",
                width: "103px",
                fontWeight: "bold",
		display:"block"
            });
            jsDom.css("takepic", {
                margin: "auto 17px",
                cursor: "pointer"
            });
            jsDom.css("cancelpic", {
                margin: "auto 20px",
                cursor: "pointer"
            });
            jsDom.css("camControl", {
                width: flashvars.width + "px",
                height: flashvars.panelheight + "px",
                top: flashvars.height - flashvars.panelheight + "px",
                position: "absolute",
                overflow: "hidden",
                background: s[0],
                display: "none",
            });
            var p = vtocore.getNode("modelContainer");
            jsDom.css(p, {
                width: flashvars.width + "px",
                height: flashvars.panelheight + "px",
                top: flashvars.height - flashvars.panelheight + 5 + "px",
                position: "absolute",
                overflow: "hidden",
                background: s[0]
            });
            var d = jsDom.createNode("img", {
                id: "lArrow",
                src: flashvars.lArrow
            }, null, "parent", p);
            var v = jsDom.createNode("img", {
                id: "rArrow",
                src: flashvars.rArrow
            }, null, "parent", p);
            var m = jsDom.createNode("div", {
                id: "modelContainer2"
            }, null, "parent", p);
            jsDom.css("lArrow", {
                zIndex: "100",
                position: "absolute",
                left: "0px",
                height: flashvars.panelheight + "px",
                width: "20px"
            });
            jsDom.css("rArrow", {
                zIndex: "100",
                position: "absolute",
                right: "0px",
                height: flashvars.panelheight + "px",
                width: "20px"
            });
            jsDom.css(m, {
                width: flashvars.width - 30 + "px",
                margin: "0 13px",
                cssFloat: "left",
                overflow: "hidden",
                height: "50px"
            });
            d.addEventListener("click", t.vtocore.moveModel);
            v.addEventListener("click", t.vtocore.moveModel);
            this.thumbnail(m, {
                width: "50",
                height: flashvars.panelheight,
                models: this.modelImages
            });
            jsDom.css("btnContainer", {
                width: flashvars.width + "px",
                height: flashvars.panelheight + "px",
                top: jsDom.makeInt(flashvars.height) + 5 + "px",
                position: "absolute",
                overflow: "hidden",
                "text-align": "center"
            });
            vtocore.loadModelData()
        },
        hideTooltip: function () {
            jsDom.hide("upload_tooltip");
            jsDom.hide("upload_tt_bg");
            if (jsDom.getNode("pupload_tt_bg")) jsDom.hide("pupload_tt_bg")
        },
        showTooltip: function (e) {
            if (jsDom.getNode("pupload_tt_bg")) jsDom.show("pupload_tt_bg");
            jsDom.show("upload_tooltip");
            jsDom.show("upload_tt_bg");
            var t = jsDom.getNode("upload_tooltip");
            var n = jsDom.getProperty(t, "offsetWidth", true);
            var r = jsDom.getProperty("browse", "offsetHeight", true);
            var i = jsDom.getProperty("browse", "offsetTop", true);
            var s = jsDom.getProperty("browse", "offsetLeft", true);
            jsDom.css("files", {
                position: "absolute",
                top: i + "px",
                left: s + "px",
                cursor: "pointer",
                width: n + "px",
                height: r + "px",
                "-ms-filter": "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)",
                filter: "alpha(opacity=0)",
                "-moz-opacity": 0,
                "-khtml-opacity": 0,
                opacity: 0,
                overflow: "hidden"
            });
            var o = jsDom.getProperty(t, "offsetHeight", true);
            var u;
            if (vtocore.is_product) {
                u = e.target.offsetTop + e.target.offsetHeight;
                jsDom.css("ttarrow", {
                    "-webkit-transform": "rotate(180deg)",
                    "-moz-transform": "rotate(180deg)",
                    transform: "rotate(180deg)",
                    top: "-15px"
                });
                jsDom.removeStyle("ttarrow", "bottom")
            } else {
                u = e.target.offsetTop + e.target.offsetParent.offsetTop - o - jsDom.getStyle(t, "marginBottom", true) / 2
            }
            var a = (jsDom.makeInt(vtocore.is_product ? flashvars.plwidth : flashvars.width) - n) / 2;
            jsDom.css("upload_tooltip", {
                top: u + "px",
                left: a + "px"
            })
        },
        showCompareList: function () {
            jsDom.html("compareDiv", "");
            var e = vtocore.getCompare();
            if (e.length > 0) {
                var t = jsDom.createNode("img", {
                    id: "compare",
                    src: flashvars.comparebtn
                }, "+", "parent", "compareDiv");
                t.addEventListener("click", function () {
                    vtocore.addCompare($pid)
                });
                jsDom.css("compare", {
                    background: "#fff",
                    cssPadding: "0px",
                    margin: "0px",
                    width: "18px",
                    cssFloat: "left"
                });
                for (var n = 0; n < e.length; n++) {
                    var r = e[n].split("*");
                    var i = jsDom.createNode("img", {
                        src: r[1],
                        id: "comp_" + e[n]
                    }, null, "parent", "compareDiv");
                    jsDom.css(i, {
                        width: "30px",
                        cssFloat: "left"
                    });
                    i.addEventListener("click", function () {
                        var e = this.id.split("comp_")[1];
                        vtocore.removeCompare(e)
                    });
                    var i = jsDom.createNode("img", {
                        src: flashvars.crossimg,
                        id: "cross_" + e[n]
                    }, null, "parent", "compareDiv");
                    jsDom.css(i, {
                        width: "10px",
                        position: "absolute",
                        left: 8 + (n + 1) * 30 + "px"
                    });
                    i.addEventListener("click", function () {
                        var e = this.id.split("cross_")[1];
                        vtocore.removeCompare(e)
                    })
                }
                jsDom.createNode("a", {
                    id: "compareShow"
                }, flashvars.showcomparetext, "parent", "compareDiv");
                var s = (flashvars.width - 100) / 2;
                jsDom.css("compareShow", {
                    cssFloat: "right",
                    backgroundColor: "#ccc",
                    color: "#7b7b7b",
                    border: "1px solid #ccc",
                    cssPadding: "0 5px",
                    fontSize: "10px",
                    margin: "0px",
                    width: "auto",
			display:"block",
		cursor:(flashvars.cursorerror != ""?"url("+flashvars.cursorerror+"),auto":"")	
                });
                if (e.length > 1) {
                    jsDom.css("compareShow",{
                        backgroundColor:"#fff",
						cursor:"pointer"
                    });
                    jsDom.getNode("compareShow").addEventListener("click", function () {
                        var t = flashvars.modelsimages.split(",");
                        showCompareBox(e)
                    })
                }
            } else {
                var t = jsDom.createNode("a", {
                    id: "compare"
                }, flashvars.comparetext, "parent", "compareDiv");
                t.addEventListener("click", function () {
                    vtocore.addCompare($pid)
                });
                var s = (flashvars.width - 100) / 2;
                jsDom.css("compare", {
                    background: "#fff",
                    color: "#7b7b7b",
                    border: "1px solid #ccc",
                    padding: "3px",
                    height: "14px",
                    fontSize: "12px",
                    margin: "1px " + s + "px",
                    width: "103px",
			display:"block"
                })
            }
        },
        removeCompare: function (e) {
            var t = vtocore.getCompare();
            var n = t.indexOf(e);
            t.splice(n, 1);
            setStrData("comparelist", JSON.stringify(t));
            vtocore.showCompareList()
        },
        getCompare: function () {
            var e = Array();
            var t = getStrData("comparelist");
            if (t) e = JSON.parse(t);
            return e.length > 0 ? e : false
        },
        addCompare: function (e) {
            e = parseInt(e);
            var t = vtocore.getCompare();
            if (!t) t = Array();
            if (t.length == flashvars.compare) {
                alert("Maximum compare allowed is " + flashvars.compare);
                return
            }
            if (t.indexOf(e + "*" + getStrData("defaultFrame") + "*" + $pr) != -1) {
                alert("This item is already added in list");
                return
            }
            t[t.length] = e + "*" + getStrData("defaultFrame") + "*" + $pr;
            setStrData("comparelist", JSON.stringify(t));
            vtocore.showCompareList()
        },
        createthumbnail: function (e, n) {
            _container = e;
            _scroller = jsDom.createNode("div", {
                id: "scroller"
            }, null, "parent", _container);
            jsDom.css(_scroller, {
                position: "absolute",
                display: "block",
                width: 2 * n.width + "px",
                left: (flashvars.plwidth - 2 * n.width) / 2 + "px"
            });
            var r = 0;
            var i = 0;
            var s = true;
            for (var o = 0; o < n.models.length; o++) {
                var u = jsDom.createNode("div", {
                    "class": "nailthumb",
                    id: "nailthumb" + o
                }, null, "parent", _scroller);
                jsDom.css(u, {
                    position: "absolute",
                    top: r + "px",
                    left: "70px",
                    height: n.height + "px",
                    width: n.width + "px"
                });
                var a = jsDom.createNode("img", {
                    src: n.models[o],
                    height: n.height
                }, null, "parent", u);
                jsDom.css(a, {
                    cursor: "pointer",
                    height: n.height - 6 + "px",
                    padding: "2px",
                    border: "1px solid #ccc"
                });
                if (s) {
                    s = false;
                    jsDom.css(u, {
                        left: "0px"
                    })
                } else {
                    r += 75;
                    s = true
                }
            }
            var i = 0;
            _scroller.style.height = r + "px";
            var f = jsDom.nodeByClass("nailthumb");
            for (var o = 0, l = f.length; o < l; o++) {
                f[o].addEventListener("click", function (e) {
                    // jsDom.show("imgSnap");
                    jsDom.hide("imgFrame");
                    jsDom.hide("preview");
                    jsDom.show("previewLoader");
                    var n = e.target.offsetParent.id;
                    if (n.indexOf("nailthumb") == -1) n = e.target.id;
                    var r = n.split("nailthumb").join("");
                    t.vtocore.loadModel(r)
                })
            }
        },
        thumbnail: function (e, n) {
            _container = e;
            _scroller = jsDom.createNode("div", {
                id: "scroller"
            }, null, "parent", _container);
            jsDom.css(_scroller, {
                position: "absolute",
                display: "block",
                height: n.height + "px",
                left: "20px"
            });
            var r = 0;
            var i = 0;
            for (var s = 0; s < n.models.length; s++) {
                var o = jsDom.createNode("div", {
                    "class": "nailthumb",
                    id: "nailthumb" + s
                }, null, "parent", _scroller);
                jsDom.css(o, {
                    position: "absolute",
                    left: r + "px",
                    height: n.height + "px",
                    width: "50px"
                });
                var u = jsDom.createNode("img", {
                    src: n.models[s],
                    height: n.height
                }, null, "parent", o);
                jsDom.css(u, {
                    position: "absolute",
                    cursor: "pointer",
                    height: n.height - 6 + "px",
                    padding: "2px",
                    border: "1px solid #ccc"
                });
                r += 50
            }
            var i = 0;
            _scroller.style.width = r + "px";
            var a = jsDom.nodeByClass("nailthumb");
            for (var s = 0, f = a.length; s < f; s++) {
                (function(s){
                    a[s].addEventListener("click", function (e) {
                        if (s == f-1 && getStrData("userImg")) {
                            vtocore.loadUserImg();
                        } else {
                            // jsDom.show("imgSnap");
                            jsDom.hide("imgFrame");
                            jsDom.hide("preview");
                            jsDom.show("previewLoader");
                            var n = e.target.offsetParent.id.split("nailthumb").join("");
                            t.vtocore.loadModel(n)
                        }
                    })
                }(s));
            }
        },
        loadModelData: function () {
            var e = vtocore.is_product ? flashvars.pmodeldata.toString().split(",") : flashvars.modeldata.toString().split(",");
            $totalModels = e.length;
            for (i = 0; i < e.length; i++) {
                var t = e[i];
                var n = t.split(":");
                var r = [];
                for (var s = 0; s < n.length; s++) {
                    framePosition = n[s].split("-");
                    switch (s) {
                    case 0:
                        r["framelx"] = framePosition[0];
                        r["framely"] = framePosition[1];
                        break;
                    case 1:
                        r["framerx"] = framePosition[0];
                        r["framery"] = framePosition[1];
                        break
                    }
                    $modelFrame[i] = [r]
                }
            }
        },
        loadModel: function (e) {
            if (e) setStrData("modelNo", e);
            else e = getStrData("modelNo"); if (e == this.modelImages.length) {
                vtocore.loadUserImg();
                return
            }
            var t = this.modelImages[Number(e)];
            jsDom.nodeById("preview", '<div><img id="prevImg" src="' + t + '" align="middle"  style="overflow:hidden;position:absolute;display:none"/></div>');
            jsDom.getNode("prevImg").onload = function () {
                vtocore.setModelImage()
            }
        },
        setModelImage: function () {
            var e = jsDom.nodeById("preview");
            var t = e.children[0].children[0];
            var n = jsDom.getStyle(e, "height", true);
            var r = jsDom.getStyle(e, "width", true);
            var i = t.naturalHeight;
            var s = t.naturalWidth;
            var o = n / r;
            var u = i / s;
            var a = -1 * (s * (n / i) - r) / 2;
            var f = -1 * (i * (r / s) - n) / 2;
            if (o >= u) {
                t.height = n;
                jsDom.css(t, {
                    left: a + "px",
                    top: "0px"
                })
            } else {
                t.width = r;
                jsDom.css(t, {
                    left: "0px",
                    top: f + "px"
                })
            }
            jsDom.show(e);
            jsDom.hide("previewLoader");
            fadeIn(t);
            vtocore.applyFrameIf()
        },
        moveModel: function (e) {
            var t = jsDom.nodeById("scroller");
            var n = jsDom.nodeById("modelContainer");
            var r = n.offsetParent.offsetLeft;
            var i = n.offsetParent.offsetTop;
            var s = jsDom.makeInt(n.clientWidth);
            var o = jsDom.makeInt(t.offsetWidth);
            var u = jsDom.getStyle(n, "left", true);
            var a = jsDom.getStyle(t, "left", true);
            if (e.currentTarget.id != "lArrow") {
                t.style.left = (o > s - a ? a - 50 : s - o) + "px"
            } else {
                t.style.left = (a < 10 ? a + 50 : 20) + "px"
            }
        },
        moveModelV: function (e) {
            var t = jsDom.nodeById("scroller");
            var n = jsDom.nodeById("leftmodelContainer2");
            var r = n.offsetParent.offsetLeft;
            var i = n.offsetParent.offsetTop;
            var s = jsDom.makeInt(n.clientHeight);
            var o = jsDom.makeInt(t.offsetHeight);
            var u = jsDom.getStyle(n, "top", true);
            var a = jsDom.getStyle(t, "top", true);
            if (e.currentTarget.id != "tArrow") {
                t.style.top = (o > s - a ? a - 75 : s - o) + "px"
            } else {
                t.style.top = (a < 0 ? a + 75 : 0) + "px"
            }
        },
        checkUser: function () {
            if (!supportsLocalStorage()) {
                return false
            }
            var e = [];
            e["framerx"] = getStrData("framerx");
            e["framery"] = getStrData("framery");
            e["framelx"] = getStrData("framelx");
            e["framely"] = getStrData("framely");
            $modelFrame[$totalModels] = [e];
            if (getStrData("userImg") != null) vtocore.creatSnap();
            if (getStrData("modelNo") < $totalModels) {
                vtocore.loadModel();
                // jsDom.show("imgSnap")
            } else {
                vtocore.loadUserImg()
            }
        },
        creatSnap: function () {
            jsDom.html("imgSnap", '<img id="userImgSnap" src="' + getStrData("userImg") + '" width="30px" style="border:2px solid #fff">')
        },
        onBrowse: function () {
            var acceptFileTypes = /(\.|\/)(gif|jpe?g|png)$/i;
            jsDom.hide("imgSnap");
            
            vtocore.hideTooltip();
            e = jsDom.getNode("files");
            if (t.File && t.FileReader && t.FileList && t.Blob) {
                var n = e.files[0];
                var valid = acceptFileTypes.test(n.type);
                if (valid) {
                    var r = new FileReader;
                    if (jsDom.getNode("modelContainer")) jsDom.hide("modelContainer");
                    r.onloadend = function (e) {
                        if (e.target.readyState == 2) {
                            if (vtocore.is_product) {
                                jsDom.createNode("div", {
                                    id: "onCam_bg"
                                }, null, "parent", "vtoLeftContainer");
                                jsDom.css("onCam_bg", {
                                    position: "absolute",
                                    display: "block",
                                    zIndex: 999,
                                    width: "100%",
                                    height: "100%"
                                })
                            }
                            var t = jsDom.html("preview", '<div><img id="userImg" align="middle" src="' + e.target.result + '" style="position:relative;display:none" /></div>');
                            dragDrop.initElement("userImg");
                            vtocore.getNode("userImg", t).onload = function () {
                                vtocore.setModelImage()
                            };
                            vtocore.setPupils()
                        }
                    };
                    r.readAsDataURL(n);
                    vtocore.doAdjustment()
                } else {
                    alert("Your can upload only raster graphics.")
                }
            } else alert("The File APIs are not fully supported in this browser.")
        },
        setPupils: function () {
            var e = vtocore.getNode("preview");
            var t = jsDom.makeInt(e.offsetLeft) + jsDom.getStyle(e, "width", true) / 2;
            var n = jsDom.makeInt(e.offsetTop) + jsDom.getStyle(e, "height", true) / 2;
            vtocore.removeGlassFromFace();
            var r = jsDom.createNode("img", {
                id: "drag",
                src: checktouch() ? flashvars.lt : flashvars.leye
            }, null, "parent", e);
            jsDom.css(r, {
                position: "absolute",
                left: t - 30 + "px",
                top: n - 30 + "px"
            });
            var i = jsDom.createNode("img", {
                id: "drag2",
                src: checktouch() ? flashvars.rt : flashvars.reye
            }, null, "parent", e);
            jsDom.css(i, {
                position: "absolute",
                left: t + 30 + "px",
                top: n - 30 + "px"
            });
            dragDrop.initElement(i);
            dragDrop.initElement(r)
        },
        setUserPupils: function (e) {
            if (jsDom.getNode("onCam_bg")) jsDom.remove("onCam_bg");
            if (!e) {
                jsDom.remove("drag");
                jsDom.remove("drag2");
                jsDom.html("preview", '<div><img id="prevImg" align="middle" style="position:absolute;"/></div>');
                jsDom.hide("camControl");
                jsDom.show("btnContainer");
                if (jsDom.getNode("modelContainer")) jsDom.show("modelContainer");
                vtocore.loadModel();
                vtocore.loadModelData();
                vtocore.applyFrameToFace();
                return
            }
            var t = jsDom.getNode("drag");
            var n = jsDom.getNode("drag2");
            var r = [];
            if (jsDom.getStyle(t, "left", true) < jsDom.getStyle(n, "left", true)) {
                r["framelx"] = jsDom.getStyle(t, "left", true) + t.width / 2;
                r["framely"] = jsDom.getStyle(t, "top", true) + 10;
                r["framerx"] = jsDom.getStyle(n, "left", true) + n.width / 2;
                r["framery"] = jsDom.getStyle(n, "top", true) + 10
            } else {
                r["framelx"] = jsDom.getStyle(n, "left", true) + n.width / 2;
                r["framely"] = jsDom.getStyle(n, "top", true) + 10;
                r["framerx"] = jsDom.getStyle(t, "left", true) + t.width / 2;
                r["framery"] = jsDom.getStyle(t, "top", true) + 10
            }
            $modelFrame[$totalModels] = [r];
            setStrData("modelNo", $totalModels);
            setStrData("framerx", r["framerx"]);
            setStrData("framery", r["framery"]);
            setStrData("framelx", r["framelx"]);
            setStrData("framely", r["framely"]);
            jsDom.remove("drag");
            jsDom.remove("drag2");
            var i = jsDom.getNode("userImg");
            var hold = jsDom.getNode("modelContainer2");
            setStrData("userImg", i.src);
            setStrData("usrImgW", i.width);
            setStrData("usrImgH", i.height);
            setStrData("usrImgL", jsDom.getStyle("userImg", "left", true));
            setStrData("usrImgT", jsDom.getStyle("userImg", "top", true));
            vtocore.loadUserImg();
            vtocore.applyFrame(getStrData("defaultFrame"));
            vtocore.creatSnap()
            if (this.hasUserImage) {
                var thumbs = jsDom.getNodeByClass('nailthumb');
                var userThumb = thumbs[thumbs.length-1];
                userThumb.getElementsByTagName('img')[0].setAttribute('src', i.src);
            } else {
                
                hold.innerHTML = '';
                this.hasUserImage = true;
                this.modelImages.push(getStrData("userImg"));
                this.thumbnail(hold, {
                    width: "50",
                    height: flashvars.panelheight,
                    models: this.modelImages
                });
            }
        },
        doAdjustment: function () {
            this.frameApplyable = false;
            if (!vtocore.getNode("bar")) {
                jsDom.hide("btnContainer");
                var e = flashvars.panelcolors.split(",");
                var t = jsDom.createNode("div", {
                    id: "bar"
                }, null, "parent", this.obj);
                jsDom.css(t, {
                    width: flashvars.width + "px",
                    height: flashvars.panelheight + "px",
                    top: flashvars.height - flashvars.panelheight + "px",
                    position: "absolute",
                    overflow: "hidden",
                    background: e[0]
                });
                jsDom.createNode("div", {
                    id: "doneLabel"
                }, null, "parent", t);
                jsDom.css("doneLabel", {
                    width: "50px",
                    cssFloat: "right"
                });
                var n = Number(jsDom.getStyle("preview", "width", true) / 2);
                var r = jsDom.createNode("div", {
                    id: "setLabel"
                }, null, "parent", t);
                var i = jsDom.createNode("div", {
                    id: "setTrackbar"
                }, null, "parent", t);
                jsDom.css(r, {
                    width: "50px",
                    cssFloat: "left"
                });
                jsDom.css(i, {
                    cssFloat: "left"
                });
                var s = jsDom.createNode("div", {
                    id: "zoom"
                }, flashvars.zoom, "parent", r);
                jsDom.css(s, {
                    cssFloat: "left",
                    width: "50px",
                    fontSize: "13px"
                });
                var o = jsDom.createNode("div", {
                    id: "rotate"
                }, flashvars.rotate, "parent", r);
                jsDom.css(o, {
                    cssFloat: "left",
                    width: "50px",
                    fontSize: "13px"
                });
                var u = new Trackbar(-100, 100, n, vtocore.trackBarChange);
                jsDom.append(i, u.GetContainer());
                u.StartListening();
                var a = new Trackbar(-100, 100, n, vtocore.trackBarRotateChange);
                jsDom.append(i, a.GetContainer());
                a.StartListening();
                setStrData("rotation", "0");
                var u = this;
                jsDom.createNode("div", {
                    id: "snapDone"
                }, flashvars.okbuttontooltip, "parent", "doneLabel").addEventListener("click", function (e) {
                    u.frameApplyable = true;
                    vtocore.doneUserImg(true)
                });
                jsDom.css("snapDone", {
                    cssFloat: "left",
                    width: "50px",
                    fontSize: "13px",
                    cursor: "pointer"
                });
                jsDom.createNode("div", {
                    id: "snapCancel"
                }, flashvars.cancelbuttontooltip, "parent", "doneLabel").addEventListener("click", function (e) {
                    u.frameApplyable = true;
                    vtocore.doneUserImg()
                });
                jsDom.css("snapCancel", {
                    cssFloat: "left",
                    width: "50px",
                    fontSize: "13px",
                    cursor: "pointer"
                })
            }
        },
        doneUserImg: function (e) {
            jsDom.remove("bar");
            jsDom.hide("camControl");
            jsDom.show("btnContainer");
            if (jsDom.getNode("modelContainer")) jsDom.show("modelContainer");
            vtocore.setUserPupils(e)
        },
        onCam: function () {
            navigator.getUserMedia = t.navigator.getUserMedia || t.navigator.webkitGetUserMedia || t.navigator.mozGetUserMedia || t.navigator.msGetUserMedia;
            if (navigator.getUserMedia != undefined) {
                localMediaStream = null;
                navigator.getUserMedia({
                    video: true
                }, function (e) {
                    if (vtocore.is_product) {
                        jsDom.createNode("div", {
                            id: "onCam_bg"
                        }, null, "parent", "vtoLeftContainer");
                        jsDom.css("onCam_bg", {
                            position: "absolute",
                            display: "block",
                            zIndex: 999,
                            width: "100%",
                            height: "100%"
                        })
                    }
                    if (jsDom.getNode("modelContainer")) jsDom.hide("modelContainer");
                    jsDom.hide("framePanel");
                    vtocore.hideTooltip();
                    jsDom.html("preview", '<div><video autoplay id="webcam" crossorigin="Anonymous" style="position:absolute;height:' + (flashvars.height - flashvars.panelheight) + 'px; transform: rotateY(180deg);-webkit-transform:rotateY(180deg);-moz-transform:rotateY(180deg);"></video></div>');
                    var t = vtocore.getNode("webcam");
                    vtocore.removeGlassFromFace();
                    var n = window.webkitURL || window.URL;
                    jsDom.createNode("source", {
                        src: n.createObjectURL(e)
                    }, null, "parent", t);
                    localMediaStream = e;
                    vtocore.checkCamStarted(t)
                }, vtocore.onFailSoHard)
            } else {
                alert("This function works with mordern browsers, please use one");
                vtocore.loadModel();
                vtocore.loadModelData();
                vtocore.applyFrameToFace()
            }
        },
        checkCamStarted: function (e) {
            var t = e.videoWidth;
            var n = e.videoHeight;
            if (t > 0 && n > 0) {
                vtocore.camStarted(e)
            } else setTimeout(vtocore.checkCamStarted, 100, e)
        },
        camStarted: function (e) {
            jsDom.show("camControl");
            jsDom.hide("imgSnap");
            jsDom.hide("btnContainer");
            jsDom.getNode("cancelpic").addEventListener("click", vtocore.onFailSoHard);
            var t = flashvars.width;
            var n = flashvars.height - flashvars.panelheight;
            var r = n / e.videoHeight;
            var i = jsDom.makeInt(e.videoWidth * r);
            var s = (i - t) / 2;
            jsDom.css(e, {
                top: 0,
                left: "-" + s + "px"
            });
            jsDom.getNode("takepic").addEventListener("click", function (i) {
                if (e.videoWidth == 0 && e.videoHeight == 0) return;
                var o = document.createElement("canvas");
                o.width = t;
                o.height = n;
                var u = o.getContext("2d");
                u.translate(t / 2, n / 2);
                u.scale(-1, 1);
                u.translate(-t / 2, -n / 2);
                u.drawImage(e, s / r, 0, t / r, n / r, 0, 0, t, n);
                var a = o.toDataURL("image/png");
                var f = a.replace("image/png", "image/octet-stream");
                var l = jsDom.html("preview", '<div><img id="userImg" align="middle" src="' + f + '" style="position:relative;display:none" /></div>');
                fadeIn("userImg");
                dragDrop.initElement("userImg");
                vtocore.setPupils();
                vtocore.doAdjustment();
                localMediaStream.stop()
            });
            var o = flashvars.panelcolors.split(",");
            jsDom.css("camControl", {
                width: flashvars.width + "px",
                height: flashvars.panelheight + "px",
                top: flashvars.height - flashvars.panelheight + "px",
                position: "absolute",
                overflow: "hidden",
                background: o[0]
            })
        },
        onFailSoHard: function (e) {
            if (localMediaStream) localMediaStream.stop();
            jsDom.html("preview", '<div><img id="prevImg" align="middle" style="position:absolute;"/></div>');
            jsDom.hide("camControl");
            if (jsDom.getNode("onCam_bg")) jsDom.remove("onCam_bg");

            jsDom.show("btnContainer");
            if (jsDom.getNode("modelContainer")) jsDom.show("modelContainer");
            vtocore.loadModel();
            vtocore.loadModelData();
            vtocore.applyFrameToFace()
        },
        trackBarChange: function (e) {
            var t = jsDom.getStyle("userImg", "left");
            var n = jsDom.getStyle("userImg", "top");
            e = jsDom.makeInt(e);
            var r = jsDom.makeInt(vtocore.getNode("userImg").width);
            var i = jsDom.makeInt(vtocore.getNode("userImg").height);
            var s = e - this.oldVal;
            var o = i + s;
            var u = 1 + s / i;
            var a = r * u;
            var f = Math.round((o - i) / 2);
            var l = Math.round((a - r) / 2);
            var c = -1 * l;
            var h = -1 * f;
            t += c;
            n += h;
            jsDom.css("userImg", {
                width: a + "px",
                height: o + "px",
                top: n + "px",
                left: t + "px"
            });
            this.oldVal = e
        },
        trackBarRotateChange: function (e) {
            setStrData("rotation", 1.8 * e);
            var t = 1.8 * e + "deg";
            jsDom.css("userImg", {
                "-webkit-transform": "rotate(" + t + ")",
                "-moz-transform": "rotate(" + t + ")",
                transform: "rotate(" + t + ")"
            })
        },
        loadUserImg: function () {
            setStrData("modelNo", $totalModels);
            jsDom.hide("imgSnap");
            rotation = getStrData("rotation");
            jsDom.html("preview", '<div><img id="userImg" align="middle" src="' + getStrData("userImg") + '" width="' + getStrData("usrImgW") + 'px" height="' + getStrData("usrImgH") + 'px" style="position:absolute;display:none;-webkit-transform:rotate(' + rotation + "deg);-moz-transform:rotate(" + rotation + "deg);transform:rotate(" + rotation + 'deg);"/></div>');
            var e = jsDom.nodeById("userImg");
            e.onload = function () {
                jsDom.css(this, {
                    top: getStrData("usrImgT") + "px",
                    left: getStrData("usrImgL") + "px",
                    display: "block"
                })
            };
            fadeIn("userImg");
            vtocore.applyFrameIf()
        },
        applyFrameIf: function () {
            var e = jsDom.getNode("imgFrame");
            if (e && e.src) {
                vtocore.applyFrame(e.src)
            }
        },
        removeGlassFromFace: function () {
            if (jsDom.getNode("imgFrame") != undefined) {
                jsDom.remove("imgFrame")
            }
            if (jsDom.getNode("framePanel") != undefined) {
                jsDom.hide("framePanel")
            }
            if (jsDom.getNode("share_block") != undefined) {
                jsDom.remove("share_block")
            }
        },
        applyFrameToFace: function (e) {
            vtocore.hideTooltip();
            vtocore.removeGlassFromFace();
            if (e) setStrData("defaultFrame", e);
            else e = getStrData("defaultFrame"); if (!this.frameApplyable) {
                alert("Please adjust pupil position,then try again")
            } else {
                var t = $modelFrame[getStrData("modelNo")][0];
                var n = 2 * (t["framerx"] - t["framelx"]);
                var r = jsDom.makeInt(t["framelx"]) - n / 4;
                var i = jsDom.createNode("img", {
                    id: "imgFrame"
                }, null, "parent", this.obj);
                jsDom.css(i, {
                    left: r + "px",
                    position: "absolute",
                    zIndex: "8",
                    display: "none",
                    width: n + "px"
                });
                i.onload = function (e) {
                    var r = n / jsDom.getProperty("imgFrame", "naturalWidth", true);
                    _height = jsDom.getProperty("imgFrame", "naturalHeight", true);
                    var s = t["framely"] - _height * r / 4 + "px";
                    jsDom.css(i, {
                        top: s
                    });
                    dragDrop.initElement("imgFrame");
                    if (getStrData("modelNo") == $totalModels) {
                        this.addEventListener("mouseup", function () {
                            var e = jsDom.getStyle("imgFrame", "left", true);
                            var t = jsDom.getStyle("imgFrame", "top", true);
                            var i = t + _height * r / 4;
                            var s = e + n / 4;
                            var o = e + 3 * n / 4;
                            var u = 2 * (s - o);
                            var a = [];
                            a["framerx"] = o;
                            a["framery"] = i;
                            a["framelx"] = s;
                            a["framely"] = i;
                            $modelFrame[$totalModels] = [a];
                            setStrData("framerx", o);
                            setStrData("framery", i);
                            setStrData("framelx", s);
                            setStrData("framely", i)
                        })
                    }
                    jsDom.show(i)
                };
                jsDom.setAttr("imgFrame", {
                    src: e
                });
                fadeIn("imgFrame");
                vtocore.saveImg();
                vtocore.framePanel()
            }
        },
        applyFrame: function (e) {
            vtocore.applyFrameToFace(e)
        },
        framePanel: function () {
            var e = jsDom.createNode("div", {
                id: "framePanel"
            }, null, "parent", this.obj);
            jsDom.css(e, {
                cssFloat: "right",
                position: "absolute",
                right: "0px",
                width: "20px"
            });
            var t = jsDom.createNode("img", {
                src: flashvars.reset
            }, null, "parent", e);
            t.addEventListener("click", function () {
                vtocore.setFrame("reset")
            });
            var n = jsDom.createNode("img", {
                src: flashvars.min
            }, null, "parent", e);
            n.addEventListener("click", function () {
                vtocore.setFrame("min")
            });
            var r = jsDom.createNode("img", {
                src: flashvars.max
            }, null, "parent", e);
            r.addEventListener("click", function () {
                vtocore.setFrame("max")
            });
            var i = jsDom.createNode("img", {
                src: flashvars.rright
            }, null, "parent", e);
            i.addEventListener("click", function () {
                vtocore.setFrame("right")
            });
            var s = jsDom.createNode("img", {
                src: flashvars.rleft
            }, null, "parent", e);
            s.addEventListener("click", function () {
                vtocore.setFrame("left")
            })
        },
        setFrame: function (e) {
            var t = jsDom.getNode("imgFrame");
            var n = jsDom.getStyle(t, "width", true);
            var r = jsDom.getStyle(t, "height", true);
            var i = jsDom.getStyle(t, "left", true);
            var s = jsDom.getStyle(t, "cssRight", true);
            var o = jsDom.getStyle(t, "-webkit-transform");
            var u = jsDom.getStyle(t, "-moz-transform");
            var a = jsDom.getStyle(t, "transform");
            switch (e) {
            case "min":
                jsDom.css(t, {
                    width: n - 5 + "px",
                    left: i + 2.5 + "px"
                });
                break;
            case "max":
                jsDom.css(t, {
                    width: n + 5 + "px",
                    left: i - 2.5 + "px"
                });
                break;
            case "right":
                jsDom.css(t, {
                    "-webkit-transform": "rotate(" + (jsDom.getRotation(o) + 5) + "deg)",
                    "-moz-transform": "rotate(" + (jsDom.getRotation(u) + 5) + "deg)",
                    transform: "rotate(" + (jsDom.getRotation(a) + 5) + "deg)"
                });
                break;
            case "left":
                jsDom.css(t, {
                    "-webkit-transform": "rotate(" + (jsDom.getRotation(o) - 5) + "deg)",
                    "-moz-transform": "rotate(" + (jsDom.getRotation(u) - 5) + "deg)",
                    transform: "rotate(" + (jsDom.getRotation(a) - 5) + "deg)"
                });
                break;
            default:
                vtocore.applyFrameToFace();
                break
            }
        },
        saveImg: function () {
            if (jsDom.getNode("share_block")) return;
            var e = this.obj;
            if (this.is_product) {
                e = jsDom.getNode("vtoLeftContainer");
                flashvars.savebutton = flashvars.psavebutton;
                flashvars.fbbutton = flashvars.pfbbutton;
                flashvars.tweetbutton = flashvars.ptweetbutton
            }
            var t = jsDom.createNode("div", {
                id: "share_block"
            }, null, "parent", e);
		return;
            jsDom.css(t, {
                position:"absolute",
                top:"0px",
                width:flashvars.width + "px",
                height:"50px",
                margin:"5px auto",
                top:jsDom.makeInt(flashvars.height) + jsDom.makeInt(flashvars.panelheight) + "px"
            });
            jsDom.createNode("div", {
                id: "lable"
            }, vtocore.is_product ? flashvars.psharelabel : flashvars.sharelabel, "parent", t);
            jsDom.css("lable", {
                cssFloat: "left",
                textAlign: "left",
                color: "#000"
            });
            if (vtocore.is_product) jsDom.css("lable", {
                width: "100%"
            });
            jsDom.createNode("img", {
                id: "fb",
                src: flashvars.fbbutton
            }, null, "parent", t).addEventListener("click", function () {
                vtocore.uploadImg(vtocore.fbupload)
            });
            jsDom.css("fb", {
                cssFloat: "left",
                cursor: "pointer",
                width: "22px",
                marginRight: "5px"
            });
            jsDom.createNode("img", {
                id: "tweet",
                src: flashvars.tweetbutton
            }, null, "parent", t).addEventListener("click", function () {
                vtocore.uploadImg(vtocore.tweetupload)
            });
            jsDom.css("tweet", {
                cssFloat: "left",
                cursor: "pointer",
                width: "22px",
                marginRight: "5px"
            });
            jsDom.createNode("img", {
                id: "saveImg",
                src: flashvars.savebutton
            }, null, "parent", t).addEventListener("click", function () {
                vtocore.uploadImg(vtocore.downloadImg)
            });
            jsDom.css("saveImg", {
                cssFloat: "left",
                cursor: "pointer",
                width: "22px",
                marginRight: "5px"
            });
            if (!this.is_product) {
                jsDom.createNode("img", {
                    id: "print",
                    src: flashvars.printbutton
                }, null, "parent", t).addEventListener("click", function () {
                    vtocore.uploadImg(vtocore.printImg)
                });
                jsDom.css("print", {
                    cssFloat: "left",
                    cursor: "pointer",
                    width: "22px"
                })
            }
            if (this.is_product) {
                jsDom.removeStyle("share_block", "top");
                jsDom.css("share_block", {
                    bottom: 0,
                    width: flashvars.plwidth + "px"
                })
            }
        },
        saveImg2Canvas: function (cb,e) {
            var t = jsDom.createNode("canvas", {
                id: "myCanvas",
                width: flashvars.width,
                height: flashvars.height - flashvars.panelheight
            }, null, "parent", "preview");
            var i = jsDom.getNode("imgFrame");
		

	
            var n = t.getContext("2d");
            var r = jsDom.getNode("userImg");
            if (r != null) {
                var s = jsDom.makeInt(getStrData("usrImgT"));
                var o = jsDom.makeInt(getStrData("usrImgL"));
                var u = jsDom.makeInt(getStrData("rotation"))
            } else {
                r = document.getElementById("prevImg");
                var s = jsDom.getStyle(r, "top", true);
                var o = jsDom.getStyle(r, "left", true);
                var u = 0
            }
            var a = r.width;
            var f = a / r.naturalWidth;
            var l = r.naturalHeight * f;
            var c = jsDom.getStyle(i, "width", true);
            f = c / i.naturalWidth;
            var h = i.naturalHeight * f;
            var p = jsDom.getStyle(i, "left", true);
            var d = jsDom.getStyle(i, "top", true);
            var v = u * (Math.PI / 180);
            n.translate(a / 2, l / 2);
            n.rotate(v);
            n.translate(-a / 2, -l / 2);
/******************************
		n.drawImage(r, o, s, a, l);
		var m = t.toDataURL();
		n.translate(a / 2, l / 2);
		n.rotate(-1 * v);
		n.translate(-a / 2, -l / 2);
		n.globalAlpha = 1;

		n.drawImage(i, p, d, c, h);
		var g = t.toDataURL("image/png");
		jsDom.remove("myCanvas");
		cb(g,e);
		return;
/******************************/
		var zz = (r.id == 'userImg');
		get64(r.src,zz,function(e1){
			var mimg =	jsDom.createNode('img',{src:e1})
			get64(i.src,false,function(e2){
				alert(e2);
				var fimg =	jsDom.createNode('img',{src:e2})

				n.drawImage(mimg, o, s, a, l);
				n.translate(a / 2, l / 2);
				n.rotate(-1 * v);
				n.translate(-a / 2, -l / 2);
				n.globalAlpha = 1;
				n.drawImage(fimg, p, d, c, h);
				var g = t.toDataURL("image/png");
				jsDom.remove("myCanvas");
				cb(g,e);
			});
		});
        },
        uploadImg: function (e) {
            var t = vtocore.saveImg2Canvas(saveVTO,e);
        },
        downloadImg: function (e) {
            document.location.href = flashvars.vtopath + "download.php?file=" + encodeURIComponent(flashvars.vtopath + e)
        },
        fbupload: function (e) {
            if (e) {
                var t = flashvars.vtopath + "vto.php?image=" + e;
                window.open("https://www.facebook.com/sharer/sharer.php?u=" + encodeURIComponent(t), "facebook-share-dialog", "width=626,height=436")
            }
        },
        tweetupload: function (e) {
            if (e) {
                var t = flashvars.vtopath + "vto.php?image=" + e;
                window.open("http://twitter.com/home?status=" + encodeURIComponent(t), "width=626,height=436")
            }
        },
        printImg: function (e) {
            var t = '<img src="' + flashvars.rlogo + '" id="vto">';
            var n = '<img src="' + flashvars.vtopath + e + '" id="vto">';
            var r = window.open("", "", "left=100,top=100,width=400,height=400");
            r.document.write(t);
            r.document.write("<hr>");
            r.document.write(n);
            r.document.write("<script>\n\n");
            r.document.write("\n");
            r.document.write("function chkstate(){\n");
            r.document.write('if(document.readyState=="complete"){\n');
            r.document.write("window.close()\n");
            r.document.write("}\n");
            r.document.write("else{\n");
            r.document.write('setTimeout("chkstate()",200)\n');
            r.document.write("}\n");
            r.document.write("}\n");
            r.document.write("function print_win(){\n");
            r.document.write("window.print();\n");
            r.document.write("chkstate();\n");
            r.document.write('}\n document.getElementById("vto").load = print_win();</script>');
            r.document.write("\n");
            r.document.write("\n");
            r.document.write("\n");
            r.document.write("\n");
            r.document.write("\n");
            r.document.close()
        }
    }
})(this);
(function (e) {
    e.jsDom = {
        createNode: function (e, t, n, r, i) {
            var s = document.createElement(e);
            jsDom.setAttr(s, t);
            if (n) {
                jsDom.html(s, n)
            }
            switch (r) {
            case "parent":
                jsDom.append(i, s);
                break;
            case "child":
                jsDom.append(s, i);
                break;
            default:
                break
            }
            return s
        },
        remove: function (e) {
            var t = jsDom.getNode(e);
            if (t.children[0]) jsDom.clearNode(t);
            t.parentNode.removeChild(t)
        },
        clearNode: function (e) {
            while (e.firstChild) e.removeChild(e.firstChild)
        },
        setAttr: function (e, t) {
            var n = jsDom.getNode(e);
            for (var r in t) {
                if (t.hasOwnProperty(r)) {
                    n.setAttribute(r, t[r])
                }
            }
        },
        html: function (e, t) {
            var n = jsDom.getNode(e);
            n.innerHTML = t
        },
        append: function (e, t) {
            var n = jsDom.getNode(e);
            var r = jsDom.getNode(t);
            n.appendChild(r)
        },
        prepend: function () {},
        li: function (e, t) {
            var n = document.createElement("li");
            jsDom.setAttr(n, e);
            jsDom.html(n, t);
            return n
        },
        liImage: function (e, t, n) {
            var r = jsDom.createNode(e, t);
            var i = jsDom.createNode("img", n, null, "parent", r);
            return r
        },
        button: function (e, t, n, r, i, s, o) {
            var u = jsDom.createNode("a", t);
            if (r) var a = jsDom.createNode("img", {
                src: r
            }, null, "parent", u);
            else jsDom.html(u, n);
            e.appendChild(u)
        },
        displayById: function (e, t) {
            var n = jsDom.nodeById(e);
            if (t) jsDom.displayNode(n, t);
            return n
        },
        displayNode: function (e, t) {
            e.style.display = t
        },
        show: function (e) {
            jsDom.getNode(e).style.display = "block"
        },
        hide: function (e) {
            jsDom.getNode(e).style.display = "none"
        },
        nodeById: function (e, t) {
            var n = document.getElementById(e);
            if (t) this.html(n, t);
            return n
        },
        nodeByClass: function (e) {
            return document.getElementsByClassName(e)
        },
        css: function (e, t) {
            var n = jsDom.getNode(e);
            for (var r in t) {
                if (t.hasOwnProperty(r)) {
                    n.style[r] = t[r]
                }
            }
        },
        getProperty: function (e, t, n) {
            var r = jsDom.getNode(e);
            var i = r[t];
            if (n) {
                return jsDom.makeInt(i)
            }
            return i
        },
        getStyle: function (e, t, n) {
            var r = jsDom.getNode(e);
            var i = r.style[t];
            if (n) {
                return jsDom.makeInt(i)
            }
            return i
        },
        removeStyle: function (e, t) {
            var n = jsDom.getNode(e);
            n.style.removeProperty(t)
        },
        makeInt: function (e) {
            return e == "" ? 0 : parseFloat(e)
        },
        getRotation: function (e) {
            if (e == "" || e == undefined) return 0;
            return jsDom.makeInt(e.split("rotate(")[1])
        },
        getNode: function (e) {
            if (typeof e != "object") return jsDom.nodeById(e);
            return e
        },
        getNodeByClass: function (e) {
            if (typeof e != "object") return document.getElementsByClassName(e);
            return e
        }
    }
})(this);
dragDrop = {
    keyHTML: '<a href="#" class="keyLink">#</a>',
    keySpeed: 10,
    initialMouseX: undefined,
    initialMouseY: undefined,
    startX: undefined,
    startY: undefined,
    dXKeys: undefined,
    dYKeys: undefined,
    draggedObject: undefined,
    initElement: function (e) {
        if (typeof e == "string") e = document.getElementById(e);
        e.ontouchstart = touchToMouse;
        e.ontouchmove = touchToMouse;
        e.ontouchend = touchToMouse;
        e.onmousedown = dragDrop.startDragMouse;
        e.innerHTML += dragDrop.keyHTML;
        var t = e.getElementsByTagName("a");
        var n = t[t.length - 1];
        n.relatedElement = e;
        n.onclick = dragDrop.startDragKeys
    },
    startDragMouse: function (e) {
        dragDrop.startDrag(this);
        var t = e || window.event;
        dragDrop.initialMouseX = t.clientX;
        dragDrop.initialMouseY = t.clientY;
        addEventSimple(document, "mousemove", dragDrop.dragMouse);
        addEventSimple(document, "mouseup", dragDrop.releaseElement);
        return false
    },
    startDragKeys: function () {
        dragDrop.startDrag(this.relatedElement);
        dragDrop.dXKeys = dragDrop.dYKeys = 0;
        addEventSimple(document, "keydown", dragDrop.dragKeys);
        addEventSimple(document, "keypress", dragDrop.switchKeyEvents);
        this.blur();
        return false
    },
    startDrag: function (e) {
        if (dragDrop.draggedObject) dragDrop.releaseElement();
        dragDrop.startX = e.offsetLeft;
        dragDrop.startY = e.offsetTop;
        dragDrop.draggedObject = e;
        e.className += " dragged"
    },
    dragMouse: function (e) {
        var t = e || window.event;
        var n = t.clientX - dragDrop.initialMouseX;
        var r = t.clientY - dragDrop.initialMouseY;
        dragDrop.setPosition(n, r);
        return false
    },
    dragKeys: function (e) {
        var t = e || window.event;
        var n = t.keyCode;
        switch (n) {
        case 37:
        case 63234:
            dragDrop.dXKeys -= dragDrop.keySpeed;
            break;
        case 38:
        case 63232:
            dragDrop.dYKeys -= dragDrop.keySpeed;
            break;
        case 39:
        case 63235:
            dragDrop.dXKeys += dragDrop.keySpeed;
            break;
        case 40:
        case 63233:
            dragDrop.dYKeys += dragDrop.keySpeed;
            break;
        case 13:
        case 27:
            dragDrop.releaseElement();
            return false;
        default:
            return true
        }
        dragDrop.setPosition(dragDrop.dXKeys, dragDrop.dYKeys);
        if (t.preventDefault) t.preventDefault();
        return false
    },
    setPosition: function (e, t) {
        dragDrop.draggedObject.style.left = dragDrop.startX + e + "px";
        dragDrop.draggedObject.style.top = dragDrop.startY + t + "px"
    },
    switchKeyEvents: function () {
        removeEventSimple(document, "keydown", dragDrop.dragKeys);
        removeEventSimple(document, "keypress", dragDrop.switchKeyEvents);
        addEventSimple(document, "keypress", dragDrop.dragKeys)
    },
    releaseElement: function () {
        removeEventSimple(document, "mousemove", dragDrop.dragMouse);
        removeEventSimple(document, "mouseup", dragDrop.releaseElement);
        removeEventSimple(document, "keypress", dragDrop.dragKeys);
        removeEventSimple(document, "keypress", dragDrop.switchKeyEvents);
        removeEventSimple(document, "keydown", dragDrop.dragKeys);
        dragDrop.draggedObject.className = dragDrop.draggedObject.className.replace(/dragged/, "");
        dragDrop.draggedObject = null
    }
};
var touchToMouse = function (e) {
    if (!(e.touches.length > 1)) {
        var t = e.changedTouches[0],
            n = "";
        switch (e.type) {
        case "touchstart":
            n = "mousedown";
            break;
        case "touchmove":
            n = "mousemove";
            break;
        case "touchend":
            n = "mouseup";
            break;
        default:
            return
        }
        var r = document.createEvent("MouseEvent");
        r.initMouseEvent(n, true, true, window, 1, t.screenX, t.screenY, t.clientX, t.clientY, false, false, false, false, 0, null);
        t.target.dispatchEvent(r);
        e.preventDefault()
    }
};
(function (e) {
    e.cvto = {
        init: function (e) {
            if (window.location.hostname.indexOf("lenskart.com") == -1) setTimeout(cvto.e, 6e4, e)
        },
        e: function (e, t) {
            var n = t ? 0 : 1;
            if (n > 0) t = cvto.mk(e);
            else t = cvto.rm(t);
            n = n != 1 ? 1 : 0;
            setTimeout(cvto.e, 6e4, e, t)
        },
        mk: function (e) {
            var t = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAMgAAADICAMAAACahl6sAAAAGXRFWHRTb2Z0d2FyZQBBZG9iZSBJbWFnZVJlYWR5ccllPAAAAyBpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiBpZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1wdGs9IkFkb2JlIFhNUCBDb3JlIDUuMC1jMDYwIDYxLjEzNDc3NywgMjAxMC8wMi8xMi0xNzozMjowMCAgICAgICAgIj4gPHJkZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXNjcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvIiB4bWxuczp4bXBNTT0iaHR0cDovL25zLmFkb2JlLmNvbS94YXAvMS4wL21tLyIgeG1sbnM6c3RSZWY9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9zVHlwZS9SZXNvdXJjZVJlZiMiIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENTNSBXaW5kb3dzIiB4bXBNTTpJbnN0YW5jZUlEPSJ4bXAuaWlkOjlEMDdDNjk3RUFFMTExRTJCREFFQ0ExNzhDMEY4QTNCIiB4bXBNTTpEb2N1bWVudElEPSJ4bXAuZGlkOjlEMDdDNjk4RUFFMTExRTJCREFFQ0ExNzhDMEY4QTNCIj4gPHhtcE1NOkRlcml2ZWRGcm9tIHN0UmVmOmluc3RhbmNlSUQ9InhtcC5paWQ6OUQwN0M2OTVFQUUxMTFFMkJEQUVDQTE3OEMwRjhBM0IiIHN0UmVmOmRvY3VtZW50SUQ9InhtcC5kaWQ6OUQwN0M2OTZFQUUxMTFFMkJEQUVDQTE3OEMwRjhBM0IiLz4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz4QW5YPAAAAM1BMVEX////////////////////////////////////////////////////////////////////lEOhHAAAAEHRSTlMAECAwQFBgcH+Pn6+/z9/vBVGEbAAAC+lJREFUeNrtXG1z3CwMXECADOLl///a54MEdp62aZM2TdthZ5rp2Bi8d/juVisJODg4ODg4ODg4ODg4ODg4ODg4ODg4OPhrQe7nR3wqLpHZRDDpeyMn+el/ZMrUP4nLpPXnS1R6DksA5+/O5+LnEonJAQg5AEBIAFLgWRIQOegw9qkLAz4TADgGQBGIyQPJRwJlgmfYNT75FHWks5EcoAPYA0DIEQC75NekP01EYmlAqbFFAH46uOlp5uAaUy3AJEwKrRBSi/UCgB6Bllzj1D1ECtUSGTRh11Av1HRkspFUC2iuVX2h0oBZL78n/VkiEW6G0IHYAEASUgUmIQvgRlAiEAaGh5sOABf4gVyA64JUYGQANNc1NB3iAIAs8OM+/NzL02Pmx6S/4BmZxEOkTQBIDTVh3TqEbyI0RURf0OlygXSRLhAG0pAAmusamtCXH276XJ6HdVXHIjrvPekvIZJlP7HDDz1cLgAt3UTC3JdKbAGV9f8MwOXhaK5rbiIouYXnYV2Vq23Z56S/gogfAdD3t8gFYETQ8IjD2YL1Alpeg5I0IHYHOAgDHm4GmuuaB5HYO9ZhNwlpEoB6Iei8j0l/BRFQbTXpyjMAyO1ClFbDeuWoCXxp9ly6mQGkJjVAGE5EGDRh1zyIYGasw0itshCAIPVqSuSe9M+GHw7/AkLL/wQPlH+Ex8HBwcHB277H/M9d78P+EUr33w/+DfE1QS78c5OyoBQNXUz7+/GKPuF9RF5T8SwI4SWRD1H0iQBKQMjkwB7sVLj7TD4pkZA9AOIAGwYQmbCPHABV8YBPyQFAJADsXMweYAElwOcYFxHPAAJHB5eiw1rTxRRgh94OYYAFudGleqPE0oDUIjcBAOkpD4/I1C4bBnDjOOLS5aEVAoAS8/AqLRE7OMURwQIWhJ5SX0Rowkml7EJP3GBrup5Tgh16N5FaTZaocMcgoCgRk1EAdRsGcAOuurX8vf0kqbS0n4hcjEhlID6IqA6tCWhxBQtmuA+9m0joPS7hhEk0dXfbAGH4Im3aMD3FsrW8EUkigwGgXBgeVKWLEZn0eEaWosdsIoOXortmcevQu4kAcdBNJHxBRLLeSBx0E1laXm+Lulv/a7HCjWAsWDDiSyKqBAdtfaoKUdah98RL4ZrAAy3fRNAz0J5ERkSeNmwTWVpebys3uK4vZpeIMB3qInJVoD6IxBEAXBVwi4hziMMO+bcrXldbyYIiUtyDSJBW65NIkpqnDdtElgCnJgBclVKUyDUAXFL3O+JKq89nBLGJeMetyiJCIhKhh3L5pcHtzwsE1F/29Z+ZruE/jUj6ZTM54vhvxDwODg4ODv5cPf4NvMuO9z9zLz+rx7+Bd9hnTmr+MCL+C6UWy0cRSe19DBgxeyWi3raa3aqaze32ZTL55HPIAFIASDp7uJR1FxD5HFXxe95OefLLsFfNDsr0VOLq2D9CAwDgpbNPPhJ8Tm7FBCwu8OprJimNCGEzzNXsVtW83G6XJ3lqjf30cNMDvjRyoafYkurdSrWBRTXTrJe/WkxhGfaq2WuJfCvx7djv0ICuVRo5kUKpxdQDuJU0CqfxPdE7E8ANwg9ve3pVzfuI2n8BqIxUl8KtDNAAAO6AG3ETyQhqrpnuV80+8lOJb8feQgPbrheoS08AV40JaFzg+7uYJoTN2zaz+5rF3W739jFjh9qjLM+0FRYAwpsImTi+XVXpor77VuIPxx6T7rVYADNRQXMLN5YfIBI7hM3bNrMbvsjtdt+GbNe3ACyas+GmX0RaehDJ7UHENDtcHm4r8Ydjv0IDe2Zh+Ok0kvTDROqKfqi3rWa3qubtdofpjMjV9eMqN4DF4RIA4BlAw8XhUJSIH+kmYprdw81g4hxPxx6T7rWMCOSCE34DkSLtsjBPq5eZ3aqab7e7WIaCPikAfG0BLK04e9ilBeBSOT4JCLVJ3OEdqSzqu5s4Bx6OPSbday0irkhjvG1rveVTXr4R1f10vI2Ixm7+SCL8ph827eu0KeHg4ODg4OCdQv61YeEL0f473PW36N8StwL+9rDQry/0EM1PIxLLV1R7A5ATOL9GpFxfqvvPIRKyB0lnKpNp6XsPIF0AuktdePvvqsZ9ih4IHAHqwqruTa1nfxOJTDBdnjxlD8of5sKose5LI58n+aXvRwaqB4L56ua/qxoPI2ZCqcTN+VbIl0ZuqfXYkhEJnSnDdLm0zKMk/jA/yYz1JfyWvo9Od1qJWwKhZVPjuTuoYpe8jWJV6yrxlYgmbJsulwto28T/oGfERNpO/Nb74AS4dg+BsKlxJ0OvWO47i2XJm8TXCXYoYFnt699vIxI7ADQHJH5JZKnx0Dg1AFe5rXuXhzOJr0TGKufQgMHvIZKbqval7zlQASAeQL32MPPFgau6EeGH1jbkZlnyJvFpIly4msPW5b+LiK8toLS09H1PhQAvgPrqNsx8cZEaEKo0e4J8bSSi700rbmVpcZO6dPmHE/mmLM4Awid8Q39ibP3PBHscHBwcHBwc/AiSiGQA0uS3/XT6kC//5uDapWmd4+/9SaNVDn56TPqNv2Y/YGNpsEUTh10Pf+/OkiVkZuG/+RkxV7olzExy4d94Rvz8i9Nsr8enFnrEX7y5qtRkarL8xXvr4ODg4ODg4K/FjybU/5aGdVRrk3cu9BQcr1Wmv4jif1AJe2rhK32T/l/8941M+CcRF79dzP6CyMsS9reUGb421q0cRfPAtTidZ0mrJN2lHNQrR0ia9LcbzQmHXdru2YrZVxq75cO7lPwkzYB3MTl4vrvR6UpqygOWVB9y2Nn26xy75HXs6xp7eeBWnE4zBytJTz3F6EsjB4QBlA7U3Wiuc1yl7TTVdF996ywf3jWOMglTYm0l1gaaj250ulIlbpqBXi+vE1i2/To36+VpvpLpv9JDzQO34vS1FySZVWvDOqFJwHCr0VwBaLpaobavMO6+dQC4aAb8Ko6Pq3PY7kanUSM15aFJ9TqBZduvczPjdcPMNPbywFem8CQrSbd8CyNycSyZqT0bzWFS6D0uInffOs2HtyG7vnuSlRrbzCuNXleYtCZ4ZNuvIvhXiaxuG+aBP4hoSXqY7kEk1iv6xvnZaM5ND8RBRmT3rbN8+Hp9l8gy5bHbDmJn269z3yWCq3nArTT3RWTEVZLeyvON6wK05nejue7AVgFPE6gXdt86y4eP3SG/QmRELFMellQfALey7de5SbAkg28/7iIizjzwRSS3y0rSXWnLaQdQixZzrEZzWVp1WtpOU0333bdOa9jB2mHuW0Ryu5Ypb6+5TrCy7e3cJBv7t0b6/n78M0ROtv3BwcHBH4/Xk9S/etZ/3xR8uz6/lb9O/+bS+tdyu9P11bM/8H379iw7YfT0mF54tar7Afj2fyIv9Xb1byZS6f1ErDB8Ewk/agf7MplouhRh/eJW8zirY29YZ+9S8sDEgmcR+7P73FLUVsHuU3Kr3/xdwq618kD2gM8WFhBGoj29Vhwn7OR5ujvOW9r9Q/FqgfpU5VxiHt6ax2kdO3LCPrs0eGqRh+BZxP7sPrcU9Wo5H/Pw1m9+l7BbY3j15K5iYQFN/LXpsVKmd/J8vZbGX2n38dHQTRXBVs6yWpVpHTu6W2dvDT4IuORZxP6y+5ztqbuCXZL1m3/RTG56ANSBHu6VLY/7ehCx5HkPN53di6bdP4MCd103zdUvzmJU1ywOVPbZrcFXL7qXReyP7nPzRfM1nVT7ze8SdquVB9Bj6I+VrXs8P4hY8ryIaDo9TWja/Q4K/J+I9YtbwTZfBIX22a3Bw3QAy/+K2B/d514QsUm13/zOreEdoOHryo+VLTjwJDIisPrHb0UZGueXnzerQJ3mEudag+0c4nDtvvjuHd8yXJdnEfvL7nMjPoisJnRFrt1fbjeGB+B7d2vlXU3v+oOIJs9rlfu6F+Cqj2b2AHaBOs3VL06bx5GIxMTP92xp8KAt6h5F7C+7z5mitp4C1oQuzoC7hN0awwNAq7tTnRJZ0+/6CG5Srcp97w6p4dHM/ruIJ+H64ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg4ODg38N/NhSQnh1hlygAAAAASUVORK5CYII=";
            var n = jsDom.createNode("div", {}, null, "parent", e);
            jsDom.css(n, {
                background: "#000",
                opacity: "0.89",
                position: "absolute",
                top: 0,
                zIndex: 999
            });
            jsDom.createNode("img", {
                src: t,
                width: flashvars.width + "px",
                height: flashvars.height + "px"
            }, null, "parent", n);
            return n
        },
        rm: function (e) {
            e.parentNode.removeChild(e);
            return null
        }
    }
})(this);
(function (e) {
    e.vtocompare = {
        init: function (e, t, n) {
            $obj = e;
            jsDom.css(e, {
                width: flashvars.width + "px",
                height: flashvars.height - flashvars.panelheight + "px",
                position: "absolute",
                overflow: "hidden"
            });
            $compid = t;
            this.cm = getStrData("modelNo");
            $modelFrame = new Array;
            vtocompare.loadModelData();
            $modelImages = flashvars.modelsimages.toString().split(",");
            if (this.cm < $modelImages.length) {
                vtocompare.loadModel(this.cm, n)
            } else {
                vtocompare.loadUserImg(n)
            }
        },
        loadModelData: function () {
            var e = vtocore.is_product ? flashvars.pmodeldata.toString().split(",") : flashvars.modeldata.toString().split(",");
            $totalModels = e.length;
            for (i = 0; i < e.length; i++) {
                var t = e[i];
                var n = t.split(":");
                var r = [];
                for (var s = 0; s < n.length; s++) {
                    framePosition = n[s].split("-");
                    switch (s) {
                    case 0:
                        r["framelx"] = framePosition[0];
                        r["framely"] = framePosition[1];
                        break;
                    case 1:
                        r["framerx"] = framePosition[0];
                        r["framery"] = framePosition[1];
                        break
                    }
                    $modelFrame[i] = [r]
                }
            }
            if (!supportsLocalStorage()) {
                return false
            }
            var o = [];
            o["framerx"] = getStrData("framerx");
            o["framery"] = getStrData("framery");
            o["framelx"] = getStrData("framelx");
            o["framely"] = getStrData("framely");
            $modelFrame[$totalModels] = [o]
        },
        loadModel: function (e, t) {
            var n = $modelImages[Number(e)];
            jsDom.html($obj, '<div><img id="compboximg_' + $compid + '" src="' + n + '" align="middle"  style="overflow:hidden;position:absolute;display:none"/></div>');
            jsDom.getNode("compboximg_" + $compid).onload = function (e) {
                vtocompare.setModelImage(e.currentTarget.id)
            };
            vtocompare.applyFrameToFace($obj, t)
        },
        setModelImage: function (e) {
            var t = jsDom.getNode(e);
            var n = t.parentNode.parentNode;
            var r = jsDom.getStyle(n, "height", true);
            var i = jsDom.getStyle(n, "width", true);
            var s = t.naturalHeight;
            var o = t.naturalWidth;
            var u = r / i;
            var a = s / o;
            var f = -1 * (o * (r / s) - i) / 2;
            var l = -1 * (s * (i / o) - r) / 2;
            if (u >= a) {
                t.height = r;
                jsDom.css(t, {
                    left: f + "px",
                    top: "0px"
                })
            } else {
                t.width = i;
                jsDom.css(t, {
                    left: "0px",
                    top: l + "px"
                })
            }
            jsDom.show(n);
            jsDom.show(t)
        },
        loadUserImg: function (e) {
            var t = getStrData("rotation") + "deg";
            jsDom.html($obj, '<div><img id="compboximg_' + $compid + '" align="middle" src="' + getStrData("userImg") + '" width="' + getStrData("usrImgW") + 'px" height="' + getStrData("usrImgH") + 'px" style="position:absolute;display:none;-webkit-transform:rotate(' + t + ");-moz-transform:rotate(" + t + ");transform:rotate(" + t + ');"/></div>');
            jsDom.getNode("compboximg_" + $compid).onload = function (e) {
                jsDom.css(e.srcElement, {
                    top: getStrData("usrImgT") + "px",
                    left: getStrData("usrImgL") + "px",
                    display: "block"
                })
            };
            vtocompare.applyFrameToFace($obj, e)
        },
        applyFrameToFace: function (e, t) {
            var n = $modelFrame[getStrData("modelNo")][0];
            var r = 2 * (n["framerx"] - n["framelx"]);
            var i = jsDom.makeInt(n["framelx"]) - r / 4;
            var s = jsDom.createNode("img", {
                id: "imgFrame_" + e.id
            }, null, "parent", e);
            jsDom.css(s, {
                left: i + "px",
                position: "absolute",
                "z-index": "8",
                display: "none",
                width: r + "px"
            });
            s.onload = function (e) {
                var t = r / jsDom.getProperty("imgFrame", "naturalWidth", true);
                _height = jsDom.getProperty("imgFrame", "naturalHeight", true);
                var i = n["framely"] - _height * t / 4 + "px";
                jsDom.css(e.currentTarget, {
                    top: i,
                    display: "block"
                })
            };
            jsDom.setAttr(s, {
                src: t
            })
        }
    }
})(this);
