@extends('app')

@section('helper')
    {{--@if($repository->getHelpBlock())--}}
        {!! $repository->getHelpBlock() !!}
    {{--@endif--}}
@endsection

@section('sticky-header', ' ')


@section('content')

        @yield('helper')
        <div class="links-panel">
            <a href="{{ \URL::previous() }}" class="back-link">{{ $repository->getCategoryName() }} ({{ $repository->getTotalCount() }})</a>
        </div>

        <div class="sorting-panel sticky-always-top">
            <div class="sorting-panel__holder sticky-item">
                <div class="btn-sort btn-sort_type_by">
                    <select class="ga-select-sort btn-sort__select" onchange="location.href = this.value;">
                        @foreach($filter->sortLinks() as $item)
                            <option value="{{ $item['url'] }}" @if($item['label'] == $filter->sortLabel()) selected @endif>{{ $item['label'] }}</option>
                        @endforeach
                    </select>
                    <span class="btn-sort__wrap">
                        <span class="btn-sort__ttl btn-sort__ttl_name_sort">SORT BY</span>
                        <span class="btn-sort__text">{{ $filter->sortLabel() }}</span>
                    </span>
                </div>
                <a href="{{ $filter->getFilterPageRoute() }}" class="btn-sort btn-sort_type_filter">
                    <span class="btn-sort__wrap">
                        <span class="btn-sort__ttl btn-sort__ttl_name_filter">FILTER</span>
                        <span class="btn-sort__text">{{ $filter->getFilterLabels() }}</span>
                    </span>
                </a>
            </div>
        </div>
        @if($data)
        <ul id="pr-list" class="products-list">
            @foreach($data as $item)
                @include('product_list.item', ['product' => $item])
            @endforeach
        </ul>
        <span id="pr-loader" style="display: none">
            Loading...
        </span>
        @else
            <div class="message message_danger">Oops! No result found in matching criteria. Please try again
                with fewer keywords/filter values to continue shopping again.</div>
        @endif
        @if($app->make('App\Services\Client')->isOldBrowser())
        <div class="pages">
            @if($filter->hasPrev())
                <a href="{{ $filter->prev() }}" class="button pages__prev">Prev Page</a>
            @endif

            @if($filter->hasNext())
                <a href="{{ $filter->next() }}" class="button pages__next">Next Page</a>
            @endif
        </div>
        @endif

        <div class="trial-panel @if(!$cartState->isFHT() || ($cartState->isFHT() && $cartState->getCount() == 0)) hidden @endif">
            <a href="{{ route('cart') }}" class="trial-panel__btn">Show Cart</a>
            <div class="trial-panel__text-holder">
                <span class="trial-panel__qty global-cart-count">{{ $cartState->getCount() }}</span>
                <div class="trial-panel__text">
                    <p>Items <strong class="trial-panel__ttl">added in trial cart</strong></p>
                </div>
            </div>
        </div>
        <div class="message-unit" data-remove-time="3">
            <div class="message-unit__message">
                <p>The product has been added to 'Free Home Trial' cart.</p>
            </div>
        </div>
        <span class="page-loader">
		    <span class="loader"></span>
	    </span>
@endsection



@section('script')
    @if($filter->hasNext() && !$app->make('App\Services\Client')->isOldBrowser())
    <script>
        $(document).ready(function(){
            var listUrl = '{{ $filter->getPartialLoadingRoute() }}',
                fhtUrl = '{{ route('addToCartFHT') }}',
                page = {{ $filter->getCurrentPage() }},
                bodyHeight = $('#wrapper').height(),
                total = {{ $repository->getTotalCount() }},
                token = '{{ csrf_token() }}',
                isLoaded = true,
                lastScrollTop = 0,
                $loader  = $('#pr-loader'),
                conflictPage = '{{ route('cartConflict') }}';

            $(window).scroll(function (event) {
                var scrollHeight = $(window).scrollTop();

                if((scrollHeight > lastScrollTop) && (bodyHeight - scrollHeight) < 1000 && isLoaded)
                {
                    isLoaded = false;
                    $loader.show();
                    $.ajax({
                        url : listUrl,
                        data : { 'page' : ++page },
                        success: function(data){
                            $('#pr-list').append(data);
                            if($('ul#pr-list > li').size() < total && data.length)
                                isLoaded = true;
                            $loader.hide();
                        }
                    })
                }

                lastScrollTop = scrollHeight;
            });

            $('footer').hide();

            $('.fht-add').on('click', function(){
                var id = $(this).attr('data-id'),
                    $that = $(this),
                    pageLoader = $('.page-loader').data('pageLoaderPlugin');

                pageLoader.show();
                $.ajax({
                        url : fhtUrl,
                        data : { id : id, _token : token },
                        dataType: 'json',
                        method : 'POST',
                        success : function(data){

                            if('error' in data){
                                $that.prop('checked', false);
                            }else{
                                if(data.count > 0){
                                    $('.trial-panel').removeClass('hidden');
                                }else{
                                    $('.trial-panel').addClass('hidden');
                                }

                                $('.global-cart-count').text(data.count);
                            }

                            if(data.message == 'redirect')
                            {
                                window.location.replace(conflictPage);
                            }
                            else
                            {
                                $('.message-unit__message > p').text(data.message);
                                $('.message-unit').data('MessagePlugin').show();

                                pageLoader.hide();
                            }
                        },
                        error : function(xhr, data){
                            console.log(xhr, data);
                            pageLoader.hide();
                            $that.prop('checked', false);
                        }
                    });
            });
        });
    </script>
    @endif
@endsection
