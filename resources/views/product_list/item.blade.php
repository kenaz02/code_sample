<li id="product_{{ $product->id }}" class="product-item">
    <div class="product-item__links">
        @if($product->is_tbyb)
            <span class="check-field">
                <input type="checkbox" id="check-{{ $product->id }}" class="fht-add" data-id="{{ $product->id }}" @if($product->is_in_fht) checked @endif>
                <label for="check-{{ $product->id }}">FREE HOME TRIAL</label>
            </span>
        @endif
        <a href="/{{ $product->product_url }}" class="more-link">SHOP NOW</a>
    </div>
    <a href="/{{ $product->product_url }}">
        <div class="product-item__image">
            <div class="product-item__image-holder">
                <img src="{{ $product->images[0] }}" width="235" height="101" alt="{{ $product->brand_name }}">
                @if($product->offer_image)
                <span class="sticker">
                    <img src="{{ $product->offer_image }}" width="39" height="39">
                </span>
                @endif
            </div>
        </div>
        <div class="product-item__info">
            <div class="product-item__detail">
                <strong class="product-item__name">{{ $product->brand_name }}</strong>
                <p>{{ $product->color }} @if($product->frame_size)<span class="product-item__detail-note">( SIZE : {{ $product->frame_size }} )</span>@endif</p>
            </div>
            <div class="product-item__price-holder">
                @if($product->marketPrice->price != $product->lenskartPrice->price )
                <div class="price-box price-box_type_old">
                    <span class="currency currency_type_indian">{{ $product->marketPrice->currency_sym }}</span> <s class="price">{{ $product->marketPrice->price }}</s>
                </div>
                @endif
                <div class="price-box">
                    <span class="currency currency_type_indian">{{ $product->lenskartPrice->currency_sym }}</span> <span class="price">{{ $product->lenskartPrice->price }}</span>
                </div>
            </div>
        </div>
    </a>
</li>