@extends('product_list.index')

@section('helper')
    <p><b>Search results for "{{ $query }}"</b></p>
@endsection