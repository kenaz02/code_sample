@extends('app')

@section('content')


    <div class="filters-block">
        <form action="#">
            <div class="filters-block__control">
                <a href="{{ \URL::previous() }}" class="back-link">Filter By</a>
                <div class="filters-block__buttons">
                    <button type="button" class="btn-filter btn-filter_type_reset ga-filter-usage">RESET</button>
                    <div class="loader-box">
                        <button id="filter-submit" type="button" class="btn-filter btn-filter_type_apply btn-show-loader ga-filter-usage">APPLY</button>
                        <span class="loader"></span>
                    </div>
                </div>
            </div>
            <div class="chosen-panel">
                <h1 class="chosen-panel__title">Refine Your Results <span class="qty"><span class="filter-total-prod-num">{{ $productNumber or '0' }}</span>Products</span></h1>
                <div class="chosen-filters"></div>
            </div>
            <div class="filter-tabs">
                <ul class="filter-tabset">
                @foreach($filters as $key => $item)
                    <li class="filter-tabset__item @if($key == 0) filter-tabset__item_state_active @endif"><a class="filter-tabset__link" href="#filter-{{ $key }}"><span class="filter-tabset__text">{{ $item['name'] }}<i class="filter-items-qty"></i></span></a></li>
                @endforeach
                </ul>
                <div class="filter-tab-content">
                    @foreach($filters as $key => $item)
                        <div class="filter-container" id="filter-{{ $key }}">
                            <input type="hidden" class="filter-option" value="{{ $item['id'] }}">
                            <ul class="filter-values-list" id="filter-list-{{ $key }}">
                            @foreach($item['options'] as $optionKey => $optionItem)
                                 <li>
                                    <input type="checkbox" value="{{ $optionItem['id'] }}" class="filter-value ga-filter-value" @if($optionItem['selected']) checked @endif id="check-{{ $item['id'] }}-{{ $optionItem['id'] }}" data-ga-label="{{ $optionItem['title'] }}" >
                                    <label for="check-{{ $item['id'] }}-{{ $optionItem['id'] }}" class="filter-label">
                                        <span class="filter-label__wrap">
                                            <span class="filter-text">
                                                {{ $optionItem['title'] }}
                                            </span>
                                            <span class="filter-qty">({{ $optionItem['no_of_products'] }})</span>
                                        </span>
                                    </label>
                                </li>
                            @endforeach
                            </ul>
                        </div>
                    @endforeach
                </div>
            </div>
            <input type="hidden" id="filter-url" value="{{ $listPageUrl }}"/>
            <input type="hidden" id="filter-active-url" value="{{ $activeServiceUrl }}"/>
        </form>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            $('#filter-submit').on('click', function(){
                var urlSeparator,
                    rootUrl,
                    filterURL = '';
                $('.filter-container').each(function(){
                    var values = '';
                    $(this).find('.filter-value').each(function(){
                        if($(this).is(':checked')){
                            values += ',' + $(this).val();
                        }
                    });
                    if(values.length){
                        values = values.substring(1);
                        filterURL += '&' + $(this).find('.filter-option').val() + '=' + values;
                    }
                });

                filterURL = filterURL.substring(1);
                rootUrl = $('#filter-url').val();


                if(filterURL.length == 0){
                    window.location.replace(rootUrl);
                    return false;
                }


                if(rootUrl.indexOf('?') > -1)
                    urlSeparator = '&';
                else
                    urlSeparator = '?';

                window.location.replace(rootUrl + urlSeparator + filterURL);
                return false;
            });

            var updateFilters = function(){
                var rootUrl,
                    requestData = {};

                $('.filter-container').each(function(){
                    var values = '';
                    $(this).find('.filter-value').each(function(){
                        if($(this).is(':checked')){
                            values += ',' + $(this).val();
                        }
                    });

                    if(values.length){
                        values = values.substring(1);
                        requestData[$(this).find('.filter-option').val()] = values;
                    }
                });

                rootUrl = $('#filter-active-url').val();

                $.getJSON(rootUrl, requestData, function(data) {
                    var filters = data.filters;

                    $('.filter-total-prod-num').text(data.productNumber);
                    $('.filter-value').each(function(){
                        var id = $(this).attr('id'),
                            $label =  $('label[for="' + id+ '"]');

                        if(id in filters){
                            $label.removeClass('disabled');
                            $(this).attr('disabled', false);
                            $label.find('.filter-qty').text('(' + filters[id] + ')');
                        }else{
                            $label.addClass('disabled');
                            $(this).attr('disabled', true);
                        }
                    });
                });
            }

            $('.filter-value').on('change', updateFilters);

            updateFilters();
        });
    </script>
@endsection
