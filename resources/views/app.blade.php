@extends('base')

@section('base')
    <header id="header">
        <div class="header-panel @yield('sticky-header', 'sticky-up')">
            <div class="header-panel__wrap sticky-item">
                <div class="header-panel__panel">
                    <div class="header-panel__nav-cell">
                        <div class="main-nav-unit">
                            <a href="#" class="nav-opener"><span class="nav-opener__ico">Menu</span></a>
                            <nav class="main-nav">
                                <div class="main-nav__holder">
                                    <div class="category-unit">
                                        <strong class="category-unit__title">SHOP FOR</strong>
                                        <ul class="category-unit__list">
                                            @foreach(Helper::menu() as $item)
                                                <li class="category-unit__item">
                                                    <a class="category-unit__link category-unit__link_js_opener ga-parent-home" href="#">{{ $item->name }}</a>
                                                    <div class="category-unit__content">
                                                        <ul class="category-unit__sub-list">
                                                            @foreach($item->children as $childItem)
                                                                <li><a href="/{{ $childItem->url }}" class="ga-btn-home">{{ $childItem->name }}</a></li>
                                                            @endforeach
                                                        </ul>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                    <div class="category-unit">
                                        <strong class="category-unit__title">OUR SERVICES</strong>
                                        <ul class="category-unit__list ga-btn-our-services">
                                            <li class="category-unit__item">
                                                <a href="{{ route('homeEyeCheckUp') }}" class="category-unit__link">Home Eye Check-up</a>
                                            </li>
                                            <li class="category-unit__item">
                                                <a href="{{ route('offer', 4289)}}" class="category-unit__link">Free Home Trial</a>
                                            </li>
                                            <li class="category-unit__item">
                                                <a href="#" class="category-unit__link">Optical Stores</a>
                                            </li>
                                            <li class="category-unit__item">
                                                <a href="#" class="category-unit__link">Local Optician v/s Lenskart</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </nav>
                        </div>
                        <div class="logo">
                            <a href="/">
                                <img src="/images/logo.png" width="74" height="33" alt="lenskart">
                            </a>
                        </div>
                    </div>
                    <div class="header-panel__landing-cell">
                        <div class="landing-nav-unit">
                            <a href="#" class="landing-opener"><span class="landing-opener__dots">Open landing links</span></a>
                            <nav class="landing-nav">
                                <ul class="landing-nav__list">
                                    @if($client->isAndroid() || $client->isIOs())
                                        <li class="landing-nav__item">
                                            <a class="landing-nav__link" target="_blank" href="@if($client->isIOs()) https://itunes.apple.com/us/app/lenskart-log-on.-play-on!/id970343205?ls=1&mt=8 @else https://play.google.com/store/apps/details?id=com.lenskart.app @endif">Download App</a>
                                        </li>
                                    @endif
                                    <li class="landing-nav__item">
                                        <a class="landing-nav__link" href="#">Contact Us</a>
                                    </li>
                                    <li class="landing-nav__item">
                                        <a class="landing-nav__link" href="#">Feedback</a>
                                    </li>
                                    <li class="landing-nav__item">
                                        @if(\Auth::check())
                                            <a class="landing-nav__link" href="{{ route('logout') }}">Logout</a>
                                        @else
                                            <a class="landing-nav__link" href="{{ route('login') }}">Login</a>
                                        @endif
                                    </li>
                                    <li class="landing-nav__item">
                                        <a class="landing-nav__link" href="#">Link4</a>
                                    </li>
                                    <li class="landing-nav__item">
                                        <a class="landing-nav__link" href="#">Link5</a>
                                    </li>
                                </ul>
                            </nav>
                        </div>
                    </div>
                    <div class="user-links-block">
                        <div class="user-links">
                            <div class="user-links__item">
                                <a href="{{ route('trackOrder') }}" class="user-links__link user-links__link_type_track">Track Order</a>
                            </div>
                            <div class="user-links__item">
                                <a href=" @if(\Auth::check()) {{ route('orders') }} @else {{ route('login') }} @endif" class="user-links__link user-links__link_type_profile">
                                    Profile
                                    @if(\Auth::check())
                                        <span class="user-links__ico-check"></span>
                                    @endif
                                </a>
                            </div>
                            <div class="user-links__item">
                                <a href="{{ route('cart') }}" class="user-links__link user-links__link_type_cart">
                                    Cart
                                            <span class="user-links__qty-wrap">
                                                <span class="user-links__qty global-cart-count">{{ $cartState->getCount() }}</span>
                                            </span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="search-panel">
            <div class="search-panel__holder">
                        <span class="offer-item">
                            <a href="#" class="ga-hot-offers">
                                <img src="/images/img-offer.png" width="78" height="39" alt="Hot offers">
                            </a>
                        </span>
                <div class="search-holder">
                    {!! Form::open(['route' => 'search', 'method' => 'GET', 'class' => 'search-form']) !!}
                    <div class="text-field">
                        {!! Form::text('q', isset($query) ? $query : '', ['id' => 'search-autocomplete' , 'data-url' => route('searchTerms')]) !!}
                        {!! $errors->first('q', '<div class="alert alert-danger">:message</div>') !!}
                    </div>
                    {!! Form::submit('GO', ['class' => 'btn-submit']) !!}
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </header>
    <div id="main">
        @yield('content')
    </div>
@endsection

@section('footer')
    <footer id="footer">
        <div class="footer-nav-panel @yield('sticky-footer', 'sticky-down')">
            <div class="footer-nav-panel__wrap sticky-item">
                <ul class="footer-nav">
                    <li class="footer-nav__item footer-nav__item_type_home">
                        <a class="footer-nav__link" href="/">Home</a>
                    </li>
                    <li class="footer-nav__item">
                        <a class="footer-nav__link" href="tel:919999899998">Call Us</a>
                    </li>
                    <li class="footer-nav__item">
                        <a class="footer-nav__link" href="#">Offers</a>
                    </li>
                    @if($client->isAndroid() || $client->isIOs())
                    <li class="footer-nav__item">
                        <a class="footer-nav__link" target="_blank" href="@if($client->isIOs()) https://itunes.apple.com/us/app/lenskart-log-on.-play-on!/id970343205?ls=1&mt=8 @else https://play.google.com/store/apps/details?id=com.lenskart.app @endif">Download App</a>
                    </li>
                    @endif
                </ul>
            </div>
        </div>
    </footer>
@endsection
