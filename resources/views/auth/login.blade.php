@extends('app')

@section('content')
<div class="login-block">
    <div class="page-heading">
        <div class="other-link">
            <p>New User <a href="{{ route('register') }}" class="other-link__link">Sign up</a></p>
        </div>
        <h1 class="page-heading__title">Sign in</h1>
    </div>
    <div class="login-buttons">
        <div class="loader-box">
            <a href="{{ route('loginSocial') }}" class="btn-login btn-login_fb btn-show-loader">Sign up with Facebook</a>
            <span class="loader"></span>
        </div>
        <p>(we’ll never post to facebook without your permission)</p>
    </div>
    <div class="or-unit">
        <span class="or-unit__or">or</span>
    </div>
    <div class="form-section">
        {!! $errors->first('submit', '<div class="alert alert-danger">:message</div>') !!}
        <div class="form-box">
            {!! Form::open(['method' => 'POST', 'class' => 'login-form']) !!}
                <div class="login-form__row @if($errors->has('email')) error @endif">
                    <label for="input-login-email">Email Address</label>
                    <div class="form-field">
                        {!!  Form::text('email', old('email'), ['class' => 'input-text required', 'id' => 'input-login-email' ]) !!}
                        {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>
                <div class="login-form__row @if($errors->has('password')) error @endif">
                    <label for="input-login-password">Password</label>
                    <div class="form-field form-field_type_password">
                        {!!  Form::password('password', ['class' => 'input-text required', 'id' => 'input-login-password' ]) !!}
                        {!! $errors->first('password', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>
                <div class="login-form__options">
                    <div class="login-form__options-item">
                        <a href="{{ route('forgotpassword') }}" class="link">Forgot Password?</a>
                        <div class="login-form__check-item">
                            <input type="checkbox" id="show-pass" data-show-pass="input-login-password">
                            <label for="show-pass">Show Password</label>
                        </div>
                    </div>
                </div>
                <div class="form-btn-row">
                    <div class="loader-box">
                        <button type="submit" class="btn-big-submit btn-show-loader">
                            Sign in
                        </button>
                        <span class="loader"></span>
                    </div>
                </div>
            {!! Form::close() !!}
            <div class="other-link">
                <p>New User <a href="{{ route('register') }}" class="other-link__link">Sign up</a></p>
            </div>
        </div>
    </div>
</div>
@endsection

