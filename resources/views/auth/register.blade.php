@extends('app')

@section('content')
    @if($errors->has('submit'))
        <div class="global-error">
            <button type="button" class="global-error__close">Close</button>
            <p>{{ $errors->first('submit', ':message') }}</p>
        </div>
    @endif

    <div class="login-block">
        <div class="page-heading">
            <div class="other-link">
                <p>Existing User <a href="{{ route('login') }}" class="other-link__link">Sign in</a></p>
            </div>
            <h1 class="page-heading__title">Sign up</h1>
        </div>
        <div class="login-buttons">
            <div class="loader-box">
                <a href="{{ route('loginSocial') }}" class="btn-login btn-login_fb btn-show-loader">Sign up with Facebook</a>
                <span class="loader"></span>
            </div>
            <p>(we’ll never post to facebook without your permission)</p>
        </div>
        <div class="or-unit">
            <span class="or-unit__or">or</span>
        </div>
        <div class="form-section">
            <div class="form-box">
                {!! Form::open(['method' => 'POST', 'class' => 'login-form']) !!}

                <div class="login-form__row @if($errors->has('email')) error @endif">
                    <label for="input-signup-email">Email Address <span class="required">*</span></label>
                    <div class="form-field">
                        {!!  Form::text('email', old('email'), ['class' => 'input-text required', 'id' => 'input-signup-email' ]) !!}
                        {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('firstname')) error @endif">
                    <label for="input-signup-firstname">First Name <span class="required">*</span></label>
                    <div class="form-field">
                        {!!  Form::text('firstname', old('firstname'), ['class' => 'input-text required', 'id' => 'input-signup-firstname' ]) !!}
                        {!! $errors->first('firstname', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('lastname')) error @endif">
                    <label for="input-signup-lastname">Last Name <span class="required">*</span></label>
                    <div class="form-field">
                        {!!  Form::text('lastname', old('lastname'), ['class' => 'input-text required', 'id' => 'input-signup-lastname' ]) !!}
                        {!! $errors->first('lastname', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('street0')) error @endif">
                    <label for="input-signup-street0">Street One</label>
                    <div class="form-field">
                        {!!  Form::text('street0', old('street0'), ['class' => 'input-text', 'id' => 'input-signup-street0' ]) !!}
                        {!! $errors->first('street0', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('dob')) error @endif">
                    <label for="input-signup-dob">DOB (optional)</label>
                    <div class="form-field">
                        {!!  Form::text('dob', old('dob'), ['class' => 'input-text date-mask date-birth', 'id' => 'input-signup-dob' ]) !!}
                        {!! $errors->first('dob', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row">
                    <label>Gender</label>
                    <div class="check-unit">
                        <div class="check-unit__item">
                            {!! Form::radio('gender', 'male', true, ['class' => 'input-radio', 'id' => 'gender-male']) !!}
                            {!! Form::label('gender-male', 'Male') !!}
                        </div>
                        <div class="check-unit__item">
                            {!! Form::radio('gender', 'female', false, ['class' => 'input-radio', 'id' => 'gender-female']) !!}
                            {!! Form::label('gender-female', 'Female') !!}
                        </div>
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('password')) error @endif">
                    <label for="input-signup-password">Password <span class="required">*</span></label>
                    <div class="form-field form-field_type_password">
                        {!!  Form::password('password', ['class' => 'input-text required', 'id' => 'input-signup-password', 'data-confirm-pass' => 'input-signup-confirm-password']) !!}
                        {!! $errors->first('password', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>

                <div class="login-form__row @if($errors->has('password_confirmation')) error @endif">
                    <label for="input-signup-confirm-password">Confirm Password <span class="required">*</span></label>
                    <div class="form-field">
                        {!!  Form::password('password_confirmation', ['class' => 'input-text required', 'id' => 'input-signup-confirm-password' ]) !!}
                        {!! $errors->first('password_confirmation', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                </div>


                <div class="login-form__options">
                    <div class="login-form__options-item">
                        <div class="login-form__check-item">
                            <input type="checkbox" id="show-pass" data-show-pass="input-signup-password">
                            <label for="show-pass">Show Password</label>
                        </div>
                    </div>
                    <div class="login-form__options-item @if($errors->has('terms_agreement')) error @endif">
                        <div class="login-form__check-item">
                            {!! Form::checkbox('terms_agreement', '1', false, ['class' => 'input-checkbox', 'id' => 'input-signup-agreement']) !!}
                            <label for="input-signup-agreement">I agree to <a href="#">terms &amp; conditions</a></label>
                            {!! $errors->first('terms_agreement', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                        </div>
                    </div>
                </div>
                <div class="form-btn-row">
                    <div class="form-btn-row">
                        <div class="loader-box">
                            <button type="submit" class="btn-big-submit btn-show-loader">
                                Sign up
                            </button>
                            <span class="loader"></span>
                        </div>
                    </div>
                </div>
                {!! Form::close() !!}
                <div class="other-link">
                    <p>Existing User <a href="{{ route('login') }}" class="other-link__link">Sign in</a></p>
                </div>
            </div>
        </div>
    </div>
@endsection


