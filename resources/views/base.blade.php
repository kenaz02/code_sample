<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">

        <meta name="description" content="Shop online for Sunglasses, Eyeglasses and Contact Lenses for men &amp;amp; women and Get upto 50% Flat Discount at lenskart.com.  ✓FREE shipping ✓Cash on Delivery ✓14 Days Return.">
        <meta name="keywords" content="Online shopping, Eyewear, Contact Lenses">
        <meta name="robots" content="INDEX,FOLLOW">

        <meta property="og:site_name" content="LensKart">
        <meta property="og:type" content="lenskartwebsite:product">
        <meta property="lenskartwebsite:price:currency" content="INR">

        <title>Sunglasses, Eyeglasses and Contact Lenses at  Best Prices | LensKart.com</title>
        <link rel="canonical" href="{{ URL::current() }}" >
        <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700,400italic,600italic,700italic" rel="stylesheet">
        <link rel="stylesheet" href="http://unbxd.s3.amazonaws.com/unbxdASlenskart.css">
        {{--<link media="all" rel="stylesheet" href="{{ elixir("css/all.css") }}">--}}
        <link media="all" rel="stylesheet" href="/css/all.css">
        <script>
            var pathInfo = {
                base: '/',
                css: 'css/'
            }
        </script>
    </head>
    <body class="@yield('body-class', '')">
        <div id="wrapper">
            <div class="main-section">
                @yield('base')
            </div>
            @yield('footer')
        </div>
        @yield('after-footer')

        <!-- Scripts -->
        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
        <script>window.jQuery || document.write('<script src="/js/jquery-1.11.2.min.js"><\/script>')</script>
        <script src="/js/jquery.main.js"></script>
        <script src="/js/analitics-events.js"></script>
        @yield('script')

    <script type="text/javascript">
        var _gaq = _gaq || [];
        _gaq.push(['_setAccount', 'UA-000000']); // This is my account number, I have added the zeros in this editor
        _gaq.push(['_trackPageview']);

        (function() {
        var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
        ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
        var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
        })();
    </script>

    <script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/handlebars.js/2.0.0/handlebars.min.js"></script>
    <script type="text/javascript" src="http://unbxd.s3.amazonaws.com/unbxdAutosuggestV3.js"></script>

    <script src="/js/Lenskart-Autosuggest.js"></script>
    </body>
</html>
