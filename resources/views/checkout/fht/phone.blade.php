@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-heading">
        <a href="{{ route('cart') }}" class="back-link back-link_type_empty"></a>
        <h1 class="cart-heading__title">Free home Trial Checkout</h1>
    </div>

    <div class="trial-checkout">
        <div class="trial-checkout__checkout">
            <div class="trial-checkout__step">
                {!! Form::model($form, ['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}
                <div class="trial-checkout__step-content">
                    <h2 class="trial-checkout__ttl">Where would you like to have trial?</h2>

                    <div class="trial-checkout__form">
                        <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-phone" data-label-text="Mobile phone"><span class="input-row__label-text">Mobile Number</span></label>
                                {!! $errors->first('telephone', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>

                            <div class="form-field">
                                <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif"/>
                                <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif">
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('pincode')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-pincode" data-label-text="Pincode"><span class="input-row__label-text">Pincode</span></label>
                                {!! $errors->first('pincode', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>

                            <div class="form-field">
                                {!! Form::input('number', 'pincode', old('pincode') ? old('pincode') : $form->pincode, ['class' => 'input-text required', 'id' => 'input-pincode', 'pattern' => '\d*']) !!}
                            </div>
                        </div>
                    </div>

                    <p>Customers try us at home and while at office - enter the pincode most convenient to you!</p>
                </div>

                <div class="trial-buttons">
                    <button type="submit" class="btn-save">Add Personal Details</button>
                </div>
                <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_PHONE }}"/>
                {!! Form::close() !!}
            </div>
        </div>

        <div class="trial-checkout-info">
            <p>We give free, at home trial services in 40+ cities in India</p>
            <div class="trial-checkout-info__more">
                <p>See <a href="#">Complete Cities list</a></p>
            </div>
        </div>
    </div>
@endsection