@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-heading">
        <a href="{{ route('cart') }}" class="back-link back-link_type_empty"></a>
        <h1 class="cart-heading__title">Free home Trial Checkout</h1>
    </div>

    @if($errors->has('submit'))
        <div class="global-error">
            <button type="button" class="global-error__close">Close</button>
            {!! $errors->first('submit', '<p>:message</p>') !!}
        </div>
    @endif

    <div class="trial-checkout">
        <div class="trial-checkout__checkout">
            {!! Form::model($form, ['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}
            <div class="trial-checkout__step trial-checkout__step_passed">
                <a href="{{ route('checkout', ['stage' => \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\Checkout\FHT\CommandContract::BACK_STATUS]) }}" class="trial-checkout__edit-link">Edit</a>
                <div class="trial-checkout__passed-text">
                    <ul class="trial-passed-list">
                        <li class="trial-passed-list__item">{{ $form->pincode }}, {{ $form->city }}</li>
                    </ul>
                </div>
            </div>

            <div class="trial-checkout__step trial-checkout__step_passed">
                <a href="{{ route('checkout', ['stage' => \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_ADDRESS, 'back' => \App\Http\Commands\Checkout\FHT\CommandContract::BACK_STATUS]) }}" class="trial-checkout__edit-link">Edit</a>
                <div class="trial-checkout__passed-text">
                    <ul class="trial-passed-list">
                        <li class="trial-passed-list__item">{{ $form->firstname }} {{ $form->lastname }}</li>
                        <li class="trial-passed-list__item">{{ $form->email }}</li>
                        <li class="trial-passed-list__item">{{ Helper::formatPhone($form->telephone) }}</li>
                        <li class="trial-passed-list__item">{{ $form->street0 }}</li>
                        <li class="trial-passed-list__item">{{ $form->city }}</li>
                    </ul>
                </div>
            </div>

            <div class="trial-checkout__step">
                <div class="trial-checkout__step-content">
                    <h2 class="trial-checkout__ttl">Enter code sent to:</h2>
                    <div class="code-unit">
                        <span class="code-unit__number">{{ Helper::formatPhone($form->telephone) }}</span>

                        <a href="{{ route('optResend', [ 'tel' => $form->telephone ]) }}" class="code-unit__resend-link">Resend</a>

                        <div class="trial-checkout__form">
                            <div class="form-row input-row @if($errors->has('otp')) error @endif">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-code" data-label-text="Code"><span class="input-row__label-text">Enter Code</span></label>
                                    {!! $errors->first('otp', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                                </div>

                                <div class="form-field">
                                    {!! Form::input('number', 'otp', old('otp'), ['class' => 'input-text required', 'id' => 'input-code', 'pattern' => '\d*']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="trial-buttons">
                    <div class="loader-box">
                        <button type="submit" class="btn-save btn-show-loader">Confirm your order</button>
                        <span class="loader"></span>
                    </div>
                </div>
            </div>
            <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_CONFIRM }}"/>
            {!! Form::close() !!}
        </div>
    </div>

@endsection