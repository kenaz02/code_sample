@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-heading">
        <a href="{{ route('cart') }}" class="back-link back-link_type_empty"></a>
        <h1 class="cart-heading__title">Free home Trial Checkout</h1>
    </div>

    <div class="trial-checkout">
        <div class="trial-checkout__checkout">
            {!! Form::model($form, ['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}
            <div class="trial-checkout__step trial-checkout__step_passed">
                <a href="{{ route('checkout', ['stage' => \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\Checkout\FHT\CommandContract::BACK_STATUS]) }}" class="trial-checkout__edit-link">Edit</a>
                <div class="trial-checkout__passed-text">
                    <ul class="trial-passed-list">
                        <li class="trial-passed-list__item">{{ $form->pincode }}, {{ $form->city }}</li>
                    </ul>
                </div>
            </div>

            <div class="trial-checkout__step">
                <div class="trial-checkout__step-content">
                    <h2 class="trial-checkout__ttl">Personal Details</h2>
                    <div class="trial-checkout__form">
                        <div class="form-row input-row @if($errors->has('firstname')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-first-name" data-label-text="First name"><span class="input-row__label-text">*First name</span></label>
                                {!! $errors->first('firstname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::text('firstname', old('firstname') ? old('firstname') : $form->firstname, ['class' => 'input-text required', 'id' => 'input-first-name', 'maxlength' => 25]) !!}
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('lastname')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-last-name" data-label-text="Last name"><span class="input-row__label-text">*Last name</span></label>
                                {!! $errors->first('lastname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::text('lastname',old('lastname') ? old('lastname') : $form->lastname , ['class' => 'input-text required', 'id' => 'input-last-name', 'maxlength' => 25]) !!}
                            </div>
                        </div>


                        <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-phone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
                                {!! $errors->first('telephone', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif"/>
                                <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif">
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('telephoneAlt')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-phone-alt" data-label-text="Alternate Mobile phone"><span class="input-row__label-text">*Alternate Mobile Phone: xxx xxx xxxx</span></label>
                                {!! $errors->first('telephoneAlt', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                <input type="tel" class="input-text required" id="input-phone-alt" maxlength="12" value="@if(old('telephoneAlt')){{ old('telephoneAlt') }}@else{{ $form->telephoneAlt }}@endif"/>
                                <input type="hidden" name="telephoneAlt" class="reformat-phone" data-target="#input-phone-alt" value="@if(old('telephoneAlt')){{ old('telephoneAlt') }}@else{{ $form->telephoneAlt }}@endif">
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('email')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-email" data-label-text="Email"><span class="input-row__label-text">*Email: name@domain.com</span></label>
                                {!! $errors->first('email', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::email('email', old('email') ? old('email') : $form->email, ['class' => 'input-text required', 'id' => 'input-email']) !!}
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('street0')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-address" data-label-text="Address"><span class="input-row__label-text">*Address: xx, street</span></label>
                                {!! $errors->first('street0', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::text('street0', old('street0') ? old('street0') : $form->street0, ['class' => 'input-text required', 'id' => 'input-address', 'maxlength' => 100]) !!}
                            </div>
                        </div>

                        <div class="form-row input-row @if($errors->has('locality')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-locality" data-label-text="Locality/town"><span class="input-row__label-text">*Locality / Town</span></label>
                                {!! $errors->first('locality', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            {{--<div class="form-field">--}}
                                <select id="select-town" name="locality">
                                    <option value=""></option>
                                    @foreach($form->localityList as $item)
                                        <option value="{{ $item }}" @if($form->locality == $item || old('locality') == $item) selected @endif>{{ $item }}</option>
                                    @endforeach
                                </select>
                            {{--</div>--}}
                        </div>

                        <div class="details-unit @if($errors->has('landmark') || old('landmark') || $form->landmark) details-unit_expanded @endif">
                            <div class="details-unit__heading">
                                <a href="#" class="details-unit__opener">Add Landmark</a>
                            </div>
                            <div class="details-unit__slide @if(!($errors->has('landmark') || old('landmark') || $form->landmark)) js-slide-hidden @endif ">
                                <div class="details-unit__content">
                                    <div class="form-row input-row @if($errors->has('landmark')) error @endif">
                                        <div class="input-row__label-holder">
                                            <label class="input-row__label" for="input-landmark" data-label-text="Landmark"><span class="input-row__label-text">Landmark</span></label>
                                            {!! $errors->first('landmark', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                                        </div>
                                        <div class="form-field">
                                            {!! Form::textarea('landmark', old('landmark') ? old('landmark') : $form->landmark, ['class' => 'input-textarea', 'id' => 'input-landmark', 'maxlength' => 100]) !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="trial-buttons">
                    <button type="submit" class="btn-save">Continue</button>
                </div>
            </div>
            <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\FHT\CommandContract::STAGE_ADDRESS }}"/>
            {!! Form::close() !!}
        </div>
    </div>

@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){

            var selectize = $('#select-town').selectize({
                create: true,
                sortField: {
                    field: 'text',
                    direction: 'asc'
                },
                dropdownParent: 'body',
                onInitialize: function () {
                    this.$parentRow = this.$input.parent('.input-row');

                    if (this.$control.hasClass('full')) {
                        this.$parentRow.addClass('input--filled');
                    }
                },
                onFocus: function () {
                    this.$parentRow.addClass('input--filled');
                },
                onBlur: function () {
                    if (this.$control.hasClass('not-full')) {
                        this.$parentRow.removeClass('input--filled');
                    }
                }
            });
        });
    </script>
@endsection