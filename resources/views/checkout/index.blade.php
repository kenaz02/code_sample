@extends('home')

@section('content')
    <div class="home-eye-checkup">
        {!! Form::model($model, ['route' => 'homeEyeCheckUp', 'method' => 'POST']) !!}

            <div>
                {!! Form::label('telephone', 'Telephone') !!}
                {!! Form::text('telephone', old('telephone'), ['class' => 'form-control']) !!}
                {!! $errors->first('telephone', '<span class="error">:message</span>') !!}
            </div>

            <div>
                {!! Form::label('pincode', 'Pincode') !!}
                {!! Form::text('pincode', old('pincode'), ['class' => 'form-control']) !!}
                {!! $errors->first('pincode', '<span class="error">:message</span>') !!}
            </div>

            <hr/>
            <br/>
            <div>
                {!! Form::label('date', 'Date') !!}
                {!! Form::text('date', old('date'), ['class' => 'form-control']) !!}
                {!! $errors->first('date', '<span class="error">:message</span>') !!}
            </div>
            <div>
                @foreach($model->time_slot as $item)
                    {!! Form::label('time_slots', $item['time']) !!}
                    {!! Form::radio('time_slots', $item['id'],['class' => 'form-control']) !!}
                    <br/>
                @endforeach
            </div>

            <hr/>
            <br/>

            <div>
                {!! Form::label('firstname', 'First name') !!}
                {!! Form::text('firstname', old('firstname'), ['class' => 'form-control']) !!}
                {!! $errors->first('firstname', '<span class="error">:message</span>') !!}
            </div>
            <div>
                {!! Form::label('lastname', 'Last name') !!}
                {!! Form::text('lastname', old('lastname'), ['class' => 'form-control']) !!}
                {!! $errors->first('lastname', '<span class="error">:message</span>') !!}
            </div>
            <div>
                {!! Form::label('email', 'Last name') !!}
                {!! Form::text('email', old('email'), ['class' => 'form-control']) !!}
                {!! $errors->first('email', '<span class="error">:message</span>') !!}
            </div>
            <div>
                {!! Form::label('street0', 'Address name') !!}
                {!! Form::text('street0', old('street0'), ['class' => 'form-control']) !!}
                {!! $errors->first('street0', '<span class="error">:message</span>') !!}
            </div>
            <div>
                {!! Form::label('city', 'Locality Town') !!}
                {!! Form::select('city', $model->cities)!!}
                {!! $errors->first('city', '<span class="error">:message</span>') !!}
            </div>

            {!! Form::submit('Submit') !!}

        {!! Form::close() !!}
    </div>
@endsection