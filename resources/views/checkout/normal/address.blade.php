@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="checkout-block__content">

        @if($errors->has('checked_address'))
            <div class="global-error">
                <button type="button" class="global-error__close">Close</button>
                <p>{{ $errors->first('checked_address', ':message') }}</p>
            </div>
        @endif

        <div class="checkout-heading">
            <h1 class="cart-heading__title">Shipping information</h1>
            <a href="{{ route('logout') }}" class="shipping-name">{{ $user->email }}</a>
        </div>
        @include('checkout.normal.cart', ['cart' => $cart])

        {!! Form::open(['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}

        <div class="step-box">

            <div class="address-section">
                <ul class="check-listing address-listing">
                    @foreach($addressList as $key => $item)
                        <li class="check-listing__item">
                            <input type="radio" name="checked_address" id="check-address-{{ $key }}" value="{{ $item->id }}" @if($form->id  == $item->id)checked="checked"@endif>

                            <label for="check-address-{{ $key }}">
												<span class="address-unit">
													<span class="address-unit__item">{{ $item->street0 }}</span>
													<span class="address-unit__item">{{ $item->state }}, {{ $item->pincode }} {{ $item->city }},</span>
													<span class="address-unit__item">{{ $item->country }}</span>
												</span>
                            </label>

                            <div class="edit-list">
                                <a class="edit-list__item edit-list__item_edit" href="{{ route('addressEdit', ['id' => $item->id]) }}">Edit</a>
                                <a class="edit-list__item edit-list__item_del" href="{{ route('addressDelete', ['id' => $item->id]) }}">Delete</a>
                            </div>
                        </li>
                    @endforeach
                </ul>

                <div class="step-box__btn-row">
                    <a href="{{ route('addressNew') }}" class="btn-change">Add new Address</a>
                </div>
            </div>

            <div class="details-list">
                <div class="details-unit">
                    <div class="details-unit__heading">
                        <div class="details-unit__heading-text">
                            <a href="#" class="details-unit__opener">Alternative Phone Number</a>

                            <p>Recommended for COD</p>
                        </div>
                    </div>
                    <div class="details-unit__slide">
                        <div class="details-unit__content">
                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-alternate-phone" data-label-text="Alternate phone"><span class="input-row__label-text">*Alternate Phone: xxx xxx xxxx</span></label>
                                    <span class="error-message"><span class="error-message__holder">Right format is xxx xxx xxxx</span></span>
                                </div>

                                <div class="form-field">
                                    <input type="tel" class="input-text required" id="input-alternate-phone">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="details-unit">
                    <div class="details-unit__heading">
                        <a href="#" class="details-unit__opener">Add Order Comments</a>
                    </div>
                    <div class="details-unit__slide">
                        <div class="details-unit__content">
                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-order-comment" data-label-text="Order Comments"><span class="input-row__label-text">Order Comments</span></label>
                                </div>
                                <div class="form-field">
                                    <textarea id="input-order-comment" class="input-textarea"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="details-unit">
                    <div class="details-unit__heading">
                        <div class="details-unit__check">
                            <input type="checkbox" id="check-gift">
                            <label class="details-unit__opener label-gift" for="check-gift">Gift Wrap My Order</label>
                        </div>
                    </div>
                    <div class="details-unit__slide">
                        <div class="details-unit__content">
                            <div class="details-unit__note">
                                <p>To keep it a surprise, please provide your mobile number below so that we'll inform only you about the order status.</p>
                            </div>

                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-phone-2" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
                                    <span class="error-message"><span class="error-message__holder">Right format is xxx xxx xxxx</span></span>
                                </div>

                                <div class="form-field">
                                    <input type="tel" class="input-text required" id="input-phone-2">
                                </div>
                            </div>

                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-from" data-label-text="From"><span class="input-row__label-text">*From</span></label>
                                    <span class="error-message"><span class="error-message__holder">Enter valid value</span></span>
                                </div>

                                <div class="form-field">
                                    <input type="tel" class="input-text required" id="input-from">
                                </div>
                            </div>

                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-to" data-label-text="To"><span class="input-row__label-text">*To</span></label>
                                    <span class="error-message"><span class="error-message__holder">Enter valid value</span></span>
                                </div>

                                <div class="form-field">
                                    <input type="tel" class="input-text required" id="input-to">
                                </div>
                            </div>

                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-message" data-label-text="message"><span class="input-row__label-text">*message</span></label>
                                </div>
                                <div class="form-field">
                                    <textarea id="input-message" class="input-textarea"></textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="loader-box">
                <button type="submit" class="btn-product btn-show-loader">
                    <span class="btn-product__wrap">continue</span>
                </button>
                <span class="loader"></span>
            </div>
        </div>

        <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\Normal\CommandContract::STAGE_SHIPPING }}"/>
        {!! Form::close() !!}

    </div>
@endsection