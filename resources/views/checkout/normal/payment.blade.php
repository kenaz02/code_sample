@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="checkout-block__content">

        <div class="checkout-heading">
            <a href="{{ route('checkout', [ 'stage' => 'shipping' ]) }}" class="back-link back-link_type_empty"></a>
            <h1 class="cart-heading__title">Payment information</h1>
            @if(\Auth::check())<a href="{{ route('logout') }}" class="shipping-name">{{ $form->email }}</a>@endif
        </div>

        @include('checkout.normal.cart', ['cart' => $cart])

        {!! Form::model($form, ['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form', 'novalidate' => 'novalidate']) !!}
        <div class="step-box">
            <h2 class="step-box__title step-box__title_warning">We Have Made Your First Frame Free</h2>

            <div class="step-box__title-note">
                <p>Discount of Rs. {{ $cart->discount }} applied to Cart</p>
            </div>

            <div class="checkbox-list">
                <div class="checkbox-list__item fake-checkbox">
                    <input type="checkbox" id="check-another-coupon" data-opener="js-check-collapse" data-target="use-another-coupon">
                    <label for="check-another-coupon">Use Another Coupon</label>
                </div>

                <div class="checkbox-list__slide js-check-collapse" id="use-another-coupon">
                    <div class="input-row">
                        <div class="input-row__label-holder">
                            <label class="input-row__label" for="input-another-coupon" data-label-text="Coupon code"><span class="input-row__label-text">Coupon code</span></label>
                            <span class="error-message"><span class="error-message__holder">Enter coupon code</span></span>
                        </div>
                        <div class="form-field">
                            <input type="text" class="input-text" id="input-another-coupon">
                        </div>
                    </div>
                </div>

                <span class="checkbox-list__or">-or-</span>

                <div class="checkbox-list__item fake-checkbox">
                    <input type="checkbox" id="check-reward-points" data-opener="js-check-collapse" data-target="apply-reward-points">
                    <label for="check-reward-points">Apply Reward Points</label>
                </div>

                <div class="checkbox-list__slide js-check-collapse" id="apply-reward-points">
                    <div class="input-row">
                        <div class="input-row__label-holder">
                            <label class="input-row__label" for="input-reward-points" data-label-text="Reward Code"><span class="input-row__label-text">Reward Code</span></label>
                            <span class="error-message"><span class="error-message__holder">Enter coupon code</span></span>
                        </div>
                        <div class="form-field">
                            <input type="text" class="input-text" id="input-reward-points">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">Select Mode Of Your Payment</h2>

            <div class="step-box__title-note">
                <p>Get Rs. 100 discount on online payment</p>
            </div>

            <div class="check-tabs">
                <div class="check-tabs__single-item">
                    <div class="fake-radio check-tabs-opener">
                        <input type="radio" name="payment_type" id="check-tab-saved" data-opener="js-check-collapse" data-target="tab-saved" checked>

                        <label for="check-tab-saved" class="fake-radio__label check-tabs-label">
                            <span class="check-tabs-label__holder">
                                <span class="check-tabs-label__text">Saved Card <span class="check-tabs-label__note">Rs. 200 off</span></span>
                            </span>
                        </label>

                        <dl class="by">
                            <dt>Powered by</dt>
                            <dd>
                                <img src="/images/logo-citrus.png" width="41" height="17" alt="Citrus">
                            </dd>
                        </dl>
                    </div>

                    <div class="check-tabs-content js-check-collapse" id="tab-saved">
                        <div class="check-tabs-content__holder">
                            <div class="check-tabs__text-block">
                                <p id="citrus-saved-message">No saved cards found for this user</p>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="check-tabs__holder">
                    <div class="check-tabs__item">
                        <div class="fake-radio check-tabs-opener">

                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::COD_TYPE, false, ['id' => 'check-tab-1', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-1']) !!}

                            <label for="check-tab-1" class="fake-radio__label check-tabs-label">
                                                    <span class="check-tabs-label__holder">
                                                        <span class="check-tabs-label__text">CoD</span>
                                                    </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-1">
                            <div class="check-tabs-content__holder">
                                <div class="check-tabs__text-block">
                                    <p>Please enter the captcha shown here to place order.</p>
                                </div>

                                <div class="captcha-unit">
                                    <div class="captcha-unit__visual">
                                        <img id="captcha-image" class="captcha-unit__image" src="{{ $captchaImage }}" width="142" height="60" alt="">
                                        <a id="captcha-reset" href="#" class="captcha-unit__reset">Reset image</a>
                                    </div>

                                    <div class="input-row @if($errors->has('captcha')) error @endif">
                                        <div class="input-row__label-holder">
                                            <label class="input-row__label" for="input-captcha-code" data-label-text="security code"><span class="input-row__label-text">*security code (Captcha)</span></label>
                                            {!! $errors->first('captcha', '<span class="error-message">:message</span>') !!}
                                        </div>
                                        <div class="form-field">
                                            {!! Form::text('captcha', old('captcha'), ['class' => 'input-text required', 'id' => 'input-captcha-code', 'maxlength' => 10]) !!}
                                        </div>
                                    </div>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    <button type="submit" class="btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="card-pm-credit" class="check-tabs__item card-pm-section">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::GIFT_VOUCHER_TYPE, false, ['id' => 'check-tab-2', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-2']) !!}
                            <label for="check-tab-2" class="fake-radio__label check-tabs-label">
                                <span class="check-tabs-label__holder">
                                    <span class="check-tabs-label__text">Credit Card <span class="check-tabs-label__note">Rs. 100 off</span></span>
                                </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-2">
                            <div class="check-tabs-content__holder">
                                <div class="check-tabs__text-block">
                                    <p>100% Secure - Pay using credit card</p>
                                </div>

                                <ul class="cards-type">
                                    <li class="cards-type__item">
                                        <img src="/images/img-card-master-card.png" width="41" height="26" alt="master card">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-visa.png" width="41" height="26" alt="visa">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-maestro.png" width="41" height="26" alt="maestro">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-ae.png" width="41" height="26" alt="american express">
                                    </li>
                                </ul>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-cc-number" data-label-text="Card Number"><span class="input-row__label-text">*Card Number</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid card number</span></span>
                                    </div>
                                    <div class="form-field form-field_credit-card">
                                        <input type="text" pattern="\d*" class="card-pm-number input-text card-mask required-credit" id="input-cc-number">
                                        <input type="hidden" class="reformat-card" data-target="#input-cc-number">
                                        <img class="input-decor" src="/images/img-credit-card.png" width="42" height="26" alt="credit card image">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-cc-name" data-label-text="Name on Card"><span class="input-row__label-text">*Name on Card</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid card name</span></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="card-pm-name input-text required" id="input-cc-name">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-cc-expiry-date" data-label-text="Expiry date"><span class="input-row__label-text">*Expiry date</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid expiry date</span></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="card-pm-expiry input-text expiry-date-mask required-expiry" id="input-cc-expiry-date">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-cc-cvv" data-label-text="Cvv"><span class="input-row__label-text">*Cvv</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid cvv</span></span>
                                    </div>
                                    <div class="form-field form-field_cvv">
                                        <input type="number" pattern="\d*" class="card-pm-cvv input-text required-cvv card-cvv" id="input-cc-cvv" maxlength="4">
                                        <img class="input-decor" src="/images/img-cvv.png" width="42" height="26" alt="cvv image">
                                    </div>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    <button type="submit" class="card-pm-submit btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="card-pm-debit" class="check-tabs__item card-pm-section">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', '', false, ['id' => 'check-tab-3', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-3']) !!}
                            <label for="check-tab-3" class="fake-radio__label check-tabs-label">
                                <span class="check-tabs-label__holder">
                                    <span class="check-tabs-label__text">Debit Card <span class="check-tabs-label__note">Rs. 100 off</span></span>
                                </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-3">
                            <div class="check-tabs-content__holder">
                                <div class="check-tabs__text-block">
                                    <p>100% Secure - Pay using debit card</p>
                                </div>

                                <ul class="cards-type">
                                    <li class="cards-type__item">
                                        <img src="/images/img-card-master-card.png" width="41" height="26" alt="master card">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-visa.png" width="41" height="26" alt="visa">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-maestro.png" width="41" height="26" alt="maestro">
                                    </li>

                                    <li class="cards-type__item">
                                        <img src="/images/img-card-ae.png" width="41" height="26" alt="american express">
                                    </li>
                                </ul>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-dc-number" data-label-text="Card Number"><span class="input-row__label-text">*Card Number</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid card number</span></span>
                                    </div>
                                    <div class="form-field form-field_credit-card">
                                        <input type="text" pattern="\d*" class="card-pm-number input-text card-mask required" id="input-dc-number">
                                        <input type="hidden" class="reformat-card" data-target="#input-dc-number">
                                        <img class="input-decor" src="/images/img-credit-card.png" width="42" height="26" alt="credit card image">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-dc-name" data-label-text="Name on Card"><span class="input-row__label-text">*Name on Card</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid card name</span></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="card-pm-name input-text required" id="input-dc-name">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-dc-expiry-date" data-label-text="Expiry date"><span class="input-row__label-text">*Expiry date</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid expiry date</span></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="card-pm-expiry input-text expiry-date-mask required" id="input-dc-expiry-date">
                                    </div>
                                </div>

                                <div class="input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-dc-cvv" data-label-text="Cvv"><span class="input-row__label-text">*Cvv</span></label>
                                        <span class="error-message"><span class="error-message__holder">Enter valid cvv</span></span>
                                    </div>
                                    <div class="form-field form-field_cvv">
                                        <input type="number" pattern="\d*" class="card-pm-cvv input-text required" id="input-dc-cvv" maxlength="4">
                                        <img class="input-decor" src="/images/img-cvv.png" width="42" height="26" alt="cvv image">
                                    </div>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    {{--<button id="payment-debit-cart" type="submit" class="btn-product btn-show-loader">--}}
                                    <button id="payment-debit-cart" class="card-pm-submit btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div id="net-banking-pm" class="check-tabs__item net-banking-pm-section">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::NET_BANKING, false, ['id' => 'check-tab-4', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-4']) !!}

                            <label for="check-tab-4" class="fake-radio__label check-tabs-label">
                                <span class="check-tabs-label__holder">
                                    <span class="check-tabs-label__text">Net Banking <span class="check-tabs-label__note">Rs. 100 off</span></span>
                                </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-4">
                            <div class="check-tabs-content__holder">
                                <div class="form-row input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="select-bank" data-label-text="Choose Bank"><span class="input-row__label-text">*Choose Bank</span></label>
                                        <span class="error-message"><span class="error-message__holder">Choose bank</span></span>
                                    </div>
                                    <div class="form-field">
                                        <div class="select-unit">
                                            <div class="select-unit__holder">
                                                {{--<select id="net-banking-pm-select" class="effect-select">--}}
                                                <select id="net-banking-pm-select" class="effect-select">
                                                    <option>*Choose Bank</option>
                                                    @foreach($payment->getAvailableNetBanks() as $code => $name)
                                                        <option value="{{ $code }}">{{ $name }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="check-tabs__note">
                                    <p>You will be redirected to bank website when you place an order.</p>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    <button id="net-banking-pm-submit" class="btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="check-tabs__item">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::GIFT_VOUCHER_TYPE, false, ['id' => 'check-tab-5', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-5']) !!}
                            <label for="check-tab-5" class="fake-radio__label check-tabs-label">
                                                    <span class="check-tabs-label__holder">
                                                        <span class="check-tabs-label__text">E-Gift Voucher</span>
                                                    </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-5">
                            <div class="check-tabs-content__holder">
                                <div class="check-tabs__text-block">
                                    <p>Use your gift voucher that you have to spend for this order</p>
                                </div>

                                <div id="form-section-coupon" class="input-row input-row_with-btn @if($errors->has('giftVoucher')) error @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-voucher" data-label-text="voucher code"><span class="input-row__label-text">*Voucher code</span></label>
                                        {!! $errors->first('giftVoucher', '<span class="error-message">:message</span>') !!}
                                    </div>
                                    <div class="form-field">
                                        {!! Form::text('giftVoucher', old('giftVoucher'), [ 'id' => 'input-voucher', 'class' => 'input-text' ]) !!}

                                        <button id="apply-btn-voucher" type="button" class="btn-small-apply">Apply</button>

                                        <span class="applied-badge">Applied</span>
                                    </div>
                                </div>

                                <div class="check-tabs__note">
                                    <p>You need to add gift voucher or choose other payment methods on thisorder</p>

                                    <p>To check your Gift Voucher balance, please click <a href="#">Here</a> </p>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    <button type="submit" class="btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="check-tabs__item">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::STORE_CREDIT_TYPE, false, ['id' => 'check-tab-6', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-6']) !!}
                             <label for="check-tab-6" class="fake-radio__label check-tabs-label">
                                                    <span class="check-tabs-label__holder">
                                                        <span class="check-tabs-label__text">Store Credit</span>
                                                    </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-6">
                            <div class="check-tabs-content__holder">
                                @if(!\Auth::check())
                                <div class="check-tabs__warning">
                                    <p>You need to login to use Store Credit!</p>
                                </div>
                                @endif

                                <div class="check-tabs__text-block">
                                    <p>Use your Store Credit that you have to spend for this order.</p>

                                    <p>You need to add a store credit or choose other payment methods to spend on this order!</p>
                                </div>

                                <div class="input-row @if($errors->has('storeCreditCode')) error @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-credit-code" data-label-text="Store Credit code"><span class="input-row__label-text">*Store Credit code</span></label>
                                        {!! $errors->first('storeCreditCode', '<span class="error-message">:message</span>') !!}
                                    </div>
                                    <div class="form-field">
                                        {!! Form::text('storeCreditCode', old('storeCreditCode'), [ 'id' => 'input-credit-code', 'class' => 'input-text required' ]) !!}

                                    </div>
                                </div>

                                <div class="input-row @if($errors->has('storeCreditAmount')) error @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-credit-amount" data-label-text="Credit amount"><span class="input-row__label-text">*Credit amount</span></label>
                                        {!! $errors->first('storeCreditAmount', '<span class="error-message">:message</span>') !!}
                                    </div>
                                    <div class="form-field">
                                        {!! Form::input('number', 'storeCreditAmount', old('storeCreditAmount'), [ 'id' => 'input-credit-amount', 'class' => 'input-text required', 'pattern' => '\d*' ]) !!}
                                    </div>
                                </div>

                                <div class="check-tabs__note">
                                    <p>To check your Store Credit balance, please click here <a href="#">Here</a></p>
                                </div>

                                <p>I accept the <a href="#">Terms and Conditions</a></p>

                                <div class="loader-box">
                                    <button type="submit" class="btn-product btn-show-loader" @if(!\Auth::check()) disabled @endif>
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="check-tabs__item">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_TYPE_WALLET, false, ['id' => 'check-tab-7', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-7']) !!}
                            <label for="check-tab-7" class="fake-radio__label check-tabs-label">
                                <span class="check-tabs-label__holder">
                                    <span class="check-tabs-label__text">Wallet</span>
                                </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-7">
                            <div class="check-tabs-content__holder">
                                <div class="figure-unit">
                                    <strong class="figure-unit__title">Please choose one of the following options to pay:</strong>

                                    <ul class="check-choose">
                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_AIRTEL_MONEY, false, ['id' => 'wallet-pay-option-airtel']) !!}
                                                <label for="wallet-pay-option-airtel" class="fake-radio__label">
                                                        <span class="check-choose-item__text-holder">
                                                            <span class="check-choose-item__text-cell">
                                                                <span class="check-choose-item__text">Airtel Money</span>
                                                            </span>
                                                            <span class="check-choose-item__image">
                                                                <img src="/images/logo-airtel-money.png" width="47" height="22" alt="airtel money">
                                                            </span>
                                                        </span>
                                                </label>
                                            </div>
                                        </li>

                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_PAYTM, false, ['id' => 'wallet-pay-option-paytm']) !!}
                                                <label for="wallet-pay-option-paytm" class="fake-radio__label">
                                                        <span class="check-choose-item__text-holder">
                                                            <span class="check-choose-item__text-cell">
                                                                <span class="check-choose-item__text">Paytm</span>
                                                            </span>
                                                            <span class="check-choose-item__image">
                                                                <img src="/images/logo-paytm.png" width="44" height="15" alt="paytm">
                                                            </span>
                                                        </span>
                                                </label>
                                            </div>
                                        </li>

                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_PAYU, false, ['id' => 'wallet-pay-option-payubiz']) !!}
                                                <label for="wallet-pay-option-payubiz" class="fake-radio__label">
                                                    <span class="check-choose-item__text-holder">
                                                        <span class="check-choose-item__text-cell">
                                                            <span class="check-choose-item__text">PayUbiz</span>
                                                        </span>
                                                        <span class="check-choose-item__image">
                                                            <img src="/images/logo-payubiz.png" width="55" height="19" alt="payUbiz">
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="check-tabs__note">
                                    <p>You will be redirected to Payment gateway upon placing the order.</p>
                                </div>

                                <div class="loader-box">
                                    <button type="submit" class="btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="check-tabs__item">
                        <div class="fake-radio check-tabs-opener">
                            {!! Form::radio('payment_type', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_TYPE_CC, false, ['id' => 'check-tab-8', 'data-opener' => 'js-check-collapse', 'data-target' => 'tab-8']) !!}
                            <label for="check-tab-8" class="fake-radio__label check-tabs-label">
                                <span class="check-tabs-label__holder">
                                    <span class="check-tabs-label__text">Cash Card</span>
                                </span>
                            </label>
                        </div>

                        <div class="check-tabs-content js-check-collapse" id="tab-8">
                            <div class="check-tabs-content__holder">
                                <div class="figure-unit">
                                    <strong class="figure-unit__title">Please choose one of the following options to pay:</strong>

                                    <ul class="check-choose">
                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_AIRTEL_MONEY, false, ['id' => 'cc-pay-option-airtel']) !!}
                                                <label for="cc-pay-option-airtel" class="fake-radio__label">
                                                        <span class="check-choose-item__text-holder">
                                                            <span class="check-choose-item__text-cell">
                                                                <span class="check-choose-item__text">Airtel Money</span>
                                                            </span>
                                                            <span class="check-choose-item__image">
                                                                <img src="/images/logo-airtel-money.png" width="47" height="22" alt="airtel money">
                                                            </span>
                                                        </span>
                                                </label>
                                            </div>
                                        </li>

                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_PAYTM, false, ['id' => 'cc-pay-option-paytm']) !!}
                                                <label for="cc-pay-option-paytm" class="fake-radio__label">
                                                        <span class="check-choose-item__text-holder">
                                                            <span class="check-choose-item__text-cell">
                                                                <span class="check-choose-item__text">Paytm</span>
                                                            </span>
                                                            <span class="check-choose-item__image">
                                                                <img src="/images/logo-paytm.png" width="44" height="15" alt="paytm">
                                                            </span>
                                                        </span>
                                                </label>
                                            </div>
                                        </li>

                                        <li class="check-choose-item">
                                            <div class="fake-radio">
                                                {!! Form::radio('payment_third_service', \App\Http\Commands\Checkout\Normal\PaymentCommand::THIRD_PARTY_PAYU, false, ['id' => 'cc-pay-option-payubiz']) !!}
                                                <label for="cc-pay-option-payubiz" class="fake-radio__label">
                                                    <span class="check-choose-item__text-holder">
                                                        <span class="check-choose-item__text-cell">
                                                            <span class="check-choose-item__text">PayUbiz</span>
                                                        </span>
                                                        <span class="check-choose-item__image">
                                                            <img src="/images/logo-payubiz.png" width="55" height="19" alt="payUbiz">
                                                        </span>
                                                    </span>
                                                </label>
                                            </div>
                                        </li>
                                    </ul>
                                </div>

                                <div class="check-tabs__note">
                                    <p>You will be redirected to Payment gateway upon placing the order.</p>
                                </div>

                                <div class="loader-box">
                                    <button type="submit" class="btn-product btn-show-loader">
                                        <span class="btn-product__wrap">PLACE ORDER</span>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\Normal\CommandContract::STAGE_PAYMENT }}"/>
        {!! Form::close() !!}

    </div>
    <span class="page-loader">
            <span class="loader"></span>
    </span>
@endsection


@section('script')
    <script src="/js/citrus.js" type="text/javascript"></script>
    <script src="/js/jquery.payment.js" type="text/javascript"></script>

    <script>
        $(document).ready(function(){
            var  captchaUrl = '{{ route('updateCaptcha') }}',
                 voucherUrl = '{{ route('cartVoucherAdd') }}',
                 _token     = '{{ csrf_token() }}',
                 pageLoader = $('.page-loader').data('pageLoaderPlugin');

            $('#captcha-reset').on('click', function(){
                pageLoader.show();
                $.getJSON(captchaUrl, {}, function(data) {
                    if('image' in data)
                    {
                      $('#captcha-image').attr('src', data.image);
                    }
                    pageLoader.hide();
                });

                return false;
            });


            var citruspg = $.citrus.gateway($.citrus.env.sandbox),
                citruswallet = $.citrus.wallet($('#walletToken').val(), $.citrus.env.sandbox),
                bill = {!! $bill !!},
                makePayment;

            makePayment = function(paymentOptions){
                citruspg.makePayment(
                        bill,
                        paymentOptions,
                        function(error, url) {
                            if (error) {
                                // log error
                                console.log(error);
                            } else {
                                $(location).attr({ href: url });
                            }
                        }
                );
            };

            $('.card-pm-submit').on('click', function(e){
                e.preventDefault();


                var $container = $(this).closest('.card-pm-section'),
                    paymentOptions;

                if($container.validateForm()){
                    paymentOptions = {
                        "mode": "card",
                        "cardNumber": $container.find('.card-pm-number').val(),
                        "cardHolder": $container.find('.card-pm-name').val(),
                        "cardExpiry": $container.find('.card-pm-expiry').val(),
                        "cardCvv": $container.find('.card-pm-cvv').val()
                    };

                    makePayment(paymentOptions);
                }
            });

            $('#net-banking-pm-submit').on('click', function(e){
                e.preventDefault();
                var paymentOptions, bank;

                bank = $('#net-banking-pm-select').val();


                paymentOptions = {
                    "mode" : "netbanking",
                    "bankCode" : bank
                };

                makePayment(paymentOptions);
            });




            var walletTemplate = '\
                <li>\
                    <input type="radio" name="paymentMode" id="token" checked="true"/>\
                    <label for="token">saved cards</label>\
                    <div>\
                        <div>\
                            <ul>\
                            </ul>\
                        </div>\
                        <div><button class="pay">make payment</button></div>\
                    </div>\
                </li>\
            ';

            var savedCard = function(card) { return '\
                <li>\
                    <input type="radio" name="walletToken" id="' + card.token + '"/>\
                    <label for="' + card.token + '">\
                        <span>' + card.number + '</span>\
                        <span>' + card.expiry + '</span>\
                        <span class="cvv">cvv: <input type="text" class="cvv"/></span>\
                    </label>\
                </li>\
            '};


//            citruswallet.load(function (card){
//                if ($('#token').length == 0) {
//                    $('ul#checkout li:first-child').before(walletTemplate);
//                }
//                var wallet = $('#token + label + div ul');
//                wallet.append(savedCard(card));
//                $('input.cvv').payment('formatCardCVC');
//                wallet.find('li:first-child input[type="radio"][name="walletToken"]').prop('checked', true);
//            });


//
//
//            $('#apply-btn-voucher').on('click', function(){
//                var voucherValue = $('#input-voucher').val(),
//                    $container = $('#form-section-coupon');
//
//                pageLoader.show();
//                $.post(voucherUrl, { _token: _token, code: voucherValue })
//                        .done(function(data){
//                            $container.removeClass('error').addClass('input-applied');
//                            //$('#old-input-coupon').val(couponValue);
//                        })
//                        .fail(function(data){
//                            data = data.responseJSON;
//                            if(data.hasOwnProperty('message'))
//                            {
//                                $container.addClass('error');
//                                $('#error-message-coupon').text(data.message);
//                            }
//                        })
//                        .always(function(){
//                            pageLoader.hide();
//                        });
//                return false;
//            });

        });
    </script>
@endsection