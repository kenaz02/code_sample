@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-heading">
        <a href="{{ route('cart') }}" class="back-link back-link_type_empty"></a>
        <h1 class="cart-heading__title">Sign In</h1>
    </div>

    <div class="checkout-block__content">
        @include('checkout.normal.cart', ['cart' => $cart])

        <div class="login-buttons">
            <a href="#" class="btn-login btn-login_fb">Login <span class="btn-login__union">with</span> Facebook</a>
        </div>

        <div class="or-unit">
            <span class="or-unit__or">or</span>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">Sign In With Email</h2>

            {!! Form::open(['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}

                <div class="form-row input-row @if($errors->has('email')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-email" data-label-text="Email"><span class="input-row__label-text">*Email: name@domain.com</span></label>
                        {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::text('email', old('email'), ['class' => 'input-text required', 'id' => 'input-email']) !!}
                    </div>
                </div>

                <ul class="check-listing">
                    <li class="check-listing__item">
                        {!! Form::radio('check_pass', 'user', true, ['class' => 'enter-item__check', 'id' => 'check-have-pass', 'data-opener' => 'js-check-collapse', 'data-target' => 'have-pass-box']) !!}
                        <label for="check-have-pass">I have password</label>
                    </li>
                    <li class="check-listing__item">
                        {!! Form::radio('check_pass', 'guest', true, ['class' => 'enter-item__check', 'id' => 'check-no-pass']) !!}
                        <label for="check-no-pass">I don’t have password</label>
                    </li>
                </ul>

                <div class="js-check-collapse pass-box" id="have-pass-box">
                    <div class="form-row input-row @if($errors->has('password')) error @endif">
                        <div class="input-row__label-holder">
                            <label class="input-row__label" for="input-password" data-label-text="Password"><span class="input-row__label-text">*Password</span></label>
                            <span class="error-message"><span class="error-message__holder">Enter password</span></span>
                        </div>
                        <div class="form-field form-field_password">
                             {!! Form::input('password', 'password', old('password'), ['class' => 'input-text required', 'id' => 'input-password']) !!}
                        </div>
                    </div>
                </div>

                <div class="signin-form__options">
                    <div class="signin-form__options-item">
                        <a href="#" class="link">Forgot Password?</a>
                    </div>
                </div>

                <div class="loader-box">
                    <button class="btn-product btn-show-loader" type="submit">Continue</button>
                    <span class="loader"></span>
                </div>
                <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\Normal\CommandContract::STAGE_SIGN_IN }}"/>

                {!! Form::close() !!}
        </div>

        <div class="step-box">
            <div class="small-faq-list">
                <div class="small-faq-list__item">
                    <h3 class="small-faq-list__title">When Can I Specify My Lens Power?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec imperdiet dui pulvinar laoreet, ipsum ligula dapibus tortor.</p>
                </div>

                <div class="small-faq-list__item">
                    <h3 class="small-faq-list__title">When Can I Specify My Lens Power?</h3>
                    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec du imperdiet</p>
                </div>
            </div>
        </div>
    </div>

@endsection