@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-heading">
        <a href="{{ route('cart') }}" class="back-link back-link_type_empty"></a>
        <h1 class="cart-heading__title">Shipping information</h1>
    </div>

    <div class="checkout-block__content">
    @include('checkout.normal.cart', ['cart' => $cart])

        {!! Form::model($form, ['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form']) !!}
            <div class="step-box">
                <h2 class="step-box__title">Your Personal Data</h2>

                <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-phone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
                        {!! $errors->first('telephone', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif"/>
                        <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif">
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('firstname')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-first-name" data-label-text="First name"><span class="input-row__label-text">*First name</span></label>
                        {!! $errors->first('firstname', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        {!! Form::text('firstname', old('firstname') ? old('firstname') : $form->firstname , ['class' => 'input-text required', 'id' => 'input-first-name', 'maxlength' => 25]) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('lastname')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-last-name" data-label-text="Last name"><span class="input-row__label-text">*Last name</span></label>
                        {!! $errors->first('lastname', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        {!! Form::text('lastname', old('lastname') ? old('lastname') : $form->lastname , ['class' => 'input-text required', 'id' => 'input-last-name', 'maxlength' => 25]) !!}
                    </div>
                </div>
            </div>

            <div class="step-box">
                <h2 class="step-box__title">Shipping Address</h2>

                <div id="row-pincode" class="form-row input-row @if($errors->has('pincode')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-pincode" data-label-text="Pincode"><span class="input-row__label-text">*Pincode: xxxxxxx</span></label>
                        <span id="error-message-pincode" class="error-message">{!! $errors->first('pincode', ':message') !!}</span>
                    </div>

                    <div class="form-field">
                        {!! Form::input('number', 'pincode', old('pincode') ? old('pincode') : $form->pincode, ['class' => 'input-text required', 'id' => 'input-pincode', 'pattern' => '\d*']) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('street0')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-address" data-label-text="Address"><span class="input-row__label-text">*Address: xx, street</span></label>
                        {!! $errors->first('street0', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        {!! Form::text('street0', old('street0') ? old('street0') : $form->street0 , ['class' => 'input-text required', 'id' => 'input-address', 'maxlength' => 100]) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('locality')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="select-town" data-label-text="Locality/town"><span class="input-row__label-text">*Locality / Town</span></label>
                        {!! $errors->first('locality', '<span class="error-message">:message</span>') !!}
                    </div>

                    <select id="select-locality" name="locality">
                        <option value=""></option>
                        {{--@foreach($form->localityList as $item)--}}
                            {{--<option value="{{ $item }}" @if($form->locality == $item || old('locality') == $item) selected @endif>{{ $item }}</option>--}}
                        {{--@endforeach--}}
                    </select>
                </div>

                <div class="form-row input-row @if($errors->has('city')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-city" data-label-text="City / district"><span class="input-row__label-text">*city / district</span></label>
                        {!! $errors->first('city', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        {!! Form::text('city', old('city') ? old('city') : $form->city , ['class' => 'input-text required', 'id' => 'input-city']) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('state')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="select-state" data-label-text="state / province"><span class="input-row__label-text">*state / province</span></label>
                        <span class="error-message">Select a state / privince</span>
                        {!! $errors->first('state', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        <div class="select-unit">
                            <div class="select-unit__holder">
                                <select class="effect-select" id="select-state" name="state">
                                    <option>State / province</option>
                                    @foreach($states as $item)
                                        <option value="{{ $item->code }}" @if($item->code == old('state')) selected @endif>{{ $item->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('country')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="select-country" data-label-text="Country"><span class="input-row__label-text">*country</span></label>
                        <span class="error-message">Select country</span>
                        {!! $errors->first('country', '<span class="error-message">:message</span>') !!}
                    </div>

                    <div class="form-field">
                        <div class="select-unit">
                            <div class="select-unit__holder">
                                <select class="effect-select" id="select-country">
                                    <option>*country</option>
                                    <option>India</option>
                                    <option>USA</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="details-list">
                    <div class="details-unit @if($errors->has('alt_telephone')) details-unit_expanded @endif">
                        <div class="details-unit__heading">
                            <div class="details-unit__heading-text">
                                <a href="#" class="details-unit__opener">Alternative Phone Number</a>

                                <p>Recommended for COD</p>
                            </div>
                        </div>

                        <div class="details-unit__slide">
                            <div class="details-unit__content">
                                <div class="form-row input-row @if($errors->has('alt_telephone')) error @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-alternate-phone" data-label-text="Alternate phone"><span class="input-row__label-text">*Alternate Phone: xxx xxx xxxx</span></label>
                                        {!! $errors->first('alt_telephone', '<span class="error-message">:message</span>') !!}
                                    </div>

                                    <div class="form-field">
                                        <input type="tel" class="input-text" id="input-alternate-phone" maxlength="12" value="@if(old('alt_telephone')){{ old('alt_telephone') }}@else{{ $form->alt_telephone }}@endif"/>
                                        <input type="hidden" name="alt_telephone" class="reformat-phone" data-target="#input-alternate-phone" value="@if(old('alt_telephone')){{ old('alt_telephone') }}@else{{ $form->alt_telephone }}@endif">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details-unit @if($errors->has('landmark')) details-unit_expanded @endif">
                        <div class="details-unit__heading">
                            <a href="#" class="details-unit__opener">Add Order Comments</a>
                        </div>
                        <div class="details-unit__slide">
                            <div class="details-unit__content">
                                <div class="form-row input-row @if($errors->has('landmark')) error @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-order-comment" data-label-text="Order Comments"><span class="input-row__label-text">Order Comments</span></label>
                                        {!! $errors->first('landmark', '<span class="error-message">:message</span>') !!}
                                    </div>
                                    <div class="form-field">
                                        {!! Form::textarea('landmark', old('landmark') ? old('landmark') : $form->landmark , ['class' => 'input-textarea', 'id' => 'input-order-comment', 'maxlength' => 250]) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details-unit">
                        <div class="details-unit__heading">
                            <div class="details-unit__check">
                                <input type="checkbox" id="check-gift">
                                <label class="details-unit__opener label-gift" for="check-gift">Gift Wrap My Order</label>
                            </div>
                        </div>
                        <div class="details-unit__slide">
                            <div class="details-unit__content">
                                <div class="details-unit__note">
                                    <p>To keep it a surprise, please provide your mobile number below so that we'll inform only you about the order status.</p>
                                </div>

                                <div class="form-row input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-phone-2" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
                                        <span class="error-message">Right format is xxx xxx xxxx</span>
                                    </div>

                                    <div class="form-field">
                                        <input type="tel" class="input-text required" id="input-phone-2">
                                    </div>
                                </div>

                                <div class="form-row input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-from" data-label-text="From"><span class="input-row__label-text">*From</span></label>
                                        <span class="error-message">Enter valid value</span>
                                    </div>

                                    <div class="form-field">
                                        <input type="tel" class="input-text required" id="input-from">
                                    </div>
                                </div>

                                <div class="form-row input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-to" data-label-text="To"><span class="input-row__label-text">*To</span></label>
                                        <span class="error-message">Enter valid value</span>
                                    </div>

                                    <div class="form-field">
                                        <input type="tel" class="input-text required" id="input-to">
                                    </div>
                                </div>

                                <div class="form-row input-row">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-message" data-label-text="message"><span class="input-row__label-text">*message</span></label>
                                    </div>
                                    <div class="form-field">
                                        <textarea id="input-message" class="input-textarea"></textarea>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="loader-box">
                    <button type="submit" class="btn-product btn-show-loader">
                        <span class="btn-product__wrap">continue</span>
                    </button>
                    <span class="loader"></span>
                </div>
            </div>

            <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\Normal\CommandContract::STAGE_SHIPPING }}"/>
        {!! Form::close() !!}

    </div>
    <span class="page-loader">
		    <span class="loader"></span>
    </span>
@endsection


@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        var pincodeCheckUrl = '{{ route('checkPincode') }}',
            pageLoader = $('.page-loader').data('pageLoaderPlugin');

        $('#input-pincode').on('change', function(){

            var that = $(this),
                id = $(this).val(),
                $row = $('#row-pincode');

            $row.removeClass('error');
            if(id){
                pageLoader.show();
                $.getJSON(pincodeCheckUrl, { id : id }, function(data) {

                    $('#input-city').val(data.city);
                    $('#select-state option[value="' + data.state + '"]').prop('selected', true);

                    var $locality = $('#select-locality').find('option')
                                                         .remove()
                                                         .end();

                    $.each(data.locality, function(key, value) {
                        $locality.append($("<option></option>")
                                 .attr("value",value)
                                 .text(value));
                    });

                }).error(function(error){
                    if(error.status == 422) {
                        $row.addClass('error');
                        $('#error-message-pincode').text(error.responseText);
                    }
                }).complete(function(){
                    pageLoader.hide();

                });
            }

        });

        var selectize = $('#select-locality').selectize({
            create: true,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body',
            onInitialize: function () {
                this.$parentRow = this.$input.parent('.input-row');

                if (this.$control.hasClass('full')) {
                    this.$parentRow.addClass('input--filled');
                }
            },
            onFocus: function () {
                this.$parentRow.addClass('input--filled');
            },
            onBlur: function () {
                if (this.$control.hasClass('not-full')) {
                    this.$parentRow.removeClass('input--filled');
                }
            }
        });

    });
</script>
@endsection