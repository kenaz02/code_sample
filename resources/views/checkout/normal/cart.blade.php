<div class="step-box cart-info-box">
    <div class="details-unit">
        <div class="details-unit__heading">
            <a href="#" class="details-unit__opener">View Cart</a>

            <div class="order-heading">
                <strong class="order-title">Order Total</strong>
                <div class="order-price-info">
                    <div class="price-box">
                        <span class="price">Rs. {{ $cart->total }}</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="details-unit__slide">
            <div class="details-unit__content">
                <div class="order-box">

                    <div class="order-info-holder">
                        <div class="order-info">
                            <dl class="order-info__item order-info__item_note">
                                <dt>You Save</dt>

                                <dd>Rs. {{ $cart->discount }}</dd>
                            </dl>

                            <dl class="order-info__item">
                                <dt>
                                    Tax Collected
                                <div class="info-icon-unit">
                                    <div class="info-icon-unit__icon info-icon-unit__icon_green">
                                        <span class="icon-info icon-info_green">Info</span>
                                    </div>
                                    <div class="info-icon-unit__popup info-icon-unit__popup_green js-slide-hidden">
                                        <button type="button" class="info-icon-unit__close info-icon-unit__close_green">Close</button>
                                        <div class="info-icon-unit__popup-holder info-icon-unit__popup-holder_green">
                                            <p>"This is part of VAT/C T, deposited by company to government. All our products are genuine and with bills."</p>
                                        </div>
                                    </div>
                                </div>
                                </dt>
                                <dd>Rs. {{ $cart->tax_collected }}</dd>
                            </dl>
                            <dl class="order-info__item">
                                <dt>Shipping Charges</dt>
                                <dd>
                                    @if($cart->shipping_charges->price == 0)
                                        Free
                                    @else
                                        Rs. {{ $cart->shipping_charges }}
                                    @endif
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>

                @foreach($cart->items as $product)
                    @include('cart.normal_product_item', ['product' => $product])
                @endforeach
            </div>
        </div>
    </div>
</div>