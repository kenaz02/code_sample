@extends('app_mini')

@section('body-class', 'login-page')

@section('content')
    <div class="cart-heading">
        <h1 class="cart-heading__title">Success</h1>
    </div>
    <div class="checkout-block__content">
        @if(!$isPowerRequired)

            <div class="step-box">
                <p>Your details for the Order no. <strong>{{ Helper::formatOrderNumber($result->getOrderId()) }}</strong> have been successfully captured.</p>
            </div>

            <div class="step-box">
                <p>Thank you for shopping with us.</p>

                <div class="enter-buttons">
                    <div class="enter-buttons__item">
                        <a href="/" class="checkbox-btn__btn btn-enter">Continue shopping</a>
                    </div>
                </div>
            </div>
        @else
            {!! Form::open(['route' => 'checkout', 'method' => 'POST', 'class' => 'steps-form', 'enctype' => 'multipart/form-data']) !!}
                <div class="step-box">
                    {!! $errors->first('submit', '<div class="alert alert-danger">:message</div>') !!}
                    <p>Complete your order <strong>{{ Helper::formatOrderNumber($order->id) }}</strong> by submitting your eye-power in one of the following ways:</p>

                    {{--<div class="enter-buttons">--}}
                        {{--<div class="enter-buttons__item">--}}
                            {{--<div class="upload-button">--}}
                                {{--<input type="radio" name="prescriptionType" id="check-upload" value="{{ \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_IMAGE }}">--}}

                                {{--<label for="check-upload" class="btn-enter">--}}
                                    {{--Upload Prescription--}}
                                    {{--<input type="file" name="prescriptionFile" accept="image/*" data-for="check-upload">--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="enter-buttons__item">--}}
                            {{--<div class="upload-button">--}}
                                {{--<input type="radio" name="prescriptionType" id="check-take-pic" value="{{ \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_IMAGE }}">--}}

                                {{--<label for="check-take-pic" class="btn-enter">--}}
                                    {{--Take Picture--}}
                                    {{--<input type="file" name="prescriptionFile" accept="image/*" capture="camera" data-for="check-take-pic">--}}
                                {{--</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="or-unit">--}}
                        {{--<span class="or-unit__or">or</span>--}}
                    {{--</div>--}}

                    {{--<div class="enter-buttons">--}}
                        {{--<div class="enter-buttons__item">--}}
                            {{--<div class="checkbox-btn">--}}
                                {{--<input type="checkbox" id="check-enter-manually" data-opener="js-check-collapse" name="prescriptionType" data-target="enter-manually" value="{{ \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_MANUALLY }}">--}}
                                {{--<label for="check-enter-manually" class="checkbox-btn__btn btn-enter">Enter manually</label>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                </div>

                {{--<div class="js-check-collapse" id="enter-manually">--}}

                    <div class="step-box">
                        <h2 class="step-box__title">Enter Prescription Manually</h2>

                        @foreach($order->items as $product)

                            <div class="feature-heading">
                                <div class="feature-heading__image">
                                    <img src="{{ $product->images[0] }}" width="107" height="52" alt="product image">
                                </div>

                                <div class="feature-heading__title-holder">
                                    <strong class="feature-heading__title">{{ $product->brand_name }} {{ $product->model_name }}</strong>
                                </div>
                            </div>

                            <div class="enter-buttons">
                                <div class="enter-buttons__item">
                                    <div class="upload-button">
                                        {!! Form::radio("{$product->id}[prescription_type]", \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_IMAGE, false, ['id' => 'check-upload-'.$product->id]) !!}

                                        <label for="check-upload-{{ $product->id }}" class="btn-enter">
                                            Upload Prescription
                                            <input type="file" name="{{ $product->id }}[file]" accept="image/*" data-for="check-upload-{{ $product->id }}">
                                        </label>
                                    </div>
                                </div>

                                <div class="enter-buttons__item">
                                    <div class="upload-button">
                                        {!! Form::radio("{$product->id}[prescription_type]", \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_IMAGE, false, ['id' => 'check-take-pic-'.$product->id]) !!}

                                        <label for="check-take-pic-{{ $product->id }}" class="btn-enter">
                                            Take Picture
                                            <input type="file" name="{{ $product->id }}[file_cum]" accept="image/*" capture="camera" data-for="check-take-pic-{{ $product->id }}">
                                        </label>
                                    </div>
                                </div>
                            </div>

                            <div class="or-unit">
                                <span class="or-unit__or">or</span>
                            </div>

                            <div class="enter-buttons">
                                <div class="enter-buttons__item">
                                    <div class="checkbox-btn">
                                        {!! Form::radio("{$product->id}[prescription_type]", \App\Http\Commands\Checkout\Normal\SuccessCommand::PRESCRIPTION_MANUALLY, false, ['id' => 'check-enter-manually-'.$product->id, 'data-opener' => 'js-check-collapse', 'data-target' => 'enter-manually-'.$product->id ]) !!}
                                        <label for="check-enter-manually-{{ $product->id }}" class="checkbox-btn__btn btn-enter">Enter manually</label>
                                    </div>
                                </div>
                            </div>


                            <div class="js-check-collapse" id="enter-manually-{{ $product->id }}">

                            @foreach($product->buy_options as $label => $optionItem)
                                <div class="select-block">
                                    <h2 class="select-block__title">{{ $label }}</h2>

                                    <div class="checkbox-list__item fake-checkbox">
                                        <input type="checkbox" id="checkbox-{{ $label }}-{{ $product->id }}" data-opener="js-check-collapse" data-target="section-{{ $label }}-{{ $product->id }}">
                                        <label for="checkbox-{{ $label }}-{{ $product->id }}">{{ $label }}</label>
                                    </div>
                                    <div class="checkbox-list__slide js-check-collapse" id="section-{{ $label }}-{{ $product->id }}">

                                        @foreach($optionItem as $subLabel => $subOptionsItem)

                                            <div class="select-block">
                                                <h2 class="select-block__title">{{ $subLabel }}</h2>

                                                <div class="select-grid">
                                                    <div class="select-grid__item">
                                                        <div class="form-row input-row">
                                                            <div class="input-row__label-holder">
                                                                <label class="input-row__label" for="select-right-3" data-label-text="Right Eye"><span class="input-row__label-text">*Right Eye</span></label>
                                                                <span class="error-message">Not selected</span>
                                                            </div>

                                                            <div class="form-field">
                                                                @if($subOptionsItem[\App\Models\Order\Product\Option::SIDE_RIGHT]->values)
                                                                    <div class="select-unit">
                                                                        <div class="select-unit__holder" id="select-right-3">
                                                                            <select class="effect-select" name="{{ $product->id }}[option_{{ $subOptionsItem[\App\Models\Order\Product\Option::SIDE_RIGHT]->id }}]">
                                                                                <option>*Right Eye</option>
                                                                                @foreach($subOptionsItem[\App\Models\Order\Product\Option::SIDE_RIGHT]->values as $id => $value)
                                                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="form-field">
                                                                        {!! Form::text($product->id.'[option_'.$subOptionsItem[\App\Models\Order\Product\Option::SIDE_RIGHT]->id.']', old($product->id.'[option_'.$subOptionsItem[\App\Models\Order\Product\Option::SIDE_RIGHT]->id.']'), ['class' => 'input-text']) !!}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="select-grid__item">
                                                        <div class="form-row input-row">
                                                            <div class="input-row__label-holder">
                                                                <label class="input-row__label" for="select-left-4" data-label-text="Left Eye"><span class="input-row__label-text">*Left Eye</span></label>
                                                                <span class="error-message">Not selected</span>
                                                            </div>

                                                            <div class="form-field">
                                                                @if($subOptionsItem[\App\Models\Order\Product\Option::SIDE_LEFT]->values)
                                                                    <div class="select-unit">
                                                                        <div class="select-unit__holder" id="select-left-4">
                                                                            <select class="effect-select" name="{{ $product->id }}[option_{{ $subOptionsItem[\App\Models\Order\Product\Option::SIDE_LEFT]->id }}]">
                                                                                <option>*Left Eye</option>
                                                                                @foreach($subOptionsItem[\App\Models\Order\Product\Option::SIDE_LEFT]->values as $id => $value)
                                                                                    <option value="{{ $id }}">{{ $value }}</option>
                                                                                @endforeach
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                @else
                                                                    <div class="form-field">
                                                                        {!! Form::text($product->id.'[option_'.$subOptionsItem[\App\Models\Order\Product\Option::SIDE_LEFT]->id, old($product->id.'[option_'.$subOptionsItem[\App\Models\Order\Product\Option::SIDE_LEFT]->id), ['class' => 'input-text']) !!}
                                                                    </div>
                                                                @endif
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            {{--<div class="select-block">--}}
                                            {{--<h2 class="select-block__title">Axis</h2>--}}

                                            {{--<div class="select-grid">--}}
                                            {{--<div class="select-grid__item">--}}
                                            {{--<div class="form-row input-row">--}}
                                            {{--<div class="input-row__label-holder">--}}
                                            {{--<label class="input-row__label" for="input-right-1" data-label-text="Right Eye"><span class="input-row__label-text">*Right Eye</span></label>--}}
                                            {{--<span class="error-message">Enter right axis</span>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-field">--}}
                                            {{--<input type="text" class="input-text" id="input-right-1">--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                            {{--<div class="select-grid__item">--}}
                                            {{--<div class="form-row input-row">--}}
                                            {{--<div class="input-row__label-holder">--}}
                                            {{--<label class="input-row__label" for="input-left-1" data-label-text="Left Eye"><span class="input-row__label-text">*Left Eye</span></label>--}}
                                            {{--<span class="error-message">Enter right axis</span>--}}
                                            {{--</div>--}}

                                            {{--<div class="form-field">--}}
                                            {{--<input type="text" class="input-text" id="input-left-1">--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}
                                            {{--</div>--}}

                                        @endforeach
                                    </div>
                                </div>
                            @endforeach

                            </div>
                        @endforeach

                        <div class="form-row input-row @if($errors->has('email')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-email" data-label-text="Email"><span class="input-row__label-text">*Email: name@domain.com</span></label>
                                {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::email('email', old('email'), ['class' => 'input-text required', 'id' => 'input-email']) !!}
                            </div>
                        </div>


                        <div class="input-row @if($errors->has('username')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-user-name" data-label-text="User name"><span class="input-row__label-text">*User name</span></label>
                                {!! $errors->first('username', '<span class="error-message">:message</span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::text('username', old('username'), ['class' => 'input-text required', 'id' => 'input-user-name']) !!}
                            </div>
                        </div>

                        <div class="input-row @if($errors->has('notes')) error @endif">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-add-notes" data-label-text="Additional Notes"><span class="input-row__label-text">Additional Notes</span></label>
                                {!! $errors->first('notes', '<span class="error-message">:message</span>') !!}
                            </div>
                            <div class="form-field">
                                {!! Form::textarea('notes', old('notes'), ['class' => 'input-textarea', 'id' => 'input-add-notes', 'maxlength' => 100]) !!}

                            </div>
                        </div>

                        <div class="loader-box">
                            <button type="submit" class="btn-product btn-show-loader">
                                <span class="btn-product__wrap">Submit</span>
                            </button>
                            <span class="loader"></span>
                        </div>
                    </div>

                    <div class="note-list">
                        <div class="note-list__item">
                            <p><strong>Spherical</strong> - lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce sodales augue, eu posuere tellus. Donec ac egestas odio, quis bibendum eros.</p>
                        </div>

                        <div class="note-list__item">
                            <p><strong>Cylindrical</strong> - curabitur faucibus tincidunt orci, quis venenatis erat. Mauris dapibus vitae elit et lobortis. Nam condimentum tellus leo.</p>
                        </div>
                    </div>
                {{--</div>--}}
            <input type="hidden" name="stage" value="{{ \App\Http\Commands\Checkout\Normal\CommandContract::STAGE_SUCCESS }}"/>

            {!! Form::close() !!}

        @endif
    </div>

@endsection