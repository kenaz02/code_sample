<div class="form-row input-row @if($errors->has('firstname')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-first-name" data-label-text="First name"><span class="input-row__label-text">*First name</span></label>
        {!! $errors->first('firstname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('firstname', old('firstname') ? old('firstname') : $form->firstname, ['class' => 'input-text required', 'id' => 'input-first-name']) !!}
    </div>
</div>

<div class="form-row input-row @if($errors->has('lastname')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-last-name" data-label-text="Last name"><span class="input-row__label-text">*Last name</span></label>
        {!! $errors->first('lastname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('lastname',old('lastname') ? old('lastname') : $form->lastname , ['class' => 'input-text required', 'id' => 'input-last-name']) !!}
    </div>
</div>


<div class="form-row input-row @if($errors->has('telephone')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-mobilephone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
        {!! $errors->first('telephone', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('telephone', old('telephone') ? old('telephone') : $form->telephone, ['class' => 'input-text required', 'id' => 'input-mobilephone']) !!}
    </div>
</div>

<div class="form-row input-row @if($errors->has('email')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-email" data-label-text="Email"><span class="input-row__label-text">*Email: name@domain.com</span></label>
        {!! $errors->first('email', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('email', old('email') ? old('email') : $form->email, ['class' => 'input-text required', 'id' => 'input-email']) !!}
    </div>
</div>

<div class="form-row input-row @if($errors->has('street0')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-address" data-label-text="Address"><span class="input-row__label-text">*Address: xx, street</span></label>
        {!! $errors->first('street0', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('street0', old('street0') ? old('street0') : $form->street0, ['class' => 'input-text required', 'id' => 'input-address']) !!}
    </div>
</div>

<div class="form-row input-row @if($errors->has('city')) error @endif">
    <div class="input-row__label-holder">
        <label class="input-row__label" for="input-locality" data-label-text="Locality/town"><span class="input-row__label-text">*Locality / Town</span></label>
        {!! $errors->first('city', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
    </div>
    <div class="form-field">
        {!! Form::text('city', old('city') ? old('city') : $form->city, ['class' => 'input-text required', 'id' => 'input-locality']) !!}
    </div>
</div>

<div class="details-unit">
    <div class="details-unit__heading">
        <a href="#" class="details-unit__opener">Add Landmark</a>
    </div>
    <div class="details-unit__slide js-slide-hidden">
        <div class="details-unit__content">
            <div class="form-row input-row">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-landmark" data-label-text="Landmark"><span class="input-row__label-text">Landmark</span></label>
                </div>
                <div class="form-field">
                    {!! Form::textarea('landmark', old('landmark'), ['class' => 'input-textarea', 'id' => 'input-landmark']) !!}
                </div>
            </div>
        </div>
    </div>
</div>