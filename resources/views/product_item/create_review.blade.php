@extends('app')

@section('content')
    <div class="links-panel">
        <a href="{{ route('page', $product->product_url) }}" class="back-link">{{ $product->full_name }}</a>
    </div>
    @include('product_item.review_form', ['productID' => $product->id])
    @include('product_item.reviews_list', ['reviews' => $product->reviews])
@endsection