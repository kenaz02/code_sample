<div class="leave-review">
    <div class="leave-review__heading">
        <h1 class="leave-review__title">RATING</h1>
        <p>How do you rate this product?</p>
    </div>
    {!! Form::open(['route' => ['createReview', $productID], 'method' => 'POST', 'class'  => 'review-form']) !!}
        <div class="vote-row">
            <ul class="vote-unit">
                <li class="vote-unit__item">
                    {!! Form::radio('rating', 1, false, ['class' => 'check-vote', 'id' => 'vote-1']) !!}
                    {!! Form::label('vote-1', '1') !!}
                </li>
                <li class="vote-unit__item">
                    {!! Form::radio('rating', 2, false, ['class' => 'check-vote', 'id' => 'vote-2']) !!}
                    {!! Form::label('vote-2', '2') !!}
                </li>
                <li class="vote-unit__item">
                    {!! Form::radio('rating', 3, false, ['class' => 'check-vote', 'id' => 'vote-3']) !!}
                    {!! Form::label('vote-3', '3') !!}
                </li>
                <li class="vote-unit__item">
                    {!! Form::radio('rating', 4, false, ['class' => 'check-vote', 'id' => 'vote-4']) !!}
                    {!! Form::label('vote-4', '4') !!}
                </li>
                <li class="vote-unit__item">
                    {!! Form::radio('rating', 5, false, ['class' => 'check-vote', 'id' => 'vote-5']) !!}
                    {!! Form::label('vote-5', '5') !!}
                </li>
            </ul>
            {!! $errors->first('rating', '<span class="error-message" style="display: block;"><span class="error-message__holder">:message</span></span>') !!}
        </div>
        <div class="form-row @if($errors->has('name')) error @endif">
            {!! Form::label('name', 'Nick Name*') !!}
            <div class="form-field form-field_type_inp">
                {!! Form::text('name', old('name'), ['class' => 'input-text required', 'maxlength' => '25']) !!}
                {!! $errors->first('name', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>
        <div class="form-row @if($errors->has('title')) error @endif">
            {!! Form::label('title', 'Summary of your review*') !!}
            <div class="form-field form-field_type_inp">
                {!! Form::text('title', old('title'), ['class' => 'input-text required', 'maxlength' => '50']) !!}
                {!! $errors->first('title', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>
        <div class="form-row @if($errors->has('title')) error @endif">
            {!! Form::label('description', 'Review*') !!}
            <div class="form-field">
                {!! Form::textarea('description', old('description'), ['class' => 'required', 'maxlength' => '500', 'cols' => 30, 'rows' => 10]) !!}
                {!! $errors->first('description', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>
        {!! Form::submit('Submit', ['class' => 'btn-submit']) !!}
    {!! Form::close() !!}
    @if(\Session::has('create:review:success'))
    <div class="thank-message">
        <div class="thank-message__message">
            <button type="button" class="thank-message__close thank-message__js-close">Close</button>
            <div class="thank-message__text">
                <p><strong class="thank-message__ttl">Thank You</strong> for review this product</p>
            </div>
        </div>
    </div>
    @endif
</div>