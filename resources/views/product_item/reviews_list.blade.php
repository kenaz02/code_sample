<div class="reviews-list">
    @foreach($reviews as $review)
        <article class="review-unit">
            <div class="review-unit__holder">
                <header class="review-unit__heading">
                    <div class="star-rating star-rating_type_review">
                        <span class="star-rating__inner" style="width: {{ $review->getConvertedRating() }}%">Rate this {{ $review->rating }} stars out of 5</span>
                    </div>
                    <strong class="review-unit__name"> {{ $review->name }}</strong>
                    <time datetime="{{ $review->date }}" class="review-unit__date"><span class="month">{{ date('M', strtotime($review->date)) }}</span> {{ date('j,Y', strtotime($review->date)) }}</time>
                </header>
                <div class="review-unit__content">
                    <h2>{{ $review->title }}</h2>
                    <p>{{ $review->description }}</p>
                </div>
            </div>
        </article>
    @endforeach
</div>