@extends('app')

@section('content')
    <div class="links-heading">
        @if($option->hasDefaultPackage())
            <a href="#" class="show-default-package">Show Default Package</a>
            <a href="#" class="view-all-packages">View All Packages</a>
        @endif
            <div class="links-heading__link-holder">
            <a href="{{ route('page', $product->product_url) }}" class="back-link back-link_type_circle">Select Lens package</a>
        </div>
    </div>

    {!! Form::open(['route' => 'addToCartBuyOptions', 'method' => 'POST']) !!}
    <div class="package-list">
        @foreach($option->lenses as $item)
            <div class="package-item @if($item->default_selection) package-item_type_default package-item_state_expanded @endif">
                <a href="#" class="package-item__head package-item__js-opener">
							<span class="package-item__opener-holder">
								<span class="package-item__title">
									<strong class="package-item__ttl">{{ $item->model_name }}</strong>
								</span>
								<span class="package-item__title-info">
									<span class="price-box">+<span class="currency currency_type_indian">₹</span><span class="price">{{ $item->lenskartPrice->price }}</span></span>
								</span>
							</span>
                </a>
                <div class="package-item__slide">
                    <div class="package-item__content">
                        <div class="package-item__content-head">
                            <div class="package-item__logo-box">
                                <img src="{{ $item->brand_image_url }}" width="113" height="22" alt="{{ $item->model_name }}">
                            </div>
                            @if($item->warranty)
                                <span class="warranty-item">
                                {!! vsprintf('<span class="warranty-item__qty">%d</span><span class="warranty-item__text"><span class="warranty-item__period">%s</span> Warranty</span>', explode(' ', $item->warranty)) !!}
                                </span>
                            @endif
                        </div>
                        <div class="features-unit">
                            <ul class="features-list features-list_state_less" data-less-qty="3">
                                @foreach($item->specifications as $specItem)
                                    <li class="features-list__item">{{ $specItem }}</li>
                                @endforeach
                            </ul>
                            @if(count($item->specifications) > 3)
                            <a href="#" class="btn-show-more btn-show-more_state_less">
                                <span class="btn-show-more__more">Learn more</span>
                                <span class="btn-show-more__less">Show less</span>
                            </a>
                            @endif
                        </div>
                        <div class="price-panel">
                            <div class="price-panel__item">
                                <strong class="price-panel__ttl">Frame Cost</strong>
                                @if($product->marketPrice->price != $product->lenskartPrice->price)
                                <div class="price-box price-box_type_old">
                                    <span class="currency currency_type_indian">₹</span>
                                    <s class="price">{{ $product->marketPrice->price }}</s>
                                </div>
                                @endif
                                <div class="price-box">
                                    <span class="currency currency_type_indian">₹</span>
                                    <span class="price">{{ $product->lenskartPrice->price }}</span>
                                </div>
                            </div>
                            <div class="price-panel__item">
                                <strong class="price-panel__ttl">Lens Cost</strong>
                                @if($item->marketPrice->price != $item->lenskartPrice->price)
                                <div class="price-box price-box_type_old">
                                    <span class="currency currency_type_indian">₹</span>
                                    <s class="price">{{ $item->marketPrice->price }}</s>
                                </div>
                                @endif
                                <div class="price-box">
                                    <span class="currency currency_type_indian">₹</span>
                                    <span class="price">{{ $item->lenskartPrice->price }}</span>
                                </div>
                            </div>
                            <div class="price-panel__item price-panel__item_type_total">
                                <div class="loader-box">
                                    <button type="submit" class="btn-show-loader ga-lens-package-submit" data-ga-lens-package-name="{{ $item->model_name }}">
                                        <label for="option_{{ $item->id }}">
                                            <strong class="price-panel__ttl">Total Cost</strong>
                                            <div class="price-box">
                                                <span class="currency currency_type_indian">₹</span>
                                                <span class="price">{{ $item->totalPrice->price }}</span>
                                            </div>
                                            <span class="price-panel__link">Buy Now</span>
                                        </label>
                                    </button>
                                    <span class="loader"></span>
                                </div>
                            </div>
                            {!!  Form::radio('option_value', $item->id, $item->default_selection, ['id' => 'option_'.$item->id, 'style' => 'display:none;' ]) !!}
                        </div>
                    </div>
                </div>
            </div>
        @endforeach
    </div>
    {!!  Form::hidden('option_id', $option->id) !!}
    {!!  Form::hidden('extra_id', $product->buy_options->extraId) !!}
    {!!  Form::hidden('extra_value', $option->label, ['id' => 'ga-lens-package-type']) !!}

    {!!  Form::hidden('id', $product->id) !!}

    {!!  Form::close() !!}
@endsection