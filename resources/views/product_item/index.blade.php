@extends('app')

@section('sticky-footer', ' ')
@section('sticky-header', ' ')

@section('content')

    <div class="product-unit">
        <div class="product-unit__heading">
            <div class="product-unit__title">
                <h1 class="product-unit__ttl">
                    <strong class="product-unit__name">{{ $product->brand_name }}</strong>
                    <span class="product-unit__id">({{ $product->id }})</span>
                    <div class="product-unit__model">{{ $product->full_name }}</div>
                </h1>
            </div>
            <div class="product-unit__heading-info">
                <div class="review-item">
                    <a href="#customer-review" class="go-to-review ga-review" data-ga-review-position="top">
                        <div class="star-rating">
                            <span class="star-rating__inner" style="width: {{ $product->avg_rating/5 * 100 }}%">Rate this <span id="ga-review-avg">{{ $product->avg_rating }}</span> stars out of 5</span>
                        </div>
                        <span class="review-item__qty">(<span id="ga-review-total">{{ $product->number_of_reviews or '0' }}</span>)</span>
                    </a>
                </div>
                @if($product->frame_size)
                    <dl class="product-unit__size">
                        <dt>Size:</dt>
                        <dd><a href="{{ route('page', 'frame-size-guide') }}" target="_blank">{{ $product->frame_size }}</a></dd>
                    </dl>
                @endif
            </div>
        </div>
        <div class="product-view">
            <ul class="tabs-list">
                <li class="tabs-list__item tabs-list__item_state_active"><a class="tabs-list__link" href="#tab-images"><span>Images</span></a></li>
                @if($product->headturn_male || $product->headturn_female)<li class="tabs-list__item"><a class="tabs-list__link ga-180-view" href="#tab-view"><span>180&deg; View</span></a></li>@endif
                @if($product->is_try_now_available)<li class="tabs-list__item tabs-list__item_type_try"><a class="tabs-list__link try-link" href="#tab-try-online"><span>Try Online</span></a></li>@endif
                @if($product->color_options)<li class="tabs-list__item"><a class="tabs-list__link" href="#tab-more-colors"><span>More Colors</span></a></li>@endif
            </ul>
            <div class="tab-content">
                <div class="tab-content__item" id="tab-images">
                    <div class="product-images" data-zoom-id="zoom-unit">
                        <div class="product-images__text">
                            <a href="#" class="product-images__open-zoom">Tap on image to zoom</a>
                        </div>
                        <div class="product-images__mask-holder">
                            <button type="button" class="btn-prev">Previous</button>
                            <button type="button" class="btn-next">Next</button>
                            <div class="product-images__mask">
                                <div class="product-images__slideset">
                                    @foreach($product->images as $imageItem)
                                        <div class="product-images__slide">
                                            <a href="#" class="product-images__link" data-image-big="{{ $imageItem }}">
                                                <img src="{{ $imageItem }}" width="628" height="301" alt="Vincent Chase">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                        <div class="bullet-nav bullet-nav_type_small"></div>
                    </div>
                </div>
                @if($product->headturn_male || $product->headturn_female)
                    <div class="tab-content__item" id="tab-view">
                        <div class="view-image-section">
                            <span class="view-tip">
                                <span class="view-tip__arrow"></span>
						        <span class="view-tip__decor">180&deg; View</span>
						        <span class="view-tip__tip">To see other angles <span class="view-tip__tilt">tilt your device or</span> <span class="view-tip__swipe">swipe the image</span> <span class="view-tip__click-btn">tap image or buttons</span></span>

						</span>
                            @if($product->headturn_male)
                                <div class="view-image view-image_state_active" id="view-image-first">
                                    <div class="view-image__mask">
                                        <div class="view-image__slideset">
                                            <img data-src="{{ $product->headturn_male }}" width="6132" height="393" alt="">
                                        </div>
                                    </div>
                                    <button type="button" class="btn-prev view-image__prev">Previous</button>
                                    <button type="button" class="btn-next view-image__next">Next</button>
                                </div>
                            @endif
                            @if($product->headturn_female)
                                <div class="view-image @if(!$product->headturn_male) view-image_state_active @endif" id="view-image-second">
                                    <div class="view-image__mask">
                                        <div class="view-image__slideset">
                                            <img data-src="{{ $product->headturn_female }}" width="6132" height="393" alt="">
                                        </div>
                                    </div>
                                    <button type="button" class="btn-prev view-image__prev">Previous</button>
                                    <button type="button" class="btn-next view-image__next">Next</button>
                                </div>
                            @endif
                            @if($product->headturn_male && $product->headturn_female)
                                <ul class="image-tabset">
                                    <li class="image-tabset__item">
                                        <a class="image-tabset__link" href="#view-image-first">
                                            <img src="/images/img-view-tabset-01.jpg" width="37" height="35" alt="">
                                        </a>
                                    </li>
                                    <li class="image-tabset__item">
                                        <a class="image-tabset__link" href="#view-image-second">
                                            <img src="/images/img-view-tabset-02.jpg" width="37" height="35" alt="">
                                        </a>
                                    </li>
                                </ul>
                            @endif
                        </div>
                    </div>
                @endif
                @if($product->is_try_now_available)
                    <div class="tab-content__item" id="tab-try-online">
                        <div class="vto-block">
                            <div id="HTML_VTOCONTAINER" class="vto-container" data-align="app" data-id="{{ $product->id }}" data-target="{{ $product->try_on_image }}" data-price="{{ $product->lenskartPrice->price }}"></div>
                        </div>
                    </div>
                @endif
                @if($product->color_options)
                    <div class="tab-content__item" id="tab-more-colors">
                        <div class="color-choice">
                            <div class="color-choice__mask">
                                <div class="color-choice__slideset">
                                    @foreach($product->color_options as $colorProductItem)
                                        <div class="color-choice__slide">
                                            <a href="/{{ $colorProductItem->product_url }}" class="color-choice__link" title="{{ $colorProductItem->brand_name }} - {{ $colorProductItem->model_name }}">
                                                    <span class="color-box">
                                                        <span class="color-box__half" style="background: {{ $colorProductItem->firstColor }};"></span>
                                                        <span class="color-box__half" style="background: {{ $colorProductItem->secondColor }};"></span>
                                                    </span>
                                                <img src="{{ $colorProductItem->images[0] }}" width="78" height="38" alt="Black">
                                            </a>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="color-choice__nav">
                                <button type="button" class="btn-prev">Previous</button>
                                <button type="button" class="btn-next">Next</button>
                            </div>
                            <div class="color-choice__note">
                                <p>Click to see details</p>
                            </div>
                        </div>
                    </div>
                @endif
            </div>
        </div>
        <div class="price-compare">
            @if($product->marketPrice->price > $product->lenskartPrice->price)
                <div class="price-compare__item">
                    <strong class="price-compare__ttl">{{ $product->marketPrice->name }}</strong>
                    <div class="price-box price-box_type_old">
                        <span class="currency currency_type_indian">{{ $product->marketPrice->currency_sym }}</span>
                        <s class="price">{{ $product->marketPrice->price }}</s>
                    </div>
                </div>
            @endif
            <div class="price-compare__item">
                <strong class="price-compare__ttl">{{ $product->lenskartPrice->name }}</strong>
                <div class="price-box">
                    <span class="currency currency_type_indian">{{ $product->lenskartPrice->currency_sym }}</span>
                    <span class="price">{{ $product->lenskartPrice->price }}</span>
                </div>
            </div>
            <div class="price-compare__item">
                <strong class="price-compare__ttl">{{ $product->getFirstBuyPrice()->name }}</strong>
                <div class="price-box">
                    <span class="currency currency_type_indian">{{ $product->getFirstBuyPrice()->currency_sym }}</span>
                    <span class="price">{{ $product->getFirstBuyPrice()->price }}</span>
                </div>
            </div>
        </div>
        {!! Form::open(['route' => 'addToCartMainStage', 'method' => 'POST', 'id' => 'buy-form']) !!}

        @if($product->type == \App\Models\Product::TYPE_EYE_GLASSES || $product->type == \App\Models\Product::TYPE_POWERED_SUN_GLASSES)
            <div class="choice-unit  @if($errors->has('lens_buy_option')) choice-unit_state_warning @endif">
                <div class="choice-unit__heading">
                    <h2 id="add-to-cart" class="choice-unit__title">PRESCRIPTION TYPE</h2>
                    <div class="info-icon-unit">
                        <div class="info-icon-unit__icon">
                            <span class="icon-info">Info</span>
                        </div>
                        <div class="info-icon-unit__popup">
                            <button type="button" class="info-icon-unit__close">Close</button>
                            <div class="info-icon-unit__popup-holder">
                                <ul class="info-icon-unit__list">
                                    @if($product->type == \App\Models\Product::TYPE_EYE_GLASSES)
                                        <li>Single Vision – Most common lenses used to correct either distance (far) vision or near (reading) vision.</li>
                                        <li>Bifocal/progressive – Dual power lenses with both near(reading) and distance (far) powers (more common in people above age 40).</li>
                                        <li>Zero power – Anti-Glare lenses for computer use, night driving and style (with No power).</li>
                                        <li>Frame Only – If you want to buy only the frame and no lenses in it.</li>
                                    @else
                                        <li>With Power - Sunglasses with power lenses. If you are in need of power for reading, writing, playing or for other activities you can choose not to wear a general glass.</li>
                                        <li>Without Power - If you want to buy only Sunglasses.</li>
                                    @endif
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @if($errors->has('lens_buy_option'))
                <div class="choice-unit__note">
                    <p>(Please select prescription type)</p>
                </div>
                @endif
                <ul class="choice-list">
                    @if($product->type == \App\Models\Product::TYPE_EYE_GLASSES)
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::SINGLE_VISION, false, ['class' => 'check-vote', 'id' => 'check-single-vision']) !!}
                            <label for="check-single-vision" class="choice-list__label"><span class="choice-list__label-text">Single vision</span></label>
                        </li>
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::BIOFOCAL, false, ['class' => 'check-vote', 'id' => 'check-bifocal']) !!}
                            <label for="check-bifocal" class="choice-list__label"><span class="choice-list__label-text">Bifocal/progressive</span></label>
                        </li>
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::ZERO_POWER, false, ['class' => 'check-vote', 'id' => 'check-zero-power']) !!}
                            <label for="check-zero-power" class="choice-list__label"><span class="choice-list__label-text">Zero power</span></label>
                        </li>
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::FRAME_ONLY, false, ['class' => 'check-vote', 'id' => 'frame-only']) !!}
                            <label for="frame-only" class="choice-list__label"><span class="choice-list__label-text">Frame only</span></label>
                        </li>
                    @else
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::WITHOUT_POWER, false, ['class' => 'check-vote', 'id' => 'check-without-power']) !!}
                            <label for="check-without-power" class="choice-list__label"><span class="choice-list__label-text">Without Power</span></label>
                        </li>
                        <li>
                            {!! Form::radio('lens_buy_option', \App\Models\Product\BuyOptions\EyeglassesOption::WITH_POWER, false, ['class' => 'check-vote', 'id' => 'check-with-power']) !!}
                            <label for="check-with-power" class="choice-list__label"><span class="choice-list__label-text">With Power</span></label>
                        </li>
                    @endif
                </ul>
            </div>
            <div class="note-unit">
                <p>(Power will be taken after placing order)</p>
            </div>
        @endif

        <div class="product-buttons">
            <div class="product-buttons__item product-buttons__item_sticky">
                <div class="sticky-item">
                    <div class="loader-box">
                        <button type="submit" class="btn-product btn-product_type_buy btn-show-loader">
                            <span class="btn-product__wrap">BUY NOW <small>(with one year warranty)</small></span>
                        </button>
                        <span class="loader"></span>
                    </div>
                </div>
            </div>
        </div>
        {!!  Form::hidden('full_name', $product->full_name) !!}
        {!!  Form::hidden('type', $product->type) !!}
        {!!  Form::hidden('id', $product->id) !!}
        {!!  Form::hidden('product_url', $product->product_url) !!}
        {!!  Form::close() !!}

        @if($product->is_tbyb)
            <div class="product-buttons">
                {!! Form::open(['route' => 'addToCartFHTForm', 'method' => 'POST', 'id' => 'buy-form-fht']) !!}
                <div class="product-buttons__item">
                    @if($product->is_in_fht)
                        <div class="btn-product btn-product_type_extra">
                            <span class="btn-product__wrap">Added to Home Trial</span>
                        </div>
                    @else
                        <div class="loader-box">
                            <button class="btn-product btn-product_type_extra btn-show-loader">
                                <span class="btn-product__wrap">+ Free home trial <small>(sample frame)</small></span>
                            </button>
                            <span class="loader"></span>
                        </div>
                    @endif
                </div>
                {!!  Form::hidden('id', $product->id) !!}
            </div>
        @endif

        <div class="status-unit">
            <dl class="status-unit__title">
                <dt>STATUS:</dt>
                <dd>READY TO SHIP <em class="status-unit__note">(LIMITED STOCK)</em></dd>
            </dl>
            <dl class="status-unit__item">
                <dt>Delivery time:</dt>
                <dd>Depends on your Power</dd>
            </dl>
            <a href="{{ route('page', 'delivery-time.html') }}" class="more-link">Click to know more</a>
        </div>
        <div class="ad-unit">
            <img src="/images/img-ad-01.jpg" width="303" height="95" alt="Free frame offer">
        </div>
        @if($product->frame_details)
            <div class="detail-mini">
                <div class="detail-mini__row">
                    <dl class="detail-mini__item">
                        <dt class="detail-mini__term">Size:</dt>
                        <dd class="detail-mini__descr"><a href="{{ route('page', 'frame-size-guide') }}" target="_blank">{{ $product->frame_size }}</a></dd>
                    </dl>
                    <dl class="detail-mini__item">
                        <dt class="detail-mini__term">Color:</dt>
                        <dd class="detail-mini__descr">{{ $product->frame_color }}</dd>
                    </dl>
                </div>
                <div class="detail-mini__row">
                    <dl class="detail-mini__item">
                        <dt class="detail-mini__term">Measurements:</dt>
                        <dd class="detail-mini__descr">{{ $product->frame_measure }}</dd>
                    </dl>
                    @if($product->frame_style)
                    <dl class="detail-mini__item">
                        <dt class="detail-mini__term">Style:</dt>
                        <dd class="detail-mini__descr">{{ $product->frame_style }}</dd>
                    </dl>
                    @endif
                </div>
            </div>

            <a href="{{ route('page', 'frame-size-guide') }}" class="btn-view" target="_blank">VIEW FRAME SIZE GUIDE</a>

            <div class="frame-size">
                <h2 class="frame-size__title">Frame Size Details</h2>
                <ul class="size-list">
                    <li class="size-list__item">
                        <img src="/images/img-frame-size-01.jpg" width="100" height="50" alt="Bridge size">
                        <strong class="size-list__text">{{ $product->frame_bridge_size }} mm</strong>
                    </li>
                    <li class="size-list__item">
                        <img src="/images/img-frame-size-02.jpg" width="100" height="50" alt="Temple size">
                        <strong class="size-list__text">{{ $product->frame_temple_size }} mm</strong>
                    </li>
                    <li class="size-list__item">
                        <img src="/images/img-frame-size-03.jpg" width="100" height="50" alt="Eye soze">
                        <strong class="size-list__text">{{ $product->frame_eye_size }} mm</strong>
                    </li>
                </ul>
            </div>
        @endif
        <div class="details-section">
            <section class="details-item">
                <div class="details-item__heading">
                    <a href="#" class="details-item__link details-item__link_js_opener">
                        <span class="details-item__decor-link">View Details</span>
                        <h2 class="details-item__title">PRODUCT DESCRIPTION</h2>
                    </a>
                </div>
                <div class="details-item__content">
                    <div class="text-unit">
                        {!! $product->description !!}
                    </div>
                </div>
            </section>
            <section class="details-item">
                <div class="details-item__heading">
                    <a href="#" class="details-item__link details-item__link_js_opener">
                        <span class="details-item__decor-link">View Details</span>
                        <h2 class="details-item__title">TECHNICAL INFORMATION</h2>
                    </a>
                </div>
                <div class="details-item__content">
                    <div class="tables-section">
                        <table class="detail-table">
                            @foreach($product->technicalSpecs as $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>{{ $item['value'] }}</td>
                                </tr>
                            @endforeach
                        </table>

                        <div class="tables-section__heading">
                            <h2 class="tables-section__title">General</h2>
                        </div>

                        <table class="detail-table">
                            @foreach($product->generalSpecs as $item)
                                <tr>
                                    <td>{{ $item['name'] }}</td>
                                    <td>{{ $item['value'] }}</td>
                                </tr>
                            @endforeach
                        </table>
                    </div>
                </div>
            </section>
            @if($product->number_of_reviews)
            <section id="customer-review" class="details-item ga-review" data-ga-review-position="bottom">
                <div class="details-item__heading">
                    <a href="#" class="details-item__link details-item__link_js_opener">
                        <span class="details-item__decor-link">View Details</span>
                        <h2 class="details-item__title">CUSTOMER REVIEW</h2>
                    </a>
                </div>
                <div class="details-item__content">
                    <div class="reviews-section">
                        <h2 class="reviews-section__title">Reviewed by {{ $product->number_of_reviews or '0' }} customers</h2>
                        @include('product_item.reviews_list', ['reviews' => $product->reviews])
                        <a href="{{ route('productReviews', $product->product_url) }}" class="review-link" title="Write a review">Write a review</a>
                    </div>
                </div>
            </section>
            @else
            <section id="customer-review" class="details-item ga-review @if(\Session::has('create:review:submit')) expanded @endif" data-ga-review-position="bottom">
                <div class="details-item__heading">
                    <a href="#" class="details-item__link details-item__link_js_opener">
                        <span class="details-item__decor-link"></span>
                        <h2 id="create-review" class="details-item__title">Be the first to review this product</h2>
                    </a>
                </div>
                <div class="details-item__content">
                        @include('product_item.review_form', ['productID' => $product->id])
                </div>
            </section>
            @endif
        </div>
        @if($product->related_items)
            <div class="related-unit slide-unit expanded">
                <h2 class="related-unit__title">
                    <a class="related-unit__opener slide-unit__opener" href="#">SIMILAR PRODUCTS</a>
                </h2>
                <div class="related-unit__content slide-unit__slide">
                    <div class="scroll-slider scroll-slider_related overthrow">
                        @foreach($product->related_items as $relatedProductItem)
                            <div class="scroll-slider__item">
                                <div class="product-related">
                                    <a href="/{{ $relatedProductItem->product_url }}">
                                        <img class="product-related__image" src="{{ $relatedProductItem->images[0] }}" width="85" height="49" alt="{{ $relatedProductItem->brand_name }}">
                                        <div class="product-related__ttl">
                                            <strong class="product-related__name">{{ $relatedProductItem->brand_name }}</strong>
                                            <p>Red</p>
                                        </div>
                                        <div class="price-box price-box_type_old">
                                            <span class="price">Rs. {{ $relatedProductItem->marketPrice->price }}</span>
                                        </div>
                                        <div class="price-box">
                                            <span class="price">Rs. {{ $relatedProductItem->lenskartPrice->price }}</span>
                                        </div>
                                    </a>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        <div class="product-buttons product-button_pos_bottom">
            <div class="product-buttons__item">
                <div class="loader-box">
                    <button type="submit" class="btn-product btn-product_type_buy btn-show-loader">
                        <span class="btn-product__wrap" onclick="document.getElementById('buy-form').submit();">BUY NOW <small>(with one year warranty)</small></span>
                    </button>
                    <span class="loader"></span>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('after-footer')
<div class="zoom-unit zoom-unit_state_hidden" id="zoom-unit">
    <button type="button" class="zoom-unit__close">close</button>
    <button type="button" class="btn-prev">Previous</button>
    <button type="button" class="btn-next">Next</button>
    <div class="zoom-unit__mask-holder">
        <div class="zoom-unit__mask overthrow"></div>
    </div>
    <div class="bullet-nav bullet-nav_type_small"></div>
</div>
@endsection