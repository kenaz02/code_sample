@extends('app')

@section('body-class', 'cart-page')

@section('content')
    <div class="logo-panel logo-panel_type_light">
        <div class="logo-panel__logo">
            <a href="/">
                <img src="/images/logo-02.png" width="152" height="16" alt="Lenskart.com">
            </a>
        </div>
    </div>
    {!! Form::open(['route' => 'addToCartBuyOptions', 'method' => 'POST', 'files' => 'true', 'id' => 'cl-buy-options-form']) !!}
    <div class="prescription-block">
        <div class="prescription-heading">
            <a href="{{ route('page', $product->product_url) }}" class="back-link back-link_type_empty"></a>
            <h1 class="prescription-heading__title">Enter prescription</h1>
        </div>
        <div class="prescription-block__content">
            @include('product_item.opts_contact_lenses.errors')

            <div class="enter-section row-validation">
                <div class="enter-item">
                    {!! Form::radio('next_stage', \App\Http\Commands\AddToCart\ContactLensesCommand::PRESCRIPTION_SUBMIT, false, ['class' => 'enter-item__check', 'id' => 'check-upload']) !!}
                    <label class="enter-unit enter-unit_upload" for="check-upload">
                        Upload Prescription from Camera/Gallery
                        {!! Form::file('prescription_file', ['accept' => 'image/*', 'data-for' => 'check-upload']) !!}
                    </label>
                </div>
                <div class="enter-item">
                    {!! Form::radio('next_stage', \App\Http\Commands\AddToCart\ContactLensesCommand::RX_SUBMIT, false, ['class' => 'enter-item__check', 'id' => 'check-manually']) !!}
                    <label class="enter-unit enter-unit_manually" for="check-manually">Enter Rx <br> manually</label>
                </div>
            </div>

            @include('product_item.opts_contact_lenses.patient_info_form')

            <div class="loader-box">
                <button id="buy-options-form-submit" type="submit" class="btn-save btn-show-loader">
                    SAVE &amp; CONTINUE
                </button>
                <span id="buy-options-loader" class="loader"></span>
            </div>

        </div>
    </div>

    <input type="hidden" name="extra_id" value="{{ $product->buy_options->extraId }}"/>
    <input type="hidden" name="stage" value="{{ \App\Http\Commands\AddToCart\ContactLensesCommand::DEFAULT_SUBMIT }}"/>
    <input type="hidden" name="id" value="{{ $product->id }}"/>

    {!!  Form::close() !!}

@endsection


@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
             $('#buy-options-form-submit').on('click', function(){
                    var values = {};

                    //TODO:Refactor this
                    values._token = '{{ csrf_token() }}';
                    $('.row-validation').each(function(){
                        var $input = $(this).find('input'),
                            name = $input.attr('name');

                        if($input.hasClass('input-text')){
                            values[name] = $input.val();
                        }
                        else{
                            if (!$('input[name="' + name + '"]:checked').val()) {
                                values[name] = '';
                            }
                            else if($input.is(":checked")){
                                values[name] = $input.val();
                            }
                        }
                    });

                    $.ajax({
                        url : '{{ route('addToCartCLValidate') }}',
                        method : 'POST',
                        data : values,
                        success : function(){
                            $('#cl-buy-options-form').submit();
                        },
                        error : function(xhr){
                            if(xhr.status == 422){
                                var data = xhr.responseJSON;
                                var errorAlert = $('.error-tape');

                                errorAlert.show();
                                $('.row-validation').each(function(){
                                    var name = $(this).find('input').attr('name');
                                    if(name in data){
                                        $(this).addClass('error');
                                        var message = data[name].join(', ');

                                        var $input = $(this).find('input');


                                        if($input.hasClass('input-text')){
                                            $(this).find('.error-message').remove();
                                            $(this).find('input').after('<span class="error-message"><span class="error-message__holder">' + message + '</span></span>');
                                        }


                                        errorAlert.find('p.error-' + name).remove();
                                        errorAlert.append('<p class="error-' + name + '">' + message + '</p>');
                                    }else{
                                        $(this).removeClass('error');
                                        $(this).find('.error-message').remove();
                                        errorAlert.find('p.error-' + name).remove();
                                    }
                                });
                                $('#buy-options-loader').data('LoaderPlugin').destroyLoader();
                            }
                        }
                    });

                 return false;
             });
        });
    </script>
@endsection