<li class="values-list__item">
    <span class="values-list__text">{{ $option->label }}</span>

    @if($option->isPowerOption())
        <div class="info-icon-unit">
            <div class="info-icon-unit__icon info-icon-unit__icon_green">
                <span class="icon-info icon-info_green">Info</span>
            </div>
            <div class="info-icon-unit__popup info-icon-unit__popup_green" style="display: none;">
                <button type="button" class="info-icon-unit__close info-icon-unit__close_green">Close</button>
                <div class="info-icon-unit__popup-holder info-icon-unit__popup-holder_green">
                    <p>This is your main power and is a + or - number or 0. To learn more, <a href="http://www.lenskart.com/understand-your-prescription_cl.html">click here</a>.</p>
                </div>
            </div>
        </div>
    @endif

    @if($option->isBoxesOption())
        <div class="info-icon-unit">
            <div class="info-icon-unit__icon info-icon-unit__icon_green">
                <span class="icon-info icon-info_green">Info</span>
            </div>
            <div class="info-icon-unit__popup info-icon-unit__popup_green" style="display: none;">
                <button type="button" class="info-icon-unit__close info-icon-unit__close_green">Close</button>
                <div class="info-icon-unit__popup-holder info-icon-unit__popup-holder_green">
                    <p>A box has 6 Lens/box for 1 eye. To learn more, <a href="http://www.lenskart.com/understanding_lens">click here</a>.</p>
                </div>
            </div>
        </div>
    @endif

    <div class="values-list__select">
        {!! Form::select('opt_'.$option->side.'_'.$option->id, $option->values , old('opt_'.$option->side.'_'.$option->id)) !!}
    </div>
</li>