@extends('app')

@section('body-class', 'cart-page')

@section('content')
    <div class="logo-panel logo-panel_type_light">
        <div class="logo-panel__logo">
            <a href="/">
                <img src="/images/logo-02.png" width="152" height="16" alt="Lenskart.com">
            </a>
        </div>
    </div>
    <div class="prescription-block">
        <div class="prescription-heading">
            <a href="{{ route('productBuyOptions', $product->product_url) }}" class="back-link back-link_type_empty"></a>
            <h1 class="prescription-heading__title">Enter prescription</h1>
        </div>
        <div class="prescription-block__content">
            @include('product_item.opts_contact_lenses.errors')
            <div class="enter-section">
                <div class="enter-item">
                    <label class="enter-unit enter-unit_upload">Upload Prescription from Camera/Gallery</label>
                </div>
                <div class="enter-item">
                    <label class="enter-unit enter-unit_manually enter-unit_state_active">Enter Rx <br> manually</label>
                </div>
            </div>
            {!! Form::model($model, ['route' => 'addToCartBuyOptions', 'method' => 'POST', 'files' => 'true']) !!}
            <div class="values-unit">
                <div class="values-unit__opener fake-check">
                    {!! Form::checkbox('side_opts[right]', 1, true, ['id'=> 'check-right-eye', 'data-opener' => 'js-check-collapse', 'data-target' => 'right-eye-od']) !!}
                    {!! Form::label('check-right-eye', 'Right Eye (OD)') !!}
                </div>
                <div class="values-unit__content js-check-collapse" id="right-eye-od">
                    <ul class="values-list values-list_type_right-eye">
                        @foreach($product->buy_options->rightOptions as $option)
                            @include('product_item.opts_contact_lenses.rx_option', ['option' => $option])
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="values-unit">
                <div class="values-unit__opener fake-check">
                    {!! Form::checkbox('side_opts[left]', 1, true, ['id'=> 'check-left-eye', 'data-opener' => 'js-check-collapse', 'data-target' => 'left-eye-os']) !!}
                    {!! Form::label('check-left-eye', 'Left Eye (Os)') !!}
                </div>
                <div class="values-unit__content js-check-collapse" id="left-eye-os">
                    <ul class="values-list values-list_type_left-eye">
                        @foreach($product->buy_options->leftOptions as $option)
                            @include('product_item.opts_contact_lenses.rx_option', ['option' => $option])
                        @endforeach
                    </ul>
                </div>
            </div>
            @include('product_item.opts_contact_lenses.patient_info_form')
            <div class="loader-box">
                <button type="submit" class="btn-save btn-show-loader">
                    SAVE &amp; CONTINUE
                </button>
                <span id="buy-options-loader" class="loader"></span>
            </div>
            <input type="hidden" name="extra_id" value="{{ $product->buy_options->extraId }}"/>
            <input type="hidden" name="stage" value="{{ \App\Http\Commands\AddToCart\ContactLensesCommand::RX_SUBMIT }}"/>
            <input type="hidden" name="id" value="{{ $product->id }}"/>

            {!!  Form::close() !!}
        </div>
    </div>


@endsection