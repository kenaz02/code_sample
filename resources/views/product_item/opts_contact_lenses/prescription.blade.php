@extends('app')

@section('body-class', 'cart-page')

@section('content')
    <div class="logo-panel logo-panel_type_light">
        <div class="logo-panel__logo">
            <a href="/">
                <img src="/images/logo-02.png" width="152" height="16" alt="Lenskart.com">
            </a>
        </div>
    </div>
    <div class="prescription-block">
        <div class="prescription-heading">
            <a href="{{ route('productBuyOptions', $product->product_url) }}" class="back-link back-link_type_empty"></a>
            <h1 class="prescription-heading__title">Enter prescription</h1>
        </div>
        <div class="prescription-block__content">
            @include('product_item.opts_contact_lenses.errors')
            <div class="uploaded-unit">
                <div class="uploaded-box">
                    <img src="{{ $model->prescriptionLocalFile }}" width="535" height="306" alt="image">
                </div>
                <div class="upload-control">
                    <div class="upload-control__btn-item">
                        <div class="upload-btn upload-btn_retake">
                            {!! Form::open(['route' => 'addToCartBuyOptions', 'method' => 'POST',  'files' => 'true']) !!}
                            <input type="hidden" name="stage" value="{{ \App\Http\Commands\AddToCart\ContactLensesCommand::PRESCRIPTION_RETAKE }}"/>
                            <input type="hidden" name="id" value="{{ $product->id }}"/>

                            <span>RETAKE</span>

                            {!! Form::file('prescription_file', ['accept' => 'image/*']) !!}

                            <div class="loader-box">
                                <button type="submit" class="upload-btn upload-btn_retake btn-show-loader">
                                    RETAKE
                                </button>
                                <span id="buy-options-loader" class="loader"></span>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                    <div class="upload-control__btn-item">
                        {!! Form::open(['route' => 'addToCartBuyOptions', 'method' => 'POST']) !!}
                            <input type="hidden" name="stage" value="{{ \App\Http\Commands\AddToCart\ContactLensesCommand::STAGE_REMOVE }}"/>
                            <input type="hidden" name="id" value="{{ $product->id }}"/>
                            <div class="loader-box">
                                <button type="submit" class="upload-btn upload-btn_remove btn-show-loader">
                                    REMOVE
                                </button>
                                <span id="buy-options-loader" class="loader"></span>
                            </div>
                        {!! Form::close() !!}

                    </div>
                </div>
            </div>
            {{--<a href="#" class="btn-add">add another prescription</a>--}}
            {!! Form::model($model, ['route' => 'addToCartBuyOptions', 'method' => 'POST']) !!}
            <div class="info-block">
                <h2>BOXES</h2>
                <ul class="values-list">
                    <li class="values-list__item">
                        <div class="fake-check">
                            {!! Form::checkbox('side_opts[right]', 1, true, ['id'=> 'check-right-eye']) !!}
                            {!! Form::label('check-right-eye', 'Right Eye (OD)') !!}
                        </div>
                        <div class="values-list__select">
                            {!! Form::select('opt_'.$product->buy_options->getRightBoxes()->side.'_'.$product->buy_options->getRightBoxes()->id, $product->buy_options->getRightBoxes()->values , old('opt_'.$product->buy_options->getRightBoxes()->side.'_'.$product->buy_options->getRightBoxes()->id)) !!}
                         </div>
                    </li>
                    <li class="values-list__item">
                        <div class="fake-check">
                            {!! Form::checkbox('side_opts[left]', 1, true, ['id'=> 'check-left-eye']) !!}
                            {!! Form::label('check-left-eye', 'Left Eye (Os)') !!}
                        </div>
                        <div class="values-list__select">
                            {!! Form::select('opt_'.$product->buy_options->getLeftBoxes()->side.'_'.$product->buy_options->getLeftBoxes()->id, $product->buy_options->getLeftBoxes()->values , old('opt_'.$product->buy_options->getLeftBoxes()->side.'_'.$product->buy_options->getLeftBoxes()->id)) !!}
                        </div>
                    </li>
                </ul>
            </div>
            @include('product_item.opts_contact_lenses.patient_info_form')
            <div class="loader-box">
                <button type="submit" class="btn-save btn-show-loader">
                    SAVE &amp; CONTINUE
                </button>
                <span id="buy-options-loader" class="loader"></span>
            </div>
            <input type="hidden" name="opt_{{$product->buy_options->extraId }}" value="{{ $model->prescriptionFile }}"/>
            <input type="hidden" name="stage" value="{{ \App\Http\Commands\AddToCart\ContactLensesCommand::PRESCRIPTION_SUBMIT }}"/>
            <input type="hidden" name="id" value="{{ $product->id }}"/>

            {!!  Form::close() !!}
        </div>
    </div>

@endsection