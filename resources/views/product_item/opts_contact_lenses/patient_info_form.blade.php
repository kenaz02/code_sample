<div class="info-block">
    <h2>Patient info</h2>
    <div class="info-form">
        <div class="form-row row-validation @if($errors->has('username')) error @endif">
            {!! Form::label('username', 'Patient Name') !!}
            <div class="form-field">
                {!! Form::text('username', old('username'), ['class' => 'input-text required', 'maxlength' => '25']) !!}
                {!! $errors->first('username', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>
        <div class="form-row row-validation @if($errors->has('dob')) error @endif">
            {!! html_entity_decode(Form::label('dob', 'Date of Birth <span class="label-note">(Optional)</span>')) !!}
            <div class="form-field">

                {!! Form::text('dob',  old('dob'), ['class' => 'input-text date-mask date-birth required', 'maxlength' => '25']) !!}
                {!! $errors->first('dob', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>

        <div class="form-row">
            <label>Gender <span class="label-note">(Optional)</span></label>
            <div class="check-unit">
                <div class="check-unit__item">
                    {!! Form::radio('gender', 'Male', true, ['class' => 'check-gender', 'id' => 'check-male']) !!}
                    {!! Form::label('check-male', 'Male') !!}
                </div>
                <div class="check-unit__item">
                    {!! Form::radio('gender', 'Female', false, ['class' => 'check-gender', 'id' => 'check-female']) !!}
                    {!! Form::label('check-female', 'Female') !!}
                </div>
            </div>
        </div>
        <div class="form-row row-validation @if($errors->has('notes')) error @endif">
            {!! html_entity_decode(Form::label('notes', 'Additional Notes <span class="label-note">(Optional)</span>')) !!}
            <div class="form-field">
                {!! Form::textarea('notes', old('notes'), ['maxlength' => '250', 'cols' => 30, 'class' => 'required', 'rows' => 10]) !!}
                {!! $errors->first('notes', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>

        </div>
    </div>
</div>