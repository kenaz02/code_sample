<div class="error-tape"  @if (count($errors) == 0) style="display: none;"@endif>
    @foreach ($errors->all() as $error)
        <p>{{ $error }}</p>
    @endforeach
</div>