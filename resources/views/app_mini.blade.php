@extends('base')

@section('base')
    <div id="main">
        <div class="logo-panel">
            <div class="logo-panel__logo">
                <a href="/">
                    <img src="/images/logo-02.png" width="152" height="16" alt="Lenskart.com">
                </a>
            </div>
        </div>
        @yield('content')
    </div>
@endsection