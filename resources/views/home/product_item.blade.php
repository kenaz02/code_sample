<div id="product_{{ $item->id }}" class="product-small">
    <a href="/{{ $item->product_url }}">
        @foreach($item->images as $imageItem)
            <img class="product-small__image" src="{{ $imageItem }}" width="150" height="57" alt="{{ $item->brand_name }}"/>
        @endforeach
        <strong class="product-small__name">{{ $item->brand_name }}</strong>
        <strong class="price"><span class="currency currency_type_indian">{{ $item->prices[0]->currency_sym }}</span> {{ $item->prices[0]->price }}</strong>
    </a>
</div>