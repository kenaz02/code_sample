<li><a href="{{ route($route, ['eyeglasses', $gender]) }}">Eyeglasses</a></li>
<li><a href="{{ route($route, ['sunglasses', $gender]) }}">Sunglasses</a></li>
<li><a href="{{ route($route, ['power-sunglasses', $gender]) }}">Prescription Sunglasses</a></li>
<li><a href="{{ route($route, ['contact-lenses', $gender]) }}">Contact Lenses</a></li>
<li><a href="{{ route($route, ['zero-power', $gender]) }}">Zero Power Eyeglasses</a></li>
<li><a href="{{ route($route, ['latest-collection', $gender]) }}">Latest Collection</a></li>