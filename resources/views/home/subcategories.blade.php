@extends('app')

@section('content')
    <div class="category-section">

        @foreach($subcategories as $item)
            <div class="category-item">
            <h2 class="category-item__title"><a href="/{{ $item->url }}">{{ $item->name }}</a></h2>
                @if(isset($item->children))
                    <ul class="category-list category-list_type_style">

                        @foreach($item->children as $childrenItem)
                            <li class="category-list__item">
                                <a class="category-list__link" href="/{{ $childrenItem->url }}">
                                    <div class="category-list__image">
                                        @if(isset($childrenItem->icon_image))
                                            <img src="{{ $childrenItem->icon_image }}"  alt="{{ $childrenItem->name }}">
                                        @endif
                                    </div>
                                    <div class="category-list__text">
                                        <strong class="category-list__ttl">{{ $childrenItem->name }}</strong>
                                    </div>
                                </a>
                            </li>
                        @endforeach

                    </ul>
                @endif
                @if($item->id == \App\Models\Repositories\CategoriesRepository::FACE_CATEGORY_ID)
                    <div class="info-unit">
                        <a class="button button_type_info info-unit__js-opener" href="#">How to tell what face shape you have <i class="ico-info">?</i></a>
                        <div class="info-unit__popup">
                            <a href="#" class="info-unit__close info-unit__js-close">close</a>
                            <p>Look directly in to a mirror and look at the outline of your face - which shape does it most closely resemble? Easiest way to do this is after a shower, when the bathroom mirror is steamed up as you can trace the outline with your finger. Remember, you are just looking for the closest match.</p>
                        </div>
                    </div>
                @endif
            </div>
        @endforeach
        <div class="category-item">
            <h2 class="category-item__title">Our Services</h2>
            <ul class="category-list category-list_type_style">
                @foreach($ourServices as $item)
                    <li class="category-list__item">
                        <a class="category-list__link" href="#">
                            <div class="category-list__image">

                            </div>
                            <div class="category-list__text">
                                <strong class="category-list__ttl">{{ $item }}</strong>
                            </div>
                        </a>
                    </li>
                @endforeach
            </ul>
        </div>
    </div>
@endsection