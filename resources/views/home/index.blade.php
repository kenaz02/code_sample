@extends('app')

@section('content')
    <div class="slogan-block">
        <h1>75% Indians who need specs, don't wear them</h1>
        <p>For every pair of glasses sold, Lenskart makes sure a person in need gets one. <a href="#" class="ga-home-one-for-one">One for One</a></p>
    </div>
    <div class="image-gallery gallery-js-ready">
        <div class="image-gallery__mask" style="height: 329px;">
            <div class="image-gallery__slideset" style="position: relative; height: 100%; margin-left: -657px;">
                @foreach($data['offers'] as $offer)
                    <div class="image-gallery__slide" style="position: absolute; left: -9999px; width: 657px;">
                        <a href="{{ $offer['category_url'] }}" class="ga-banner-offer">
                            <img src="{{ $offer['cover_image_url'] }}" width="320" height="160">
                        </a>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="bullet-nav">
            <ul>
                @foreach($data['offers'] as $count => $offer)
                    <li class=""><a href="#">{{ $count }}</a></li>
                @endforeach
            </ul>
        </div>
    </div>
    <nav class="category-menu">
        <div class="category-unit">
            <h2 class="category-unit__title">SHOP FOR</h2>
            <ul class="category-unit__list">
                <li class="category-unit__item">
                    <a class="category-unit__link category-unit__link_js_opener" href="#">Men</a>
                    <div class="category-unit__content">
                        <ul class="ga-btn-category category-unit__sub-list" data-key="Men">
                            @include('home.index_categories', ['gender' => 'men', 'route' => 'subcategories'])
                        </ul>
                    </div>
                </li>
                <li class="category-unit__item">
                    <a class="category-unit__link category-unit__link_js_opener" href="#">Women</a>
                    <div class="category-unit__content">
                        <ul class="ga-btn-category category-unit__sub-list" data-key="Women">
                            @include('home.index_categories', ['gender' => 'women', 'route' => 'subcategories'])
                        </ul>
                    </div>
                </li>
                <li class="category-unit__item">
                    <a class="category-unit__link category-unit__link_js_opener" href="#">Kids</a>
                    <div class="category-unit__content">
                        <ul class="ga-btn-category category-unit__sub-list" data-key="Kids">
                            @include('home.index_categories', ['gender' => 'kids', 'route' => 'subcategories'])
                        </ul>
                    </div>
                </li>
            </ul>
        </div>
        <div class="category-unit">
            <h2 class="category-unit__title">OUR SERVICES</h2>
            <ul class="category-unit__list">
                <li class="category-unit__item">
                    <a href="{{ route('homeEyeCheckUp') }}" class="category-unit__link">Home Eye Check-up</a>
                </li>
                <li class="category-unit__item">
                    <a href="{{ route('offer', 4289)}}" class="category-unit__link">Free Home Trial</a>
                </li>
                <li class="category-unit__item">
                    <a href="#" class="category-unit__link">Optical Stores</a>
                </li>
                <li class="category-unit__item">
                    <a href="#" class="category-unit__link">Local Optician v/s Lenskart</a>
                </li>
            </ul>
        </div>
    </nav>
    <section class="opinion-item">
        <div class="opinion-item__flag">
            <img src="images/img-flag-01.jpg" width="55" height="36" alt="The image of the Indian flag">
        </div>
        <h2 class="opinion-item__title">Loved by over 10 Lakh Indians</h2>
        <div class="opinion-item__stars">
            <img src="images/img-review-stars.png" width="83" height="14" alt="5 stars">
        </div>
        <blockquote class="opinion-quote">
            <q class="opinion-quote__text">Good product, reasonable prices and very prompt delivery.Highly recommended.</q>
            <cite class="opinion-quote__author">Pulak Bhattacharyya</cite>
        </blockquote>
    </section>
    <section class="explain-block">
        <h2 class="explain-block__title">Why Buy from Lenskart?</h2>
        <div class="explain-block__holder">
            <div class="explain-item">
                <div class="explain-item__title-box">
                    <h3 class="explain-item__title">Lenskart</h3>
                </div>
                <ul class="explain-item__list">
                    <li>Lenses shaped by laser precision machines</li>
                    <li>Same price no matter what your power</li>
                    <li>1 year warranty on all lenses</li>
                    <li>50% lower prices than local optician (on average)</li>
                    <li>5,000 frames in stock in a range of styles and brands</li>
                </ul>
            </div>
            <div class="explain-item">
                <div class="explain-item__title-box">
                    <h3 class="explain-item__title explain-item__title_blue">Local Optician</h3>
                </div>
                <ul class="explain-item__list explain-item__list_x">
                    <li>Lenses shaped by hand (risk of human error)</li>
                    <li>Prices vary depending on power of lenses</li>
                    <li>No warranty on lenses</li>
                    <li>50% higher prices than Lenskart (on average)</li>
                    <li>Limited selection of frames in stock</li>
                </ul>
            </div>
        </div>
    </section>
    <div class="banner-item">
        <a href="#">
            <img src="images/banner-free-home-trial.jpg" width="411" height="170" alt="Free home trial. Upto 5 frames. At your doorstep 100% free. Try now">
        </a>
    </div>
    <div class="offer-section">
        <div class="products-unit slide-unit expanded ga-home-product-section" data-gakey="Our best seller">
            <h2 class="products-unit__title">
                <a class="products-unit__opener slide-unit__opener" href="#">OUR BEST SELLERS</a>
            </h2>
            <div class="products-unit__content slide-unit__slide">
                <div class="scroll-slider scroll-slider_products ga-home-product-container overthrow" data-gakey="Bestseller">
                    @foreach($data['best_sellers'] as $item)
                        <div class="scroll-slider__item ga-evholder">
                            @include('home.product_item', ['item' => $item])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        <div class="products-unit slide-unit expanded ga-home-product-section" data-gakey="Our new arrivals">
            <h2 class="products-unit__title">
                <a class="products-unit__opener slide-unit__opener" href="#">OUR NEW ARRIVALS</a>
            </h2>
            <div class="products-unit__content slide-unit__slide">
                <div class="scroll-slider scroll-slider_products ga-home-product-container overthrow" data-gakey="New Arrivals">
                    @foreach($data['new_arrivals'] as $item)
                        <div class="scroll-slider__item ga-evholder">
                            @include('home.product_item', ['item' => $item])
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
        @if($data['recently_viewed'])
            <div class="products-unit slide-unit expanded ga-home-product-section" data-gakey="Your recently visited">
                <h2 class="products-unit__title">
                    <a class="products-unit__opener slide-unit__opener" href="#">YOUR RECENTLY VISITED</a>
                </h2>
                <div class="products-unit__content slide-unit__slide">
                    <div class="scroll-slider scroll-slider_products ga-home-product-container overthrow" data-gakey="Rec view">
                        @foreach($data['recently_viewed'] as $item)
                            <div class="scroll-slider__item ga-evholder">
                                @include('home.product_item', ['item' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
        @if($data['recommendation'])
            <div class="products-unit slide-unit expanded ga-home-product-section" data-gakey="Recommendation for you">
                <h2 class="products-unit__title">
                    <a class="products-unit__opener slide-unit__opener" href="#">RECOMMENDATION FOR YOU</a>
                </h2>
                <div class="products-unit__content slide-unit__slide">
                    <div class="scroll-slider scroll-slider_products ga-home-product-container overthrow" data-gakey="Recommend">
                        @foreach($data['recommendation'] as $item)
                            <div class="scroll-slider__item ga-evholder">
                                @include('home.product_item', ['item' => $item])
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        @endif
    </div>



    <div class="banner-item">
        <a href="https://play.google.com/store/apps/details?id=com.lenskart.app">
            <img src="images/banner-google-play.jpg" width="351" height="140" alt="Move to a better Faster Shopping Experience. With Lenskart Mobile App. Get it on Google play">
        </a>
    </div>
    <div class="info-tile">
        <div class="info-tile__holder">
                <span class="info-tile__item">
                    <span class="info-tile__item-holder">14 day <small>returns</small></span>
                </span>
                <span class="info-tile__item">
                    <span class="info-tile__item-holder">cash on  <small>delivery</small></span>
                </span>
                <span class="info-tile__item">
                    <span class="info-tile__item-holder">genuine <small>products</small></span>
                </span>
        </div>
    </div>
    <nav class="extra-nav">
        <ul>
            <li><a href="tel:09999899998">24x7 HELP : (0) 99-99-8 99-99-8</a></li>
            <li><a href="{{ route('login') }}">Login</a></li>
        </ul>
    </nav>
@endsection