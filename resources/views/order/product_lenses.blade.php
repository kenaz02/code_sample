@extends('home')

@section('content')
    <h2>{{ $product->getLenses()->label }}</h2>
    <ul>
    @foreach($product->getLenses()->getItems() as $item)
        <li>
            <h3>{{ $item->model_name }}</h3>
            <p>
                <img src="{{ $item->brand_image_url }}"/>
            </p>
            <p>Warranty: "{{ $item->warranty }}"</p>
            <ul>
                @foreach($item->specifications as $specItem)
                    <li>{{ $specItem }}</li>
                @endforeach
            </ul>
            <button type="submit">Buy</button>
            <hr/>
        </li>
    @endforeach
    </ul>
@endsection