@extends('home');

@section('content')
    <ul>
        <li><a href="{{ route('checkout', ['step' => 1 ]) }}"><b>Login</b></a></li>
        <li><a href="{{ route('checkout', ['step' => 2 ]) }}">Address</a></li>
        <li><a href="{{ route('checkout', ['step' => 3 ]) }}">Payment</a></li>
    </ul>

    <div class="user">
        {!! Form::model($model, ['route' => 'checkout', 'method' => 'POST']) !!}
            {!! Form::label('email', 'Email') !!}
            {!! Form::text('email', old('email')) !!}
            {!! $errors->first('email', '<span class="error">:message</span>') !!}

            {!! Form::hidden('step', 1) !!}
        <div class="form-group">
            {!! Form::submit('Continue') !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection