@extends('home');

@section('content')
    <ul>
        <li><a href="{{ route('checkout', ['step' => 1 ]) }}">Login</a></li>
        <li><a href="{{ route('checkout', ['step' => 2 ]) }}"><b>Address</b></a></li>
        <li><a href="{{ route('checkout', ['step' => 3 ]) }}">Payment</a></li>
    </ul>

    <div class="addresses">
        {!! Form::model($model, ['route' => 'checkout', 'method' => 'POST']) !!}
        <ul>
            @foreach($data as $item)
                <div class="address-item">
                    {{ $item->firstname }} {{ $item->lastname }} </br>
                    {{ $item->street0 }} </br>
                    {{ $item->street1 }} </br>
                    {{ $item->country }} {{ $item->state }} {{ $item->city }} </br>
                    T: {{ $item->telephone }}
                    {!! Form::radio('address', $item->id) !!}
                </div>
            @endforeach
        </ul>

        <label>Custom Address {!! Form::radio('address', '-1', ['checked']) !!}</label>

        <div class="form-group">
            {!! Form::label('customAddress[firstname]', 'First Name') !!}
            {!! Form::text('customAddress[firstname]', old('customAddress[firstname]'), ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[firstname]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[lastname]', 'Last Name') !!}
            {!! Form::text('customAddress[lastname]', old('customAddress[lastname]'), ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[lastname]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[street0]', 'Street One') !!}
            {!! Form::text('customAddress[street0]', old('customAddress[street0]'), ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[street0]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[street1]', 'Street Two') !!}
            {!! Form::text('customAddress[street1]', old('customAddress[street1]'), ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[street1]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[pincode]', 'Zip Code') !!}
            {!! Form::text('customAddress[pincode]', null, ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[pincode]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[telephone]', 'Telephone') !!}
            {!! Form::text('customAddress[telephone]', old('customAddress[telephone]'), ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[telephone]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[state]', 'State') !!}
            {!! Form::text('customAddress[state]', null, ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[state]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[city]', 'City') !!}
            {!! Form::text('customAddress[city]', null, ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[city]', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('customAddress[country]', 'Country') !!}
            {!! Form::text('customAddress[country]', null, ['class' => 'form-control']) !!}
            {!! $errors->first('customAddress[country]', '<span class="error">:message</span>') !!}
        </div>


        {!! Form::hidden('step', 2) !!}
        <div class="form-group">
            {!! Form::submit('Continue') !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection