@extends('home');

@section('content')
    <ul>
        <li><a href="{{ route('checkout', ['step' => 1 ]) }}">Login</a></li>
        <li><a href="{{ route('checkout', ['step' => 2 ]) }}">Address</a></li>
        <li><a href="{{ route('checkout', ['step' => 3 ]) }}"><b>Payment</b></a></li>
    </ul>

    <div class="payment">
        {!! Form::model($model, ['route' => ['checkout'], 'method' => 'POST']) !!}

            {!! Form::label('payment', 'Try Before You Buy') !!}
            {!! Form::radio('payment', 'paytbyb') !!}

            {!! Form::label('payment', 'Net Banking') !!}
            {!! Form::radio('payment', 'nb') !!}

            {!! Form::label('payment', 'Credit Card') !!}
            {!! Form::radio('payment', 'cc') !!}

            {!! Form::label('payment', 'Debit Card') !!}
            {!! Form::radio('payment', 'dc') !!}

            {!! Form::label('payment', 'Cash on Delivery') !!}
            {!! Form::radio('payment', 'cod') !!}

            {!! Form::label('payment', 'Bank Transfer') !!}
            {!! Form::radio('payment', 'bt') !!}

        <div class="form-group">
            {!! Form::submit('Place Order') !!}
        </div>
        {!! Form::close() !!}
    </div>
@endsection