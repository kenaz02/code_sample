@extends('home')

@section('body-class', 'cart-page')

@section('content')
    @if($cart->items)
        <div class="logo-panel">
            <div class="logo-panel__logo">
                <a href="/">
                    <img src="images/logo-02.png" width="152" height="16" alt="Lenskart.com">
                </a>
            </div>
        </div>
        <div class="cart-block cart-block_fill">
            <div class="cart-heading">
                <a href="{{ URL::previous() }}" class="back-link back-link_type_empty"></a>
                <h1 class="cart-heading__title">My Cart</h1>
            </div>
            <div class="order-section">

                @if($errors->has('cart:quantity:submit'))
                    <div class="alert-unit alert-unit_danger">
                        <div class="alert-unit__text">
                            {!! $errors->first('cart:quantity:submit', '<p>:message</p>') !!}
                        </div>
                        <ul class="alert-unit__list">
                            {!! $errors->first('cart:quantity:product', '<li>:message</li>') !!}
                        </ul>
                    </div>
                @endif

                @if(\Session::has('cart:remove:submit'))
                    <div class="alert-unit alert-unit_removed">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ \Session::get('cart:remove:submit') }}" Removed</strong>
                        </div>
                    </div>
                @endif

                @if(\Session::has('cart:quantity:updated'))
                    <div class="alert-unit alert-unit_success">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ str_limit(\Session::get('cart:quantity:updated'), 32) }}" product quantity has been updated in the cart.</strong>
                        </div>
                    </div>
                @endif

                @if(\Session::has('cart:adding:success'))
                    <div class="alert-unit alert-unit_success">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ str_limit(\Session::get('cart:adding:success'), 32) }}" has been added.</strong>
                        </div>
                    </div>
                @endif

                <div class="order-box">
                    <div class="order-box__heading">
                        <strong class="order-box__title">Order Total</strong>
                        <div class="order-box__price-info">
                            <div class="price-box">
                                <span class="price">Rs. {{ $cart->total }}</span>
                            </div>
                            <span class="order-box__price-note">(You Save Rs. {{ $cart->discount }})</span>
                        </div>
                    </div>
                    <div class="order-info-holder">
                        <div class="order-info">
                            <dl class="order-info__item">
                                <dt>Tax Collected</dt>
                                <dd>Rs. {{ $cart->tax_collected }}</dd>
                            </dl>
                            <dl class="order-info__item">
                                <dt>Shipping Charges</dt>
                                <dd>
                                @if($cart->shipping_charges->price == 0)
                                    Free
                                @else
                                    Rs. {{ $cart->shipping_charges }}
                                @endif
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>

                @foreach($cart->items as $product)
                    <div class="order-box order-item">
                        <div id="p_{{ $product->id }}" class="order-item__heading">
                            <div class="order-item__heading-holder">
                                <div class="order-item__image">
                                    <img src="{{ $product->images }}" width="108" height="42" alt="product image">
                                </div>
                                <div class="order-item__title-holder">
                                    <div class="order-item__title-text">
                                        <strong class="order-item__title">
                                            <a href="#">{{ $product->full_name }}</a>
                                        </strong>
                                    </div>
                                    <div class="order-control">
                                        <div class="loader-box">
                                            @if($product->type != \App\Models\Product::TYPE_CONTACT_LENS)
                                            <span class="order-control__ttl">Qty</span>
                                            <div class="counter-item">
                                                {!! Form::open(['route' => 'quantityCart', 'method' => 'POST']) !!}
                                                    <button class="counter-item__btn counter-item__decrease btn-show-loader" type="submit">-</button>
                                                    <span class="loader"></span>
                                                    <input class="counter-item__field" type="number" name="quantity" value="{{ $product->quantity }}">
                                                    <input type="hidden" name="id" value="{{ $product->id }}">
                                                    <input type="hidden" name="full_name" value="{{ $product->full_name }}">
                                                    <button class="counter-item__btn counter-item__increase btn-show-loader" type="submit">+</button>
                                                    <span class="loader"></span>
                                                {!! Form::close() !!}
                                            </div>
                                            @endif
                                            {!! Form::open(['route' => 'removeFromCart', 'method' => 'POST']) !!}
                                                <input type="hidden" name="id" value="{{ $product->id }}">
                                                <input type="hidden" name="full_name" value="{{ $product->full_name }}">
                                                <button class="btn-trash btn-show-loader" type="submit"></button>
                                                <span class="loader"></span>
                                            {!! Form::close() !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="order-item__content">
                            <div class="price-unit">
                                <div class="terms-unit">
                                    <div class="term term_first">
                                        <div class="term__title">
                                            <strong class="term__ttl">
                                                @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                                                    Product Price
                                                @else
                                                    Frame Price
                                                @endif
                                            </strong>
                                        </div>
                                        @if($product->marketPrice->price != $product->lenskartPrice->price)
                                            <div class="price-box price-box_type_old">
                                                <span class="currency">Rs.</span>
                                                <span class="price">{{ $product->marketPrice }}</span>
                                            </div>
                                        @endif
                                        <div class="price-box">
                                            <span class="currency">Rs.</span>
                                            <span class="price">{{ $product->lenskartPrice }}</span>
                                        </div>
                                    </div>
                                    @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                                        <div class="term-details">
                                            <div class="term-details__holder">
                                                <strong class="term-details__ttl">DETAILS:</strong>
                                                <ul class="term-details__list">
                                                    @if($product->options->hasLeftOptions)<li>{{ preg_replace('/^[0-9 ]+/', '$0 Left ', $product->options->getLeftBoxes()->value) }}</li>@endif
                                                    @if($product->options->hasRightOptions)<li>{{ preg_replace('/^[0-9 ]+/', '$0 Right ', $product->options->getRightBoxes()->value) }}</li>@endif
                                                </ul>
                                            </div>
                                        </div>
                                    @endif
                                    @if(($product->type == \App\Models\Product::TYPE_EYE_GLASSES || $product->type == \App\Models\Product::TYPE_POWERED_SUN_GLASSES) && $product->hasOptions())
                                    <span class="terms-unit__plus">+</span>
                                    <div class="term term_second">
                                        <div class="term__title">
                                            <strong class="term__ttl">Lens Package:</strong>
                                            <p>{{ $product->options->model_name }}</p>
                                        </div>
                                        @if($product->options->marketPrice->price != $product->options->lenskartPrice->price)
                                            <div class="price-box price-box_type_old">
                                                <span class="currency">Rs.</span>
                                                <span class="price">{{ $product->options->marketPrice }}</span>
                                            </div>
                                        @endif
                                        <div class="price-box">
                                            <span class="currency">Rs.</span>
                                            <span class="price">{{ $product->options->lenskartPrice }}</span>
                                        </div>
                                    </div>
                                    @endif
                                </div>
                                <div class="price-total">
                                    <span class="price-total__equal">=</span>
                                    <strong class="price-total__ttl">subtotal</strong>
                                    <div class="price-box">
                                        <span class="currency">Rs.</span>
                                        <span class="price">{{ $product->totalPrice }}</span>
                                    </div>
                                </div>
                            </div>
                            @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                            <div class="details-unit">
                                <div class="details-unit__heading">
                                    <a href="#" class="details-unit__opener">Details</a>
                                </div>
                                <div class="details-unit__slide">
                                    <div class="details-unit__content">
                                        <table class="details-table">
                                            <thead>
                                            <tr>
                                                <th></th>
                                                @if($product->options->hasRightOptions)
                                                    <th>right eye (od)</th>
                                                @endif
                                                @if($product->options->hasLeftOptions)
                                                    <th>left eye (os)</th>
                                                @endif
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($product->options->getAdditionalOptions() as $key => $item)
                                                <tr>
                                                    <th>{{ $key }}</th>
                                                    @if($product->options->hasRightOptions)
                                                        <td>{{ $product->options->getRightOption($key)->value }}</td>
                                                    @endif
                                                    @if($product->options->hasLeftOptions)
                                                        <td>{{ $product->options->getLeftOption($key)->value }}</td>
                                                    @endif
                                                </tr>
                                            @endforeach
                                            {{--<tr>--}}
                                                {{--<th>Rx</th>--}}
                                                {{--<td>Image Uploaded</td>--}}
                                                {{--<td>Image Uploaded</td>--}}
                                            {{--</tr>--}}
                                            <tr>
                                                <th>Price</th>
                                                @if($product->options->hasRightOptions)
                                                    <td>Rs. {{ $product->options->getRightBoxes()->original_price }}</td>
                                                @endif
                                                @if($product->options->hasLeftOptions)
                                                    <td>Rs. {{ $product->options->getLeftBoxes()->original_price  }}</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>Boxes</th>
                                                @if($product->options->hasRightOptions)
                                                    <td>{{ $product->options->getRightBoxes()->value }}</td>
                                                @endif
                                                @if($product->options->hasLeftOptions)
                                                    <td>{{ $product->options->getLeftBoxes()->value }}</td>
                                                @endif
                                            </tr>
                                            <tr>
                                                <th>Total</th>
                                                @if($product->options->hasRightOptions)
                                                    <td>Rs. {{ $product->options->getRightBoxes()->grand_total }}</td>
                                                @endif
                                                @if($product->options->hasLeftOptions)
                                                    <td>Rs. {{ $product->options->getLeftBoxes()->grand_total  }}</td>
                                                @endif
                                            </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            @endif
                            <em class="date-item">
                                Estimated Dispatch Date:
                                <time datetime="2015-07-31">31st July 2015</time>
                            </em>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="btn-panel btn-panel_sticky">
            <div class="sticky-item">
                <div class="loader-box">
                    <a href="{{ route('checkout') }}" class="btn-countinue btn-countinue_size_big btn-show-loader">Proceed to checkout</a>
                    <span class="loader"></span>
                </div>
            </div>
        </div>
    @else
        <div class="logo-panel">
            <div class="logo-panel__logo">
                <a href="#">
                    <img src="/images/logo-02.png" width="152" height="16" alt="Lenskart.com">
                </a>
            </div>
        </div>
        <div class="cart-block">
            <div class="cart-heading">
                <a href="{{ URL::previous() }}" class="back-link back-link_type_empty"></a>
                <h1 class="cart-heading__title">My Cart</h1>
            </div>
					<span class="goods-number">
						<span class="goods-number__number-wrap">
							<span class="goods-number__number">0</span>
						</span>
					</span>
            <div class="cart-text">
                <h2 class="cart-text__title">YOUR SHOPPING CART IS EMPTY</h2>
                <p>Shop for products and add items to the cart</p>
            </div>
            <div class="cart-block__btn-row">
                <a href="/" class="btn-countinue">CONTINUE SHOPPING</a>
            </div>
        </div>
        <nav class="search-nav">
            <strong class="search-nav__title">Search for</strong>
            <ul class="search-nav__list">
                <li class="search-nav__item">
                    <a href="/eyeglasses.html" class="search-nav__link">
                        <img class="search-nav__image" src="/images/img-search-01.png" width="73" height="73" alt="Eyeglasses">
                        <strong class="search-nav__ttl">Eyeglasses</strong>
                    </a>
                </li>
                <li class="search-nav__item">
                    <a href="/sunglasses.html" class="search-nav__link">
                        <img class="search-nav__image" src="/images/img-search-02.png" width="73" height="73" alt="Sunglasses">
                        <strong class="search-nav__ttl">Sunglasses</strong>
                    </a>
                </li>
                <li class="search-nav__item">
                    <a href="/contact-lenses.html" class="search-nav__link">
                        <img class="search-nav__image" src="/images/img-search-03.png" width="73" height="73" alt="Contact Lenses">
                        <strong class="search-nav__ttl">Contact Lenses</strong>
                    </a>
                </li>
            </ul>
        </nav>
    @endif

@endsection

@yield('sticky-footer', ' ')
