@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    @if($cart->items)
        <div class="cart-heading">
            @include('back_link')
            <h1 class="cart-heading__title">Free home Trial Cart</h1>
        </div>

        <div class="trial-cart">
            <div class="trial-cart__heading">
                {!! Form::open(['route' => 'cleanCart', 'method' => 'POST']) !!}
                    <button type="submit" class="btn-trash">Delete</button>
                {!! Form::close() !!}
                <strong class="trial-cart__ttl">You have <span class="trial-cart__qty">{{ $cart->getCount() }} items</span></strong>
            </div>

            <div class="trial-cart__cart">
                <ol class="trial-list">

                    @foreach($cart->items as $product)
                        <li class="trial-list__item">
                            <div class="trial-list__image">
                                <a href="/{{ $product->url }}" title="{{ $product->full_name }}">
                                    <img src="{{ $product->images }}" width="91" height="39" alt="product image">
                                </a>
                            </div>

                            <div class="trial-list__text">
                                <strong class="trial-list__ttl">{{ $product->brand_name }}</strong>
                                <p>{{ $product->full_name }}</p>
                                @if($product->is_surprise_me)
                                    <a href="{{ route('selectForMe', ['id' => $product->product_id ]) }}" class="again-link">Select again</a>
                                @endif
                            </div>

                            {!! Form::open(['route' => 'removeFromCart', 'method' => 'POST']) !!}
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="full_name" value="{{ $product->full_name }}">
                                <button class="trial-list__delete" type="submit">Delete</button>
                                <span class="loader"></span>
                            {!! Form::close() !!}
                        </li>
                    @endforeach


                    @for($i = 1; $i <= $cart->getAvailableSlotsCount(); $i++)
                        <li class="trial-list__item trial-list__item_empty">
                            <span class="trial-list__number-cell">
                                <span class="trial-list__number">{{ 5 - $cart->getAvailableSlotsCount() + $i }}.</span>
                            </span>
                            <div class="trial-list__add-holder">
                                <ul class="trial-add-list">
                                    <li class="trial-add-list__item trial-add-list__item_sel trial-add-list__item_js-popup">
                                        <div class="trial-add-list__text">
                                            <a href="{{ route('selectForMe') }}" class="trial-add-list__link">Select for me</a>
                                        </div>

                                        <div class="trial-info">
                                            <a href="#" class="trial-add-list__help-link">Help</a>

                                            <div class="trial-popup">
                                                <button type="button" class="trial-popup__close">Close</button>
                                                <p>We will send you another frame recommended from our style experts</p>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="trial-add-list__item trial-add-list__item_add">
                                        <div class="trial-add-list__text">
                                            <a href="{{ route('offer', ['id' => 4289]) }}" class="trial-add-list__link">Add From catalog</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </li>
                    @endfor
                </ol>

                <div class="trial-buttons">
                    @if($cart->getAvailableSlotsCount() > 0)
                       <div class="trial-buttons__popup">
                            <div class="trial-buttons__note">
                               <p>We recommend trying at least {{ $cart->getAvailableSlotsCount() }} products to find your perfect frame</p>
                               <div class="trial-note-buttons">
                                   <a href="#" class="trial-note-buttons__btn trial-note-buttons__btn_no">No Thanks</a>
                                   <a href="{{ route('offer', ['id' => 4289]) }}" class="trial-note-buttons__btn trial-note-buttons__btn_add">Add more</a>
                               </div>
                           </div>
                       </div>
                    @endif
                    <a href="{{ route('checkout') }}" class="btn-save @if($cart->getAvailableSlotsCount() >= 2) btn-trial_popup @endif btn-trial">Checkout</a>
                    <p><a href="#">Learn how FREE HOME TRIAL works</a></p>
                </div>

            </div>
        </div>
    @else
        @include('cart.empty')
    @endif

@endsection

@yield('sticky-footer', ' ')
