@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    <div class="cart-block cart-block_fill">
        <div class="cart-heading">
            @include('back_link')
            <h1 class="cart-heading__title">My Cart</h1>
        </div>

        <div class="order-box cart-alert">
            <div class="cart-alert__text">
                <h1 class="cart-alert__title">You cannot combine a purchase with a 'free home trial'.</h1>
                <p>Complete your purchase <br> or continue with free home trial order.</p>
            </div>

            <div class="cart-alert__buttons">
                <a href="{{ URL::previous() }}" class="cart-alert__link">Back to free home trial</a>
                {!! Form::open(['route' => 'cartConflict', 'method' => 'POST', 'id' => 'buy-form']) !!}
                    <div class="product-buttons__item">
                        <button class="btn-product">
                            <span class="btn-product__wrap">Add to cart</span>
                        </button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection