<div class="order-box order-item">
    <div id="p_{{ $product->id }}" class="order-item__heading">
        <div class="order-item__heading-holder">
            <div class="order-item__image">
                <a href="{{ $product->product_url }}">
                    <img src="{{ $product->images }}" width="108" height="42" alt="{{ $product->full_name }}">
                </a>
            </div>
            <div class="order-item__title-holder">
                <div class="order-item__title-text">
                    <strong class="order-item__title">
                        <a href="{{ $product->product_url }}" title="{{ $product->full_name }}">{{ $product->full_name }}</a>
                    </strong>
                    @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                        <span class="order-item__title-label">for {{ $product->options->getUserName() }}</span>
                    @endif
                </div>
                <div class="order-control">
                    <div class="loader-box">
                        @if($product->type != \App\Models\Product::TYPE_CONTACT_LENS)
                            <span class="order-control__ttl">Qty</span>
                            <div class="counter-item">
                                {!! Form::open(['route' => 'quantityCart', 'method' => 'POST']) !!}
                                @if($product->quantity > 1)
                                    <button class="counter-item__btn counter-item__decrease btn-show-loader" type="submit">-</button>
                                    <span class="loader"></span>
                                @endif
                                <input class="counter-item__field" type="number" name="quantity" value="{{ $product->quantity }}">
                                <input type="hidden" name="id" value="{{ $product->id }}">
                                <input type="hidden" name="full_name" value="{{ $product->full_name }}">
                                @if($product->quantity <= $product->available_quantity)
                                    <button class="counter-item__btn counter-item__increase btn-show-loader" type="submit">+</button>
                                    <span class="loader"></span>
                                @endif
                                {!! Form::close() !!}
                            </div>
                        @endif
                        {!! Form::open(['route' => 'removeFromCart', 'method' => 'POST']) !!}
                        <input type="hidden" name="id" value="{{ $product->id }}">
                        <input type="hidden" name="full_name" value="{{ $product->full_name }}">
                        <button class="btn-trash btn-show-loader" type="submit"></button>
                        <span class="loader"></span>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="order-item__content">
        <div class="price-unit">
            <div class="terms-unit">
                <div class="term term_first">
                    <div class="term__title">
                        <strong class="term__ttl">
                            @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                                Product Price
                            @else
                                Frame Price
                            @endif
                        </strong>
                    </div>
                    @if($product->marketPrice->price != $product->lenskartPrice->price)
                        <div class="price-box price-box_type_old">
                            <span class="currency">Rs.</span>
                            <span class="price">{{ $product->marketPrice }}</span>
                        </div>
                    @endif
                    <div class="price-box">
                        <span class="currency">Rs.</span>
                        <span class="price">{{ $product->lenskartPrice }}</span>
                    </div>
                </div>
                @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
                    <div class="term-details">
                        <div class="term-details__holder">
                            <strong class="term-details__ttl">DETAILS:</strong>
                            <ul class="term-details__list">
                                @if($product->options->hasLeft)<li>{{ preg_replace('/^[0-9 ]+/', '$0 Left ', $product->options->getLeftBoxes()->value) }}</li>@endif
                                @if($product->options->hasRight)<li>{{ preg_replace('/^[0-9 ]+/', '$0 Right ', $product->options->getRightBoxes()->value) }}</li>@endif
                            </ul>
                        </div>
                    </div>
                @endif
                @if(($product->type == \App\Models\Product::TYPE_EYE_GLASSES || $product->type == \App\Models\Product::TYPE_POWERED_SUN_GLASSES) && $product->hasOptions())
                    <span class="terms-unit__plus">+</span>
                    <div class="term term_second">
                        <div class="term__title">
                            <strong class="term__ttl">Lens Package:</strong>
                            <p>{{ $product->options->model_name }}</p>
                        </div>
                        @if($product->options->marketPrice->price != $product->options->lenskartPrice->price)
                            <div class="price-box price-box_type_old">
                                <span class="currency">Rs.</span>
                                <span class="price">{{ $product->options->marketPrice }}</span>
                            </div>
                        @endif
                        <div class="price-box">
                            <span class="currency">Rs.</span>
                            <span class="price">{{ $product->options->lenskartPrice }}</span>
                        </div>
                    </div>
                @endif
            </div>
            <div class="price-total">
                <span class="price-total__equal">=</span>
                <strong class="price-total__ttl">subtotal</strong>
                <div class="price-box">
                    <span class="currency">Rs.</span>
                    <span class="price">{{ $product->totalPrice }}</span>
                </div>
            </div>
        </div>
        @if($product->type == \App\Models\Product::TYPE_CONTACT_LENS)
            <div class="details-unit">
                <div class="details-unit__heading">
                    <a href="#" class="details-unit__opener">Details</a>
                </div>
                <div class="details-unit__slide">
                    <div class="details-unit__content">
                        <table class="details-table">
                            <thead>
                            <tr>
                                <th></th>
                                @if($product->options->hasRight)
                                    <th>right eye (od)</th>
                                @endif
                                @if($product->options->hasLeft)
                                    <th>left eye (os)</th>
                                @endif
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($product->options->getOptions() as $key => $item)
                                <tr>
                                    <th>{{ $key }}</th>
                                    @if($product->options->hasRight)
                                        <td>{{ $item[\App\Models\Cart\Product\BuyOptions\ContactLensesOption::SIDE_RIGHT]->value }}</td>
                                    @endif
                                    @if($product->options->hasLeft)
                                        <td>{{ $item[\App\Models\Cart\Product\BuyOptions\ContactLensesOption::SIDE_LEFT]->value }}</td>
                                    @endif
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
        <em class="date-item">
            Estimated Dispatch Date:
            <time datetime="2015-07-31">31st July 2015</time>
        </em>
    </div>
</div>