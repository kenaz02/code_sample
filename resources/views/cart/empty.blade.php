<div class="cart-block">
    <div class="cart-heading">
        @include('back_link')
        <h1 class="cart-heading__title">My Cart</h1>
    </div>
					<span class="goods-number">
						<span class="goods-number__number-wrap">
							<span class="goods-number__number">0</span>
						</span>
					</span>
    <div class="cart-text">
        <h2 class="cart-text__title">YOUR SHOPPING CART IS EMPTY</h2>
        <p>Shop for products and add items to the cart</p>
    </div>
    <div class="cart-block__btn-row">
        <a href="/" class="btn-countinue">CONTINUE SHOPPING</a>
    </div>
</div>
<nav class="search-nav">
    <strong class="search-nav__title">Search for</strong>
    <ul class="search-nav__list">
        <li class="search-nav__item">
            <a href="/eyeglasses.html" class="search-nav__link">
                <img class="search-nav__image" src="/images/img-search-01.png" width="73" height="73" alt="Eyeglasses">
                <strong class="search-nav__ttl">Eyeglasses</strong>
            </a>
        </li>
        <li class="search-nav__item">
            <a href="/sunglasses.html" class="search-nav__link">
                <img class="search-nav__image" src="/images/img-search-02.png" width="73" height="73" alt="Sunglasses">
                <strong class="search-nav__ttl">Sunglasses</strong>
            </a>
        </li>
        <li class="search-nav__item">
            <a href="/contact-lenses.html" class="search-nav__link">
                <img class="search-nav__image" src="/images/img-search-03.png" width="73" height="73" alt="Contact Lenses">
                <strong class="search-nav__ttl">Contact Lenses</strong>
            </a>
        </li>
    </ul>
</nav>