@extends('app_mini')

@section('body-class', 'cart-page')

@section('content')
    @if($cart->items)
        <div class="cart-block cart-block_fill">
            <div class="cart-heading">
                @include('back_link')
                <h1 class="cart-heading__title">My Cart</h1>
            </div>
            <div class="order-section">

                @if($errors->has('cart:quantity:submit'))
                    <div class="alert-unit alert-unit_danger" data-remove-seconds="3">
                        <div class="alert-unit__text">
                            {!! $errors->first('cart:quantity:submit', '<p>:message</p>') !!}
                        </div>
                        <ul class="alert-unit__list">
                            {!! $errors->first('cart:quantity:product', '<li>:message</li>') !!}
                        </ul>
                    </div>
                @endif

                @if(\Session::has('cart:remove:submit'))
                    <div class="alert-unit alert-unit_removed" data-remove-seconds="3">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ \Session::get('cart:remove:submit') }}" Removed</strong>
                        </div>
                    </div>
                @endif

                @if(\Session::has('cart:quantity:updated'))
                    <div class="alert-unit alert-unit_success" data-remove-seconds="3">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ str_limit(\Session::get('cart:quantity:updated'), 32) }}" product quantity has been updated in the cart.</strong>
                        </div>
                    </div>
                @endif

                @if(\Session::has('cart:adding:success'))
                    <div class="alert-unit alert-unit_success" data-remove-seconds="3">
                        <div class="alert-unit__text">
                            <strong class="alert-unit__ttl">"{{ str_limit(\Session::get('cart:adding:success'), 32) }}" has been added.</strong>
                        </div>
                    </div>
                @endif

                <div class="order-box">
                    <div class="order-box__heading">
                        <strong class="order-box__title">Order Total</strong>
                        <div class="order-box__price-info">
                            <div class="price-box">
                                <span class="price">Rs. {{ $cart->total }}</span>
                            </div>
                            <span class="order-box__price-note">(You Save Rs. {{ $cart->discount }})</span>
                        </div>
                    </div>
                    <div class="order-info-holder">
                        <div class="order-info">
                            <dl class="order-info__item">
                                <dt>Tax Collected</dt>
                                <dd>Rs. {{ $cart->tax_collected }}</dd>
                            </dl>
                            <dl class="order-info__item">
                                <dt>Shipping Charges</dt>
                                <dd>
                                    @if($cart->shipping_charges->price == 0)
                                        Free
                                    @else
                                        Rs. {{ $cart->shipping_charges }}
                                    @endif
                                </dd>
                            </dl>
                        </div>
                    </div>
                </div>

                @foreach($cart->items as $product)
                    @include('cart.normal_product_item', ['product' => $product])
                @endforeach
            </div>
        </div>
        <div class="btn-panel btn-panel_sticky">
            <div class="sticky-item">
                <div class="loader-box">
                    <a href="{{ route('checkout') }}" class="btn-countinue btn-countinue_size_big btn-show-loader">Proceed to checkout</a>
                    <span class="loader"></span>
                </div>
            </div>
        </div>
    @else
       @include('cart.empty')
    @endif

@endsection

@yield('sticky-footer', ' ')
