<div class="row">
    <div class="col-lg-12">
        <ul>
            <li><a href="">Account Dashboard</a></li>
            <li><a href="">Account Information</a></li>
            <li><a href="{{ route('addresses') }}">Address Book</a></li>
            <li><a href="">My Prescriptions</a></li>
            <li><a href="{{ route('orders') }}">My Orders</a></li>
            <li><a href="">My Product Reviews</a></li>
            <li><a href="">Newsletter Subscriptions</a></li>
            <li><a href="">My Invitations</a></li>
            <li><a href="">My Reward Points</a></li>
            <li><a href="">Check Gift Voucher Balance</a></li>
            <li><a href="">Store Credit</a></li>
        </ul>
    </div>
</div>
