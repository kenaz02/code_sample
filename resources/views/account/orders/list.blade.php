@extends('app')

@section('body-class', 'my-order-page')

@section('content')
    <div class="my-orders-section">
        <div class="my-orders-heading">
            <a href="{{ \URL::previous() }}" class="back-link back-link_type_empty"></a>
            <h1 class="my-orders-heading__title">My orders</h1>
        </div>

        <div class="my-orders-section__content">
            @if($orders)
                @foreach($orders as $item)
                    <div class="my-orders-box">
                        <div class="my-order-info">
                            <div class="my-order-info__additional">
                                <a href="{{ route('orderItem', $item->id) }}" class="btn-order">View Details</a>
                            </div>

                            <div class="my-order-info__info">
                                <div class="spec-list">
                                    <dl class="spec-list__item">
                                        <dt class="spec-list__term">Order Id :</dt>
                                        <dd class="spec-list__desc">{{ $item->id }}</dd>
                                    </dl>
                                    <dl class="spec-list__item">
                                        <dt class="spec-list__term">Oder Placed:</dt>
                                        <dd class="spec-list__desc">{{ \Helper::formatDate($item->order_date, 'd F Y', 'Y-m-d H:i:s') }}</dd>
                                    </dl>
                                    <dl class="spec-list__item">
                                        <dt class="spec-list__term">Order Total:</dt>
                                        <dd class="spec-list__desc">Rs. {{ $item->cost }}</dd>
                                    </dl>
                                </div>
                            </div>
                        </div>

                        <div class="order-unit order-unit_single">
                            <div class="order-unit__image">
                                <div class="order-unit__image-holder">
                                    <img src="{{ $item->items[0]->images[0] }}" width="162" height="76" alt="order image">
                                </div>
                            </div>

                            <div class="order-unit__info">
                                <h2 class="order-unit__title">{{ $item->items[0]->brand_name }} </h2>
                                <p>{{ $item->items[0]->model_name }}</p>
                                <p>eyeglasses</p>
                            </div>
                        </div>
                    </div>

                @endforeach

            {{--<div class="order-btn-row order-btn-row_load">--}}
                {{--<a href="#" class="btn-load">Load more</a>--}}
            {{--</div>--}}
            @else
                <div class="my-orders-box">
                    <div class="no-order">
                        <p>You haven’t ordered any item.</p>
                        <a href="/" class="btn-order">shop now</a>
                        <span class="no-order__or">Or</span>
                        <a href="{{ Helper::backLink() }}" class="btn-order btn-order_cancel">Go back</a>
                    </div>
                </div>
            @endif
        </div>
    </div>
@endsection