


@extends('app')

@section('body-class', 'my-order-page')

@section('content')
    <div class="my-orders-section">
        <div class="my-orders-heading">
            <a href="{{ Helper::backLink(route('orders')) }}" class="back-link back-link_type_empty"></a>
            <h1 class="my-orders-heading__title">Orders: {{ $order->id }}</h1>
        </div>

        @if($order)
            <div class="status-panel">
                <p>Status: {{ $order->status }}</p>
            </div>

            <div class="order-btn-row">
                <a href="#" class="btn-order">Track order</a>
            </div>

            <div class="my-orders-section__content">
                <div class="my-orders-box">
                    <div class="my-order-info">
                        <div class="my-order-info__info">
                            <div class="spec-list">
                                <dl class="spec-list__item">
                                    <dt class="spec-list__term">Order Id :</dt>
                                    <dd class="spec-list__desc">{{ $order->id }}</dd>
                                </dl>
                                <dl class="spec-list__item">
                                    <dt class="spec-list__term">Oder Placed:</dt>
                                    <dd class="spec-list__desc">{{ \Helper::formatDate($order->order_date, 'd F Y', 'Y-m-d H:i:s') }}</dd>
                                </dl>
                            </div>
                        </div>
                    </div>

                    <div class="my-orders-list">
                        @foreach($order->items as $item)
                            <div class="order-unit">
                                <div class="order-unit__image">
                                    <div class="order-unit__image-holder">
                                        <img src="{{ $item->images[0] }}" width="162" height="76" alt="order image">
                                    </div>
                                </div>

                                <div class="order-unit__info">
                                    <h2 class="order-unit__title">{{ $item->brand_name }} </h2>
                                    <p>{{ $item->model_name }}</p>
                                    {{--<p>eyeglasses</p>--}}
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>

                <div class="my-orders-box">
                    <div class="order-info-unit">
                        <h2 class="order-info-unit__title">Shipping address</h2>

                        <address class="order-info-unit__address">
                            <span class="order-info-unit__address-item">{{ $order->address->firstname }} {{ $order->address->lastname }}</span>
                            <span class="order-info-unit__address-item">{{ $order->address->street0 }}</span>
                            @if($order->address->street1)<span class="order-info-unit__address-item">{{ $order->address->street1 }}</span>@endif
                            <span class="order-info-unit__address-item">{{ ucwords(strtolower($order->address->state)) }}, {{ $order->address->city }} {{ $order->address->pincode }}</span>
                        </address>
                    </div>
                </div>

                {{--<div class="my-orders-box">--}}
                    {{--<div class="order-info-unit">--}}
                        {{--<h2 class="order-info-unit__title">Billing address</h2>--}}

                        {{--<address class="order-info-unit__address">--}}
                            {{--<span class="order-info-unit__address-item">Harsh Singh</span>--}}
                            {{--<span class="order-info-unit__address-item">Gautam Road</span>--}}
                            {{--<span class="order-info-unit__address-item">7th Floor Vatika Building</span>--}}
                            {{--<span class="order-info-unit__address-item">Faridabad, Haryana 11001</span>--}}
                        {{--</address>--}}
                    {{--</div>--}}
                {{--</div>--}}

                <div class="my-orders-box">
                    <div class="order-info-unit">
                        <h2 class="order-info-unit__title">Order total</h2>

                        <div class="total-list">
                            <dl class="total-list__item">
                                <dt class="total-list__term">Total before VAT:</dt>
                                <dd class="total-list__desc">Rs. 1299</dd>
                            </dl>

                            <dl class="total-list__item">
                                <dt class="total-list__term">Discount</dt>
                                <dd class="total-list__desc">Rs. 1299</dd>
                            </dl>

                            <dl class="total-list__item">
                                <dt class="total-list__term">Tax</dt>
                                <dd class="total-list__desc">Rs. 1299</dd>
                            </dl>

                            <dl class="total-list__item total-list__item-total">
                                <dt class="total-list__term">Order total</dt>
                                <dd class="total-list__desc">Rs. {{ $order->cost  }}</dd>
                            </dl>
                        </div>
                    </div>
                </div>

                <div class="order-btn-row">
                    <a href="#" class="btn-order btn-order_cancel">Cancel order</a>
                    <a href="#" class="btn-order">Reorder</a>
                </div>
            </div>
        @else
            <div class="my-orders-section__content">
                <div class="my-orders-box">
                    <div class="no-order">
                        <p>You haven’t ordered any item.</p>
                        <a href="/" class="btn-order">shop now</a>
                        <span class="no-order__or">Or</span>
                        <a href="#" class="btn-order btn-order_cancel">Go back</a>
                    </div>
                </div>
            </div>
        @endif
    </div>
@endsection

