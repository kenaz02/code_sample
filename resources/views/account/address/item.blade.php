@extends('app')

@section('body-class', 'my-order-page')

@section('content')
    <div class="my-orders-section">
        <div class="my-orders-heading">
            <a href="{{ route('checkout', ['stage' => 'shipping']) }}" class="back-link back-link_type_empty"></a>
            <h1 class="my-orders-heading__title">Edit My Address</h1>
        </div>

        {!! $errors->first('submit', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}

        {!! Form::model($address, ['route' => 'addressSave', 'method' => 'POST']) !!}

            <div class="form-row input-row @if($errors->has('firstname')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-first-name" data-label-text="First name"><span class="input-row__label-text">*First name</span></label>
                    {!! $errors->first('firstname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                </div>
                <div class="form-field">
                    {!! Form::text('firstname', old('firstname') ? old('firstname') : $address->firstname, ['class' => 'input-text required', 'id' => 'input-first-name']) !!}
                </div>
            </div>

            <div class="form-row input-row @if($errors->has('lastname')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-last-name" data-label-text="Last name"><span class="input-row__label-text">*Last name</span></label>
                    {!! $errors->first('lastname', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                </div>
                <div class="form-field">
                    {!! Form::text('lastname',old('lastname') ? old('lastname') : $address->lastname , ['class' => 'input-text required', 'id' => 'input-last-name']) !!}
                </div>
            </div>

            <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-mobilephone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxx xxx xxxx</span></label>
                    {!! $errors->first('telephone', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                </div>
                <div class="form-field">
                    <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $address->telephone }}@endif"/>
                    <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $address->telephone }}@endif">
                </div>
            </div>

            <div id="row-pincode" class="form-row input-row @if($errors->has('pincode')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-pincode" data-label-text="Pincode"><span class="input-row__label-text">*Pincode: xxxxxxx</span></label>
                    <span id="error-message-pincode" class="error-message">{!! $errors->first('pincode', ':message') !!}</span>
                </div>

                <div class="form-field">
                    {!! Form::input('number', 'pincode', old('pincode') ? old('pincode') : $address->pincode, ['class' => 'input-text required', 'id' => 'input-pincode', 'pattern' => '\d*']) !!}
                </div>
            </div>

            <div class="form-row input-row @if($errors->has('street0')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-address" data-label-text="Address"><span class="input-row__label-text">*Address: xx, street</span></label>
                    {!! $errors->first('street0', '<span class="error error-message"><span class="error-message__holder">:message</span></span>') !!}
                </div>
                <div class="form-field">
                    {!! Form::text('street0', old('street0') ? old('street0') : $address->street0, ['class' => 'input-text required', 'id' => 'input-address']) !!}
                </div>
            </div>

            <div class="form-row input-row @if($errors->has('city')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="input-city" data-label-text="City / district"><span class="input-row__label-text">*City / District</span></label>
                    {!! $errors->first('city', '<span class="error-message">:message</span>') !!}
                </div>

                <div class="form-field">
                    {!! Form::text('city', old('city') ? old('city') : $address->city , ['class' => 'input-text required', 'id' => 'input-city']) !!}
                </div>
            </div>

            <div class="form-row input-row @if($errors->has('locality')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="select-town" data-label-text="Locality/town"><span class="input-row__label-text">*Locality / Town</span></label>
                    {!! $errors->first('locality', '<span class="error-message">:message</span>') !!}
                </div>

                <select id="select-locality" name="locality">
                    <option value=""></option>
                    {{--@foreach($form->localityList as $item)--}}
                    {{--<option value="{{ $item }}" @if($form->locality == $item || old('locality') == $item) selected @endif>{{ $item }}</option>--}}
                    {{--@endforeach--}}
                </select>
            </div>

            <div class="form-row input-row @if($errors->has('state')) error @endif">
                <div class="input-row__label-holder">
                    <label class="input-row__label" for="select-state" data-label-text="state / province"><span class="input-row__label-text">*State / Province</span></label>
                    <span class="error-message">Select a state / privince</span>
                    {!! $errors->first('state', '<span class="error-message">:message</span>') !!}
                </div>

                <div class="form-field">
                    <div class="select-unit">
                        <div class="select-unit__holder">
                            <select class="effect-select" id="select-state" name="state">
                                <option>State / province</option>
                                @foreach($states as $item)
                                    <option value="{{ $item->code }}" @if($item->code == old('state') || $item->code == $address->state) selected @endif>{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            {!! Form::label('default', 'Default') !!}
            {!! Form::checkbox('default', true) !!}

            <div class="details-unit">
                <div class="details-unit__heading">
                    <a href="#" class="details-unit__opener">Add Landmark</a>
                </div>
                <div class="details-unit__slide js-slide-hidden">
                    <div class="details-unit__content">
                        <div class="form-row input-row">
                            <div class="input-row__label-holder">
                                <label class="input-row__label" for="input-landmark" data-label-text="Landmark"><span class="input-row__label-text">Landmark</span></label>
                            </div>
                            <div class="form-field">
                                {!! Form::textarea('landmark', old('landmark'), ['class' => 'input-textarea', 'id' => 'input-landmark']) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <input type="hidden" name="id" value="{{ $address->id }}"/>
            <input type="hidden" name="country" value="{{ $address->country or 'IN' }}"/>

            <div class="loader-box">
                <button type="submit" class="btn-product btn-show-loader">
                    <span class="btn-product__wrap">continue</span>
                </button>
                <span class="loader"></span>
            </div>

        {!! Form::close() !!}
    </div>
    <span class="page-loader">
		    <span class="loader"></span>
    </span>
@endsection

@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            var pincodeCheckUrl = '{{ route('checkPincode') }}',
                    pageLoader = $('.page-loader').data('pageLoaderPlugin');

            $('#input-pincode').on('change', function(){

                var that = $(this),
                        id = $(this).val(),
                        $row = $('#row-pincode');

                $row.removeClass('error');
                if(id){
                    pageLoader.show();
                    $.getJSON(pincodeCheckUrl, { id : id }, function(data) {

                        $('#input-city').val(data.city);
                        $('#select-state option[value="' + data.state + '"]').prop('selected', true);

                        var $locality = $('#select-locality').find('option')
                                .remove()
                                .end();

                        $.each(data.locality, function(key, value) {
                            $locality.append($("<option></option>")
                                    .attr("value",value)
                                    .text(value));
                        });

                    }).error(function(error){
                        if(error.status == 422) {
                            $row.addClass('error');
                            $('#error-message-pincode').text(error.responseText);
                        }
                    }).complete(function(){
                        pageLoader.hide();

                    });
                }

            });

            var selectize = $('#select-locality').selectize({
                create: true,
                sortField: {
                    field: 'text',
                    direction: 'asc'
                },
                dropdownParent: 'body',
                onInitialize: function () {
                    this.$parentRow = this.$input.parent('.input-row');

                    if (this.$control.hasClass('full')) {
                        this.$parentRow.addClass('input--filled');
                    }
                },
                onFocus: function () {
                    this.$parentRow.addClass('input--filled');
                },
                onBlur: function () {
                    if (this.$control.hasClass('not-full')) {
                        this.$parentRow.removeClass('input--filled');
                    }
                }
            });

        });
    </script>
@endsection

