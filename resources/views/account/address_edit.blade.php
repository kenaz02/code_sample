@extends('app')

@section('content')
    <div class="address-block container-fluid">
        <div class="page-title">
            <h1>Edit address</h1>
        </div>
        <div class="links-panel">
            <a href="#" class="back">Back</a>
        </div>
        {!! Form::model($model, ['route' => $model->id ? ['addressEditSub', $model->id] : 'addressNewSub', 'method' => 'POST']) !!}

        <div class="form-group">
            {!! Form::label('firstname', 'First Name') !!}
            {!! Form::text('firstname', old('firstname'), ['class' => 'form-control']) !!}
            {!! $errors->first('firstname', '<span class="error">:message</span>') !!}
        </div>
        <div class="form-group">
            {!! Form::label('lastname', 'Last Name') !!}
            {!! Form::text('lastname', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('street0', 'Street One') !!}
            {!! Form::text('street0', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('street1', 'Street Two') !!}
            {!! Form::text('street1', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('telephone', 'Telephone') !!}
            {!! Form::text('telephone', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('state', 'State') !!}
            {!! Form::text('state', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('city', 'City') !!}
            {!! Form::text('city', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('country', 'Country') !!}
            {!! Form::text('country', null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group">
            {!! Form::label('default', 'Default') !!}
            {!! Form::checkbox('default', '0') !!}
        </div>

        <div class="form-group">
            {!! Form::submit('Save') !!}
        </div>
        {!! Form::close() !!}

    </div>
@endsection