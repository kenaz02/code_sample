@extends('app')

@section('content')
    <div class="container-fluid">
        @include('account.nav')

        <div class="row">
            <div class="col-lg-12">
                 @foreach($data as $item)
                    <div class="well well-sm">
                        {{ $item->firstname }} {{ $item->lastname }} </br>
                        {{ $item->street0 }} </br>
                        {{ $item->street1 }} </br>
                        {{ $item->country }} {{ $item->state }} {{ $item->city }} </br>
                        T: {{ $item->telephone }}
                        @if($item->default)
                            <a href="{{ route('addressEdit', $item->id) }}">Change Shipping Address</a>
                        @else
                            <a href="{{ route('addressEdit', $item->id) }}">Edit Address</a>| <a href="">Delete Address</a>
                        @endif
                    </div>
                @endforeach
            </div>
        </div>

    </div>

@endsection