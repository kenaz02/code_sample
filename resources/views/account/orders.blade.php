@extends('app')


@section('content')
    <div class="container-fluid">
        @include('account.nav')

    <div class="row">
        <div class="col-lg-12">
            <table class="table table-bordered">
                <tr>
                    <th>Order #</th>
                    <th>Date</th>
                    <th>Ship To</th>
                    <th>Order Total</th>
                    <th>Order Status</th>
                    <th></th>
                </tr>
                @foreach($data as $item)
                    <tr>
                        <td>{{ $item->id }}</td>
                        <td>{{ $item->order_date }}</td>
                        <td>{{ $item->street0 or '' }} {{ $item->street1 or '' }}</td>
                        <td>
                            @foreach($item->cost as $priceItem)
                                <p>{{ $priceItem->name }} {{ $priceItem->currency_code }} {{ $priceItem->price }}</p>
                            @endforeach
                        </td>
                        <td>{{ $item->status }}</td>
                        <td><a href="">View Order</a></td>
                    </tr>
                @endforeach
            </table>
        </div>
    </div>

    </div>

@endsection