@extends('app')


@section('content')

    @if($errors->has('submit'))
        <div class="global-error">
            <button type="button" class="global-error__close">Close</button>
            {!! $errors->first('submit', '<p>:message</p>') !!}
        </div>
    @endif

    <div class="steps-section">

        <div class="step-box step-box_head">
            <div class="step-box__heading">
                <h1 class="step-box__head-title">Home Try On Program</h1>
                <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::BACK_STATUS ]) }}" class="back-link back-link_type_empty">Back</a>
            </div>
            <p>Provide your details and we will check your eye power in the comfort of your home.</p>
            <div class="step-box__note">
                <p>•Service is charged at a nominal fee of Rs.50</p>
            </div>
            <div class="details-unit">
                <div class="details-unit__heading">
                    <a href="#" class="details-unit__opener">Conditions</a>
                </div>
                <div class="details-unit__slide js-slide-hidden">
                    <div class="details-unit__content">
                        <ul class="conditions-list">
                            <li class="conditions-list__item">••Currently, we are not checking eye powers of the age group of bellow 12 years.</li>
                            <li class="conditions-list__item">•••We can not come up to you if you live above the second floor and building no lift.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">Where Would You Like To Attend Your Appointment?</h2>
            <div class="step-box__done">
                <div class="step-box__done-text">
                    <p>{{ Helper::formatPhone($form->telephone) }}, {{ $form->pincode }}, {{ $form->city }}</p>
                </div>
                <div class="step-box__btn-row">
                    <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::BACK_STATUS ]) }}" class="btn-change">Change phone or location</a>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">When Would You Prefer To Have Your Appointment?</h2>
            {!! Form::open(['route' => 'homeEyeCheckUp', 'method' => 'POST', 'class' => 'steps-form', 'id' => 'form']) !!}
                <div class="date-pick-block">
                    <div id="when-datepicker" class="date-picker"></div>

                    <div class="form-row input-row">
                        <div class="input-row__label-holder">
                            <label class="input-row__label" for="time-slot-select" data-label-text="Time slot"><span class="input-row__label-text">*Time slot</span></label>
                            <span class="error-message"><span class="error-message__holder">Right format is xxx xxx xxxx</span></span>
                        </div>
                        <div class="form-field">
                            <div class="select-unit @if(!$availableTimeSlots) disabled @endif">
                                <div class="select-unit__holder">
                                    <select id="time-slot-select" name="timeSlot" class="effect-select @if(!$availableTimeSlots) disabled @endif" @if(!$availableTimeSlots) disabled="disabled" @endif >
                                        @if($availableTimeSlots)
                                            <option value="default">*time slot</option>
                                            @foreach($availableTimeSlots as $item)
                                                <option value="{{ $item['id']  }}%%{{ $item['time'] }}" @if($form->timeSlot == $item['id']) selected="selected" @endif>{{ $item['time'] }}</option>
                                            @endforeach
                                        @else
                                            <option value="default">No slots available</option>
                                        @endif
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <input id="date" type="hidden" name="date" value=""/>
                <input type="hidden" name="stage" value="{{ \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_DATE }}"/>
            {!! Form::close() !!}

        </div>
    </div>
    <span class="page-loader">
		    <span class="loader"></span>
    </span>
@endsection


@section('script')
    <script type="text/javascript">
        $(document).ready(function(){
            var timeSlotUrl = '{{ route('homeEyeCheckUpTimeSlot') }}',
                cityId      = '{{ $form->cityId }}',
                availableSlotsUrl = '{{ route('homeEyeCheckUpTimeSlotDates') }}',
                pincode           = '{{ $form->pincode }}',
                availableDates    = {!! $availableSlots !!},
                $datepicker       = $('#when-datepicker'),
                pageLoader = $('.page-loader').data('pageLoaderPlugin');


            var datePickerConfig = {
                minDate: +1,
                altField: '#date',
                dateFormat : 'dd/mm/yy',
                defaultDate: '{{ $form->date or date('d/m/Y', time()) }}',
                beforeShowDay: function (date) {
                    // example booked slot. Add class 'state-booked' to 7th
                    if ((date.getDate() in availableDates) && availableDates[date.getDate()].is_available == false) {
                        return [false, 'state-booked'];
                    } else {
                        return [true, ''];
                    }

                },
                onSelect : function(date) {
                    pageLoader.show();
                    $.getJSON(timeSlotUrl, { city_id : cityId, date : date }, function(data) {
                        var $select = $('#time-slot-select');
                        if($select.hasClass('disabled')){
                            $select.removeClass('disabled');
                            $select.removeAttr('disabled');
                            $('.select-unit').removeClass('disabled');
                        }

                        $select.empty();
                        $select.append($("<option></option>").attr('value', 'default').text('*time slot'));
                        for(var key in data)
                        {
                            $select.append($("<option></option>").attr('value', data[key].id + '%%' + data[key].time).text(data[key].time));
                        }
                        pageLoader.hide();
                    });
                },
                onChangeMonthYear: function (year, month) {
                    var dateRange = ('0' + month).slice(-2) + '/' + year;
                    pageLoader.show();
                    $.getJSON(availableSlotsUrl, { pincode : pincode, month: dateRange}, function(data){
                        availableDates = data;
                        $datepicker.datepicker( "refresh" );
                        pageLoader.hide();
                    });

                }
            };

            $datepicker.datepicker(datePickerConfig);

            $('#time-slot-select').on('change', function(){
                pageLoader.show();
                if($(this).val() !== 'default')
                    $('#form').submit();
                else
                    pageLoader.hide();
            });
        });
    </script>
@endsection