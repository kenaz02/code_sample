@extends('app')


@section('content')
    <div class="steps-section">
        <div class="step-box step-box_head">
            <div class="step-box__heading">
                <h1 class="step-box__head-title">Home Try On Program</h1>
                <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::BACK_STATUS ]) }}" class="back-link back-link_type_empty">Back</a>
            </div>
            <p>Provide your details and we will check your eye power in the comfort of your home.</p>
            <div class="step-box__note">
                <p>•Service is charged at a nominal fee of Rs.50</p>
            </div>
            <div class="details-unit">
                <div class="details-unit__heading">
                    <a href="#" class="details-unit__opener">Conditions</a>
                </div>
                <div class="details-unit__slide js-slide-hidden">
                    <div class="details-unit__content">
                        <ul class="conditions-list">
                            <li class="conditions-list__item">••Currently, we are not checking eye powers of the age group of bellow 12 years.</li>
                            <li class="conditions-list__item">•••We can not come up to you if you live above the second floor and building no lift.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">Where Would You Like To Attend Your Appointment?</h2>
            {!! Form::model($form, ['route' => 'homeEyeCheckUp', 'method' => 'POST', 'class' => 'steps-form', 'id' => 'form']) !!}
                <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-phone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxxx xxxxxx</span></label>
                        {!! $errors->first('telephone', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif"/>
                        <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif">
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('pincode')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-pincode" data-label-text="Pincode"><span class="input-row__label-text">*Pincode: xxxxxxx</span></label>
                        {!! $errors->first('pincode', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::input('number', 'pincode', old('pincode') ? old('pincode') : $form->pincode, ['class' => 'input-text required', 'id' => 'input-pincode', 'pattern' => '\d*']) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('cityId')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="select-city" data-label-text="Your city"><span class="input-row__label-text">Your city</span></label>
                        {!! $errors->first('cityId', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        <input type="text" disabled class="input-text" value="@if(old('pincode') || !$form->city) Your City @else{{ $form->city }}@endif"/>
                    </div>
                    <div class="form-note">
                        <p>Currently available in Mumbai, Bangalore, Chennai, Hyderabad, Kolkata, Pune, Delhi, Gurgaon, Noida, Faridabad, Ghaziabad.</p>
                    </div>
                </div>

                <div class="loader-box">
                    <button type="submit" class="btn-product btn-show-loader">
                        <span class="btn-product__wrap">Go to next step <small>(select appointment time)</small></span>
                    </button>
                    <span class="loader"></span>
                </div>
                <input type="hidden" name="stage" value="{{ \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_PHONE }}"/>
            {!! Form::close() !!}
        </div>
    </div>
@endsection