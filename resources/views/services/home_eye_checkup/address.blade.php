@extends('app')


@section('content')

    @if($errors->has('submit'))
    <div class="global-error">
        <button type="button" class="global-error__close">Close</button>
        {!! $errors->first('submit', '<p>:message</p>') !!}
    </div>
    @endif

    <div class="steps-section">
        <div class="step-box step-box_head">
            <div class="step-box__heading">
                <h1 class="step-box__head-title">Home Try On Program</h1>
                <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_DATE]) }}" class="back-link back-link_type_empty">Back</a>
            </div>
            <p>Provide your details and we will check your eye power in the comfort of your home.</p>
            <div class="step-box__note">
                <p>•Service is charged at a nominal fee of Rs.50</p>
            </div>
            <div class="details-unit">
                <div class="details-unit__heading">
                    <a href="#" class="details-unit__opener">Conditions</a>
                </div>
                <div class="details-unit__slide">
                    <div class="details-unit__content">
                        <ul class="conditions-list">
                            <li class="conditions-list__item">••Currently, we are not checking eye powers of the age group of bellow 12 years.</li>
                            <li class="conditions-list__item">•••We can not come up to you if you live above the second floor and building no lift.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">Where Would You Like To Attend Your Appointment?</h2>
            <div class="step-box__done">
                <div class="step-box__done-text">
                    <p>{{ $form->telephone }}, {{ $form->pincode }}, {{ $form->city }}</p>
                </div>
                <div class="step-box__btn-row">
                    <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_PHONE, 'back' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::BACK_STATUS ]) }}" class="btn-change">Change phone or location</a>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">When Would You Prefer To Have Your Appointment?</h2>
            <div class="step-box__done">
                <div class="step-box__done-text">
                    <p>{{ Helper::formatDate($form->date, 'l, d F Y') }}</p>
                    <p>{{ $form->timeSlotLabel }}</p>
                </div>
                <div class="step-box__btn-row">
                    <a href="{{ route('homeEyeCheckUp', [ 'stage' => \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_DATE]) }}" class="btn-change">Change time or date</a>
                </div>
            </div>
        </div>

        <div class="step-box">
            <h2 class="step-box__title">And Finally Your Personal Data</h2>
            <div class="list-figure">
                <strong class="list-figure__title">You Have Selected Appointment For:</strong>
                <ul class="figure-list">
                    <li class="figure-list__item">{{ Helper::formatPhone($form->telephone) }}, {{ $form->pincode }}, {{ $form->city }}</li>
                    <li class="figure-list__item">{{ Helper::formatDate($form->date, 'l, d F Y') }}</li>
                    <li class="figure-list__item">{{ $form->timeSlotLabel }}</li>
                </ul>
            </div>
            <p>Please note this is only a preffered date and time. We will call you to confirm final date/time.</p>

            {!! Form::model($form, ['route' => 'homeEyeCheckUp', 'method' => 'POST', 'class' => 'steps-form', 'id' => 'form']) !!}

                <div class="form-row input-row @if($errors->has('firstname')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-first-name" data-label-text="First name"><span class="input-row__label-text">*First name</span></label>
                        {!! $errors->first('firstname', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::text('firstname', old('firstname'), ['class' => 'input-text required', 'id' => 'input-first-name' , 'maxlength' => 25 ]) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('lastname')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-last-name" data-label-text="Last name"><span class="input-row__label-text">*Last name</span></label>
                        {!! $errors->first('lastname', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::text('lastname', old('lastname'), ['class' => 'input-text required', 'id' => 'input-last-name', 'maxlength' => 25]) !!}
                    </div>
                </div>


                <div class="form-row input-row @if($errors->has('telephone')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-mobilephone" data-label-text="Mobile phone"><span class="input-row__label-text">*Mobile Phone: xxxx xxxxxx</span></label>
                        {!! $errors->first('telephone', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        <input type="tel" class="input-text required" id="input-phone" maxlength="12" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif"/>
                        <input type="hidden" name="telephone" class="reformat-phone" data-target="#input-phone" value="@if(old('telephone')){{ old('telephone') }}@else{{ $form->telephone }}@endif">
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('email')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-email" data-label-text="Email"><span class="input-row__label-text">*Email: name@domain.com</span></label>
                        {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::email('email', old('email'), ['class' => 'input-text required', 'id' => 'input-email']) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('street0')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="input-address" data-label-text="Address"><span class="input-row__label-text">*Address: xx, street</span></label>
                        {!! $errors->first('street0', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>
                    <div class="form-field">
                        {!! Form::text('street0', old('street0'), ['class' => 'input-text required', 'id' => 'input-address']) !!}
                    </div>
                </div>

                <div class="form-row input-row @if($errors->has('city')) error @endif">
                    <div class="input-row__label-holder">
                        <label class="input-row__label" for="select-town" data-label-text="Locality/town"><span class="input-row__label-text">*Locality / Town</span></label>
                        {!! $errors->first('city', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
                    </div>

                    <select id="select-town" name="locality">
                        <option value=""></option>
                        @foreach($form->localityList as $item)
                            <option value="{{ $item }}" @if($form->locality == $item || old('locality') == $item) selected @endif>{{ $item }}</option>
                        @endforeach
                    </select>
                </div>


                <div class="details-unit">
                    <div class="details-unit__heading">
                        <a href="#" class="details-unit__opener">Add Landmark</a>
                    </div>
                    <div class="details-unit__slide js-slide-hidden">
                        <div class="details-unit__content">
                            <div class="form-row input-row">
                                <div class="input-row__label-holder">
                                    <label class="input-row__label" for="input-landmark" data-label-text="Landmark"><span class="input-row__label-text">Landmark</span></label>
                                </div>
                                <div class="form-field">
                                    {!! Form::textarea('landmark', old('landmark'), ['class' => 'input-textarea', 'id' => 'input-landmark', 'maxlength' => 100]) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="check-block">
                    <div class="check-block__columns">
                        <div class="check-block__col">
                            <strong class="check-block__title">Given Address is?*</strong>

                            <ul class="check-listing">
                                <li class="check-listing__item">
                                    {!! Form::radio('addressType', 'official', false, ['id' => 'check-given-off', 'data-opener' => 'js-check-collapse', 'data-target' => 'official-slide']) !!}
                                    <label for="check-given-off">Official</label>
                                </li>
                                <li class="check-listing__item">
                                    {!! Form::radio('addressType', 'residence', true, ['id' => 'check-given-residental']) !!}
                                    <label for="check-given-residental">Residental</label>
                                </li>
                                <li class="check-listing__item">
                                    {!! Form::radio('addressType', 'campus', false, ['id' => 'check-given-campus']) !!}
                                    <label for="check-given-campus">Campus</label>
                                </li>
                            </ul>
                        </div>
                        <div class="check-block__col">
                            <strong class="check-block__title">Lift Availability*</strong>

                            <ul class="check-listing">
                                <li class="check-listing__item">
                                    {!! Form::radio('liftAvailable', 'true', false, ['id' => 'check-availability-yes']) !!}
                                    <label for="check-availability-yes">Yes</label>
                                </li>
                                <li class="check-listing__item">
                                    {!! Form::radio('liftAvailable', 'false', true, ['id' => 'check-availability-no', 'data-opener' => 'js-check-collapse', 'data-target' => 'floor-slide']) !!}
                                    <label for="check-availability-no">No</label>
                                    <div class="check-listing-slide js-check-collapse" id="floor-slide">
                                        <div class="select-unit">
                                            <div class="select-unit__holder">
                                                <select class="effect-select" name="floor" data-select-slide="prevent-slide">
                                                    <option value="0" @if(old('floor') == '0') selected @endif>0 Floor</option>
                                                    <option value="1" @if(old('floor') == '1') selected @endif>1 Floor</option>
                                                    <option value="2" @if(old('floor') == '2') selected @endif>2 Floor</option>
                                                    <option value="3" class="js-select-opener" @if(old('floor') == '3') selected @endif>Above 2nd</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </li>
                            </ul>
                        </div>

                    </div>
                    <div class="prevent-unit js-check-collapse" id="official-slide">
                        <p>Please note team carries heavy optical instruments along with frames and would be needing a small meeting room or a cafetaria space in your office</p>
                    </div>

                    <div class="prevent-unit js-check-collapse" data-slide="prevent-slide">
                        <div class="prevent-unit__warn">
                            <p>•••Our team carries heavy optical instruments along with frames and we would not be able to offer our services above 2nd floor without lift facility.</p>

                            <a href="#" class="prevent-unit__link">Try With Another Address</a>
                        </div>
                    </div>
                </div>


                <div class="details-list">
                    <div class="details-unit">
                        <div class="details-unit__heading">
                            <a href="#" class="details-unit__opener">Add Discount Coupon</a>
                        </div>

                        <div class="details-unit__slide">
                            <div class="details-unit__content">
                                <div id="form-section-coupon" class="form-row input-row input-row_with-btn @if($cart->coupon) input-applied @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-coupon" data-label-text="Coupon code"><span class="input-row__label-text">Coupon code: xx xxxx</span></label>
                                        <span id="error-message-coupon" class="error-message"></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="input-text" id="input-coupon" @if($cart->coupon) value="{{ $cart->coupon }}" @endif>
                                        <input id="old-input-coupon" type="hidden" name="coupon">
                                        <button id="apply-btn-coupon" type="button" class="btn-small-apply">Apply</button>

                                        <span class="applied-badge">Applied</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="details-unit">
                        <div class="details-unit__heading">
                            <a href="#" class="details-unit__opener">Add Gift Voucher</a>
                        </div>

                        <div class="details-unit__slide">
                            <div class="details-unit__content">
                                <div id="form-section-voucher" class="form-row input-row input-row_with-btn @if($cart->vouchers) input-applied @endif">
                                    <div class="input-row__label-holder">
                                        <label class="input-row__label" for="input-voucher" data-label-text="voucher code"><span class="input-row__label-text">Voucher code: xxxx xxxx</span></label>
                                        <span id="error-message-voucher" class="error-message"></span>
                                    </div>
                                    <div class="form-field">
                                        <input type="text" class="input-text" id="input-voucher" @if($cart->vouchers) value="{{ $cart->vouchers }}" @endif>
                                        <input id="old-input-voucher" type="hidden" name="voucher">
                                        <button id="apply-btn-voucher" type="button" class="btn-small-apply">Apply</button>

                                        <span class="applied-badge">Applied</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="steps-form__buttons">
                    <div class="loader-box">
                        <button type="submit" class="btn-product btn-show-loader payment-submit" data-payment="now">
                            <span class="btn-product__wrap">Pay Now</span>
                        </button>
                        <span class="loader"></span>
                    </div>

                    @if($form->isWeekDay())
                        <span class="or">OR for selected days you can</span>

                        <div class="loader-box">
                            <button type="submit" class="btn-product btn-product_type_extra btn-show-loader payment-submit" data-payment="visit">
                                <span class="btn-product__wrap">Pay at Visit</span>
                            </button>
                            <span class="loader"></span>
                        </div>
                    @endif
                </div>
                <input id="payment-submit-value" type="hidden" name="payment_type" />
                <input type="hidden" name="stage" value="{{ \App\Http\Commands\HomeEyeCheckUp\CommandContract::STAGE_ADDRESS }}"/>
            {!! Form::close() !!}
        </div>
    </div>
    <span class="page-loader">
		    <span class="loader"></span>
    </span>
@endsection

@section('script')
<script type="text/javascript">
    $(document).ready(function(){
        var couponUrl = '{{ route('cartCouponAdd') }}',
            voucherUrl = '{{ route('cartVoucherAdd') }}',
            _token     = '{{ csrf_token() }}',
            pageLoader = $('.page-loader').data('pageLoaderPlugin');

        $('#apply-btn-coupon').on('click', function(){
            var couponValue = $('#input-coupon').val(),
                $container = $('#form-section-coupon');

            pageLoader.show();
            $.post(couponUrl, { _token: _token, code: couponValue })
                    .done(function(data){
                        $container.removeClass('error').addClass('input-applied');
                        $('#old-input-coupon').val(couponValue);
                    })
                    .fail(function(data){
                        data = data.responseJSON;
                        if(data.hasOwnProperty('message'))
                        {
                            $container.addClass('error');
                            $('#error-message-coupon').text(data.message);
                        }
                    })
                    .always(function(){
                        pageLoader.hide();
                    });
            return false;
        });

        $('#apply-btn-voucher').on('click', function(){
            var voucherValue = $('#input-voucher').val(),
                $container = $('#form-section-voucher');

            pageLoader.show();
            $.post(voucherUrl, { _token: _token, code: voucherValue })
                    .done(function(data){
                        $container.removeClass('error').addClass('input-applied');
                        $('#old-input-voucher').val(voucherValue);
                    })
                    .fail(function(data){
                        data = data.responseJSON;
                        if(data.hasOwnProperty('message'))
                        {
                            $container.addClass('error');
                            $('#error-message-voucher').text(data.message);
                        }
                    })
                    .always(function(){
                        pageLoader.hide();
                    });
            return false;
        });

        $('.payment-submit').on('click', function(){
            $('#payment-submit-value').val($(this).attr('data-payment'));
        });

        var selectize = $('#select-town').selectize({
            create: true,
            sortField: {
                field: 'text',
                direction: 'asc'
            },
            dropdownParent: 'body',
            onInitialize: function () {
                this.$parentRow = this.$input.parent('.input-row');

                if (this.$control.hasClass('full')) {
                    this.$parentRow.addClass('input--filled');
                }
            },
            onFocus: function () {
                this.$parentRow.addClass('input--filled');
            },
            onBlur: function () {
                if (this.$control.hasClass('not-full')) {
                    this.$parentRow.removeClass('input--filled');
                }
            }
        });
    });
</script>
@endsection