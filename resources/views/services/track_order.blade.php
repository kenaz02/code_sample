@extends('app')


@section('content')
    {!! Form::open(['route' => 'trackOrder', 'method' => 'POST', 'class'  => '']) !!}

    {!! $errors->first('submit', '<div class="alert alert-danger">:message</div>') !!}

        <div class="form-row @if($errors->has('orderId')) error @endif">
            {!! Form::label('orderId', 'Order Id*') !!}
            <div class="form-field form-field_type_inp">
                {!! Form::text('orderId', old('orderId'), ['class' => 'input-text']) !!}
                {!! $errors->first('orderId', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>
        <div class="form-row @if($errors->has('email')) error @endif">
            {!! Form::label('title', 'Email*') !!}
            <div class="form-field form-field_type_inp">
                {!! Form::text('email', old('email'), ['class' => 'input-text']) !!}
                {!! $errors->first('email', '<span class="error-message"><span class="error-message__holder">:message</span></span>') !!}
            </div>
        </div>

    {!! Form::submit('Submit', ['class' => 'btn-submit']) !!}
    {!! Form::close() !!}

@endsection