(function(window, document, callback) {
        var d;
        var loaded = false;
        var j = window.jQuery;
        var script = document.createElement("script");
        script.type = "text/javascript";
        script.src = "//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js";
        script.onload = script.onreadystatechange = function() {
            if (!loaded && (!(d = this.readyState) || d == "loaded" || d == "complete")) {
                callback((j = window.jQuery).noConflict(1), loaded = true, old = window.jQuery);
                j(script).remove();
            }
        };
        (document.getElementsByTagName("head")[0] || document.documentElement).appendChild(script);
    })(window, document, function($, jquery_loaded, old){
        /*
         CONFIG
         */
        window.searchUrl = "search.lenskart.com"; // Write the valid search URL here.
        // ---- example
        // www.lenskart.com/catalogsearch/result/?cat=0&q=
        // for Unbxd case it will be
        // search.lenskart.com
        window.UnbxdSiteName = "lenskart_com-u1418736834941";
        window.UnbxdAPIKey = "6c1aa3ba216de83b8340d2d4d891d309";
        /*
         Autosuggest Object
         */
        unbxdAutoSuggestFunction($, Handlebars); // Pass a valid jquery object in place of $
        $(function(){
            $("#search-autocomplete").unbxdautocomplete({
                siteName : window.UnbxdSiteName
                ,APIKey : window.UnbxdAPIKey
                ,minChars : 1
                ,showCarts: false
                ,inputClass:"input"
                ,template : "2column" // "2column"
                ,mainTpl: ['keywordSuggestions','inFields']
                ,sideTpl: ['popularProducts']
                ,cartType : "inline"
                ,sideContentOn: "left"
                ,width: 226
                ,topAdjust: 0
                ,hbsHelpers: function(){
                    Handlebars.registerHelper('safestring', function(value) {
                        return new Handlebars.SafeString(value);
                    });

                }
                ,onCartClick : function(obj){
                    // console.log("onCartClick");
                }
                ,onSimpleEnter: function(){
                    var q = this.input.value;
                    customKeywords = {
                        "men eyeglasses": "http://www.lenskart.com/eyeglasses/gender/mens-eyeglasses.html",
                        "women eyeglasses": "http://www.lenskart.com/eyeglasses/gender/womens-eyeglasses.html",
                        "kids eyeglasses": "http://www.lenskart.com/eyeglasses/gender/kids-eyeglasses.html",
                        "unisex eyeglasses": "http://www.lenskart.com/eyeglasses/gender/unisex-eyeglasses-3.html",
                        "teen eyeglasses": "http://www.lenskart.com/eyeglasses/gender/teen-eyeglasses.html",
                        "best fit": "http://www.lenskart.com/eyeglasses/collections/best-fit-eyeglasses.html",
                        "ultra light weight collection": "http://www.lenskart.com/eyeglasses/collections/ultra-light-weight-collection-137.html",
                        "light weight collection": "http://www.lenskart.com/eyeglasses/collections/light-eyeglasses.html",
                        "retro collection": "http://www.lenskart.com/eyeglasses/collections/retro-collection.html",
                        "vintage collection": "http://www.lenskart.com/eyeglasses/collections/vintage-collection.html",
                        "sports collection": "http://www.lenskart.com/eyeglasses/collections/sports-collection.html",
                        "premium collection": "http://www.lenskart.com/eyeglasses/collections/premium-eyeglasses.html",
                        "latest collection": "http://www.lenskart.com/eyeglasses/collections/latest-collection.html",
                        "bluetooth eyeglasses ": "http://www.lenskart.com/eyeglasses/collections/bluetooth-eyeglasses.html",
                        "colorful collection": "http://www.lenskart.com/eyeglasses/collections/colorful-collection.html",
                        "standard collection": "http://www.lenskart.com/eyeglasses/collections/standard-collection.html",
                        "reading glasses": "http://www.lenskart.com/eyeglasses/collections/reading-eyeglasses.html",
                        "computer eyewear": "http://www.lenskart.com/eyeglasses/collections/computer-sunglasses-collection.html",
                        "kids collection": "http://www.lenskart.com/eyeglasses/collections/kids-frames.html",
                        "power swimming goggles": "http://www.lenskart.com/eyeglasses/collections/power-swimming-goggles.html",
                        "youth collection": "http://www.lenskart.com/eyeglasses/collections/youthful-collection-eyeglasses.html",
                        "full collection": "http://www.lenskart.com/eyeglasses/collections/eyeglasses-clearance-sale.html",
                        "world's lightest eyeglasses": "http://www.lenskart.com/eyeglasses/collections/world-s-lightest-eyeglasses.html",
                        "men sunglasses": "http://www.lenskart.com/sunglasses/find-eyewear/mens-sunglasses.html",
                        "women sunglasses": "http://www.lenskart.com/sunglasses/find-eyewear/womens-sunglasses.html",
                        "kids sunglasses": "http://www.lenskart.com/sunglasses/find-eyewear/kids-sunglasses.html",
                        "teen eyeglasses": "http://www.lenskart.com/sunglasses/find-eyewear/teen-eyeglasses.html",
                        "aviators &amp; wayfarers": "http://www.lenskart.com/sunglasses/collections/aviators-wayfarers.html",
                        "sports sunglasses": "http://www.lenskart.com/sunglasses/collections/sports-sunglasses.html",
                        "premium sunglasses": "http://www.lenskart.com/sunglasses/collections/premium-sunglasses.html",
                        "power sunglasses": "http://www.lenskart.com/sunglasses/collections/power-sunglasses.html",
                        "polarized sunglasses": "http://www.lenskart.com/sunglasses/collections/polarized-sunglasses.html",
                        "retro collection": "http://www.lenskart.com/sunglasses/collections/retro-collection.html",
                        "kids sunglasses": "http://www.lenskart.com/sunglasses/collections/kids-sunglasses.html",
                        "all": "http://www.lenskart.com/sunglasses/collections/featured-sunglasses.html",
                        "nightwear sunglasses": "http://www.lenskart.com/sunglasses/collections/nightwear-sunglasses.html",
                        "sunglasses": "http://www.lenskart.com/sunglasses.html",
                        "eyeglasses": "http://www.lenskart.com/eyeglasses.html",
                        "power sunglasses": "http://www.lenskart.com/power-sunglasses-main.html",
                        "contact lenses": "http://www.lenskart.com/contact-lenses.html",
                        "sports eyewear": "http://www.lenskart.com/sports-eyewear.html",
                        "anti glare" : "http://www.lenskart.com/anti-glare-lenses.html",
                        "antiglare" : "http://www.lenskart.com/anti-glare-lenses.html",
                        "anti-glare" : "http://www.lenskart.com/anti-glare-lenses.html",
                        "anti glare eyeglasses" : "http://www.lenskart.com/anti-glare-lenses.html",
                        "antiglare eyeglasses" : "http://www.lenskart.com/anti-glare-lenses.html",
                        "titan eye":"http://www.lenskart.com/eyeglasses/brands/john-jacobs-eyeglasses.html",
                        "Night Vision":"http://www.lenskart.com/sunglasses/collections/nightwear-sunglasses.html",
                        "free frame" : "http://www.lenskart.com/eyeglasses/popular-searches/first-pair-free.html",
                        "frame free" : "http://www.lenskart.com/eyeglasses/popular-searches/first-pair-free.html",
                        "first frame" : "http://www.lenskart.com/eyeglasses/popular-searches/first-pair-free.html",
                        "ray-ban eyeglasses" : "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "rayban eyeglasses" : "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "ray ban eyeglasses": "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "ray-ban eyeglass" : "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "rayban eyeglass" : "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "ray ban eyeglass": "http://www.lenskart.com/eyeglasses/brands/ray-ban-eyeglasses.html",
                        "paytm" : "http://www.lenskart.com/paytm-coupon.html",
                        "paytms" : "http://www.lenskart.com/paytm-coupon.html"


                    }

                    if(q.toLowerCase() in customKeywords) {
                        url = window.location.href.split('?')[0] + '?q=REDIRECT:' +q;
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        //_gaq.push(['_trackPageview', url]);
                        setTimeout(function(){
                            url = (customKeywords[q.toLowerCase()].indexOf('\?') == -1)?(customKeywords[q.toLowerCase()] + '?q=' + q) : (customKeywords[q.toLowerCase()] + '&q=' + q);
                            window.location = url;
                        }, 200);

                    }else{
                        url = window.location.href.split('?')[0] + '?q=' +q;
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        //_gaq.push(['_trackPageview', url]);
                        var filter = "";
                        if (jQuery("#searchautocomplete_nav .UI-CATEGORY-TEXT").size() > 0){
                            var category_text = jQuery("#searchautocomplete_nav .UI-CATEGORY-TEXT")[0].textContent.trim();
                            if (category_text !== undefined){
                                if (category_text != 'ALL' && category_text != ''){
                                    filter = "&" + "filter=" + "Category_fq:"+ "\""+ category_text +"\""
                                }
                            }
                        }
                        window.location = "//search.lenskart.com/" + "?q=" + encodeURIComponent(this.input.value) + filter;
                    }
                }
                ,onItemSelect:function(select, obj){
                    if (select.type == "KEYWORD_SUGGESTION"){
                        url = window.location.href.split('?')[0] + '?q=' +select.value;
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        window.location = "//search.lenskart.com/" + "?q=" + encodeURIComponent(select.value)
                    }else if (select.type == "IN_FIELD"){
                        url = window.location.href.split('?')[0] + '?q=' +select.value;
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        var filterString = "filter="
                        if (select.filtername == "c2c_frame_shape"){
                            console.log("FILTER:::" + select.filtervalue);
                            filterString = filterString + "Frame_Shape_fq:"+ "\"" + select.filtervalue+ "\"";
                        }else if (select.filtername == "c2c_frametype"){
                            filterString = filterString + "Frame_Type_fq:"+ "\"" + select.filtervalue+ "\"";
                        }
                        window.location = "//search.lenskart.com/" + "?q=" + encodeURIComponent(select.value) + "&" + filterString
                    }else if (select.type == "POPULAR_PRODUCTS"){
                        url = window.location.href.split('?')[0] + '?q=POP:' +select.value;
                        //_gaq.push(['_trackPageview', url]);
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        if (obj.url_path !== undefined){
                            obj.url_path = (obj.url_path.indexOf('\?') == -1)?(obj.url_path + '?q=' + select.value) : (obj.url_path + '&q=' + select.value);
                            window.location =  obj.url_path.replace("http://admin.lenskart.com:8080/index.php","//www.lenskart.com");
                        }else{
                            if (obj.url_key !== undefined){
                                window.location = "//lenskart.com/" + obj.url_key + ".html?q=" + select.value;
                            }
                        }
                    } else {
                        url = window.location.href.split('?')[0] + '?q=' +select.value;
                        _gaq.push(['_trackEvent', 'Header', 'search', 'page: ' + url]) ;
                        window.location = "//search.lenskart.com/" + "?q=" + encodeURIComponent(select.value)
                    }
                }
                ,inFields:{
                    count: 2,
                    fields:{
                        'c2c_frame_shape':3
                        ,'c2c_frametype':3
                    }
                },
                topQueries:{
                    count: 3
                },
                keywordSuggestions:{
                    count: 5,
                    header: ''
                }
                ,popularProducts:{
                    count: 4
                    ,title:true
                    ,price:true
                    ,priceFunctionOrKey : function(obj){
                        return obj.special_price <= 0 || obj.special_price === undefined ? "" : "Rs. "+parseFloat(obj.special_price).toFixed(2);
                    }
                    ,image:true
                    ,imageUrlOrFunction: function(option){
                        return option.small_image;
                    }
                    ,currency : ""
                    ,tpl: '{{#if ../showCarts}}'
                        +'{{#unbxdIf ../../cartType "inline"}}'//"inline" || "separate"
                        +'<div class="unbxd-as-popular-product-inlinecart">'
                        +'<div class="unbxd-as-popular-product-image-container">'
                        +'{{#if image}}'
                        +'<img src="{{image}}"/>'
                        +'{{/if}}'
                        +'</div>'
                        +'<div  class="unbxd-as-popular-product-name">'
                        +'<div style="table-layout:fixed;width:100%;display:table;">'
                        +'<div style="display:table-row">'
                        +'<div style="display:table-cell;text-overflow:ellipsis;overflow: hidden;white-space: nowrap;">'
                        +'{{{safestring highlighted}}}'
                        +'</div>'
                        +'</div>'
                        +'</div>'
                        +'</div>'
                        +'{{#if price}}'
                        +'<div class="unbxd-as-popular-product-price">'
                        +'{{currency}}{{price}}'
                        +'</div>'
                        +'{{/if}}'
                        +'<div class="unbxd-as-popular-product-quantity">'
                        +'<div class="unbxd-as-popular-product-quantity-container">'
                        +'<span>Qty</span>'
                        +'<input class="unbxd-popular-product-qty-input" value="1"/>'
                        +'</div>'
                        +'</div>'
                        +'<div class="unbxd-as-popular-product-cart-action">'
                        +'<button class="unbxd-as-popular-product-cart-button">Add to cart</button>'
                        +'</div>'
                        +'</div>'
                        +'{{else}}'
                        +'<div class="unbxd-as-popular-product-info">'
                        +'<div class="unbxd-as-popular-product-image-container">'
                        +'{{#if image}}'
                        +'<img src="{{image}}"/>'
                        +'{{/if}}'
                        +'</div>'
                        +'<div  class="unbxd-as-popular-product-name">'
                        +'{{{safestring highlighted}}} <br/><span class="unbxd-as-popular-product-name-brand">{{this._original.c2c_brandname}}</span><br/>'
                        +'{{#if price}}'
                        +'<div class="unbxd-as-popular-product-price">'
                        +'{{currency}}{{price}}'
                        +'</div>'
                        +'{{/if}}'
                        +'</div>'
                        +'<div class="unbxd-as-popular-product-cart">'
                        +'<div class="unbxd-as-popular-product-cart-action">'
                        +'<button class="unbxd-as-popular-product-cart-button">Add to cart</button>'
                        +'</div>'
                        +'<div class="unbxd-as-popular-product-quantity">'
                        +'<div class="unbxd-as-popular-product-quantity-container">'
                        +'<span>Qty</span>'
                        +'<input class="unbxd-popular-product-qty-input" value="1"/>'
                        +'</div>'
                        +'</div>'
                        +'</div>'+
                        +'</div>'
                        +'</div>'
                        +'{{/unbxdIf}}'
                        +'{{else}}'
                        +'<div class="unbxd-as-popular-product-info">'
                        +'<div class="unbxd-as-popular-product-image-container">'
                        +'{{#if image}}'
                        +'<img src="{{image}}" onerror="this.src=\'http://sol-us-prod-1.cloudapp.net/cilory/ina.jpg\'"/>'
                        +'{{/if}}'
                        +'</div>'
                        +'<div  class="unbxd-as-popular-product-name">'
                        +'<span class="unbxd-as-popular-product-name-text">{{{safestring highlighted}}}</span> <span class="unbxd-as-popular-product-name-price">{{currency}}{{price}}</span>'
                        +'</div>'
                        +'</div>'
                        +'{{/if}}'
                        +'</li>'
                }
            });
        });
    });