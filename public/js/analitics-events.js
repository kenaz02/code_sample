(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-61571581-1', 'auto');
ga('send', 'pageview');

$(document).ready(function () {
   $('ul.ga-btn-category > li > a').on('click', function(){
       ga('send', 'event', 'Home page - mobile', 'Shop for', $(this).closest('.ga-btn-category').attr('data-key'));
   });

    $('.ga-btn-home').on('click', function(){
        ga('send', 'event', 'Menu', 'Shop for/Our services', $(this).closest('.category-unit').find('.ga-parent-home').text() + " " + $(this).text());
    });

    $('ul.ga-btn-our-services > li > a').on('click', function(){
        ga('send', 'event', 'Home page', $(this).text());
    });

   $('select.ga-select-sort').on('change', function(){
       ga('send', 'event', 'Product Page', 'Sort by', $(this).find("option:selected").text());
   });

    $('a.ga-banner-offer').on('click', function(){
        ga('send', 'event', 'Home page - mobile', 'Main Banner:' + ($(this).closest('.image-gallery__slide').index() + 1));
    });

    $('.ga-home-product-container a').on('click', function(){
        ga('send', 'event', 'Home page - mobile', $(this).closest('.ga-home-product-container').attr('data-gakey') + ':' + ($(this).closest('.ga-evholder').index() + 1 ), $(this).attr('href'));
    });

    $('.ga-home-product-section').on('click', function () {
        ga('send', 'event', $(this).attr('data-gakey'), ($(this).hasClass('expanded'))? 'Open' : 'Close');
    });

    $('.ga-home-one-for-one').on('click', function () {
        ga('send', 'event', 'Home page', 'One for one');
    });

    //On clicking "180 view" tab on product page
    $('.ga-180-view').on('click', function() {
        ga('send', 'event', 'Product Page', '180 View', $('.view-image_state_active').find('img').attr('data-src'));
    });

    //On clicking rating start from top (or) clicking on review section
    $('.ga-review').on('click', function() {
        ga('send', 'event', 'Write/Read Review:' + $(this).attr('data-ga-review-position'), 'Avg Rating: ' + $('#ga-review-avg').text(), 'No of Reviews: ' + $('#ga-review-total').text());
    });

    //On tap of offer widget from home page
    $('.ga-hot-offers').on('click', function(){
        ga('send', 'event', 'Header', '180 View', 'Offers page');
    });

    //Selection of lens package (On buy click)
    $('.ga-lens-package-submit').on('click', function(){
        ga('send', 'event', 'Lens Package', $('#ga-lens-package-type').val() + ':' + $(this).attr('data-ga-lens-package-name'));
    });

    //Clicking on top right side text on lens package page
    $('.ga-lens-package-view').on('click', function(){
        ga('send', 'event', 'Lens Package', $(this).text());
    });

    //Filters usage
    $('.ga-filter-usage').on('click', function(){
        var options = '';
        $('.ga-filter-value').each(function(){
            if($(this).is(':checked')){
                options += $(this).attr('data-ga-label') + ', ';
            }
        });

        if(options)
            options = options.substring(0, options.length - 2);

        ga('send', 'event', 'Category Page', 'Filter Usage', options);

        return false;
    });

});
